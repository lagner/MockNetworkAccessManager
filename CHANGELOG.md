# Changelog # {#changelog}

\brief The changelog of the MockNetworkAccessManager library.

This project adheres to [Semantic Versioning](http://semver.org/).

This changelog follows the [Keep a Changelog](http://keepachangelog.com) format.


---


## [0.4.0] - 2019-02-04 ##

### Added ###
- [#7] List of known Qt bugs to document flaws of `Behavior_Expected`.
- [#17] Support for multi-valued query parameters (list/array type for query parameter).
- [#18] `passThroughManager` parameter on `Rule::passThrough()` method to allow passing requests through
  to different network access manager based on the rule.

### Fixed ###
- [#7] Handling of relative redirects did not match QNetworkAccessManager's behavior.
- [!25] `MockReply::clone()` and therefore `MockReplyBuilder::createReply()` did not correctly handle
  relative `QNetworkRequest::LocationHeader`s due to QTBUG-41061.


---


## [0.3.0] - 2019-01-05 ##
Improves compatibility with Qt and extends API documentation.

### Added ###
- [#2] Missing API documentation
- [#3] Qt version detection to set default behavior
- `Behavior_NoAutomatic308Redirect` and `Behavior_RedirectWithGet` flags

### Changed ###

#### Breaking Changes ####
- Automatic redirect following now follows redirects with an unknown redirect code using a GET request instead
  of preserving the request method. This makes the behavior conform to Qt 5.9.

### Fixed ###
- [#14] Issues with Qt versions before 5.6

### Removed ###
- `Behavior_Qt_5_6_Redirect` flag in favor of `Behavior_NoAutomatic308Redirect` and `Behavior_RedirectWithGet`.


---


## [0.2.0] - 2018-11-13 ##
Predicate enhancements.

### Added ###
- [#8] `Generic` predicate and `Rule::isMatching()` and `Rule::isNotMatching()` methods to allow
  callables as predicates.
- [#12] Predicates `QueryParameter`, `QueryParameterMatching`, `QueryParameterTemplates` and
  `RawHeaderTemplates`

### Removed ###
- [#16] Comparison operators for all predicates.


---


## [0.1.0] - 2018-10-27 ##

### Added ###
- [#1] Cookie support: `MockReplyBuilder::withCookie()`, `Cookie` and `CookieMatching` predicates
- [#6] `\issue` documentation alias

### Changed ###

#### Breaking Changes ####
- [#9] Refactored `Rule::passThrough()` and related methods. Also changed the default behavior if `passThrough()`
  is called without parameters.
- [#10] Renamed `MockConfig` to `Rule`

### Fixed ###
- [#11] Clarified documentation of `Url` predicate regarding empty `QUrl`s


---


## [0.0.2] - 2018-09-30 ##
Initial release.

### Added ###
- Basic predicates
- Support for automatic redirection following
- Support for HSTS
- Support for HTTP Basic authentication


---


[0.3.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.3.0
[0.2.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.2.0
[0.1.0]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.1.0
[0.0.2]: https://gitlab.com/julrich/MockNetworkAccessManager/tags/0.0.2
