/*! \file
 *
 * MockNetworkAccessManager
 * https://gitlab.com/julrich/MockNetworkAccessManager
 *
 * \version 0.4.0
 * \author Jochen Ulrich <jochen.ulrich@t-online.de>
 * \copyright © 2018-2019 Jochen Ulrich. Licensed under MIT license (https://opensource.org/licenses/MIT)
 * except for the HttpStatus namespace which is licensed under Creative Commons CC0 (http://creativecommons.org/publicdomain/zero/1.0/).
 */
/*
Copyright © 2018-2019 Jochen Ulrich

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef MOCKNETWORKACCESSMANAGER_HPP
#define MOCKNETWORKACCESSMANAGER_HPP

#include <QtCore>
#include <QtNetwork>

#include <algorithm>
#include <vector>

#ifndef Q_NAMESPACE
#define Q_NAMESPACE
#endif

#ifndef Q_ENUM_NS
#define Q_ENUM_NS(x)
#endif

/*! Provides functions and classes to mock network replies in %Qt applications.
 */
namespace MockNetworkAccess
{

	Q_NAMESPACE

/*! Behavior switches defining different behaviors for the classes of the Manager.
 * \sa page_behaviorFlags
 */
enum BehaviorFlag
{
	/*! Defines that a class behaves as expected according to the documentation and standards
	 * (RFCs etc.). This also means it should behave like most %Qt bugs being fixed
	 * (see \ref page_knownQtBugs for a list of exceptions).
	 * This flag cannot be combined with other BehaviorFlags.
	 * \sa \ref page_knownQtBugs
	 */
	Behavior_Expected = 0,

	/*! Defines that the MockReplies emits an `uploadProgress(0, 0)` signal after the download.
	 * There is QTBUG-44782 in QNetworkReply which causes it to emit an `uploadProgress(0, 0)` signal after
	 * the download of the reply has finished.
	 * \sa https://bugreports.qt.io/browse/QTBUG-44782
	 */
	Behavior_FinalUpload00Signal       = 1<<1,
	/*! Defines that the Manager does not automatically redirect on 308 "Permanent Redirect" responses.
	 * %Qt does not respect the 308 status code for automatic redirection up to %Qt 5.9.3 (was fixed with QTBUG-63075).
	 * \sa https://bugreports.qt.io/browse/QTBUG-63075
	 * \since 0.3.0
	 */
	Behavior_NoAutomatic308Redirect    = 1<<2,
	/*! Defines that the Manager follows all redirects with a GET request (except the initial request was a HEAD
	 * request in which case it follows with a HEAD request as well).
	 * %Qt up to 5.9.3 followed all redirected requests except HEAD requests with a GET request. QTBUG-63142 fixes this
	 * to not change the request method for 307 and 308 requests.
	 * \sa https://bugreports.qt.io/browse/QTBUG-63142
	 * \since 0.3.0
	 */
	Behavior_RedirectWithGet           = 1<<3,
	/*! Defines that the Manager assumes Latin-1 encoding for username and password for HTTP authentication.
	 * By default, the Manager uses the `charset` parameter of the authentication scheme and defaults to UTF-8 encoding.
	 */
	Behavior_HttpAuthLatin1Encoding    = 1<<4,
	/*! Defines that the Manager rewrites the request verbs OPTIONS and TRACE to GET when following redirects.
	 * [RFC 7231, Section 4.2.1](https://tools.ietf.org/html/rfc7231#section-4.2.1) defines the HTTP verbs OPTIONS and TRACE
	 * as "safe" request methods so it should be fine to use them when automatically following redirects for HTTP
	 * status codes 301 and 302. This behavior defines that the Manager should still redirect with a GET request in that
	 * case.
	 * \note Behavior_RedirectWithGet overrides this flag. So if Behavior_RedirectWithGet is set, this flag is ignored.
	 * \since 0.3.0
	 */
	Behavior_IgnoreSafeRedirectMethods = 1<<5,

	/*! Defines the behavior of %Qt 5.6.
	 */
	Behavior_Qt_5_6_0 = Behavior_FinalUpload00Signal
	                  | Behavior_NoAutomatic308Redirect
	                  | Behavior_RedirectWithGet
	                  | Behavior_HttpAuthLatin1Encoding
	                  | Behavior_IgnoreSafeRedirectMethods,
	/*! Defines the behavior of %Qt 5.2.
	 * \since 0.3.0
	 */
	Behavior_Qt_5_2_0 = Behavior_Qt_5_6_0
};
/*! QFlags type of \ref BehaviorFlag
 */
Q_DECLARE_FLAGS(BehaviorFlags, BehaviorFlag)
Q_ENUM_NS(BehaviorFlag)


// LCOV_EXCL_START
/*! HTTP Status Codes - %Qt Variant
 *
 * https://github.com/j-ulrich/http-status-codes-cpp
 *
 * \version 1.1.1
 * \author Jochen Ulrich <jochenulrich@t-online.de>
 * \copyright Licensed under Creative Commons CC0 (http://creativecommons.org/publicdomain/zero/1.0/)
 */
namespace HttpStatus{
enum Code{Continue=100,SwitchingProtocols=101,Processing=102,EarlyHints=103,OK=200,Created=201,Accepted=202,NonAuthoritativeInformation=203,NoContent=204,ResetContent=205,PartialContent=206,MultiStatus=207,AlreadyReported=208,IMUsed=226,MultipleChoices=300,MovedPermanently=301,Found=302,SeeOther=303,NotModified=304,UseProxy=305,TemporaryRedirect=307,PermanentRedirect=308,BadRequest=400,Unauthorized=401,PaymentRequired=402,Forbidden=403,NotFound=404,MethodNotAllowed=405,NotAcceptable=406,ProxyAuthenticationRequired=407,RequestTimeout=408,Conflict=409,Gone=410,LengthRequired=411,PreconditionFailed=412,PayloadTooLarge=413,URITooLong=414,UnsupportedMediaType=415,RangeNotSatisfiable=416,ExpectationFailed=417,ImATeapot=418,UnprocessableEntity=422,Locked=423,FailedDependency=424,UpgradeRequired=426,PreconditionRequired=428,TooManyRequests=429,RequestHeaderFieldsTooLarge=431,UnavailableForLegalReasons=451,InternalServerError=500,NotImplemented=501,BadGateway=502,ServiceUnavailable=503,GatewayTimeout=504,HTTPVersionNotSupported=505,VariantAlsoNegotiates=506,InsufficientStorage=507,LoopDetected=508,NotExtended=510,NetworkAuthenticationRequired=511};
inline bool isInformational(int code){return(code>=100&&code<200);}inline bool isSuccessful(int code){return(code>=200&&code<300);}inline bool isRedirection(int code){return(code>=300&&code<400);}inline bool isClientError(int code){return(code>=400&&code<500);}inline bool isServerError(int code){return(code>=500&&code<600);}inline bool isError(int code){return(code>=400);}inline QString reasonPhrase(int code){switch(code){case 100: return QStringLiteral("Continue");case 101: return QStringLiteral("Switching Protocols");case 102: return QStringLiteral("Processing");case 103: return QStringLiteral("Early Hints");case 200: return QStringLiteral("OK");case 201: return QStringLiteral("Created");case 202: return QStringLiteral("Accepted");case 203: return QStringLiteral("Non-Authoritative Information");case 204: return QStringLiteral("No Content");case 205: return QStringLiteral("Reset Content");case 206: return QStringLiteral("Partial Content");case 207: return QStringLiteral("Multi-Status");case 208: return QStringLiteral("Already Reported");case 226: return QStringLiteral("IM Used");case 300: return QStringLiteral("Multiple Choices");case 301: return QStringLiteral("Moved Permanently");case 302: return QStringLiteral("Found");case 303: return QStringLiteral("See Other");case 304: return QStringLiteral("Not Modified");case 305: return QStringLiteral("Use Proxy");case 307: return QStringLiteral("Temporary Redirect");case 308: return QStringLiteral("Permanent Redirect");case 400: return QStringLiteral("Bad Request");case 401: return QStringLiteral("Unauthorized");case 402: return QStringLiteral("Payment Required");case 403: return QStringLiteral("Forbidden");case 404: return QStringLiteral("Not Found");case 405: return QStringLiteral("Method Not Allowed");case 406: return QStringLiteral("Not Acceptable");case 407: return QStringLiteral("Proxy Authentication Required");case 408: return QStringLiteral("Request Timeout");case 409: return QStringLiteral("Conflict");case 410: return QStringLiteral("Gone");case 411: return QStringLiteral("Length Required");case 412: return QStringLiteral("Precondition Failed");case 413: return QStringLiteral("Payload Too Large");case 414: return QStringLiteral("URI Too Long");case 415: return QStringLiteral("Unsupported Media Type");case 416: return QStringLiteral("Range Not Satisfiable");case 417: return QStringLiteral("Expectation Failed");case 418: return QStringLiteral("I'm a teapot");case 422: return QStringLiteral("Unprocessable Entity");case 423: return QStringLiteral("Locked");case 424: return QStringLiteral("Failed Dependency");case 426: return QStringLiteral("Upgrade Required");case 428: return QStringLiteral("Precondition Required");case 429: return QStringLiteral("Too Many Requests");case 431: return QStringLiteral("Request Header Fields Too Large");case 451: return QStringLiteral("Unavailable For Legal Reasons");case 500: return QStringLiteral("Internal Server Error");case 501: return QStringLiteral("Not Implemented");case 502: return QStringLiteral("Bad Gateway");case 503: return QStringLiteral("Service Unavailable");case 504: return QStringLiteral("Gateway Time-out");case 505: return QStringLiteral("HTTP Version Not Supported");case 506: return QStringLiteral("Variant Also Negotiates");case 507: return QStringLiteral("Insufficient Storage");case 508: return QStringLiteral("Loop Detected");case 510: return QStringLiteral("Not Extended");case 511: return QStringLiteral("Network Authentication Required");default: return QString();}}
inline int networkErrorToStatusCode(QNetworkReply::NetworkError error){switch(error){case QNetworkReply::AuthenticationRequiredError: return Unauthorized;case QNetworkReply::ContentAccessDenied: return Forbidden;case QNetworkReply::ContentNotFoundError: return NotFound;case QNetworkReply::ContentOperationNotPermittedError: return MethodNotAllowed;case QNetworkReply::ProxyAuthenticationRequiredError: return ProxyAuthenticationRequired;
#if QT_VERSION >= QT_VERSION_CHECK(5,3,0)
case QNetworkReply::ContentConflictError: return Conflict;case QNetworkReply::ContentGoneError: return Gone;case QNetworkReply::InternalServerError: return InternalServerError;case QNetworkReply::OperationNotImplementedError: return NotImplemented;case QNetworkReply::ServiceUnavailableError: return ServiceUnavailable;case QNetworkReply::UnknownServerError: return InternalServerError;
#endif
case QNetworkReply::NoError: return OK;case QNetworkReply::ProtocolInvalidOperationError: return BadRequest;case QNetworkReply::UnknownContentError: return BadRequest;default:return-1;}}inline QNetworkReply::NetworkError statusCodeToNetworkError(int code){if(!isError(code))return QNetworkReply::NoError;switch(code){case BadRequest: return QNetworkReply::ProtocolInvalidOperationError;case Unauthorized: return QNetworkReply::AuthenticationRequiredError;case Forbidden: return QNetworkReply::ContentAccessDenied;case NotFound: return QNetworkReply::ContentNotFoundError;case MethodNotAllowed: return QNetworkReply::ContentOperationNotPermittedError;case ProxyAuthenticationRequired: return QNetworkReply::ProxyAuthenticationRequiredError;
#if QT_VERSION >= QT_VERSION_CHECK(5,3,0)
case Conflict: return QNetworkReply::ContentConflictError;case Gone: return QNetworkReply::ContentGoneError;case InternalServerError: return QNetworkReply::InternalServerError;case NotImplemented: return QNetworkReply::OperationNotImplementedError;case ServiceUnavailable: return QNetworkReply::ServiceUnavailableError;
#endif
case ImATeapot: return QNetworkReply::ProtocolInvalidOperationError;default:break;}if(isClientError(code))return QNetworkReply::UnknownContentError;
#if QT_VERSION >= QT_VERSION_CHECK(5,3,0)
if(isServerError(code))return QNetworkReply::UnknownServerError;
#endif
return QNetworkReply::ProtocolFailure;}
} // namespace HttpStatus
// LCOV_EXCL_STOP

/*! \internal Implementation details
 */
namespace detail
{

/*! \internal
 * Formats a pointer's address as string.
 * \param pointer The pointer.
 * \return A string representing the \p pointer's address.
 */
inline QString pointerToQString(void* pointer)
{
	// From https://stackoverflow.com/a/16568641/490560
	return QString("0x%1").arg(reinterpret_cast<quintptr>(pointer), QT_POINTER_SIZE * 2, 16, QChar('0'));
}

} // namespace detail


/*! Provides helper methods for tasks related to HTTP.
 *
 * \sa https://tools.ietf.org/html/rfc7230
 */
namespace HttpUtils
{
	/*! \return The scheme of the Hypertext Transfer Protocol (HTTP) in lower case characters.
	 */
	inline const QString& httpScheme()
	{
		static const QString httpSchemeString("http");
		return httpSchemeString;
	}

	/*! \return The scheme of the Hypertext Transfer Protocol Secure (HTTPS) in lower case characters.
	 */
	inline const QString& httpsScheme()
	{
		static const QString httpsSchemeString("https");
		return httpsSchemeString;
	}

	/*! \return The name of the Location header field.
	 */
	inline const QByteArray& locationHeader()
	{
		static const QByteArray locationHeaderKey("Location");
		return locationHeaderKey;
	}

	/*! \return The name of the WWW-Authenticate header field.
	 */
	inline const QByteArray& wwwAuthenticateHeader()
	{
		static const QByteArray wwwAuthenticateHeaderKey("WWW-Authenticate");
		return wwwAuthenticateHeaderKey;
	}

	/*! \return The name of the Proxy-Authenticate header field.
	 */
	inline const QByteArray& proxyAuthenticateHeader()
	{
		static const QByteArray proxyAuthenticateHeaderKey("Proxy-Authenticate");
		return proxyAuthenticateHeaderKey;
	}

	/*! \return The name of the Authorization header field.
	 */
	inline const QByteArray& authorizationHeader()
	{
		static const QByteArray authorizationHeaderKey("Authorization");
		return authorizationHeaderKey;
	}

	/*! \return The name of the Proxy-Authorization header field.
	 */
	inline const QByteArray& proxyAuthorizationHeader()
	{
		static const QByteArray proxyAuthorizationHeaderKey("Proxy-Authorization");
		return proxyAuthorizationHeaderKey;
	}

	/*! \return The regular expression pattern to match tokens according to RFC 7230 3.2.6.
	 */
	inline const QString& tokenPattern()
	{
		static const QString token("(?:[0-9a-zA-Z!#$%&'*+\\-.^_`|~]+)");
		return token;
	}

	/*! \return The regular expression pattern to match token68 according to RFC 7235 2.1.
	 */
	inline const QString& token68Pattern()
	{
		static const QString token68("(?:[0-9a-zA-Z\\-._~+\\/]+=*)");
		return token68;
	}
	/*! \return The regular expression pattern to match successive linear whitespace according to RFC 7230 3.2.3.
	 */
	inline const QString& lwsPattern()
	{
		static const QString lws("(?:[ \t]+)");
		return lws;
	}
	/*! \return The regular expression pattern to match obsolete line folding (obs-fold) according to RFC 7230 3.2.4.
	 */
	inline const QString& obsFoldPattern()
	{
		static const QString obsFold("(?:\r\n[ \t])");
		return obsFold;
	}
	/*! Returns a version of a string with linear whitespace according to RFC 7230 3.2.3 removed from the
	 * beginning and end of the string.
	 *
	 * \param string The string whose leading and trailing linear whitespace should be removed.
	 * \return A copy of \p string with all horizontal tabs and spaces removed from the beginning and end of the
	 * string.
	 */
	inline QString trimmed(const QString& string)
	{
		static const QRegularExpression leadingLwsRegEx("^"+lwsPattern()+"+");
		static const QRegularExpression trailingLwsRegEx(lwsPattern()+"+$");

		QString trimmed(string);

		QRegularExpressionMatch leadingMatch = leadingLwsRegEx.match(trimmed);
		if (leadingMatch.hasMatch())
			trimmed.remove(0, leadingMatch.capturedLength(0));

		QRegularExpressionMatch trailingMatch = trailingLwsRegEx.match(trimmed);
		if (trailingMatch.hasMatch())
			trimmed.remove(trailingMatch.capturedStart(0), trailingMatch.capturedLength(0));

		return trimmed;
	}
	/*! Returns a version of a string with obsolete line folding replaced with a space and whitespace trimmed,
	 * both according to RFC 7230.
	 *
	 * \param string The string which should be trimmed and whose obs-folding should be removed.
	 * \return A copy of \p string with all obsolete line foldings (RFC 7230 3.2.4) replaced with a space
	 * and afterwards, trimmed using trimmed().
	 *
	 * \sa trimmed()
	 */
	inline QString whiteSpaceCleaned(const QString& string)
	{
		static const QRegularExpression obsFoldRegEx(obsFoldPattern());
		QString cleaned(string);
		cleaned.replace(obsFoldRegEx, QLatin1String(" "));
		return trimmed(cleaned);
	}

	/*! Checks if a given string is a token according to RFC 7230 3.2.6.
	 *
	 * \param string The string to be checked to be a token.
	 * \return \c true if \p string is a valid token or \c false otherwise.
	 */
	inline bool isValidToken(const QString& string)
	{
		static const QRegularExpression tokenRegEx("^"+tokenPattern()+"$");
		return tokenRegEx.match(string).hasMatch();
	}

	/*! Checks if a character is illegal to occur in a header field according to RFC 7230 3.2.6.
	 *
	 * \param character The character to be checked.
	 * \return \c true if \p character is an forbidden character for a header field value.
	 */
	inline bool isIllegalHeaderCharacter(const unsigned char character)
	{
		return character != 0x09 // HTAB
		    && character != 0x20 // SP
		    && (character < 0x21 || character > 0x7E)  // VCHAR
		    && (character < 0x80 /*|| character > 0xFF*/); // obs-text
	}

	/*! Checks if a string is a valid quoted-string according to RFC 7230 3.2.6.
	 *
	 * \param string The string to be tested. \p string is expected to *not* contain obsolete line folding (obs-fold).
	 * Use whiteSpaceCleaned() to ensure this.
	 * \return \c true if the \p string is a valid quoted-string.
	 */
	inline bool isValidQuotedString(const QString& string)
	{
		// String needs to contain at least the quotes
		if (string.size() < 2)
			return false;

		unsigned int backslashCount = 0;
		for (int i = 0; i < string.size(); ++i)
		{
			const unsigned char c = static_cast<unsigned char>(string.at(i).toLatin1()); // Non-Latin-1 characters will return 0

			// String must not contain illegal characters
			if (Q_UNLIKELY(isIllegalHeaderCharacter(c)))
				return false;

			if (c == '\\')
				++backslashCount;
			else
			{
				if (i == 0)
				{
					// First character must be a quote
					if (c != '"')
						return false;
				}
				else if (i == (string.size() - 1))
				{
					// Last character must be a quote and it must not be escaped
					if (c != '"' || (backslashCount % 2) != 0)
						return false;
				}
				else
				{
					// Other quotes and obs-text must be escaped
					if ((c == '"' || c >= 0x80) && (backslashCount % 2) == 0)
						return false;
				}

				backslashCount = 0;
			}
		}

		return true;
	}

	/*! Converts a quoted-string according to RFC 7230 3.2.6 to it's unquoted version.
	 *
	 * \param quotedString The quoted string to be converted to "plain" text.
	 * \return A copy of \p quotedString with all quoted-pairs converted to the second character of the pair and the
	 * leading and trailing double quotes removed. If \p quotedString is not a valid quoted-string, a null
	 * QString() is returned.
	 */
	inline QString unquoteString(const QString& quotedString)
	{
		if (!isValidQuotedString(quotedString))
			return QString();

		QString unquotedString(quotedString.mid(1, quotedString.size()-2));

		static const QRegularExpression quotedPairRegEx("\\\\.");
		QStack<QRegularExpressionMatch> quotedPairMatches;
		QRegularExpressionMatchIterator quotedPairIter = quotedPairRegEx.globalMatch(unquotedString);
		while (quotedPairIter.hasNext())
			quotedPairMatches.push(quotedPairIter.next());

		while (!quotedPairMatches.isEmpty())
		{
			QRegularExpressionMatch match = quotedPairMatches.pop();
			unquotedString.remove(match.capturedStart(0), 1);
		}

		return unquotedString;
	}

	/*! Converts a string to it's quoted version according to RFC 7230 3.2.6.
	 *
	 * \param unquotedString The "plain" text to be converted to a quoted-string.
	 * \return A copy of \p unquotedString surrounded with double quotes and all double quotes, backslashes
	 * and obs-text characters escaped. If the \p unquotedString contains any characters that are not allowed
	 * in a header field value, a null QString() is returned.
	 */
	inline QString quoteString(const QString& unquotedString)
	{
		QString escapedString;

		for (int i=0; i < unquotedString.size(); ++i)
		{
			const unsigned char c = static_cast<const unsigned char>(unquotedString.at(i).toLatin1()); // Non-Latin-1 characters will return 0

			if (Q_UNLIKELY(isIllegalHeaderCharacter(c)))
				return QString();

			if (c == '"' || c == '\\' || c >= 0x80)
				escapedString += '\\';
			escapedString += static_cast<char>(c);
		}

		return "\"" + escapedString + "\"";
	}

	/*! Splits a string containing a comma-separated list according to RFC 7230 section 7.
	 *
	 * \param commaSeparatedList A string containing a comma-separated list. The list can contain
	 * quoted strings and commas within quoted strings are not treated as list separators.
	 * \return QStringList consisting of the elements of \p commaSeparatedList.
	 * Empty elements in \p commaSeparatedList are omitted.
	 */
	inline QStringList splitCommaSeparatedList(const QString& commaSeparatedList)
	{
		QStringList split;

		bool inString = false;
		bool escaped = false;

		QString nextEntry;

		QString::const_iterator iter;
		for (iter = commaSeparatedList.cbegin(); iter != commaSeparatedList.cend(); ++iter)
		{
			if (inString)
			{
				if (*iter == '\\')
					escaped = !escaped;
				else
				{
					if (*iter == '"')
					{
						if (!escaped)
							inString = false;
					}
					escaped = false;
				}
				nextEntry += *iter;
			}
			else
			{
				if (*iter == ',')
				{
					const QString trimmedEntry = trimmed(nextEntry);
					if (!trimmedEntry.isEmpty())
						split << trimmedEntry;
					nextEntry.clear();
				}
				else
				{
					if (*iter == '"')
						inString = true;
					nextEntry += *iter;
				}
			}
		}

		const QString trimmedEntry = trimmed(nextEntry);
		if (!trimmedEntry.isEmpty())
			split << trimmedEntry;

		return split;
	}

	/*! Namespace for HTTP authentication related classes.
	 *
	 * \sa https://tools.ietf.org/html/rfc7235
	 */
	namespace Authentication
	{
		/*! Returns the authentication scope of a URL according to RFC 7617 2.2.
		 *
		 * \param url The URL whose authentication scope should be returned.
		 * \return A URL which has the same scheme, host, port and path up to the last slash
		 * as \p url.
		 */
		inline QUrl authenticationScopeForUrl(const QUrl& url)
		{
			QUrl authScope;
			authScope.setScheme(url.scheme());
			authScope.setHost(url.host());
			authScope.setPort(url.port());
			QFileInfo urlPath(url.path(QUrl::FullyEncoded));
			QString path = urlPath.path(); // Remove the part after the last slash using QFileInfo::path()
			if (path == ".")
				path = "/";
			else if (!path.endsWith('/'))
				path += '/';
			authScope.setPath(path);
			return authScope;
		}

		/*! \internal Implementation details
		 */
		namespace detail
		{

			static QByteArray authorizationHeaderKey()
			{
				static const QByteArray authHeader("Authorization");
				return authHeader;
			}

			static QString authParamPattern()
			{
				static const QString authParam("(?:(?<authParamName>"+HttpUtils::tokenPattern()+")"+HttpUtils::lwsPattern()+"?="+HttpUtils::lwsPattern()+"?(?<authParamValue>"+HttpUtils::tokenPattern()+"|\".*\"))");
				return authParam;
			}

		} // namespace detail


		/*! Represents an HTTP authentication challenge according to RFC 7235.
		 */
		class Challenge
		{
		public:
			/*! QSharedPointer to a Challenge object.
			 */
			typedef QSharedPointer<Challenge> Ptr;

			/*! Defines the supported HTTP authentication schemes.
			 */
			enum AuthenticationScheme
			{
				/* WARNING: The numerical value defines the preference when multiple
				 * challenges are provided by the server.
				 * The lower the numerical value, the lesser the preference.
				 * So give stronger methods higher values.
				 * See strengthGreater()
				 */
				BasicAuthenticationScheme   = 100, //!< HTTP Basic authentication according to RFC 7617
				UnknownAuthenticationScheme = -1   //!< Unknown authentication scheme
			};

			/*! Creates an invalid authentication challenge.
			 *
			 * Sets the behaviorFlags() to Behavior_Expected.
			 */
			Challenge()
			{
				setBehaviorFlags(Behavior_Expected);
			}
			/*! Enforces a virtual destructor.
			 */
			virtual ~Challenge() {}

			/*! \return The authentication scheme of this Challenge.
			 */
			virtual AuthenticationScheme scheme() const = 0;

			/*! Checks if the Challenge is valid, meaning it contains all
			 * parameters required for the authentication scheme.
			 *
			 * \return \c true if the Challenge is valid.
			 */
			virtual bool isValid() const = 0;
			/*! \return The parameters of the Challenge as a QVariantMap.
			 */
			virtual QVariantMap parameters() const = 0;
			/*! \return The value of an Authenticate header representing this Challenge.
			 */
			virtual QByteArray authenticateHeader() const = 0;
			/*! Compares the cryptographic strength of this Challenge with another
			 * Challenge.
			 *
			 * \param other The Challenge to compare against.
			 * \return \c true if this Challenge is considered cryptographically
			 * stronger than \p other. If they are equal or if \p other is stronger,
			 * \c false is returned.
			 */
			virtual bool strengthGreater(const Challenge::Ptr& other) { return this->scheme() > other->scheme(); }

			/*! Tunes the behavior of this Challenge.
			 *
			 * \param behaviorFlags Combination of BehaviorFlags to define some details of this Challenge's behavior.
			 * \note Only certain BehaviorFlags have an effect on a Challenge.
			 * \sa BehaviorFlag
			 */
			void setBehaviorFlags(BehaviorFlags behaviorFlags)
			{
				m_behaviorFlags = behaviorFlags;
			}

			/*! \return The BehaviorFlags currently active for this Challenge.
			 */
			BehaviorFlags behaviorFlags() const { return m_behaviorFlags; }

			/*! \return The realm of this Challenge according to RFC 7235 2.2.
			 */
			QString realm() const { return m_realm; }

			/*! \return The name of the realm parameter. Also used as key in QVariantMaps.
			 */
			static inline QString realmKey() { return QStringLiteral("realm"); }

			/*! Adds an authorization header for this Challenge to a given request.
			 *
			 * \param request The QNetworkRequest to which the authorization header will be added.
			 * \param operation The HTTP verb.
			 * \param body The message body of the request.
			 * \param authenticator The QAuthenticator providing the credentials to be used for the
			 * authorization.
			 */
			void addAuthorization(QNetworkRequest& request, QNetworkAccessManager::Operation operation, const QByteArray& body, const QAuthenticator& authenticator)
			{
				QByteArray authHeaderValue = authorizationHeaderValue(request, operation, body, authenticator);
				request.setRawHeader(detail::authorizationHeaderKey(), authHeaderValue);
			}

			/*! Verifies if a given request contains a valid authorization for this Challenge.
			 *
			 * \param request The request which requests authorization.
			 * \param authenticator The QAuthenticator providing a set of valid credentials.
			 * \return \c true if the \p request contains a valid authorization header matching
			 * this Challenge and the credentials provided by the \p authenticator.
			 * Note that for certain authentication schemes, this method might always return \c false if this Challenge
			 * is invalid (see isValid()).
			 */
			virtual bool verifyAuthorization(const QNetworkRequest& request, const QAuthenticator& authenticator) = 0;

			/*! Implements a "lesser" comparison based on the cryptographic strength of a Challenge.
			 */
			struct StrengthCompare
			{
				/*! Implements the lesser comparison.
				 *
				 * \param left The left-hand side Challenge of the comparison.
				 * \param right The right-hand side Challenge of the comparison.
				 * \return \c true if \p left < \p right regarding the strength of the algorithm
				 * used by the challenges. Otherwise \c false.
				 */
				bool operator() (const Challenge::Ptr& left, const Challenge::Ptr& right)
				{
					return right->strengthGreater(left);
				}
			};

		protected:

			/*! Generates a new authorization header value for this Challenge.
			 *
			 * \note This method is non-const because an authentication scheme might need
			 * to remember parameters from the authorizations it gave (like the \c cnonce in the Digest scheme).
			 *
			 * \param request The request for with the authorization header should be generated.
			 * \param operation The HTTP verb of the request.
			 * \param body The message body of the request.
			 * \param authenticator The QAuthenticator providing the credentials to be used to generate the authorization header.
			 * \return The value of the Authorization header to request authorization for this Challenge using the
			 * credentials provided by the \p authenticator.
			 *
			 * \sa addAuthorization()
			 */
			virtual QByteArray authorizationHeaderValue(const QNetworkRequest& request, QNetworkAccessManager::Operation operation, const QByteArray& body, const QAuthenticator& authenticator) = 0;

			/*! Splits a list of authentication parameters according to RFC 7235 2.1. into a QVariantMap.
			 *
			 * \param authParams The list of name=value strings.
			 * \param[out] paramsValid If not \c NULL, the value of this boolean will be set to \c false if one of the
			 * parameters in \p authParams was malformed or to \c true otherwise. If \p paramsValid is \c NULL, it is
			 * ignored.
			 * \return A QVariantMap mapping the names of the authentication parameters to their values.
			 * The names of the authentication parameters are converted to lower case.
			 * The values are *not* unquoted in case they are quoted strings.
			 */
			static QVariantMap stringParamListToMap(const QStringList& authParams, bool *paramsValid = Q_NULLPTR)
			{
				QVariantMap result;

				QStringList::const_iterator paramIter;
				static const QRegularExpression authParamRegEx(detail::authParamPattern());

				for (paramIter = authParams.cbegin(); paramIter != authParams.cend(); ++paramIter)
				{
					QRegularExpressionMatch authParamMatch = authParamRegEx.match(*paramIter);
					if (!authParamMatch.hasMatch())
					{
						qWarning("MockNetworkAccessManager: Invalid authentication header: malformed auth-param: %s", paramIter->toLatin1().constData());
						if (paramsValid)
							*paramsValid = false;
						return QVariantMap();
					}
					const QString authParamName = authParamMatch.captured("authParamName").toLower();
					const QString authParamValue = authParamMatch.captured("authParamValue");

					if (result.contains(authParamName))
						qWarning("MockNetworkAccessManager: Invalid authentication header: auth-param \"%s\" occurred multiple times", authParamName.toLatin1().constData());

					result.insert(authParamName, authParamValue);
				}

				if (paramsValid)
					*paramsValid = true;
				return result;
			}

		protected:
			/*! The realm of this Challenge according to RFC 7235 2.2.
			 * Derived classes are free to use this member in any way they wish.
			 */
			QString m_realm;

		private:
			BehaviorFlags m_behaviorFlags;
		};

		/*! HTTP Basic authentication scheme according to RFC 7617.
		 *
		 * \sa https://tools.ietf.org/html/rfc7617
		 */
		class Basic : public Challenge
		{
		public:
			/*! Creates a Basic authentication Challenge with parameters as a QStringList.
			 *
			 * \param authParams The parameters of the challenge as a list of name=value strings.
			 */
			Basic(const QStringList& authParams)
				: Challenge()
			{
				bool paramsValid = false;
				const QVariantMap authParamsMap = stringParamListToMap(authParams, &paramsValid);
				if (paramsValid)
					readParameters(authParamsMap);
			}

			/*! Creates a Basic authentication Challenge with parameters a QVariantMap.
			 *
			 * \param authParams The parameters of the challenge as a map.
			 */
			Basic(const QVariantMap& authParams)
				: Challenge()
			{
				readParameters(authParams);
			}

			/*! Creates a Basic authentication Challenge with the given realm.
			 *
			 * \param realm The realm.
			 */
			Basic(const QString& realm)
				: Challenge()
			{
				QVariantMap params;
				params.insert(realmKey(), realm);
				readParameters(params);
			}


			/*! \return Challenge::BasicAuthenticationScheme.
			 */
			virtual AuthenticationScheme scheme() const { return BasicAuthenticationScheme; }

			/*! \return The identifier string of the Basic authentication scheme.
			 */
			static inline QByteArray schemeString() { return "Basic"; }
			/*! \return The name of the charset parameter. Also used as key in QVariantMaps.
			 */
			static inline QString charsetKey() { return QStringLiteral("charset"); }

			/*! \return \c true if the realm parameter is defined. Note that the realm can still
			 * be empty (`""`).
			 */
			virtual bool isValid() const { return !m_realm.isNull(); }

			/*! \return A map containing the realm and charset parameters (if given).
			 * \sa realmKey(), charsetKey().
			 */
			virtual QVariantMap parameters() const
			{
				QVariantMap params;
				params[realmKey()] = m_realm;
				if (!m_charset.isEmpty())
					params[charsetKey()] = m_charset;
				return params;
			}
			/*! \copydoc Challenge::authenticateHeader()
			 */
			virtual QByteArray authenticateHeader() const
			{
				if (!isValid())
					return QByteArray();

				QByteArray result = (schemeString() + " " + realmKey() + "=" + quoteString(m_realm)).toLatin1();
				if (!m_charset.isEmpty())
					result += ", " + charsetKey() + "=" + quoteString(m_charset);
				return result;
			}

			/*! \copydoc Challenge::verifyAuthorization(const QNetworkRequest&, const QAuthenticator&)
			 */
			virtual bool verifyAuthorization(const QNetworkRequest& request, const QAuthenticator& authenticator)
			{
				/* Since the authorization header of the Basic scheme is very simple, we can simply compare
				 * the textual representations.
				 * Additionally, we can verify the authorization even if this challenge is invalid.
				 */
				const QByteArray reqAuth = request.rawHeader(detail::authorizationHeaderKey());
				const QByteArray challengeAuth = this->authorizationHeaderValue(QNetworkRequest(), QNetworkAccessManager::GetOperation, QByteArray(), authenticator);
				return reqAuth == challengeAuth;
			}


		protected:
			/*! \copydoc Challenge::authorizationHeaderValue()
			 */
			virtual QByteArray authorizationHeaderValue(const QNetworkRequest& request, QNetworkAccessManager::Operation operation, const QByteArray& body, const QAuthenticator& authenticator)
			{
				Q_UNUSED(request);
				Q_UNUSED(operation);
				Q_UNUSED(body);

				QByteArray userName;
				QByteArray password;
				if (behaviorFlags().testFlag(Behavior_HttpAuthLatin1Encoding))
				{
					userName = authenticator.user().toLatin1();
					password = authenticator.password().toLatin1();
				}
				else // No need to check m_charset since UTF-8 is the only allowed encoding at the moment and we use UTF-8 by default anyway (so even if charset was not specified)
				{
					userName = authenticator.user().normalized(QString::NormalizationForm_C).toUtf8();
					password = authenticator.password().normalized(QString::NormalizationForm_C).toUtf8();
				}

				return schemeString() + " " + (userName + ":" + password).toBase64();
			}

		private:
			void readParameters(const QVariantMap& params)
			{
				if (!params.contains(realmKey()))
				{
					m_realm.clear();
					qWarning("MockNetworkAccessManager: Invalid authentication header: Missing required \"realm\" parameter");
					return;
				}

				// Realm
				const QString realmValue = params.value(realmKey()).toString();
				const QString realm = HttpUtils::isValidToken(realmValue)? realmValue : HttpUtils::unquoteString(realmValue);
				if (!realm.isNull())
					m_realm = realm;
				else
				{
					qWarning("MockNetworkAccessManager: Invalid authentication header: invalid \"realm\" parameter");
					return;
				}

				// Charset
				if (params.contains(charsetKey()))
				{
					const QString charsetValue = params.value(charsetKey()).toString();
					const QString charset = (HttpUtils::isValidToken(charsetValue)? charsetValue : HttpUtils::unquoteString(charsetValue)).toLower();
					m_charset = charset;
				}
			}

			QString m_charset;
		};

		/*! \internal Implementation details
		 */
		namespace detail
		{

			static QVector<Challenge::Ptr> parseAuthenticateHeader(const QString& headerValue, const QUrl&)
			{
				QVector<Challenge::Ptr> result;

				static const QRegularExpression challengeStartRegEx("^"+HttpUtils::tokenPattern()+"(?:"+HttpUtils::lwsPattern()+"(?:"+HttpUtils::token68Pattern()+"|"+detail::authParamPattern()+"))?");

				QStringList headerSplit = HttpUtils::splitCommaSeparatedList(headerValue);

				QVector<QPair<int, int> > challengeIndexes;
				int challengeStartIndex = headerSplit.indexOf(challengeStartRegEx);
				if (challengeStartIndex < 0)
				{
					qWarning("MockNetworkAccessManager: Invalid authentication header: expected start of authentication challenge");
					return result;
				}
				while (challengeStartIndex != -1)
				{
					int nextChallengeStartIndex = headerSplit.indexOf(challengeStartRegEx, challengeStartIndex + 1);
					challengeIndexes << qMakePair(challengeStartIndex, nextChallengeStartIndex);
					challengeStartIndex = nextChallengeStartIndex;
				};

				QVector<QPair<int, int> >::const_iterator challengeIndexIter;

				for (challengeIndexIter = challengeIndexes.cbegin(); challengeIndexIter != challengeIndexes.cend(); ++challengeIndexIter)
				{
					Challenge::Ptr authChallenge;

					const QString challengeStart = headerSplit.at(challengeIndexIter->first);
					const int schemeSeparatorIndex = challengeStart.indexOf(' ');
					const QString authScheme = HttpUtils::trimmed(challengeStart.left(schemeSeparatorIndex)).toLower();
					const QString firstAuthParam = (schemeSeparatorIndex > 0)? HttpUtils::trimmed(challengeStart.mid(schemeSeparatorIndex+1)) : QString();

					// Get the first paramter of the challenge
					QStringList authParams;
					if (!firstAuthParam.isEmpty())
						authParams << firstAuthParam;
					// Append further parameters of the challenge
					const int challengeCslEntryLength = (challengeIndexIter->second == -1)
					                                    ? (headerSplit.size() - challengeIndexIter->first)
					                                    : challengeIndexIter->second - challengeIndexIter->first;
					if (challengeCslEntryLength > 1)
						authParams << headerSplit.mid(challengeIndexIter->first + 1, challengeCslEntryLength - 1);

					static const QLatin1String basicAuthScheme("basic");
					if (authScheme == basicAuthScheme)
						authChallenge.reset(new Basic(authParams));
					else
						qWarning("MockNetworkAccessManager: Unsupported authentication scheme \"%s\"", authScheme.toLatin1().constData());

					if (authChallenge && authChallenge->isValid())
						result << authChallenge;
				}

				return result;
			}

		} // namespace detail

		/*! Extracts all authentication challenges from a QNetworkReply.
		 *
		 * \param reply The reply object potentially containing authentication challenges.
		 * \return A vector of Challenge::Ptrs. The vector can be empty if \p reply did not
		 * contain any authentication challenges.
		 */
		static QVector<Challenge::Ptr> getAuthenticationChallenges(QNetworkReply* reply)
		{
			QVector<Challenge::Ptr> authChallenges;
			QUrl requestingUrl = reply->url();

			switch (static_cast<HttpStatus::Code>(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt()))
			{
			case HttpStatus::Unauthorized:
			{
				static const QByteArray wwwAuthenticateHeaderLower = wwwAuthenticateHeader().toLower();

				QList<QByteArray>::const_iterator headerIter;
				QList<QByteArray> rawHeaderList = reply->rawHeaderList();
				for (headerIter = rawHeaderList.cbegin(); headerIter != rawHeaderList.cend(); ++headerIter)
				{
					if (headerIter->toLower() == wwwAuthenticateHeaderLower)
					{
						QString headerValue = HttpUtils::whiteSpaceCleaned(QString::fromLatin1(reply->rawHeader(*headerIter)));
						if (headerValue.isEmpty())
							continue;

						authChallenges << detail::parseAuthenticateHeader(headerValue, requestingUrl);
					}
				}
				break;
			}

			case HttpStatus::ProxyAuthenticationRequired:
				// TODO
				qWarning("MockNetworkAccessManager: Proxy authentication is not supported at the moment");
				break;
			// LCOV_EXCL_START
			default:
				Q_ASSERT_X(false, Q_FUNC_INFO, "MockNetworkAccessManager: Internal error: trying to authenticate request which doesn't require authentication");
				break;
			// LCOV_EXCL_STOP
			}

			return authChallenges;
		}

	} // namespace Authentication

} // namespace HttpUtils


/*! Represents a version number.
 * A version number is a sequence of (dot separated) unsigned integers potentially followed by a suffix.
 *
 * \since 0.3.0
 */
struct VersionNumber
{
	/*! The container type holding the version segments.
	 * \sa segments
	 */
	typedef std::vector<unsigned int> SegmentVector;

	/*! The numeric segments that make up the version number.
	 */
	SegmentVector segments;
	/*! The non-numeric suffix of the version number.
	 */
	QString suffix;

	/*! \return `"."` which is the string separating the version segments in the string representation of the version number.
	 */
	static const QString& segmentSeparator()
	{
		static const QString separator(QStringLiteral("."));
		return separator;
	}

	/*! Creates an empty VersionNumber.
	 */
	VersionNumber()
	{
	}

	/*! Creates a VersionNumber from three segments.
	 * \param major The major version number.
	 * \param minor The minor version number.
	 * \param patch The patch version number.
	 * \param suffix An	optional version suffix.
	 */
	explicit VersionNumber(unsigned int major, unsigned int minor, unsigned int patch, const QString& suffix = QString())
	{
		segments.push_back(major);
		segments.push_back(minor);
		segments.push_back(patch);
		this->suffix = suffix;
	}

	/*! Creates a VersionNumber from a string representation.
	 * \param versionStr The string representing the version number.
	 * \return A VersionNumber object corresponding to the \p versionStr or an
	 * empty VersionNumber object if the \p versionStr could not be parsed.
	 */
	static VersionNumber fromString(const QString& versionStr)
	{
		VersionNumber version;
		const QStringList split = versionStr.split(segmentSeparator());

		version.segments.reserve(static_cast<SegmentVector::size_type>(split.size()));

		bool converted = true;
		QStringList::const_iterator iter;
		for ( iter = split.cbegin(); iter != split.cend(); ++iter )
		{
			const unsigned int number = iter->toUInt(&converted);
			if (!converted)
				break;
			version.segments.push_back(number);
		}

		if (!converted)
		{
			// There is a suffix
			const QString lastSegment = *iter;
			const QRegularExpression digitRegEx("^\\d+");
			const QRegularExpressionMatch match = digitRegEx.match(lastSegment);
			if (match.hasMatch())
				version.segments.push_back(match.captured().toUInt());
			version.suffix = lastSegment.mid(match.capturedLength());
		}

		return version;
	}

	/*! \return The string representation of this version number.
	 */
	QString toString() const
	{
		QString result;
		SegmentVector::const_iterator segIter;
		const SegmentVector& segs = segments;
		for (segIter = segs.begin(); segIter != segs.end(); ++segIter)
			result += QString::number(*segIter) + segmentSeparator();
		result.chop(segmentSeparator().size());
		result += suffix;
		return result;
	}

	/*! Compares two VersionNumbers for equality.
	 * \param left One VersionNumber.
	 * \param right Another VersionNumber.
	 * \return \c true if \p left and \p right represent the same version number.
	 * \note Missing parts in a VersionNumber are interpreted as 0.
	 */
	friend bool operator==(const VersionNumber& left, const VersionNumber& right)
	{
		SegmentVector leftSegments, rightSegments;

		leftSegments = left.segments;
		rightSegments = right.segments;
		const SegmentVector::size_type maxSize = std::max(leftSegments.size(), rightSegments.size());
		leftSegments.resize(maxSize);
		rightSegments.resize(maxSize);
		return leftSegments == rightSegments
		       && left.suffix == right.suffix;
	}

	/*! Compares two VersionNumbers for inequality.
	 * \param left One VersionNumber.
	 * \param right Another VersionNumber.
	 * \return \c true if \p left and \p right represent different version numbers.
	 * \note Missing parts in a VersionNumber are interpreted as 0.
	 */
	friend bool operator!=(const VersionNumber& left, const VersionNumber& right)
	{
		return !( left == right );
	}

	/*! Compares if a VersionNumber is lesser than another VersionNumber.
	 * \param left One VersionNumber.
	 * \param right Another VersionNumber.
	 * \return \c true if \p left is lesser than \p right.
	 * \note Missing parts in a VersionNumber are interpreted as 0.
	 */
	friend bool operator<(const VersionNumber& left, const VersionNumber& right)
	{
		std::vector<unsigned int>::const_iterator leftIter, rightIter;

		for (leftIter = left.segments.begin(),
		     rightIter = right.segments.begin();
		     leftIter != left.segments.end()
		     || rightIter != right.segments.end();)
		{
			const unsigned int leftPart = (leftIter != left.segments.end())? *(leftIter++) : 0;
			const unsigned int rightPart = (rightIter != right.segments.end())? *(rightIter++) : 0;

			if (leftPart < rightPart)
				return true;
			if (leftPart > rightPart)
				return false;
		}

		if (left.suffix.isEmpty() && !right.suffix.isEmpty())
			return false;
		if (!left.suffix.isEmpty() && right.suffix.isEmpty())
			return true;
		return left.suffix < right.suffix;
	}

	/*! Compares if a VersionNumber is greater than another VersionNumber.
	 * \param left One VersionNumber.
	 * \param right Another VersionNumber.
	 * \return \c true if \p left is greater than \p right.
	 * \note Missing parts in a VersionNumber are interpreted as 0.
	 */
	friend bool operator>(const VersionNumber& left, const VersionNumber& right)
	{
		std::vector<unsigned int>::const_iterator leftIter, rightIter;

		for (leftIter = left.segments.begin(),
		     rightIter = right.segments.begin();
		     leftIter != left.segments.end()
		     || rightIter != right.segments.end();)
		{
			const unsigned int leftPart = (leftIter != left.segments.end())? *(leftIter++) : 0;
			const unsigned int rightPart = (rightIter != right.segments.end())? *(rightIter++) : 0;

			if (leftPart > rightPart)
				return true;
			if (leftPart < rightPart)
				return false;
		}

		if (left.suffix.isEmpty() && !right.suffix.isEmpty())
			return true;
		if (!left.suffix.isEmpty() && right.suffix.isEmpty())
			return false;
		return left.suffix > right.suffix;
	}

	/*! Compares if a VersionNumber is greater than or equal to another VersionNumber.
	 * \param left One VersionNumber.
	 * \param right Another VersionNumber.
	 * \return \c true if \p left is greater than or equal to \p right.
	 * \note Missing parts in a VersionNumber are interpreted as 0.
	 */
	friend bool operator>=(const VersionNumber& left, const VersionNumber& right)
	{
		return !(left < right);
	}

	/*! Compares if a VersionNumber is lesser than or equal to another VersionNumber.
	 * \param left One VersionNumber.
	 * \param right Another VersionNumber.
	 * \return \c true if \p left is lesser than or equal to \p right.
	 * \note Missing parts in a VersionNumber are interpreted as 0.
	 */
	friend bool operator<=(const VersionNumber& left, const VersionNumber& right)
	{
		return !(left > right);
	}
};

class Rule;
class MockReplyBuilder;
template<class Base>
class Manager;

/*! QList of QByteArray. */
typedef QList<QByteArray> ByteArrayList;
/*! QSet of [QNetworkRequest::Attribute].
 * [QNetworkRequest::Attribute]: http://doc.qt.io/qt-5/qnetworkrequest.html#Attribute-enum
 */
typedef QSet<QNetworkRequest::Attribute> AttributeSet;
/*! QHash holding [QNetworkRequest::KnowHeaders] and their corresponding values.
 * \sa QNetworkRequest::header()
 * [QNetworkRequest::KnowHeaders]: http://doc.qt.io/qt-5/qnetworkrequest.html#KnownHeaders-enum
 */
typedef QHash<QNetworkRequest::KnownHeaders, QVariant> HeaderHash;
/*! QSet holding [QNetworkRequest::KnowHeaders].
 * [QNetworkRequest::KnowHeaders]: http://doc.qt.io/qt-5/qnetworkrequest.html#KnownHeaders-enum
 */
typedef QSet<QNetworkRequest::KnownHeaders> KnownHeadersSet;
/*! QHash holding raw headers and their corresponding values.
 * \sa QNetworkRequest::rawHeader()
 */
typedef QHash<QByteArray, QByteArray> RawHeaderHash;
/*! QHash holding query parameter names and their corresponding values.
 * \sa QUrlQuery
 */
typedef QHash<QString, QString> QueryParameterHash;
/*! QHash holding query parameter names and their corresponding values.
 * \sa QUrlQuery
 * \since 0.4.0
 */
typedef QHash<QString, QStringList> MultiValueQueryParameterHash;
/*! QVector of QRegularExpression QPairs.
 */
typedef QVector<QPair<QRegularExpression, QRegularExpression> > RegExPairVector;

/*! Determines the MIME type of data.
 * \param url The URL of the \p data.
 * \param data The data itself.
 * \return The MIME type of the \p data located at \p url.
 * \sa QMimeDatabase::mimeTypeForFileNameAndData()
 */
inline QMimeType guessMimeType(const QUrl& url, const QByteArray& data)
{
	QFileInfo fileInfo(url.path());
	return QMimeDatabase().mimeTypeForFileNameAndData(fileInfo.fileName(), data);
}

/*! Provides access to the request data.
 *
 * This mainly groups all the request data into a single struct for convenience.
 */
struct Request
{
	/*! The HTTP request verb.
	 */
	QNetworkAccessManager::Operation operation;
	/*! The QNetworkRequest object.
	 * This provides access to the details of the request like URL, headers and attributes.
	 */
	QNetworkRequest qRequest;
	/*! The body data.
	 */
	QByteArray body;
	/*! The timestamp when the Manager began handling the request.
	 * For requests received through the public API of QNetworkAccessManager,
	 * this can be considered the time when the Manager received the request.
	 */
	QDateTime timestamp;

	/*! Creates an invalid Request object.
	 * \sa isValid()
	 */
	Request() : operation(QNetworkAccessManager::CustomOperation) {}

	/*! Creates a Request struct.
	 * \param op The Request::operation.
	 * \param req The Request:.qRequest.
	 * \param data The Request::body.
	 * \note The Request::timestamp will be set to the current date and time.
	 */
	Request(QNetworkAccessManager::Operation op,
	        const QNetworkRequest& req,
	        const QByteArray& data = QByteArray())
		: operation(op)
		, qRequest(req)
		, body(data)
		, timestamp(QDateTime::currentDateTime())
	{}

	/*! Creates a Request struct.
	 * \param req The Request:.qRequest.
	 * \param op The Request::operation.
	 * \param data The Request::body.
	 * \note The Request::timestamp will be set to the current date and time.
	 */
	Request(const QNetworkRequest& req,
	        QNetworkAccessManager::Operation op = QNetworkAccessManager::GetOperation,
	        const QByteArray& data = QByteArray())
		: operation(op)
		, qRequest(req)
		, body(data)
		, timestamp(QDateTime::currentDateTime())
	{}

	/*! \return \c true if the Request specifies a valid HTTP verb and the qRequest contains a valid URL.
	 * The HTTP is not valid if operation is QNetworkAccessManager::CustomOperation
	 * and the [QNetworkRequest::CustomVerbAttribute] of qRequest is empty.
	 * [QNetworkRequest::CustomVerbAttribute]: http://doc.qt.io/qt-5/qnetworkrequest.html#Attribute-enum
	 */
	bool isValid() const
	{
		return qRequest.url().isValid()
		    && (operation != QNetworkAccessManager::CustomOperation
		        || !qRequest.attribute(QNetworkRequest::CustomVerbAttribute).toByteArray().trimmed().isEmpty());
	}

	/*! Checks if two Request structs are equal.
	 * \param left One Request struct to be compared.
	 * \param right The other Request struct to be compared with \p left.
	 * \return \c true if all fields of \p left and \c right are equal (including the Request::timestamp). \c false otherwise.
	 */
	friend bool operator==(const Request& left, const Request& right)
	{
		return left.operation == right.operation
		    && left.qRequest  == right.qRequest
		    && left.body      == right.body
		    && left.timestamp == right.timestamp;
	}

	/*! Checks if two Request structs differ.
	 * \param left One Request struct to be compared.
	 * \param right The other Request struct to be compared with \p left.
	 * \return \c true if at least one field of \p left and \c right differs (including the Request::timestamp). \c false if \p left and \p right are equal.
	 */
	friend bool operator!=(const Request& left, const Request& right)
	{
		return !(left == right);
	}
};

/*! QList of Request structs.*/
typedef QList<Request> RequestList;

/*! Mocked QNetworkReply.
 *
 * The MockReply is returned by the Manager instead of a real QNetworkReply.
 * Instead of sending the request to the server and returning the reply,
 * the MockReply returns the predefined (mocked) data.
 *
 * A MockReply behaves like an HTTP based QNetworkReply, except that it doesn't emit
 * the implementation specific signals like QNetworkReplyHttpImpl::startHttpRequest() or
 * QNetworkReplyHttpImpl::abortHttpRequest().
 */
class MockReply : public QNetworkReply
{
	Q_OBJECT

	template<class Base>
	friend class Manager;
	friend class MockReplyBuilder;
	friend class Rule;

public:

	/*! \return The message body of this reply. */
	QByteArray body() const { return m_body.data(); }

	/*! \return The set of attributes defined on this reply.
	 */
	QSet<QNetworkRequest::Attribute> attributes() const { return m_attributeSet; }

	/*! \return \c true
	 * \sa QIODevice::isSequential()
	 */
	bool isSequential() const { return true; }
	/*! \return The number of bytes available for reading.
	 *\sa QIODevice::bytesAvailable()
	 */
	qint64 bytesAvailable() const { return m_body.bytesAvailable(); }

	/*! Aborts the simulated network communication.
	 * \note At the moment, this method does nothing else than calling close()
	 * since the MockReply is already finished before it is returned by the Manager.
	 * However, future versions might simulate network communication and then,
	 * this method allows aborting that.\n
	 * See issue \issue{4}.
	 */
	void abort()
	{
		if (this->isRunning())
		{
			this->setError(QNetworkReply::OperationCanceledError);
			setFinished(true);
			// TODO: Need to actually finish including emitting signals
		}
		close();
	}

	/*! Prevents reading further body data from the reply.
	 * \sa QNetworkReply::close()
	 */
	void close()
	{
		m_body.close();
		QNetworkReply::close();
	}

	/*! Creates a clone of this reply.
	 *
	 * \return A new MockReply which has the same properties as this MockReply.
	 * The caller takes ownership of the returned object.
	 */
	virtual MockReply* clone() const
	{
		MockReply* clone = new MockReply();
		clone->setBody(this->body());
		clone->setRequest(this->request());
		clone->setUrl(this->url());
		clone->setOperation(this->operation());
		clone->setError(this->error(), this->errorString());
		clone->setSslConfiguration(this->sslConfiguration());
		clone->setReadBufferSize(this->readBufferSize());
		clone->setBehaviorFlags(this->m_behaviorFlags);

		static const QByteArray setCookieHeader("Set-Cookie");
		KnownHeadersSet copyKnownHeaders;

		const ByteArrayList rawHeaders = this->rawHeaderList();
		for (ByteArrayList::const_iterator iter = rawHeaders.cbegin(); iter != rawHeaders.cend(); ++iter)
		{
			if (*iter == setCookieHeader)
			{
				/* Qt doesn't properly concatenate Set-Cookie entries when returning
				 * rawHeader(). Therefore, we need to copy that header using header()
				 * (see below).
				 */
				copyKnownHeaders.insert(QNetworkRequest::SetCookieHeader);
				continue;
			}
			if (*iter == HttpUtils::locationHeader())
			{
				const QUrl locationHeader = this->locationHeader();
				if (locationHeader.isValid() && locationHeader.scheme().isEmpty() && locationHeader == this->header(QNetworkRequest::LocationHeader))
				{
					/* Due to QTBUG-41061, relative location headers are not set correctly when using
					 * setRawHeader(). Therefore, we need to copy that header using header()
					 * (see below).
					 */
					copyKnownHeaders.insert(QNetworkRequest::LocationHeader);
					continue;
				}
			}
			clone->setRawHeader(*iter, this->rawHeader(*iter));
		}

		for (KnownHeadersSet::const_iterator iter = copyKnownHeaders.cbegin(); iter != copyKnownHeaders.cend(); ++iter)
			clone->setHeader(*iter, this->header(*iter));

		for (AttributeSet::const_iterator iter = m_attributeSet.cbegin(); iter != m_attributeSet.cend(); ++iter)
			clone->setAttribute(*iter, this->attribute(*iter));

		if (this->isOpen())
			clone->open(this->openMode());

		clone->setFinished(this->isFinished());

		return clone;
	}

	/*! Checks if this reply indicates a redirect that can be followed automatically.
	 * \return \c true if this reply's HTTP status code and \c Location header
	 * are valid and the status code indicates a redirect that can be followed automatically.
	 */
	bool isRedirectToBeFollowed() const
	{
		QVariant statusCodeAttr = this->attribute( QNetworkRequest::HttpStatusCodeAttribute );
		if ( ! statusCodeAttr.isValid() )
			return false;

		const QUrl locationHeaderUrl = this->locationHeader();
		if ( ! locationHeaderUrl.isValid() )
			return false;

		switch ( statusCodeAttr.toInt() )
		{
		case HttpStatus::MovedPermanently:  // 301
		case HttpStatus::Found:             // 302
		case HttpStatus::SeeOther:          // 303
		case HttpStatus::UseProxy:          // 305
		case HttpStatus::TemporaryRedirect: // 307
			return true;
		case HttpStatus::PermanentRedirect: // 308
			if ( m_behaviorFlags.testFlag( Behavior_NoAutomatic308Redirect ) )
				return false; // Qt doesn't recognize 308 for automatic redirection
			else
				return true;
		default:
			return false;
		}
	}

	/*! Checks if this reply indicates that the request requires authentication.
	 * \return \c true if the HTTP status code indicates that the request must be resend
	 * with appropriate authentication to succeed.
	 */
	bool requiresAuthentication() const
	{
		switch ( this->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt() )
		{
		case HttpStatus::Unauthorized:                // 401
		case HttpStatus::ProxyAuthenticationRequired: // 407
			return true;
		default:
			return false;
		}
	}

	/*! Returns the URL of the HTTP Location header field of a given QNetworkReply.
	 * This is a workaround for QTBUG-4106 which prevents that the QNetworkReply::header() method returns a valid
	 * QUrl for relative redirection URLs.
	 * \param reply The QNetworkReply for which the Location header should be returned.
	 * \return The value of the Location header field as a QUrl.
	 * \sa https://bugreports.qt.io/browse/QTBUG-41061
	 * \since 0.4.0
	 */
	static QUrl locationHeader( const QNetworkReply* reply )
	{
		const QByteArray rawHeader = reply->rawHeader( HttpUtils::locationHeader() );
		if ( rawHeader.isEmpty() )
			return QUrl();
		else
			return QUrl::fromEncoded( rawHeader, QUrl::StrictMode );
	}

	/*! Returns the URL of the HTTP Location header field.
	 *
	 * \return The value of the Location header field as a QUrl.
	 * \sa locationHeader(const QNetworkReply*)
	 * \since 0.4.0
	 */
	QUrl locationHeader() const
	{
		return locationHeader( this );
	}

protected:
	/*! Creates a MockReply object.
	 * \param parent Parent QObject.
	 */
	MockReply(QObject* parent = Q_NULLPTR)
		: QNetworkReply(parent)
		, m_behaviorFlags(Behavior_Expected)
		, m_redirectCount(0)
	{}

	/*! Reads bytes from the reply's body.
	 * \param[out] data A pointer to an array where the bytes will be written to.
	 * \param maxlen The maximum number of bytes that should be read.
	 * \return The number of bytes read or -1 if an error occurred.
	 * \sa QIODevice::readData()
	 */
	qint64 readData(char *data, qint64 maxlen) { return m_body.read(data, maxlen); }

	/*! Sets the message body of this reply.
	 * \param data The body data.
	 */
	void setBody(const QByteArray& data) { m_body.setData(data); }

	/*! Sets an attribute for this reply.
	 * \param attribute The attribute key.
	 * \param value The value for the attribute.
	 * \sa QNetworkReply::setAttribute()
	 */
	void setAttribute(QNetworkRequest::Attribute attribute, const QVariant& value)
	{
		m_attributeSet.insert(attribute);
		QNetworkReply::setAttribute(attribute, value);
	}

	/*! Sets the error for this reply.
	 *
	 * \param error The error code.
	 * \param errorString A human-readable string describing the error.
	 */
	void setError(QNetworkReply::NetworkError error, const QString& errorString)
	{
		QNetworkReply::setError(error, errorString);
	}

	/*! \overload
	 * This overload uses %Qt's default error strings for the given \p error code.
	 * \param error The error code to be set for this reply.
	 */
	void setError(QNetworkReply::NetworkError error)
	{
		QNetworkReply::setError(error, errorStringForErrorCode(error));
	}


	/*! Returns default error messages for selected error codes.
	 * \note This method contains the error messages only for the error codes that the
	 * Manager sets internally.
	 * \note \parblock Even if this method returns an error message, it might be different
	 * from the error message returned by %Qt in the same error case.
	 * \internal The reason for this is that %Qt sometimes "overrides" error messages
	 * in protocol specific implementations (http:// vs. file:// etc.).
	 * \endinternal \endparblock
	 * \param error The error code whose corresponding default error message will be returned.
	 * \return The default error message from %Qt for the given \p error code
	 * or a null \c QString() if this method does not contain the default error message.
	 */
	static QString errorStringForErrorCode(QNetworkReply::NetworkError error)
	{
		switch (error)
		{
		case QNetworkReply::ProtocolUnknownError:             return QCoreApplication::translate("QHttp", "Unknown protocol specified");
		case QNetworkReply::OperationCanceledError:           return QCoreApplication::translate("QNetworkReplyImpl", "Operation canceled");
		case QNetworkReply::AuthenticationRequiredError:      return QCoreApplication::translate("QHttp", "Host requires authentication");
		case QNetworkReply::ProxyAuthenticationRequiredError: return QCoreApplication::translate("QHttp", "Proxy requires authentication");
#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
		case QNetworkReply::InsecureRedirectError:            return QCoreApplication::translate("QHttp", "Insecure redirect");
		case QNetworkReply::TooManyRedirectsError:            return QCoreApplication::translate("QHttp", "Too many redirects");
#endif // Qt >= 5.6.0
		default:
			return QString();
		}
	}



protected Q_SLOTS:


	/*! Prepares the MockReply for returning to the caller of the Manager.
	 *
	 * This method ensures that this reply has proper values set for the required headers, attributes and properties.
	 * For example, it will set the [QNetworkRequest::ContentLengthHeader] and try to guess the correct value for the
	 * QNetworkRequest::ContentTypeHeader.
	 * However, it will *not* override headers and attributes which have been set explicitly.
	 *
	 * \param request The request this reply is answering.
	 *
	 * [QNetworkRequest::ContentLengthHeader]: http://doc.qt.io/qt-5/qnetworkrequest.html#KnownHeaders-enum
	 */
	void prepare( const Request& request )
	{
		this->setRequest( request.qRequest );
		if ( ! this->url().isValid() )
			this->setUrl( request.qRequest.url() );
		this->setOperation( request.operation );
		this->setSslConfiguration( request.qRequest.sslConfiguration() );

		this->setAttribute( QNetworkRequest::ConnectionEncryptedAttribute, QVariant::fromValue( this->url().scheme().toLower() == HttpUtils::httpsScheme() ) );

		if (   ! this->header( QNetworkRequest::ContentTypeHeader ).isValid()
		    && ! this->body().isEmpty() )
		{
			QMimeType mimeType = guessMimeType( this->url(), m_body.data() );
			this->setHeader( QNetworkRequest::ContentTypeHeader, QVariant::fromValue( mimeType.name() ) );
		}

		if (     this->rawHeader( "Transfer-Encoding" ).isEmpty()
		    && ! this->header( QNetworkRequest::ContentLengthHeader ).isValid()
		    && ! this->body().isNull() )
				this->setHeader( QNetworkRequest::ContentLengthHeader, QVariant::fromValue( this->body().length() ) );

		#if QT_VERSION >= QT_VERSION_CHECK(5, 9, 0)
			if ( ! this->attribute( QNetworkRequest::OriginalContentLengthAttribute ).isValid() )
				this->setAttribute( QNetworkRequest::OriginalContentLengthAttribute, this->header( QNetworkRequest::ContentLengthHeader ) );
		#endif // Qt >= 5.9.0

		QVariant statusCodeAttr = this->attribute( QNetworkRequest::HttpStatusCodeAttribute );
		if ( statusCodeAttr.isValid() )
		{
			bool canConvertToInt = false;
			statusCodeAttr.toInt( &canConvertToInt );
			if ( ! canConvertToInt )
			{
				qWarning( "MockNetworkAccessManager: Invalid type for HttpStatusCodeAttribute: %s", statusCodeAttr.typeName() );
				statusCodeAttr = QVariant();
				this->setAttribute( QNetworkRequest::HttpStatusCodeAttribute, statusCodeAttr );
			}
		}
		if ( ! statusCodeAttr.isValid() )
		{
			int statusCode = HttpStatus::networkErrorToStatusCode( this->error() );
			if ( statusCode > 0 )
			{
				statusCodeAttr = QVariant::fromValue( statusCode );
				this->setAttribute( QNetworkRequest::HttpStatusCodeAttribute, statusCodeAttr );
			}
		}

		if ( ! this->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).isValid() )
		{
			if ( statusCodeAttr.isValid() )
				this->setAttribute( QNetworkRequest::HttpReasonPhraseAttribute, HttpStatus::reasonPhrase( statusCodeAttr.toInt() ).toUtf8() );
		}

		// Qt doesn't set the RedirectionTargetAttribute for 305 redirects. See QNetworkReplyHttpImplPrivate::checkForRedirect(const int statusCode)
		const QUrl locationHeaderUrl = locationHeader();
		if ( this->isRedirectToBeFollowed() && statusCodeAttr.toInt() != static_cast<int>( HttpStatus::UseProxy ) )
			this->setAttribute( QNetworkRequest::RedirectionTargetAttribute, locationHeaderUrl );
	}

	/*! Finishes the MockReply and emits signals accordingly.
	 *
	 * This method will set the reply to finished (see setFinished()), open it for reading and emit the QNetworkReply
	 * signals according to the properties of this reply:
	 * - QNetworkReply::uploadProgress() to indicate that uploading has finished (if applicable)
	 * - QNetworkReply::metaDataChanged() to indicate that the headers of the reply are available
	 * - QIODevice::readyRead() and QNetworkReply::downloadProgress() to indicate that the downloading has finished
	 * (if applicable)
	 * - QNetworkReply::error() to indicate an error (if applicable)
	 * - QIODevice::readChannelFinished() and QNetworkReply::finished() to indicate that the reply has finished and is
	 * ready to be read
	 *
	 * \param request The request this reply is answering.
	 */
	void finish(const Request& request)
	{
		if (!request.body.isEmpty())
			QMetaObject::invokeMethod(this, "uploadProgress", Qt::QueuedConnection, Q_ARG(qint64, request.body.size()), Q_ARG(qint64, request.body.size()));

		QMetaObject::invokeMethod(this, "metaDataChanged", Qt::QueuedConnection);

		m_body.open(QIODevice::ReadOnly);
		QNetworkReply::open(QIODevice::ReadOnly);

		const qint64 bodySize = m_body.size();
		if (bodySize > 0)
		{
			QMetaObject::invokeMethod(this, "readyRead", Qt::QueuedConnection);
			QMetaObject::invokeMethod(this, "downloadProgress", Qt::QueuedConnection, Q_ARG(qint64, bodySize), Q_ARG(qint64, bodySize));
		}

		if (this->error() != QNetworkReply::NoError)
			QMetaObject::invokeMethod(this, "error", Qt::QueuedConnection, Q_ARG(QNetworkReply::NetworkError, this->error()));

		QMetaObject::invokeMethod(this, "downloadProgress", Qt::QueuedConnection, Q_ARG(qint64, bodySize), Q_ARG(qint64, bodySize));
		if (m_behaviorFlags.testFlag(Behavior_FinalUpload00Signal) && !request.body.isEmpty())
			QMetaObject::invokeMethod(this, "uploadProgress", Qt::QueuedConnection, Q_ARG(qint64, 0), Q_ARG(qint64, 0));
		QMetaObject::invokeMethod(this, "readChannelFinished", Qt::QueuedConnection);

		this->setFinished(true);
		QMetaObject::invokeMethod(this, "finished", Qt::QueuedConnection);
	}


	/*! Tunes the behavior of this MockReply.
	 *
	 * \param behaviorFlags Combination of BehaviorFlas to define some details of this MockReply's behavior.
	 * \note Only certain BehaviorFlags have an effect on a MockReply.
	 * \sa BehaviorFlag
	 */
	void setBehaviorFlags(BehaviorFlags behaviorFlags) { m_behaviorFlags = behaviorFlags; }

private:

	QBuffer m_body;
	AttributeSet m_attributeSet;
	BehaviorFlags m_behaviorFlags;
	int m_redirectCount;
	QVector<QUrl> m_followedRedirects;
};


/*! Creates MockReply objects with predefined properties.
 *
 * This class is a configurable factory for MockReply objects.
 * The \c with*() methods configure the properties of the created replies.
 * To create a reply according to the configured properties, call createReply().
 *
 * Similar to the Rule class, the MockReplyBuilder implements a chainable interface for the configuration.
 */
class MockReplyBuilder
{
public:
	/*! Creates an unconfigured MockReplyBuilder.
	*
	* \note Calling createReply() on an unconfigured MockReplyBuilder will return a \c Q_NULLPTR.
	*/
	MockReplyBuilder() : m_replyPrototype(Q_NULLPTR) {}

	/*! Creates a MockReplyBuilder by copying another one.
	* \param other The MockReplyBuilder which is being copied.
	*/
	MockReplyBuilder(const MockReplyBuilder& other)
	{
		if (other.m_replyPrototype)
			m_replyPrototype.reset(other.m_replyPrototype->clone());
	}

#if __cplusplus >= 201103L || ( defined(_MSC_VER) && _MSC_VER >= 1900 )
	/*! Creates a MockReplyBuilder by moving another one.
	 * \note This constructor is only available if the compiler supports C++11.
	 * \param other The MockReplyBuilder which is being moved.
	 */
	MockReplyBuilder(MockReplyBuilder&& other) noexcept : MockReplyBuilder()
	{ swap(other); }
#endif // C++11

	/*! Swaps this MockReplyBuilder with another one.
	 * \param other The MockReplyBuilder to be exchanged with this one.
	 */
	void swap(MockReplyBuilder& other)
	{ m_replyPrototype.swap(other.m_replyPrototype); }

	/*! Swaps two MockReplyBuilders.
	 * \param left One MockReplyBuilder to be be exchanged.
	 * \param right The other MockReplyBuilder to be exchanged.
	 */
	friend void swap(MockReplyBuilder& left, MockReplyBuilder& right)
	{ left.swap(right); }

	/*! Configures this MockReplyBuilder identical to another one.
	 * \param other The MockReplyBuilder whose configuration is being copied.
	 * \return \c this
	 */
	MockReplyBuilder& operator=(const MockReplyBuilder& other)
	{
		if (other.m_replyPrototype)
			m_replyPrototype.reset(other.m_replyPrototype->clone());
		else
			m_replyPrototype.reset();
		return *this;
	}

#if __cplusplus >= 201103L || ( defined(_MSC_VER) && _MSC_VER >= 1900 )
	/*! Configures this MockReplyBuilder identical to another one by moving the other one.
	 * \note This operator is only available if the compiler supports C++11.
	 * \param other The MockReplyBuilder which is being moved.
	 * \return \c this
	 */
	MockReplyBuilder& operator=(MockReplyBuilder&& other) noexcept
	{
		swap(other);
		return *this;
	}
#endif // C++11

	/*! Compares two MockReplyBuilders for equality.
	 * \param left One MockReplyBuilder to be compared.
	 * \param right The other MockReplyBuilder to be compared.
	 * \return \c true if \p left and \p right have the same properties configured
	 * and thus create equal MockReply objects.
	 */
	friend bool operator==(const MockReplyBuilder& left, const MockReplyBuilder& right)
	{
		MockReply* leftReply = left.m_replyPrototype.data();
		MockReply* rightReply = right.m_replyPrototype.data();

		if (leftReply == rightReply)
			return true;

		if (!leftReply || !rightReply)
			return false;

		if (   leftReply->body()           != rightReply->body()
		    || leftReply->rawHeaderPairs() != rightReply->rawHeaderPairs()
		    || leftReply->attributes()     != rightReply->attributes()
		    || leftReply->error()          != rightReply->error()
		    || leftReply->errorString()    != rightReply->errorString())
			return false;

		QSet<QNetworkRequest::Attribute>::const_iterator iter;
		const QSet<QNetworkRequest::Attribute> attributes = leftReply->attributes().unite(rightReply->attributes());
		for (iter = attributes.cbegin(); iter != attributes.cend(); ++iter)
		{
			if (leftReply->attribute(*iter) != rightReply->attribute(*iter))
				return false;
		}

		return true;
	}

	/*! Compares two MockReplyBuilders for inequality.
	 * \param left One MockReplyBuilder to be compared.
	 * \param right The other MockReplyBuilder to be compared.
	 * \return \c true if \p left and \p right have different properties configured
	 * and thus create different MockReply objects.
	 */
	friend bool operator!=(const MockReplyBuilder& left, const MockReplyBuilder& right)
	{
		return !(left == right);
	}

	/*! Configures this MockReplyBuilder identical to another one.
	 * This method is identical to the copy operator and exists just to provide a consistent, chainable interface.
	 * \param other The MockReplyBuilder which is being copied.
	 * \return \c this
	 */
	MockReplyBuilder* with(const MockReplyBuilder* other)
	{ *this = *other; return this; }

	/*! \overload
	 * \param other The MockReplyBuilder which is being copied.
	 * \return \c this
	 */
	MockReplyBuilder* with(const MockReplyBuilder& other)
	{ *this = other; return this; }

#if __cplusplus >= 201103L || ( defined(_MSC_VER) && _MSC_VER >= 1900 )
	/*! Configures this MockReplyBuilder identical to another one by moving the other one.
	 *
	 * This method is identical to the move operator and exists just to provide a consistent, chainable interface.
	 *
	 * \note This method is only available if the compiler supports C++11.
	 *
	 * \param other The MockReplyBuilder which is being moved.
	 * \return \c this
	 */
	MockReplyBuilder* with(MockReplyBuilder&& other)
	{ swap(other); return this; }
#endif // C++11

	/*! Sets the body for the replies.
	 * \param data The data used as the message body for the replies.
	 * \return \c this
	 */
	MockReplyBuilder* withBody(const QByteArray& data)
	{
		ensureReplyPrototype()->setBody(data);
		return this;
	}
	/*! Sets the body for the replies to a JSON document.
	 * \param json The data used as the message body for the replies.
	 * \return \c this
	 */
	MockReplyBuilder* withBody(const QJsonDocument& json)
	{
		MockReply* proto = ensureReplyPrototype();
		proto->setBody(json.toJson(QJsonDocument::Compact));
		proto->setHeader(QNetworkRequest::ContentTypeHeader, QVariant::fromValue(QStringLiteral("application/json")));
		return this;
	}
	/*! Sets the body for the replies to the content of a file.
	 *
	 * The file needs to exist at the time this method is called because the file's
	 * content is read and stored in this MockReplyBuilder by this method.
	 *
	 * This method also tries to determine the file's MIME type using
	 * QMimeDatabase::mimeTypeForFileNameAndData() and sets
	 * the [QNetworkRequest::ContentTypeHeader] accordingly.
	 * If this does not determine the MIME type correctly or if you want to set the
	 * MIME type explicitly, use withHeader() or withRawHeader() *after* calling this method.
	 *
	 * \param filePath The path to the file whose content is used as the message body for the replies.
	 * \return \c this
	 * \sa [QNetworkRequest::ContentTypeHeader]
	 * \sa withHeader()
	 * [QNetworkRequest::ContentTypeHeader]: http://doc.qt.io/qt-5/qnetworkrequest.html#KnownHeaders-enum
	 */
	MockReplyBuilder* withFile(const QString& filePath)
	{
		MockReply* proto = ensureReplyPrototype();

		QFile file(filePath);
		if (file.open(QIODevice::ReadOnly))
		{
			QByteArray data = file.readAll();
			file.close();
			proto->setBody(data);
			QMimeType mimeType = QMimeDatabase().mimeTypeForFileNameAndData(filePath, data);
			proto->setHeader(QNetworkRequest::ContentTypeHeader, QVariant::fromValue(mimeType.name()));
		}
		return this;
	}

	/*! Sets the status code and reason phrase for the replies.
	 *
	 * \note \parblock
	 * If the \p statusCode is an error code, this will also set the corresponding QNetworkReply::NetworkError.\n
	 * If you do not want this, call withError() afterwards to override.
	 * \endparblock
	 *
	 * \param statusCode The HTTP status code.
	 * \param reasonPhrase The HTTP reason phrase. If it is a null QString(), the default reason phrase for the \p statusCode will be used, if available.
	 * \return \c this
	 */
	MockReplyBuilder* withStatus(int statusCode = static_cast<int>(HttpStatus::OK), const QString& reasonPhrase = QString())
	{
		MockReply* proto = ensureReplyPrototype();
		proto->setAttribute(QNetworkRequest::HttpStatusCodeAttribute, QVariant::fromValue(statusCode));
		QString phrase = reasonPhrase;
		if (phrase.isNull())
			phrase = HttpStatus::reasonPhrase(statusCode);
		proto->setAttribute(QNetworkRequest::HttpReasonPhraseAttribute, QVariant::fromValue(phrase));
		if (HttpStatus::isError(statusCode))
			proto->setError(HttpStatus::statusCodeToNetworkError(statusCode));
		return this;
	}

	/*! Sets a header for the replies.
	 *
	 * Calling this method with the same header again will override the previous value.
	 *
	 * \param header The header.
	 * \param value The value for the header.
	 * \return \c this
	 *
	 * \sa QNetworkReply::setHeader()
	 */
	MockReplyBuilder* withHeader(QNetworkRequest::KnownHeaders header, const QVariant& value)
	{
		ensureReplyPrototype()->setHeader(header, value);
		return this;
	}

	/*! Sets a raw header for the replies.
	 *
	 * Calling this method with the same header again will override the previous value.
	 * To add multiple header values for the same header, concatenate the values
	 * separated by comma. A notable exception from this rule is the \c Set-Cookie
	 * header which should be separated by newlines (`\\n`).
	 *
	 * \param header The header.
	 * \param value The value for the header.
	 * \return \c this
	 *
	 * \sa QNetworkReply::setRawHeader()
	 */
	MockReplyBuilder* withRawHeader(const QByteArray& header, const QByteArray& value)
	{
		ensureReplyPrototype()->setRawHeader(header, value);
		return this;
	}

	/*! Sets an attribute for the replies.
	 *
	 * Calling this method with the same attribute again will override the previous value.
	 *
	 * \param attribute The attribute.
	 * \param value The value for the attribute.
	 * \return \c this
	 */
	MockReplyBuilder* withAttribute(QNetworkRequest::Attribute attribute, const QVariant& value)
	{
		ensureReplyPrototype()->setAttribute(attribute, value);
		return this;
	}

	/*! Sets the error for the replies.
	 *
	 * \param error The error code.
	 * \param errorMessage An optional error message.
	 * \return \c this
	 */
	MockReplyBuilder* withError(QNetworkReply::NetworkError error, const QString& errorMessage = QString())
	{
		ensureReplyPrototype()->setError(error, errorMessage);
		return this;
	}

	/*! Convenience method to configure redirection for the replies.
	 *
	 * This sets the [QNetworkRequest::LocationHeader] and the HTTP status code.
	 * \note Due to QTBUG-41061, the [QNetworkRequest::LocationHeader] returned by QNetworkReply::header() will be an
	 * empty (invalid) URL when \p targetUrl is relative. The redirection will still work as expected.
	 * QNetworkReply::rawHeader() always returns the correct value for the Location header.
	 *
	 * \param targetUrl The URL of the redirection target. Can be relative or absolute.
	 * If it is relative, it will be made absolute using the URL of the requests that matched the Rule as base.
	 * \param statusCode The HTTP status code to be used. Should normally be in the 3xx range.
	 * \return \c this
	 * \sa https://bugreports.qt.io/browse/QTBUG-41061
	 * [QNetworkRequest::LocationHeader]: http://doc.qt.io/qt-5/qnetworkrequest.html#KnownHeaders-enum
	 */
	MockReplyBuilder* withRedirect( const QUrl& targetUrl, HttpStatus::Code statusCode = HttpStatus::Found )
	{
		ensureReplyPrototype()->setRawHeader( HttpUtils::locationHeader(), targetUrl.toEncoded() );
		withStatus( static_cast<int>( statusCode ) );
		return this;
	}

	/*! Adds an HTTP authentication challenge to the replies and sets their HTTP status code to 401 (Unauthorized).
	 *
	 * \param authChallenge The authentication challenge to be added to the replies. Must be a valid Challenge or
	 * this method does not add the authentication challenge.
	 * \return \c this
	 *
	 * \sa HttpUtils::Authentication::Challenge::isValid()
	 * \sa QNetworkReply::setRawHeader()
	 */
	MockReplyBuilder* withAuthenticate(const HttpUtils::Authentication::Challenge::Ptr& authChallenge)
	{
		MockReply* proto = ensureReplyPrototype();
		if (authChallenge && authChallenge->isValid())
		{
			proto->setRawHeader(HttpUtils::wwwAuthenticateHeader(), authChallenge->authenticateHeader());
			withStatus(static_cast<int>(HttpStatus::Unauthorized));
		}
		return this;
	}

	/*! Adds an HTTP Basic authentication challenge to the replies and sets their HTTP status code to 401 (Unauthorized).
	 *
	 * \param realm The realm to be used for the authentication challenge.
	 * \return \c this
	 *
	 * \sa withAuthenticate(HttpUtils::AuthenticationChallenge::Ptr)
	 */
	MockReplyBuilder* withAuthenticate(const QString& realm)
	{
		HttpUtils::Authentication::Challenge::Ptr authChallenge(new HttpUtils::Authentication::Basic(realm));
		return withAuthenticate(authChallenge);
	}

	/*! Adds a cookie to the replies.
	 *
	 * \note \parblock
	 * - The cookie will be appended to the current list of cookies.
	 * To replace the complete list of cookies, use withHeader() and set the
	 * [QNetworkRequest::SetCookieHeader] to a QList<QNetworkCookie>.
	 * - This method does *not* check if a cookie with the same name already
	 * exists in the [QNetworkRequest::SetCookieHeader].
	 * RFC 6265 says that replies SHOULD NOT contain multiple cookies with the
	 * same name. However, to allow simulating misbehaving servers, this method
	 * still allows this.
	 * \endparblock
	 *
	 * \param cookie The cookie to be added to the replies.
	 * \return \c this
	 *
	 * \sa [QNetworkRequest::SetCookieHeader]
	 * [QNetworkRequest::SetCookieHeader]: http://doc.qt.io/qt-5/qnetworkrequest.html#KnownHeaders-enum
	 */
	MockReplyBuilder* withCookie(const QNetworkCookie& cookie)
	{
		MockReply* proto = ensureReplyPrototype();
		QList<QNetworkCookie> cookies = proto->header(QNetworkRequest::SetCookieHeader).value<QList<QNetworkCookie> >();
		cookies.append(cookie);
		proto->setHeader(QNetworkRequest::SetCookieHeader, QVariant::fromValue(cookies));
		return this;
	}


	/*! Creates a reply using the configured properties.
	 * \return A new MockReply with properties as configured in this factory or a Q_NULLPTR if no properties have been configured.
	 * The caller is responsible for deleting the object when it is not needed anymore.
	 */
	MockReply* createReply() const
	{
		if (m_replyPrototype)
			return m_replyPrototype->clone();
		else
			return Q_NULLPTR;
	}

protected:
	/*! Creates a MockReply as prototype if necessary and returns it.
	 * \return A MockReply which acts as a prototype for the replies created by createReply().
	 * Modify the properties of the returned reply to change the configuration of this factory.
	 * The ownership of the returned reply stays with the MockReplyBuilder so do not delete it.
	 */
	MockReply* ensureReplyPrototype()
	{
		if (!m_replyPrototype)
			m_replyPrototype.reset(new MockReply());
		return m_replyPrototype.data();
	}

private:
	QScopedPointer<MockReply> m_replyPrototype;
};


/*! Configuration object for the Manager.
 *
 * The Rule combines predicates for matching requests with a MockReplyBuilder which generates MockReplies when the predicates match.
 *
 * ### Usage ###
 * The Rule implements a chainable interface. This means that the methods return a pointer to the Rule
 * itself to allow calling its methods one after the other in one statement.
 * Additionally, the Manager provides convenience methods to create Rule objects.
 * So the typical way to work with Rules is:
\code
using namespace MockNetworkAccess;
using namespace MockNetworkAccess::Predicates;
Manager<QNetworkAccessManager> mockNAM;

mockNAM.whenGet(QUrl("http://example.com"))
       ->has(HeaderMatching(QNetworkRequest::UserAgentHeader, QRegularExpression(".*MyWebBrowser.*")))
       ->reply()->withBody(QJsonDocument::fromJson("{\"response\": \"hello\"}"));
\endcode
 *
 * \note Rule objects cannot be copied but they can be cloned. See clone()
 *
 * ### Matching ###
 * To add predicates to a Rule, use the has() and hasNot() methods.
 * For a Rule to match a request, all its predicates must match. So the predicates have "and" semantics.
 * To achieve "or" semantics, configure multiple Rule in the Manager or implement a dynamic predicate (see \ref page_dynamicMockNam_dynamicPredicates).
 * Since the first matching Rule in the Manager will be used to create a reply, this provides "or" semantics.
 * In addition to negating single Predicates (see hasNot() or Predicate::negate()), the matching of the whole Rule object can be negated by calling negate().
 * \note
 * \parblock
 * The order of the Predicates in a Rule has an impact on the performance of the matching.
 * So, fast Predicates should be added before complex Predicates (for example, Predicates::Header before Predicates::BodyMatching).
 * \endparblock
 *
 * ### Creating Replies ###
 * When a Rule matches a request, the Manager will request it to create a reply for the request.
 * The actual creation of the reply will be done by the Rule's MockReplyBuilder which can be accessed through the reply() method.
 *
 * ### Extending Rule ###
 * Both the matching of requests and the generation of replies can be extended and customized.
 * To extend the matching, implement new Predicate classes.
 * To extend or customize the generation of replies, override the createReply() method. You can then use a MockReplyBuilder to create a reply based on the request.
 * These extension possibilities allow implementing dynamic matching and dynamic replies. That is, depending on the concrete values of the request, the matching behaves differently
 * or the reply has different properties. This also allows introducing state and effectively evolves the Rule into a simple fake server.\n
 * See \ref page_dynamicMockNam for further details.
 */
class Rule
{
	template<class Base>
	friend class Manager;

public:

	/*! Smart pointer to a Rule object. */
	typedef QSharedPointer<Rule> Ptr;

	/*! Abstract base class for request matching.
	 * A Predicate defines a condition which a request must match.
	 * If all Predicates of a Rule match the request, the Rule is
	 * considered to match the request.
	 *
	 * To create custom Predicates, derive from this class and implement the private match() method.
	 */
	class Predicate
	{
	public:
		/*! Smart pointer to a Predicate. */
		typedef QSharedPointer<Predicate> Ptr;
		/*! Default constructor
		 */
		Predicate() : m_negate(false) {}
		/*! Default destructor
		 */
		virtual ~Predicate() {}
		/*! Matches a request against this Predicate.
		 * \param request The request to test against this predicate.
		 * \return \c true if the Predicate matches the \p request.
		 */
		bool matches(const Request& request)
		{
			return match(request) != m_negate;
		}

		/*! Negates the matching of this Predicate.
		 * \param negate If \c true, the result of matches() is negated before returned.
		 */
		void negate(bool negate = true)
		{ m_negate = negate; }


	private:
		/*! Performs the actual matching.
		 * \param request The request.
		 * \return Must return \c true if the Predicate matches the \p request. Otherwise, \c false.
		 */
		virtual bool match(const Request& request) = 0;

		bool m_negate;
	};

	/*! This enum defines the behaviors of a Rule regarding passing matching requests through to the next network access
	 * manager.
	 */
	enum PassThroughBehavior
	{
		DontPassThrough,                /*!< The rule consumes matching requests and the Manager returns a MockReply
		                                 * generated by the MockReplyBuilder of the rule (see reply()).
		                                 * The request is **not** passed through.\n
		                                 * This is the default behavior.
		                                 */
		PassThroughReturnMockReply,     /*!< The rule passes matching requests through to the next network access manager
		                                 * but the Manager still returns a MockReply generated by the MockReplyBuilder
		                                 * of the rule (see reply()).
		                                 * The reply returned by the next network access manager is discarded.
		                                 * \note If the rule has no reply() configured, matching request will not
		                                 * be passed through since the Rule is considered "invalid" by the Manager.
		                                 */
		PassThroughReturnDelegatedReply /*!< The rule passes matching requests through to the next network access manager
		                                 * and the Manager returns the reply returned by the next network access manager.
		                                 */
	};

	/*! Creates a Rule which matches every request but creates no replies.
	 *
	 * In regard to the Manager, such a Rule is invalid and is ignored by the Manager.
	 * To make it valid, configure the MockReplyBuilder returned by reply().
	 * \sa Manager
	 */
	Rule()
	: m_negate(false)
	, m_passThroughBehavior(DontPassThrough)
	{
	}

#if __cplusplus >= 201103L || ( defined(_MSC_VER) && _MSC_VER >= 1900 )
	/*! Default destructor.
	 */
	virtual ~Rule() = default;

	/*! Deleted copy constructor.
	 */
	Rule(const Rule&) = delete;

	/*! Default move operator.
	 */
	Rule(Rule&&) = default;

	/*! Deleted assignment operator.
	 */
	Rule& operator=(const Rule&) = delete;
#else // ! C++11
	/*! Default destructor
	 */
	virtual ~Rule() {}

private:
	Rule(const Rule&);
	Rule& operator=(const Rule&);

#endif // ! C++11

public:
	/*! Negates the matching of this rule.
	 * \param negate If \c true, the result of the matching is negated, meaning if _any_ of the predicates does _not_ match, this Rule matches.
	 * If \c false, the negation is removed reverting to normal "and" semantics.
	 * \return \c this
	 * \sa matches()
	 */
	Rule* negate(bool negate = true)
	{ m_negate = negate; return this; }

	/*! \return \c true if this rule negates the matching. \c false otherwise.
	 *
	 * \sa negate()
	 */
	bool isNegated() const
	{
		return m_negate;
	}

	/*! Adds a Predicate to the Rule.
	 * \tparam PredicateType The type of the \p predicate. \p PredicateType must
	 * be copy-constructable for this method to work.
	 * \param predicate The Predicate to be added to the Rule.
	 * Note that \p predicate will be copied and the copy is added.
	 * \return \c this.
	 */
	template<class PredicateType>
	Rule* has(const PredicateType& predicate)
	{
		m_predicates.append(Predicate::Ptr(new PredicateType(predicate)));
		return this;
	}

	/*! Adds a Predicate to the Rule.
	 * \param predicate Smart pointer to the Predicate to be added to the Rule.
	 * \return \c this.
	 */
	Rule* has(const Predicate::Ptr& predicate)
	{
		m_predicates.append(predicate);
		return this;
	}

	/*! Negates a Predicate and adds it to the Rule.
	 * \tparam PredicateType The type of the \p predicate. \p PredicateType must
	 * be copy-constructable for this method to work.
	 * \param predicate The Predicate to be negated and added to the Rule.
	 * Note that \p predicate will be copied and the copy is negated and added.
	 * \return \c this.
	 */
	template<class PredicateType>
	Rule* hasNot(const PredicateType& predicate)
	{
		Predicate::Ptr copy(new PredicateType(predicate));
		copy->negate();
		m_predicates.append(copy);
		return this;
	}

	/*! Negates a Predicate and adds it to the Rule.
	 * \param predicate Smart pointer to the Predicate to be negated and added to the Rule.
	 * \return \c this.
	 * \sa Predicate::negate()
	 */
	Rule* hasNot(const Predicate::Ptr& predicate)
	{
		predicate->negate();
		m_predicates.append(predicate);
		return this;
	}

	/*! Creates a Generic Predicate and adds it to the Rule.
	 * \tparam Matcher The type of the callable object.
	 * \param matcher The callable object used to create the Generic predicate.
	 * \return \c this
	 * \sa Generic
	 * \sa Predicates::createGeneric()
	 */
	template<class Matcher>
	Rule* isMatching(const Matcher& matcher);

	/*! Creates a Generic Predicate, negates it and adds it to the Rule.
	 * \tparam Matcher The type of the callable object.
	 * \param matcher The callable object used to create the Generic predicate.
	 * \return \c this
	 * \sa Generic
	 * \sa Predicates::createGeneric()
	 */
	template<class Matcher>
	Rule* isNotMatching(const Matcher& matcher);

	/*! \return The Predicates of this Rule.
	 */
	QVector<Predicate::Ptr> predicates() const { return m_predicates; }
	/*! Sets the Predicates of this Rule.
	 * This removes all previous Predicates of this Rule.
	 * \param predicates The new Predicates for this Rule.
	 */
	void setPredicates(const QVector<Predicate::Ptr>& predicates) { m_predicates = predicates; }

	/*! \return The MockReplyBuilder used to create replies in case this Rule matches. Use the returned factory to
	 * configure the replies.
	 * The ownership of the MockReplyBuilder stays with the Rule.
	 */
	MockReplyBuilder* reply()
	{ return &m_replyBuilder; }

	/*! Defines whether matching requests should be passed through to the next network access manager.
	 * \param behavior How the Rule should behave in regard to passing requests through.
	 * \param passThroughManager The network access manager to which requests are passed through.
	 * If this is null, the pass through manager of this Rule's manager is used to pass requests through (see Manager::setPassThroughNam()).
	 * \n **Since** 0.4.0
	 * \return \c this
	 * \sa PassThroughBehavior
	 * \sa \ref page_passThrough
	 */
	Rule* passThrough( PassThroughBehavior behavior = PassThroughReturnDelegatedReply, QNetworkAccessManager* passThroughManager = Q_NULLPTR )
	{
		m_passThroughBehavior = behavior;
		m_passThroughManager = passThroughManager;
		return this;
	}

	/*! \return Whether this rule passes matching requests through to the next network access manager and what
	 * is returned by the Manager if the request is passed through.
	 *
	 * \sa PassThroughBehavior
	 */
	PassThroughBehavior passThroughBehavior() const
	{
		return m_passThroughBehavior;
	}

	/*! \return The network access manager to which matching requests are passed through.
	 * \sa passThrough()
	 * \sa PassThroughBehavior
	 * \since 0.4.0
	 */
	QNetworkAccessManager* passThroughManager() const
	{
		return m_passThroughManager;
	}

	/*! Matches a request against the predicates of this Rule.
	 * \param request The request to be tested against the predicates.
	 * \return \c true if the \p request matches all predicates.
	 */
	bool matches(const Request& request)
	{
		bool returnValue = true;

		QVector<Predicate::Ptr>::const_iterator iter;
		for (iter = m_predicates.cbegin(); iter != m_predicates.cend(); ++iter)
		{
			if (!(*iter)->matches(request))
			{
				returnValue = false;
				break;
			}
		}

		return returnValue != m_negate;
	}

	/*! Creates a MockReply using the MockReplyBuilder of this Rule.
	 *
	 * The base implementation simply calls MockReplyBuilder::createReply().
	 *
	 * \note When you reimplement this method, you can also return a null pointer. In that case, it is treated as if the
	 * Rule didn't match the request. This is useful if you create the replies dynamically and get into a
	 * situation where you cannot generate an appropriate reply.
	 *
	 * \param request The request to be answered.
	 * \return A new MockReply object created by the MockReplyBuilder (see reply()).
	 * The caller takes ownership of the returned MockReply and should delete it
	 * when it is not needed anymore.
	 */
	virtual MockReply* createReply(const Request& request)
	{
		Q_UNUSED(request)
		return m_replyBuilder.createReply();
	}

	/*! Creates a clone of this Rule.
	 *
	 * \return A Rule object with the same properties as this Rule except for the matchedRequests().
	 * Note that the predicates() are shallow copied meaning that this Rule and the clone will have pointers to
	 * the same Predicate objects. All other properties except for the matchedRequests() are copied.
	 * The caller is taking ownership of the returned Rule object and should delete it when it is not needed
	 * anymore.
	 */
	virtual Rule* clone() const
	{
		Rule* cloned = new Rule();
		cloned->m_predicates = m_predicates;
		cloned->m_replyBuilder = m_replyBuilder;
		cloned->m_negate = m_negate;
		cloned->m_passThroughBehavior = m_passThroughBehavior;
		cloned->m_passThroughManager = m_passThroughManager;
		return cloned;
	}

	/*! \return The requests that matched this Rule.
	 */
	QVector<Request> matchedRequests() const
	{
		return m_matchedRequests;
	}

protected:
	/*! The predicates used by this Rule to match requests. */
	QVector<Predicate::Ptr> m_predicates;
	/*! The MockReplyBuilder returned by reply() and used by the createReply() base implementation to generate the reply. */
	MockReplyBuilder m_replyBuilder;
	/*! Whether the matching is negated.
	 * \sa negate()
	 */
	bool m_negate;
	/*! Defines the PassThroughBehavior of this Rule.
	 * \sa passThrough()
	 */
	PassThroughBehavior m_passThroughBehavior;

private:
	QVector<Request> m_matchedRequests;
	QPointer<QNetworkAccessManager> m_passThroughManager;
};


/*! Namespace for the matching predicates provided by the MockNetworkAccessManager library.
 * \sa Rule::Predicate
 */
namespace Predicates
{
	/*! Matches any request.
	 * This is useful to handle unexpected requests.
	 */
	class Anything : public Rule::Predicate
	{
	public:
		/*! Creates a predicate which matches any request.
		 */
		Anything() : Predicate() {}

	private:
		virtual bool match(const Request&)
		{ return true; }
	};

	/*! Matches if a given callable object matches the request.
	 * \tparam Matcher A callable type which is used to match the request.
	 * The \p Matcher must accept a `const Request&` as parameter and return a `bool`.
	 * When the predicate is tested against a request, the \p Matcher is invoked
	 * and its return value defines whether this predicate matches.
	 */
	template<class Matcher>
	class Generic : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching using a callable object.
		 * \param matcher The callable object which is invoked to match the request.
		 */
		Generic(const Matcher& matcher) : Predicate(), m_matcher(matcher) {}

	private:
		virtual bool match(const Request& request)
		{
			return m_matcher(request);
		}

		Matcher m_matcher;
	};

	/*! Creates a Generic predicate.
	 * This factory method mainly exists to take advantage of template argument deduction when creating a Generic
	 * predicate.
	 * \tparam Matcher The type of the callable object.
	 * \param matcher The callable object.
	 * \return A smart pointer to a Generic predicate created with \p matcher.
	 * \sa Generic
	 */
	template<class Matcher>
	static Rule::Predicate::Ptr createGeneric(const Matcher& matcher)
	{
		return Rule::Predicate::Ptr(new Generic<Matcher>(matcher));
	}

	/*! Matches if the HTTP verb equals a given verb.
	 */
	class Verb : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching the HTTP verb.
		 * \param operation The verb to match.
		 * \param customVerb If \p operation is QNetworkAccessManager::CustomOperation, \p customVerb defines the custom verb to match.
		 * In other cases, this parameter is ignored.
		 */
		explicit Verb(QNetworkAccessManager::Operation operation, const QByteArray& customVerb = QByteArray()) : Predicate(), m_operation(operation)
		{
			if (m_operation == QNetworkAccessManager::CustomOperation)
				m_customVerb = customVerb;
		}

	private:
		virtual bool match(const Request& request)
		{
			if (request.operation != m_operation)
				return false;
			if (request.operation == QNetworkAccessManager::CustomOperation
			    && request.qRequest.attribute(QNetworkRequest::CustomVerbAttribute).toByteArray() != m_customVerb)
				return false;
			return true;
		}

		QNetworkAccessManager::Operation m_operation;
		QByteArray m_customVerb;
	};

	/*! Matches if the request URL matches a regular expression.
	 * \note To match query parameters, it is typically easier to use the predicate QueryParameters.
	 */
	class UrlMatching : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching the request URL against a regular expression.
		 * \param urlRegEx The regular expression.
		 * \param format QUrl::FormattingOptions to be used to convert the QUrl to a QString when matching the regular expression.
		 * The default is QUrl::PrettyDecoded since it is also the default for QUrl::toString().
		 * Note that QUrl::FullyDecoded does *not* work since QUrl::toString() does not permit it.
		 */
		explicit UrlMatching(const QRegularExpression& urlRegEx, QUrl::FormattingOptions format = QUrl::FormattingOptions(QUrl::PrettyDecoded))
			: Predicate()
			, m_urlRegEx(urlRegEx)
			, m_format(format)
		{}

	private:
		virtual bool match(const Request& request)
		{
			const QString url = request.qRequest.url().toString(m_format);
			return m_urlRegEx.match(url).hasMatch();
		}

		QRegularExpression m_urlRegEx;
		QUrl::FormattingOptions m_format;
	};

	/*! Matches if the request URL equals a given URL.
	 * \note This predicate does an exact matching of the URL so it is stricter than the other URL predicates.
	 */
	class Url : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching if the request URL equals a given URL.
		 * \note Invalid QUrls are treated like empty QUrls for the comparison.
		 * In other words, the following QUrl objects are all considered equal: `QUrl()`, `QUrl("")`, `QUrl("http://..")`,
		 * `QUrl("http://!!")`
		 * \param url The URL which is compared against the request URL.
		 * \param defaultPort Allows defining a default port to be considered when the request or \p url does not specify
		 * a port explicitly.
		 * The default ports for HTTP (80) and HTTPS (443) are used when no \p defaultPort was specified (that is, when
		 * \p defaultPort is -1).
		 */
		explicit Url(const QUrl& url, int defaultPort = -1) : Predicate(), m_url(url), m_defaultPort(defaultPort)
		{
			detectDefaultPort();
		}

		/*! \overload
		 *
		 * \param url The URL compared against the request URL. If it is empty, it always matches.
		 * \param defaultPort Allows defining a default port to be considered when the request or \p url does not specify
		 * a port explicitly.
		 * The default ports for HTTP (80) and HTTPS (443) are used when no \p defaultPort was specified.
		 */
		explicit Url(const QString& url, int defaultPort = -1) : Predicate(), m_url(url), m_defaultPort(defaultPort)
		{
			detectDefaultPort();
		}

	private:
		void detectDefaultPort()
		{
			if ( m_defaultPort == -1 )
			{
				const QString urlProtocol = m_url.scheme().toLower();
				if ( urlProtocol == HttpUtils::httpScheme() )
					m_defaultPort = 80;
				else if ( urlProtocol == HttpUtils::httpsScheme() )
					m_defaultPort = 443;
			}
		}

		virtual bool match(const Request& request)
		{
			const QUrl requestUrl = request.qRequest.url();
			return (requestUrl == m_url)
			    || (m_defaultPort > -1
			        /* QUrl::matches() could be used here instead of QUrl::adjusted() but it is buggy:
			         * https://bugreports.qt.io/browse/QTBUG-70774
			        && m_url.matches(requestUrl, QUrl::RemovePort)
			         */
			        && m_url.adjusted(QUrl::RemovePort) == requestUrl.adjusted(QUrl::RemovePort)
			        && m_url.port(m_defaultPort) == requestUrl.port(m_defaultPort));
		}

		QUrl m_url;
		int m_defaultPort;
	};

	/*! Matches if the request URL contains a given query parameter.
	 * Note that the URL can contain more query parameters. This predicate just checks that the given parameter exists with the given value.
	 *
	 * This predicate is especially useful in combination with the regular expression predicate UrlMatching()
	 * since query parameters typically don't have a defined order which makes it very hard to match them with regular expressions.
	 */
	class QueryParameter : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching a URL query parameter.
		 * \param key The name of the query parameter.
		 * \param value The value that the query parameter needs to have.
		 * \param format QUrl::ComponentFormattingOptions used to convert the query parameter value to a QString.
		 * The default is QUrl::PrettyDecoded since it is also the default for QUrlQuery::queryItemValue().
		 */
		explicit QueryParameter(const QString& key, const QString& value, QUrl::ComponentFormattingOptions format = QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded))
			: Predicate()
			, m_key(key)
			, m_values(value)
			, m_format(format)
		{
		}

		/*! Creates a predicate matching a URL query parameter with a list of values.
		 * \param key The name of the query parameter.
		 * \param values The values that the query parameter needs to have in the order they appear in the query.
		 * \param format QUrl::ComponentFormattingOptions used to convert the query parameter value to a QString.
		 * The default is QUrl::PrettyDecoded since it is also the default for QUrlQuery::queryItemValue().
		 * \since 0.4.0
		 */
		explicit QueryParameter(const QString& key, const QStringList& values, QUrl::ComponentFormattingOptions format = QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded))
			: Predicate()
			, m_key(key)
			, m_values(values)
			, m_format(format)
		{
		}

	private:
		virtual bool match(const Request& request)
		{
			const QUrlQuery query(request.qRequest.url());
			return query.hasQueryItem(m_key) && query.allQueryItemValues(m_key, m_format) == m_values;
		}

		QString m_key;
		QStringList m_values;
		QUrl::ComponentFormattingOptions m_format;
	};

	/*! Matches if the request URL contains a given query parameter with a value matching a given regular expression.
	 * If the query parameter contains multiple values, **all** of its values must match the given regular expression.
	 *
	 * Note that the URL can contain more query parameters. This predicate just checks that the given parameter exists with a matching value.
	 *
	 * This predicate is especially useful in combination with the regular expression predicate UrlMatching()
	 * since query parameters typically don't have a defined order which makes it very hard to match them with regular expressions.
	 */
	class QueryParameterMatching : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching an URL query parameter value .
		 * \param key The name of the query parameter.
		 * \param regEx The regular expression matched against the query parameter value.
		 * \param format QUrl::ComponentFormattingOptions to be used to convert the query parameter value to a QString when matching the regular expression.
		 * The default is QUrl::PrettyDecoded since it is also the default for QUrlQuery::queryItemValue().
		 */
		explicit QueryParameterMatching(const QString& key, const QRegularExpression& regEx, QUrl::ComponentFormattingOptions format = QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded))
			: Predicate()
			, m_key(key)
			, m_regEx(regEx)
			, m_format(format)
		{
		}

	private:
		virtual bool match(const Request& request)
		{
			const QUrlQuery query(request.qRequest.url());
			if ( !query.hasQueryItem(m_key) )
				return false;
			const QStringList values = query.allQueryItemValues(m_key);
			QStringList::const_iterator iter;
			for ( iter = values.cbegin(); iter != values.cend(); ++iter )
			{
				if ( ! m_regEx.match(*iter).hasMatch() )
					return false;
			}
			return true;
		}

		QString m_key;
		QRegularExpression m_regEx;
		QUrl::ComponentFormattingOptions m_format;
	};

	/*! Matches if the request URL contains given query parameters.
	 * Note that the URL can contain more query parameters. This predicate just checks that the given parameters exist with the given values.
	 *
	 * This predicate is especially useful in combination with the regular expression predicate UrlMatching()
	 * since query parameters typically don't have a defined order which makes it very hard to match them with regular expressions.
	 */
	class QueryParameters : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching URL query parameters.
		 * \param parameters A QHash of query parameters that need to be present in the URL with defined values.
		 * The keys of the hash are the expected parameter names and the corresponding values of the hash are the
		 * expected parameter values.
		 * \param format QUrl::ComponentFormattingOptions used to convert the query parameter value to a QString.
		 * The default is QUrl::PrettyDecoded since it is also the default for QUrlQuery::queryItemValue().
		 */
		explicit QueryParameters(const QueryParameterHash& parameters, QUrl::ComponentFormattingOptions format = QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded))
			: Predicate()
			, m_format(format)
		{
			QueryParameterHash::const_iterator iter;
			for ( iter = parameters.cbegin(); iter != parameters.cend(); ++iter )
			{
				m_queryParameters.insert( iter.key(), QStringList() << iter.value() );
			}
		}

		/*! Creates a predicate matching URL query parameters.
		 * \param parameters A QHash of query parameters that need to be present in the URL with defined values.
		 * The keys of the hash are the expected parameter names and the corresponding values of the hash are the
		 * expected parameter values in the order they appear in the query.
		 * \param format QUrl::ComponentFormattingOptions used to convert the query parameter value to a QString.
		 * The default is QUrl::PrettyDecoded since it is also the default for QUrlQuery::queryItemValue().
		 * \since 0.4.0
		 */
		explicit QueryParameters(const MultiValueQueryParameterHash& parameters, QUrl::ComponentFormattingOptions format = QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded))
			: Predicate()
			, m_queryParameters(parameters)
			, m_format(format)
		{}

	private:
		virtual bool match(const Request& request)
		{
			const QUrlQuery query(request.qRequest.url());
			MultiValueQueryParameterHash::const_iterator iter;
			for(iter = m_queryParameters.cbegin(); iter != m_queryParameters.cend(); ++iter)
			{
				if (!query.hasQueryItem(iter.key())
				    || query.allQueryItemValues(iter.key(), m_format) != iter.value())
				{
					return false;
				}
			}
			return true;
		}

		MultiValueQueryParameterHash m_queryParameters;
		QUrl::ComponentFormattingOptions m_format;
	};

	/*! Matches if *all* URL query parameters match one of the given regular expression pairs.
	 *
	 * This predicates checks all URL query parameters against the given regular expression pairs in the order
	 * they are given. If the first regular expression of a pair matches the name of the query parameter, then the
	 * second regular expression must match the value of the parameter. If the value does not match or if the parameter
	 * name does not match any of the first regular expressions of the pairs, then the predicate does not match.
	 * If all query parameter names match one of the first regular expressions and the parameter values match the
	 * corresponding second regular expression, then this predicate matches.
	 *
	 * Note that for parameters with multiple values, all values of the parameter need to match the second regular
	 * expression.
	 *
	 * This predicate can be used to ensure that there are not unexpected query parameters.
	 */
	class QueryParameterTemplates : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching all query parameters against regular expression pairs.
		 *
		 * \param templates QVector of QRegularExpression pairs. The first regular expressions are matched against the
		 * query parameter names and the second regular expressions are matched against the query parameter values.
		 * \param format QUrl::ComponentFormattingOptions used to convert the query parameter value to a QString.
		 * The default is QUrl::PrettyDecoded since it is also the default for QUrlQuery::queryItemValue().
		 */
		explicit QueryParameterTemplates( const RegExPairVector& templates, QUrl::ComponentFormattingOptions format = QUrl::ComponentFormattingOptions( QUrl::PrettyDecoded ) )
			: Predicate()
			, m_templates( templates )
			, m_format( format )
		{}

	private:
		virtual bool match( const Request& request )
		{
			typedef QList< QPair< QString, QString > > StringPairList;

			const QUrlQuery query( request.qRequest.url() );
			const StringPairList queryParams = query.queryItems( m_format );

			StringPairList::const_iterator queryParamsIter;
			for ( queryParamsIter = queryParams.cbegin(); queryParamsIter != queryParams.cend(); ++queryParamsIter )
			{
				bool matched = false;

				RegExPairVector::const_iterator templateIter;
				for ( templateIter = m_templates.cbegin(); templateIter != m_templates.cend(); ++templateIter )
				{
					if ( templateIter->first.match( queryParamsIter->first ).hasMatch() )
					{
						matched = templateIter->second.match( queryParamsIter->second ).hasMatch();
						break;
					}
				}

				if ( !matched )
					return false;
			}

			return true;
		}

		RegExPairVector m_templates;
		QUrl::ComponentFormattingOptions m_format;
	};

	/*! Matches if the request body matches a regular expression.
	 *
	 * To match against the regular expression, the body needs to be converted to a QString.
	 * If a \p codec is provided in the constructor, it is used to convert the body.
	 * Else, the predicate tries to determine the codec from the [QNetworkRequest::ContentTypeHeader]:
	 * - If the content type header contains codec information using the `"charset:<CODEC>"` format, this codec is used, if supported.
	 *   - If the codec is not supported, a warning is printed and the predicate falls back to Latin-1.
	 * - If the content type header does not contain codec information, the MIME type is investigated.
	 *   - If the MIME type is known and
	 * inherits from text/plain, the predicate uses QTextCodec::codecForUtfText() to detect the codec and falls back to UTF-8 if
	 * the codec cannot be detected.
	 * - In all other cases, including the case that there is no content type header at all and the case that the content is binary, the
	 * predicate uses QTextCodec::codecForUtfText() to detect the codec and falls back to Latin-1 if the codec cannot be detected.
	 * \note
	 * \parblock
	 * When trying to match without using the correct codec, (for example, when matching binary content), the regular expression patterns
	 * must be aware of the codec mismatch. In such cases, the best approach is to use the numerical value of the encoded character.
	 * For example, matching the character "ç" (LATIN SMALL LETTER C WITH CEDILLA) encoded in UTF-8 when the predicate uses Latin-1
	 * encoding would require the pattern \c "Ã§" assuming the pattern itself is encoded using UTF-8.
	 * Since this can lead to mistakes easily, one should rather use the pattern \c "\\xC3\\x83".
	 * \endparblock
	 *
	 * \sa QMimeDatabase
	 * [QNetworkRequest::ContentTypeHeader]: http://doc.qt.io/qt-5/qnetworkrequest.html#KnownHeaders-enum
	 */
	class BodyMatching : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching the request body using a regular expression.
		 * \param bodyRegEx The regular expression to match against the request body.
		 * \param codec The codec to be used to convert the body into a QString. If null, the predicate
		 * tries to determine the codec based on the [QNetworkRequest::ContentTypeHeader].
		 * [QNetworkRequest::ContentTypeHeader]: http://doc.qt.io/qt-5/qnetworkrequest.html#KnownHeaders-enum
		 */
		explicit BodyMatching(const QRegularExpression& bodyRegEx, QTextCodec* codec = Q_NULLPTR)
			: Predicate()
			, m_bodyRegEx(bodyRegEx)
			, m_codec(codec)
			, m_charsetFieldRegEx("charset:(.*)")
		{}

	private:
		virtual bool match(const Request& request)
		{
			QTextCodec* codec = m_codec;
			const QString contentTypeHeader = request.qRequest.header(QNetworkRequest::ContentTypeHeader).toString();
			if (!codec && !contentTypeHeader.isEmpty())
			{
				QStringList contentTypeFields = contentTypeHeader.split(QChar(';'));
				int charsetFieldIndex = contentTypeFields.indexOf(m_charsetFieldRegEx);
				if (charsetFieldIndex >= 0)
				{
					const QString& charsetField = contentTypeFields.at(charsetFieldIndex);
					const QString charset = HttpUtils::trimmed(m_charsetFieldRegEx.match(charsetField).captured(1));
					codec = QTextCodec::codecForName(charset.toUtf8());
					if (!codec)
					{
						qWarning("MockNetworkAccessManager: Unsupported codec: %s", charset.toUtf8().constData());
						codec = QTextCodec::codecForName("Latin-1");
					}
				}
				else
				{
					const QMimeType mimeType = QMimeDatabase().mimeTypeForName(contentTypeFields.first());
					if (mimeType.inherits("text/plain"))
						codec = QTextCodec::codecForUtfText(request.body, QTextCodec::codecForName("utf-8"));
				}
			}

			if (!codec)
				codec = QTextCodec::codecForUtfText(request.body, QTextCodec::codecForName("Latin-1"));

			const QString decodedBody = codec->toUnicode(request.body);

			return m_bodyRegEx.match(decodedBody).hasMatch();
		}

		QRegularExpression m_bodyRegEx;
		QTextCodec* m_codec;
		const QRegularExpression m_charsetFieldRegEx;
	};

	/*! Match if the request body contains a given snippet.
	 */
	class BodyContaining : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching a snippet in the request body.
		 * \param bodySnippet The byte sequence that needs to exist in the request body.
		 */
		explicit BodyContaining(const QByteArray& bodySnippet) : Predicate(), m_bodySnippet(bodySnippet) {}

	private:
		virtual bool match(const Request& request)
		{
			return request.body.contains(m_bodySnippet);
		}

		QByteArray m_bodySnippet;
	};

	/*! Matches if the request body equals a given body.
	 * \note This predicate does an exact matching so it is stricter than the
	 * other body predicates.
	 */
	class Body : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matchhing the request body.
		 * \param body The body to be compared to the request body.
		 */
		explicit Body(const QByteArray& body) : Predicate(), m_body(body) {}

	private:
		virtual bool match(const Request& request)
		{
			return request.body == m_body;
		}

		QByteArray m_body;
	};

	/*! Matches if the request contains given headers.
	 * Note that the request can contain more headers. This predicate just checks that the given headers exist with the given values.
	 * \note For this predicate to work correctly, the type of the header field must be registered with qRegisterMetaType()
	 * and QMetaType::registerComparators() or QMetaType::registerEqualsComparator().
	 * \sa QNetworkRequest::header()
	 */
	class Headers : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching a set of request headers.
		 * \param headers QHash of headers that need to be present in the request
		 * with defined values. The keys of the hash are the names of the expected
		 * headers and the corresponding values of the hash are the expected values
		 * of the headers.
		 */
		explicit Headers(const HeaderHash& headers) : Predicate(), m_headers(headers) {}

	private:
		virtual bool match(const Request& request)
		{
			for (HeaderHash::const_iterator iter = m_headers.cbegin(); iter != m_headers.cend(); ++iter)
			{
				if (request.qRequest.header(iter.key()) != iter.value())
					return false;
			}
			return true;
		}

		HeaderHash m_headers;
	};

	/*! Match if the request contains a given header.
	 * Note that the request can contain more headers. This predicate just checks that the given header exists with the given value.
	 * \note For this predicate to work correctly, the type of the header field must be registered with qRegisterMetaType()
	 * and QMetaType::registerComparators() or QMetaType::registerEqualsComparator().
	 * \sa QNetworkRequest::header()
	 */
	class Header : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching a request header.
		 * \param header The header that needs to be present in the request.
		 * \param value The value that the \p header needs to have.
		 */
		explicit Header(QNetworkRequest::KnownHeaders header, const QVariant& value)
			: Predicate()
			, m_header(header)
			, m_value(value)
		{}

	private:
		virtual bool match(const Request& request)
		{
			const QVariant headerValue = request.qRequest.header(m_header);
			return headerValue == m_value;
		}

		QNetworkRequest::KnownHeaders m_header;
		QVariant m_value;
	};

	/*! Matches if a header value matches a regular expression.
	 * \note
	 * \parblock
	 * - The \p header's value is converted to a string using QVariant::toString() to match it against the regular expression.
	 * - This predicate does not distinguish between the case that the header has not been set and the case that the header
	 * has been set to an empty value. So both cases match if the \p regEx matches empty strings.
	 * \endparblock
	 * \sa QNetworkRequest::header()
	 */
	class HeaderMatching : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching a header value using a regular expression.
		 * \param header The header whose value needs to match.
		 * \param regEx The regular expression matched against the \p header's value.
		 */
		explicit HeaderMatching(QNetworkRequest::KnownHeaders header, const QRegularExpression& regEx)
			: Predicate()
			, m_header(header)
			, m_regEx(regEx)
		{}

	private:
		virtual bool match(const Request& request)
		{
			const QVariant headerValue = request.qRequest.header(m_header);
			return m_regEx.match(headerValue.toString()).hasMatch();
		}

		QNetworkRequest::KnownHeaders m_header;
		QRegularExpression m_regEx;
	};

	/*! Matches if the request contains given raw headers.
	 * Note that the request can contain more headers. This predicate just checks that the given headers exist with the given values.
	 * \sa QNetworkRequest::rawHeader()
	 */
	class RawHeaders : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching a set of raw headers.
		 * \param rawHeaders QHash of raw headers that need to be present in the request with defined values.
		 * The keys of the hash are the names of the expected headers and
		 * the values of the hash are the corresponding expected values of the headers.
		 */
		explicit RawHeaders(const RawHeaderHash& rawHeaders) : Predicate(), m_rawHeaders(rawHeaders) {}

	private:
		virtual bool match(const Request& request)
		{
			for (RawHeaderHash::const_iterator iter = m_rawHeaders.cbegin(); iter != m_rawHeaders.cend(); ++iter)
			{
				if (request.qRequest.rawHeader(iter.key()) != iter.value())
					return false;
			}
			return true;
		}

		RawHeaderHash m_rawHeaders;
	};

	/*! Matches if the request contains a given raw header.
	 * Note that the request can contain more headers. This predicate just checks that the given header exists with the given value.
	 * \sa QNetworkRequest::rawHeader()
	 */
	class RawHeader : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching a raw request header.
		 * \param header The raw header that needs to be present in the request.
		 * \param value The value that the \p header needs to have.
		 */
		explicit RawHeader(const QByteArray& header, const QByteArray& value)
			: Predicate()
			, m_header(header)
			, m_value(value)
		{}

	private:
		virtual bool match(const Request& request)
		{
			return request.qRequest.rawHeader(m_header) == m_value;
		}

		QByteArray m_header;
		QByteArray m_value;
	};

	/*! Matches if a raw header value matches a regular expression.
	 * \note
	 * \parblock
	 * - The \p header's value is converted to a string using QString::fromUtf8() to match it against the \p regEx.
	 * - This predicate does not distinguish between the case that the header has not been set and the case that the header
	 * has been set to an empty value. So both cases match if the \p regEx matches empty strings.
	 * \endparblock
	 * \sa QNetworkRequest::rawHeader()
	 */
	class RawHeaderMatching : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching the value of a raw header using a regular expression.
		 * \param header The raw header whose value needs to match.
		 * \param regEx The regular expression matched against the \p header's value.
		 */
		explicit RawHeaderMatching(const QByteArray& header, const QRegularExpression& regEx)
			: Predicate()
			, m_header(header)
			, m_regEx(regEx)
		{}

	private:
		virtual bool match(const Request& request)
		{
			const QString headerValue = QString::fromUtf8(request.qRequest.rawHeader(m_header));
			return m_regEx.match(headerValue).hasMatch();
		}

		QByteArray m_header;
		QRegularExpression m_regEx;
	};

	/*! Matches if *all* request headers match one of the given regular expression pairs.
	 *
	 * This predicates checks all defined request headers against the given regular expression pairs in the order
	 * they are given. If the first regular expression of a pair matches the name of the header, then the
	 * second regular expression must match the value of the header. If the value does not match or if the header
	 * name does not match any of the first regular expressions of the pairs, then the predicate does not match.
	 * If all header names match one of the first regular expressions and the header values match the
	 * corresponding second regular expression, then this predicate matches.
	 *
	 * This predicate can be used to ensure that there are not unexpected headers.
	 *
	 * \note \parblock
	 * - This predicate also checks the headers defined using QNetworkRequest::setHeader().
	 * - Be aware that the Manager might add QNetworkCookies to the [QNetworkRequest::CookieHeader] in case
	 * [QNetworkRequest::CookieLoadControlAttribute] is set to [QNetworkRequest::Automatic].
	 * \endparblock
	 * [QNetworkRequest::CookieHeader]: http://doc.qt.io/qt-5/qnetworkrequest.html#KnownHeaders-enum
	 * [QNetworkRequest::CookieLoadControlAttribute]: http://doc.qt.io/qt-5/qnetworkrequest.html#Attribute-enum
	 * [QNetworkRequest::Automatic]: http://doc.qt.io/qt-5/qnetworkrequest.html#LoadControl-enum
	 */
	class RawHeaderTemplates : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching all headers against regular expression pairs.
		 *
		 * \param templates QVector of QRegularExpression pairs. The first regular expressions are matched against the
		 * header names and the second regular expressions are matched against the header values.
		 */
		RawHeaderTemplates( const RegExPairVector& templates )
			: Predicate()
			, m_templates( templates )
		{
		}

	private:
		virtual bool match( const Request& request )
		{
			QList<QByteArray>::const_iterator headerIter;
			const QList<QByteArray> headerList = request.qRequest.rawHeaderList();

			for (headerIter = headerList.cbegin(); headerIter != headerList.cend(); ++headerIter)
			{
				bool matched = false;

				RegExPairVector::const_iterator templateIter;
				for (templateIter = m_templates.cbegin(); templateIter != m_templates.cend(); ++templateIter)
				{
					if (templateIter->first.match( QString::fromUtf8( *headerIter ) ).hasMatch())
					{
						const QByteArray headerValue = request.qRequest.rawHeader( *headerIter );

						matched = templateIter->second.match( QString::fromUtf8( headerValue ) ).hasMatch();
						break;
					}
				}

				if (!matched)
					return false;
			}

			return true;
		}

		RegExPairVector m_templates;
	};

	/*! Match if the request has a given attribute.
	 * Note that the request can have more attributes. This predicate just checks that the given attribute exists with the given value.
	 * \note
	 * \parblock
	 * - This predicate cannot match the default values of the attributes since QNetworkRequest::attribute()
	 * does not return the default values. As a workaround, use the \p matchInvalid flag: when you want to match the default value,
	 * set \p value to the default value and set \p matchInvalid to \c true. Then the predicate will match either when the attribute
	 * has been set to the default value explicitly or when the attribute has not been set at all and therefore falls back to the default value.
	 * - Since the attributes are an internal feature of %Qt and are never sent to a server, using this predicate means mocking the
	 * behavior of the QNetworkAccessManager instead of the server.
	 * \endparblock
	 * \sa QNetworkRequest::attribute()
	 */
	class Attribute : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching a request attribute.
		 * \param attribute The request attribute whose values is matched by this predicate.
		 * \param value The value that the \p attribute needs to have.
		 * \param matchInvalid If \c true, this predicate will match if the attribute has not been specified
		 * on the request. So the predicate matches if either the attribute has been set to the given \p value
		 * or not set at all. If \c false, this predicate will only match if the attribute has been set
		 * to the specified \p value explicitly.
		 */
		explicit Attribute(QNetworkRequest::Attribute attribute, const QVariant& value, bool matchInvalid = false)
			: Predicate()
			, m_attribute(attribute)
			, m_value(value)
			, m_matchInvalid(matchInvalid)
		{}

	private:
		virtual bool match(const Request& request)
		{
			QVariant attribute = request.qRequest.attribute(m_attribute);
			return (m_matchInvalid && !attribute.isValid()) || attribute == m_value;
		}

		QNetworkRequest::Attribute m_attribute;
		QVariant m_value;
		bool m_matchInvalid;
	};

	/*! Matches if a attribute value matches a regular expression.
	 * \note
	 * \parblock
	 * - The \p attributes's value is converted to a string using QVariant::toString() to match it against the regular expression.
	 * - This predicate does not distinguish between the case that the attribute has not been set and the case that the attribute has
	 * been set to an empty value. So both cases match if the \p regEx matches empty strings.
	 * - Since the attributes are an internal feature of %Qt and are never sent to a server, using this predicate means mocking the
	 * behavior of the QNetworkAccessManager instead of the server.
	 * \endparblock
	 * \sa QNetworkRequest::attribute()
	 */
	class AttributeMatching : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching an attribute value using a regular expression.
		 * \param attribute The attribute whose value needs to match.
		 * \param regEx The regular expression matched against the \p attribute's value.
		 */
		explicit AttributeMatching(QNetworkRequest::Attribute attribute, const QRegularExpression& regEx)
			: Predicate()
			, m_attribute(attribute)
			, m_regEx(regEx)
		{}

	private:
		virtual bool match(const Request& request)
		{
			QVariant attributeValue = request.qRequest.attribute(m_attribute);
			return m_regEx.match(attributeValue.toString()).hasMatch();
		}

		QNetworkRequest::Attribute m_attribute;
		QRegularExpression m_regEx;
	};

	/*! Matches if the request contains a specified Authorization header.
	 * In case an unsupported authentication method is required, you might use RawHeaderMatching to "manually" match authorized requests.
	 * \sa RawHeaderMatching
	 */
	class Authorization : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching an authorization using the HTTP Basic authentication scheme with given username and password.
		 * \param username The username that must be given.
		 * \param password The password that must be given.
		 */
		explicit Authorization(const QString& username, const QString& password) : Predicate()
		{
			QAuthenticator authenticator;
			authenticator.setUser(username);
			authenticator.setPassword(password);
			m_authenticators.append(authenticator);
			m_authChallenge.reset(new HttpUtils::Authentication::Basic("dummy"));
		}

		/*! Creates a predicate matching an authorization using the HTTP Basic authentication scheme with
		 * a selection of username and password combinations.
		 * \param credentials QHash of  username and password combinations. The authirzation in the request must match one of these \p credentials.
		 */
		explicit Authorization(const QHash<QString, QString>& credentials) : Predicate()
		{
			QHash<QString, QString>::const_iterator iter;
			for (iter = credentials.cbegin(); iter != credentials.cend(); ++iter)
			{
				QAuthenticator authenticator;
				authenticator.setUser(iter.key());
				authenticator.setPassword(iter.value());
				m_authenticators.append(authenticator);
			}
			m_authChallenge.reset(new HttpUtils::Authentication::Basic("dummy"));
		}

		/*! Creates a predicate matching an  authorization which matches a given authentication challenge with credentials defined by a given QAuthenticator.
		 * \param authChallenge The authentication challenge which the authorization in the request must match.
		 * \param authenticators Allowed username and password combinations. The authorization in the request must match one of these combinations.
		 */
		explicit Authorization(const HttpUtils::Authentication::Challenge::Ptr& authChallenge, const QVector<QAuthenticator>& authenticators)
			: Predicate()
			, m_authChallenge(authChallenge)
			, m_authenticators(authenticators)
		{
		}

	private:
		virtual bool match(const Request& request)
		{
			QVector<QAuthenticator>::const_iterator iter;
			for (iter = m_authenticators.cbegin(); iter != m_authenticators.cend(); ++iter)
			{
				if (m_authChallenge->verifyAuthorization(request.qRequest, *iter))
					return true;
			}

			return false;
		}


		HttpUtils::Authentication::Challenge::Ptr m_authChallenge;
		QVector<QAuthenticator> m_authenticators;
	};

	/*! Matches if a request contains a cookie with a given value.
	 * Note that the request can contain more cookies. This predicate just checks that the given cookie exists with the given value.
	 *
	 * \note
	 * \parblock
	 * - If there is no cookie with the given name, this predicate does not match.
	 * - In case there are multiple cookies with the given name, the first one is used and the other ones are ignored.
	 * \endparblock
	 *
	 * \sa [QNetworkRequest::CookieHeader]
	 * [QNetworkRequest::CookieHeader]: http://doc.qt.io/qt-5/qnetworkrequest.html#KnownHeaders-enum
	 */
	class Cookie : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching a cookie value.
		 * \param cookie The cookie which should exist. Only the QNetworkCookie::name() and QNetworkCookie::value()
		 * are used to match. Other properties of the cookie (like QNetworkCookie::domain() or QNetworkCookie::expiryDate())
		 * are ignored.
		 */
		explicit Cookie(const QNetworkCookie& cookie)
			: Predicate()
			, m_cookie(cookie)
		{}

	private:
		virtual bool match(const Request& request)
		{
			const QList<QNetworkCookie> requestCookies = request.qRequest.header(QNetworkRequest::CookieHeader).value<QList<QNetworkCookie> >();
			QList<QNetworkCookie>::const_iterator iter;

			for (iter = requestCookies.cbegin(); iter != requestCookies.cend(); ++iter)
			{
				QNetworkCookie requestCookie = *iter;
				/* We use the first matching cookie and ignore possible other cookies with the same name.
				 * RFC 6265 does not define a "correct" way to handle this but this seems to be the common practice.
				 * See https://stackoverflow.com/a/24214538/490560
				 */
				if (requestCookie.name() == m_cookie.name())
					return (requestCookie.value() == m_cookie.value());
			}

			return false;
		}

		QNetworkCookie m_cookie;
	};

	/*! Matches if a request contains a cookie with a value matching a regular expression.
	 * \note
	 * \parblock
	 * - The cookies's value is converted to a string using QString::fromUtf8() to match it against the \p regEx.
	 * - If there is no cookie with the given name, this predicate does not match, no matter what \p regEx is.
	 * - If the cookie's value is empty, it is matched against the \p regEx.
	 * - In case there are multiple cookies with the given name, the first one is used and the other ones are ignored.
	 * \endparblock
	 * \sa QNetworkRequest::rawHeader()
	 */
	class CookieMatching : public Rule::Predicate
	{
	public:
		/*! Creates a predicate matching the value of a cookie using a regular expression.
		 * \param cookieName The name of the cookie whose value needs to match.
		 * \param regEx The regular expression matched against the \p header's value.
		 */
		explicit CookieMatching(const QByteArray& cookieName, const QRegularExpression& regEx)
			: Predicate()
			, m_cookieName(cookieName)
			, m_regEx(regEx)
		{}

	private:
		virtual bool match(const Request& request)
		{
			const QList<QNetworkCookie> cookies = request.qRequest.header(QNetworkRequest::CookieHeader).value<QList<QNetworkCookie> >();
			QList<QNetworkCookie>::const_iterator iter;
			for (iter = cookies.cbegin(); iter != cookies.cend(); ++iter)
			{
				const QByteArray cookieName = iter->name();
				/* We use the first matching cookie and ignore possible other cookies with the same name.
				 * RFC 6265 does not define a "correct" way to handle this but this seems to be the common practice.
				 * See https://stackoverflow.com/a/24214538/490560
				 */
				if (m_cookieName == cookieName)
				{
					const QString cookieValue = QString::fromUtf8(iter->value());
					return m_regEx.match(cookieValue).hasMatch();
				}
			}
			return false;
		}

		QByteArray m_cookieName;
		QRegularExpression m_regEx;
	};


} // namespace Predicates

/*! Defines the possible behaviors of the Manager when a request does not match any Rule.
 */
enum UnmatchedRequestBehavior
{
	PassThrough,    /*!< Unmatched requests are passed through
	                 * to the next network access manager.
	                 * \sa Manager::setPassThroughNam()
	                 * \sa \ref page_passThrough
	                 */
	PredefinedReply /*!< The manager will return a predefined reply for unmatched requests.
	                 * \sa Manager::setUnmatchedRequestBuilder()
	                 */
};

} // namespace MockNetworkAccess

Q_DECLARE_METATYPE(MockNetworkAccess::VersionNumber)
Q_DECLARE_METATYPE(MockNetworkAccess::Request)
Q_DECLARE_METATYPE(MockNetworkAccess::Rule::Ptr)

namespace MockNetworkAccess
{

/*! Helper class which emits signals for the Manager.
 *
 * Since template classes cannot use the Q_OBJECT macro, they cannot define signals or slots.
 * For this reason, this helper class is needed to allow emitting signals from the Manager.
 */
class SignalEmitter : public QObject
{
	Q_OBJECT

	template<class Base>
	friend class Manager;

public:
	/*! Default destructor
	 */
	virtual ~SignalEmitter() {}

private:
	/*! Creates a SignalEmitter object.
	 *
	 * \note This register the types Request and Rule::Ptr in the %Qt meta type system
	 * using qRegisterMetaType().
	 *
	 * \param parent Parent QObject.
	 */
	SignalEmitter(QObject* parent = Q_NULLPTR) : QObject(parent)
	{
		registerMetaTypes();
	}

Q_SIGNALS:

	/*! Emitted when the Manager receives a request through its public interface (QNetworkAccessManager::get() etc.).
	 * \param request The request.
	 */
	void receivedRequest(const MockNetworkAccess::Request& request);

	/*! Emitted when the Manager handles a request.
	 *
	 * This signal is emitted for requests received through the public interface (see receivedRequest()) as well as requests
	 * created internally by the Manager for example when automatically following redirects or when handling authentication.
	 *
	 * \param request The request.
	 */
	void handledRequest(const MockNetworkAccess::Request& request);

	/*! Emitted when a request matches a Rule.
	 * \param request The request.
	 * \param rule The matched Rule.
	 */
	void matchedRequest(const MockNetworkAccess::Request& request, MockNetworkAccess::Rule::Ptr rule);

	/*! Emitted when the Manager received a request which did not match any of its Rules.
	 * \param request The request.
	 */
	void unmatchedRequest(const MockNetworkAccess::Request& request);

	/*! Emitted when the Manager passed a request through to the next network access manager.
	 * \param request The request.
	 * \sa Manager::setPassThroughNam()
	 */
	void passedThrough(const MockNetworkAccess::Request& request);

private:
	static void registerMetaTypes()
	{
		static QMutex metaTypesLock;
		QMutexLocker locker(&metaTypesLock);
		static bool registered = false;
		if (!registered)
		{
			qRegisterMetaType<Request>();
			qRegisterMetaType<Rule::Ptr>();
			registered = true;
		}
	}
};

/*! \internal Implementation details.
 */
namespace detail
{

/*! \internal
 * Updates the state of a QNetworkAccessManager according to reply headers.
 * This includes updating cookies and HSTS entries.
 */
class ReplyHeaderHandler : public QObject
{
	Q_OBJECT

public:
	ReplyHeaderHandler(QNetworkAccessManager* manager, QObject* parent = Q_NULLPTR) : QObject(parent), m_manager(manager) {}

	virtual ~ReplyHeaderHandler() {}

public Q_SLOTS:
	void handleReplyHeaders(QNetworkReply* sender = Q_NULLPTR)
	{
		QNetworkReply* reply = sender;
		if (!reply)
			reply = qobject_cast<QNetworkReply*>(this->sender());
		Q_ASSERT(reply);

		QNetworkRequest request = reply->request();

		const bool saveCookies = (static_cast<QNetworkRequest::LoadControl>(request.attribute(QNetworkRequest::CookieSaveControlAttribute, static_cast<int>(QNetworkRequest::Automatic)).toInt()) == QNetworkRequest::Automatic);

		// Handle known headers
		QNetworkCookieJar* cookieJar = m_manager->cookieJar();
		if (saveCookies && cookieJar)
		{
			QList<QNetworkCookie> cookies = reply->header(QNetworkRequest::SetCookieHeader).value<QList<QNetworkCookie> >();
			if (!cookies.isEmpty())
				cookieJar->setCookiesFromUrl(cookies, reply->url());
		}

		// Handle other (raw) headers
		QList<QNetworkReply::RawHeaderPair> rawHeaderPairs = reply->rawHeaderPairs();
		QList<QNetworkReply::RawHeaderPair>::const_iterator headerIter;
		for (headerIter = rawHeaderPairs.cbegin(); headerIter != rawHeaderPairs.cend(); ++headerIter)
		{
			const QByteArray headerName = headerIter->first.toLower(); // header field-name is ASCII according to RFC 7230 3.2

#if (QT_VERSION >= QT_VERSION_CHECK(5, 9, 0))
			static const QByteArray stsHeader("strict-transport-security");
			if (headerName == stsHeader)
			{
				QStringList stsHeaders = HttpUtils::splitCommaSeparatedList(headerIter->second);
				QStringList::const_iterator stsHeaderIter;
				for (stsHeaderIter = stsHeaders.constBegin(); stsHeaderIter != stsHeaders.constEnd(); ++stsHeaderIter)
				{
					/* If the header has an invalid syntax, we ignore it and continue
					 * until we find a valid STS header.
					 */
					if (processStsHeader(stsHeaderIter->toLatin1(), reply->url()))
						break; // following STS headers are ignored
					continue;
				}
			}
#endif // Qt >= 5.9.0
		}
	}

private:

#if (QT_VERSION >= QT_VERSION_CHECK(5, 9, 0))
	bool processStsHeader(const QByteArray& header, const QUrl& host)
	{
		const QString headerData = QString::fromLatin1(header);
		const QStringList directives = headerData.split(QChar(';'));

		static const QRegularExpression basicDirectiveRegEx("^([^=]*)=?(.*)$");
		static const QRegularExpression maxAgeValueRegEx("\\d+");

		static const QLatin1String maxAgeDirective("max-age");

		QHstsPolicy policy;
		policy.setHost(host.host());

		QSet<QString> foundDirectives;

		QStringList::const_iterator directiveIter;
		for (directiveIter = directives.cbegin(); directiveIter != directives.cend(); ++directiveIter)
		{
			const QString cleanDirective = HttpUtils::whiteSpaceCleaned(*directiveIter);

			QRegularExpressionMatch match;

			match = basicDirectiveRegEx.match(cleanDirective);
			// This should be impossible since basicDirectiveRegEx matches everything
			Q_ASSERT_X(match.hasMatch(), Q_FUNC_INFO, "Could not parse directive.");

			const QString directiveName = HttpUtils::whiteSpaceCleaned(match.captured(1)).toLower();
			if (foundDirectives.contains(directiveName))
				return false; // Invalid header: duplicate directive
			foundDirectives.insert(directiveName);

			const QString rawDirectiveValue = HttpUtils::whiteSpaceCleaned(match.captured(2));
			const QString directiveValue = HttpUtils::isValidToken(rawDirectiveValue)? rawDirectiveValue : HttpUtils::unquoteString(rawDirectiveValue);

			if (directiveName == maxAgeDirective)
			{
				match = maxAgeValueRegEx.match(directiveValue);
				if (!match.hasMatch())
					return false; // Invalid header: incorrect max-age value
				qint64 maxAge = match.captured(0).toLongLong();
				policy.setExpiry(QDateTime::currentDateTimeUtc().addSecs(maxAge));
				continue;
			}

			if (directiveName == QLatin1String("includesubdomains"))
			{
				policy.setIncludesSubDomains(true);
				continue;
			}

			// else we check if the directive is legal at all
			if (!HttpUtils::isValidToken(directiveName))
				return false; // Invalid header: illegal directive name

			if (!HttpUtils::isValidToken(directiveValue) && !HttpUtils::isValidQuotedString(directiveValue))
				return false; // Invalid header: illegal directive value

			// Directive seems legal but simply unknown. So we ignore it.
		}

		if (!foundDirectives.contains(maxAgeDirective))
			return false; // Invalid header: missing required max-age directive

		m_manager->addStrictTransportSecurityHosts(QVector<QHstsPolicy>() << policy);
		return true;
	}
#endif // Qt >= 5.9.0

	QPointer<QNetworkAccessManager> m_manager;
};


} // namespace detail


/*! Determines the behavior of the %Qt version in use.
 * This is also the default behavior of Manager objects if not overridden using Manager::setBehaviorFlags().
 * \return The BehaviorFlags matching the behavior of the %Qt version used at runtime.
 * \sa qVersion()
 * \sa BehaviorFlag
 * \since 0.3.0
 */
inline BehaviorFlags getDefaultBehaviorFlags()
{
	#if QT_VERSION < QT_VERSION_CHECK(5,2,0)
		#error MockNetworkAccessManager requires Qt 5.2.0 or later
	#endif
	const char* qtVersion = qVersion();
	VersionNumber versionInUse = VersionNumber::fromString(QString::fromLatin1(qtVersion));

	if (versionInUse >= VersionNumber(5,6,0))
		return Behavior_Qt_5_6_0;
	else
		return Behavior_Qt_5_2_0;
}


/*! Mixin class to mock network replies from QNetworkAccessManager.
 * Manager mocks the QNetworkReplys instead of sending the requests over the network.
 * Manager is a mixin class meaning it can be used "on top" of every class inheriting publicly from QNetworkAccessManager.
 *
 * \tparam Base QNetworkAccessManager or a class publicly derived from QNetworkAccessManager.
 *
 *
 * ### Limitations ###
 * The Manager currently has a few limitations:
 * - When a request with automatic redirect following is passed through and gets redirected,
 *   the rules of the initial Manager are not applied to the redirect
 *   (see \ref page_passThrough_redirects and issue \issue{15}).
 * - When a request is redirected and then passed through to a separate QNetworkAccessManager
 *   (see setPassThroughNam()), the QNetworkReply::metaDataChanged() and
 *   QNetworkReply::redirected() signals of the mocked redirections are emitted out of order (namely after all other signals).
 * - The mocked replies do not emit the implementation specific signals of a real HTTP based QNetworkReply
 *   (that is the signals of QNetworkReplyHttpImpl).
 * - Only HTTP Basic authentication support is built-in. However, this should not be a problem in most cases
 *   since the handling of authentication is normally done internally between the Manager and the MockReply.
 *   This only limits you if you manually create Authorization headers and have to rely on HTTP Digest or
 *   NTLM authentication.
 * - The QAuthenticator passed in the `QNetworkAccessManager::authenticationRequired()` signal does not provide the `realm`
 *   parameter via the `QAuthenticator::realm()` method in %Qt before 5.4.0 but only as option with the key `realm`
 *   (for example, via `authenticator->option("realm")`).
 * - Proxy authentication is not supported at the moment.
 * - [QNetworkRequest::UserVerifiedRedirectPolicy] is not supported at the moment.
 * - The error messages of the replies (QNetworkReply::errorString()) may be different from the ones of real QNetworkReply
 *   objects.
 * - QNetworkReply::setReadBufferSize() is ignored at the moment.
 *
 *
 * Some of these limitations might be removed in future versions. Feel free to create a feature (or merge) request if you hit one these limitations.
 *
 * Additionally, the Manager supports only selected [QNetworkRequest::Attributes].
 * The following attributes are supported:
 * - QNetworkRequest::HttpStatusCodeAttribute
 * - QNetworkRequest::HttpReasonPhraseAttribute
 * - QNetworkRequest::RedirectionTargetAttribute
 * - QNetworkRequest::ConnectionEncryptedAttribute
 * - QNetworkRequest::CustomVerbAttribute
 * - QNetworkRequest::CookieLoadControlAttribute
 * - QNetworkRequest::CookieSaveControlAttribute
 * - QNetworkRequest::FollowRedirectsAttribute
 * - QNetworkRequest::OriginalContentLengthAttribute
 * - QNetworkRequest::RedirectPolicyAttribute
 *
 * All other attributes are ignored when specified on a QNetworkRequest and are not set when returning a MockReply.
 * However, if desired, the attributes can be matched on a request using Predicates::Attribute or Predicates::AttributeMatching
 * and can be set on a MockReply using MockReplyBuilder::withAttribute().
 *
 * \note
 * \parblock
 * At the moment, the Manager does not handle large request bodies well since it reads them into
 * memory completely to be able to provide them to all the Rule objects.
 *
 * With setInspectBody(), you can disable this if you need to use the Manager with large request
 * bodies and you do not need to match against the body.
 * \endparblock
 *
 *
 * ### Configuration ###
 * To define which and how requests are answered with mocked replies, the Manager is configured using Rule.
 * Whenever the Manager is handed over a request, it matches the request against its Rule one after the other.
 * When a Rule reports a match for the request, the Manager requests the Rule to create a reply for that request.
 * If the Rule creates a reply, then this reply is returned by the Manager.
 * If the Rule does not create a reply, the Manager continues matching the request against the remaining Rule.
 * If no Rule matches the request or no one creates a reply, the request is passed
 * through to the next network access manager (see setPassThroughNam())
 * and the corresponding QNetworkReply is returned.
 *
 * To define which requests match a Rule, the Rule object is configured by adding predicates.
 *
 * To define the properties of the created replies, the Rule object exposes a MockReplyBuilder via the Rule::reply() method.
 *
 * To add Rule to the Manager, you can either:
 * - create a Rule object, configure it and add it using addRule().
 * - use the convenience methods whenGet(), whenPost(), when() etc. and configure the returned Rule.
 *
 * To retrieve or remove Rules or change their order, use the methods rules() and setRules().
 *
 *
 * #### Example ####
 *
\code
using namespace MockNetworkAccess;
using namespace MockNetworkAccess::Predicates;

// Create the Manager
Manager<QNetworkAccessManager> mockNam;

// Simple configuration
mockNam.whenGet(QRegularExpression("https?://example.com/data/.*"))
       ->reply()->withBody(QJsonDocument::fromJson("{ \"id\": 736184, \"data\": \"Hello World!\" }");

// More complex configuration
Rule::Ptr accountInfoRequest(new Rule);
accountInfoRequest->has(Verb(QNetworkAccessManager::GetOperation))
                  ->has(UrlMatching(QRegularExpression("https?://example.com/accountInfo/.*")));

Rule::Ptr authorizedAccountInfoRequest(accountInfoRequest->clone());
authorizedAccountInfoRequest->has(RawHeaderMatching("Authorization", QRegularExpression("Bearer: .*")))
                            ->reply()->withBody("{ \"name\": \"John Doe\", \"email\": \"john.doe@example.com\" }");

Rule::Ptr unauthorizedAccountInfoRequest(accountInfoRequest->clone());
unauthorizedAccountInfoRequest->reply()->withStatus(401);

// The order is important here since the
// first matching rule will create the reply.
mockNam.add(authorizedAccountInfoRequest);
mockNam.add(unauthorizedAccountInfoRequest);

// All other requests
MockReplyBuilder unmatchedRequestResponse;
unmatchedRequestResponse.withStatus(404);
mockNam.setUnmatchedRequestBuilder(unmatchedRequestResponse);
mockNam.setUnmatchedRequestBehavior(PredefinedReply);

// Use the Manager
MyNetworkClient myNetworkClient;
myNetworkClient.setNetworkManager(&mockNam);
myNetworkClient.run();
\endcode
 *
 * #### Signals ####
 * Since the Manager is a template class, it cannot define signals due to limitations of %Qt's meta object compiler (moc).
 *
 * To solve this, the Manager provides a SignalEmitter (see signaleEmitter()) which emits the signals on behalf of the Manager.
 *
 * [QNetworkRequest::UserVerifiedRedirectPolicy]: http://doc.qt.io/qt-5/qnetworkrequest.html#RedirectPolicy-enum
 * [QNetworkRequest::Attributes]: http://doc.qt.io/qt-5/qnetworkrequest.html#Attribute-enum
 */
template<class Base>
class Manager : public Base
{
	// cannot use Q_OBJECT with template class
public:

	/*! Creates a Manager.
	 * \param parent Parent QObject.
	 */
	Manager(QObject* parent = Q_NULLPTR)
		: Base(parent)
		, m_inspectBody(true)
		, m_behaviorFlags(getDefaultBehaviorFlags())
		, m_passThroughNam(Q_NULLPTR)
		, m_signalEmitter(Q_NULLPTR)
		, m_unmatchedRequestBehavior(PassThrough)
		, m_replyHeaderHandler(new detail::ReplyHeaderHandler(this))
	{}

	/*! Default destructor */
	virtual ~Manager() {}

	/*! Defines whether the message body of requests should be used to match requests.
	 * By default, the Manager reads the complete request body into memory to match it against the Rules.
	 * Setting \p inspectBody to \c false prevents that the request body is read into memory.
	 * However, the matching is then done using a null QByteArray() as request body. So Rules with body predicates will not match unless they match an empty body.
	 * \param inspectBody If \c true (the default), the request body will be read and matched against the predicates of the Rules. If \c false, the request body will not be read by the Manager but a null QByteArray() will be used instead.
	 */
	void setInspectBody(bool inspectBody) { m_inspectBody = inspectBody; }

	/*! \return The behavior flags active on this Manager.
	 */
	BehaviorFlags behaviorFlags() const { return m_behaviorFlags; }

	/*! Tunes the behavior of this Manager.
	 *
	 * \param behaviorFlags Combination of BehaviorFlags to define some details of this Manager's behavior.
	 * \sa BehaviorFlag
	 */
	void setBehaviorFlags(BehaviorFlags behaviorFlags) { m_behaviorFlags = behaviorFlags; }

	/*! Defines how the Manager handles requests that do not match any Rule.
	 *
	 * \param unmatchedRequestBehavior An UnmatchedRequestBehavior flag to define the new behavior.
	 */
	void setUnmatchedRequestBehavior(UnmatchedRequestBehavior unmatchedRequestBehavior) { m_unmatchedRequestBehavior = unmatchedRequestBehavior; }

	/*! \return How the Manager handles unmatched requests.
	 */
	UnmatchedRequestBehavior unmatchedRequestBehavior() const { return m_unmatchedRequestBehavior; }

	/*! Defines a reply builder being used to create replies for requests that do not match any Rule in the Manager.
	 *
	 * \note This builder is only used when unmatchedRequestBehavior() is PredefinedReply.
	 *
	 * \param builder The MockReplyBuilder creating the replies for unmatched requests.
	 * \sa setUnmatchedRequestBehavior()
	 */
	void setUnmatchedRequestBuilder(const MockReplyBuilder& builder) { m_unmatchedRequestFactory = builder; }

	/*! Defines the QNetworkAccessManager to be used in case requests should be passes through to the network.
	 * By default, the \p Base class of this Manager is used.
	 * \param passThroughNam The network access manager to be used to pass requests through. If this is a null pointer, the \p Base class of this Manager is used.
	 * \note This could also be another MockNetworkAccess::Manager. This allows building up a hierarchy of Managers.
	 * \sa setUnmatchedRequestBehavior()
	 * \sa Rule::passThrough()
	 * \sa \ref page_passThrough
	 */
	void setPassThroughNam(QNetworkAccessManager* passThroughNam) { m_passThroughNam = passThroughNam; }

	/*! \return The network access manager to which requests are passed through or a \c Q_NULLPTR if the requests are passed through to the \p Base class of this Manager.
	 */
	QNetworkAccessManager* passThroughNam() const { return m_passThroughNam; }

	/*! \return The Rules of this Manager.
	 */
	QVector<Rule::Ptr> rules() const { return m_rules; }
	/*! Sets the Rules for this Manager.
	 * This will remove all previous Rules.
	 * \param rules the new rules for this Manager.
	 */
	void setRules(const QVector<Rule::Ptr>& rules) { m_rules = rules; }
	/*! Adds a Rule to this Manager.
	 * The rule is appended to the existing list of Rules.
	 * \param rule A QSharedPointer to the Rule to be added to this Manager.
	 */
	void addRule(const Rule::Ptr& rule) { m_rules.append(rule); }

	/*! Creates and adds a Rule which matches \c GET requests with a URL matching a regular expression.
	 * \param urlRegEx The regular expression matched against the request's URL.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr whenGet(const QRegularExpression& urlRegEx)
	{
		return when(QNetworkAccessManager::GetOperation, urlRegEx);
	}

	/*! Creates and adds a Rule which matches \c GET requests with a given URL.
	 * \param url The URL matched against the request's URL.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr whenGet(const QUrl& url)
	{
		return when(QNetworkAccessManager::GetOperation, url);
	}

	/*! Creates and adds a Rule which matches \c POST requests with a URL matching a regular expression.
	 * \param urlRegEx The regular expression matched against the request's URL.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr whenPost(const QRegularExpression& urlRegEx)
	{
		return when(QNetworkAccessManager::PostOperation, urlRegEx);
	}

	/*! Creates and adds a Rule which matches \c POST requests with a given URL.
	 * \param url The URL matched against the request's URL.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr whenPost(const QUrl& url)
	{
		return when(QNetworkAccessManager::PostOperation, url);
	}

	/*! Creates and adds a Rule which matches \c PUT requests with a URL matching a regular expression.
	 * \param urlRegEx The regular expression matched against the request's URL.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr whenPut(const QRegularExpression& urlRegEx)
	{
		return when(QNetworkAccessManager::PutOperation, urlRegEx);
	}

	/*! Creates and adds a Rule which matches \c PUT requests with a given URL.
	 * \param url The URL matched against the request's URL.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr whenPut(const QUrl& url)
	{
		return when(QNetworkAccessManager::PutOperation, url);
	}

	/*! Creates and adds a Rule which matches \c DELETE requests with a URL matching a regular expression.
	 * \param urlRegEx The regular expression matched against the request's URL.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr whenDelete(const QRegularExpression& urlRegEx)
	{
		return when(QNetworkAccessManager::DeleteOperation, urlRegEx);
	}

	/*! Creates and adds a Rule which matches \c DELETE requests with a given URL.
	 * \param url The URL matched against the request's URL.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr whenDelete(const QUrl& url)
	{
		return when(QNetworkAccessManager::DeleteOperation, url);
	}

	/*! Creates and adds a Rule which matches \c HEAD requests with a URL matching a regular expression.
	 * \param urlRegEx The regular expression matched against the request's URL.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr whenHead(const QRegularExpression& urlRegEx)
	{
		return when(QNetworkAccessManager::HeadOperation, urlRegEx);
	}

	/*! Creates and adds a Rule which matches \c HEAD requests with a given URL.
	 * \param url The URL matched against the request's URL.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr whenHead(const QUrl& url)
	{
		return when(QNetworkAccessManager::HeadOperation, url);
	}

	/*! Creates and adds a Rule which matches requests with a given HTTP verb and a URL matching a regular expression.
	 * \param operation The HTTP verb which the request needs to match.
	 * \param urlRegEx The regular expression matched against the request's URL.
	 * \param customVerb The HTTP verb in case \p operation is QNetworkAccessManager::CustomOperation. Else this parameter is ignored.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr when(QNetworkAccessManager::Operation operation, const QRegularExpression& urlRegEx, const QByteArray& customVerb = QByteArray())
	{
		using namespace Predicates;
		Rule::Ptr rule(new Rule());
		rule->has(Verb(operation, customVerb));
		rule->has(UrlMatching(urlRegEx));
		m_rules.append(rule);
		return rule;
	}

	/*! Creates and adds a Rule which matches requests with a given HTTP verb and a given URL.
	 * \param operation The HTTP verb which the request needs to match.
	 * \param url The URL matched against the request's URL.
	 * \param customVerb The HTTP verb in case \p operation is QNetworkAccessManager::CustomOperation. Else this parameter is ignored.
	 * \return A QSharedPointer to the created Rule.
	 */
	Rule::Ptr when(QNetworkAccessManager::Operation operation, const QUrl& url, const QByteArray& customVerb = QByteArray())
	{
		using namespace Predicates;
		Rule::Ptr rule(new Rule());
		rule->has(Verb(operation, customVerb));
		rule->has(Url(url));
		m_rules.append(rule);
		return rule;
	}

	/*! Provides access to signals of the Manager.
	 *
	 * \return A SignalEmitter object which emits signals on behalf of the Manager.
	 *
	 * \sa SignalEmitter
	 */
	SignalEmitter* signalEmitter() const
	{
		if (!m_signalEmitter)
			m_signalEmitter.reset(new SignalEmitter());
		return m_signalEmitter.data();
	}

	/*! \return A vector of all requests which this Manager received through its public interface.
	 */
	QVector<Request> receivedRequests() const { return m_receivedRequests; }

	/*! Returns all requests which were handled by this Manager.
	 *
	 * This includes the requests received through the public interface (see receivedRequests()) as well as requests
	 * created internally by the Manager for example when automatically following redirects or when handling authentication.
	 *
	 * \return A vector of all requests handled by this Manager.
	 */
	QVector<Request> handledRequests() const { return m_handledRequests; }

	/*! \return A vector of all requests which matched a Rule.
	 */
	QVector<Request> matchedRequests() const { return m_matchedRequests; }

	/*! \return A vector of all requests which did not match any Rule.
	 */
	QVector<Request> unmatchedRequests() const { return m_unmatchedRequests; }

	/*! \return A vector of all requests which where passed through to the next (real) network access manager.
	 * \sa setPassThroughNam()
	 */
	QVector<Request> passedThroughRequests() const { return m_passedThroughRequests; }

protected:
	/*! Implements the creation of mocked replies.
	 *
	 * \param operation The HTTP verb of the operation.
	 * \param request The QNetworkRequest object.
	 * \param data Optional request body.
	 * \return A pointer to a QNetworkReply object. The caller takes ownership of the returned reply object. The reply
	 * can either be a real QNetworkReply or a mocked reply. In case of a mocked reply, it is an instance of MockReply.
	 *
	 * \sa QNetworkAccessManager::createRequest()
	 */
	virtual QNetworkReply* createRequest(QNetworkAccessManager::Operation operation, const QNetworkRequest& request, QIODevice* data = Q_NULLPTR);

private:
	QNetworkRequest prepareRequest(const QNetworkRequest& origRequest);
	QNetworkReply* handleRequest( const Request& request );
	QIODevice* createIODevice(const QByteArray& data) const;
	QNetworkReply* passThrough( const Request& request, QNetworkAccessManager* overridePassThroughNam = Q_NULLPTR );
	QNetworkReply* authenticateRequest(MockReply* unauthedReply, const Request& unauthedReq);
	QAuthenticator getAuthenticator(MockReply* unauthedReply, const Request& unauthedReq, const HttpUtils::Authentication::Challenge::Ptr& authChallenge);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
	QNetworkReply* followRedirect(MockReply* prevReply, const Request& prevReq);
#endif // Qt >= 5.6.0
#if (QT_VERSION >= QT_VERSION_CHECK(5, 9, 0))
	bool applyRedirectPolicy(QNetworkRequest::RedirectPolicy policy, MockReply* prevReply, const QNetworkRequest& prevRequest, const QUrl& redirectTarget);
	void prepareHstsHash();
	bool elevateHstsUrl(const QUrl& url);
#endif // Qt >= 5.9.0
	void prepareReply(MockReply* reply, const Request& request) const;
	void finishReply(QNetworkReply* reply, const Request& initialRequest) const;


	bool m_inspectBody;
	BehaviorFlags m_behaviorFlags;
	QPointer<QNetworkAccessManager> m_passThroughNam;
	QVector<Rule::Ptr> m_rules;
	QVector<Request> m_receivedRequests;
	QVector<Request> m_handledRequests;
	QVector<Request> m_matchedRequests;
	QVector<Request> m_unmatchedRequests;
	QVector<Request> m_passedThroughRequests;
	mutable QScopedPointer<SignalEmitter> m_signalEmitter;
	UnmatchedRequestBehavior m_unmatchedRequestBehavior;
	MockReplyBuilder m_unmatchedRequestFactory;
	QScopedPointer<detail::ReplyHeaderHandler> m_replyHeaderHandler;
	QHash<QString, QAuthenticator> m_authenticationCache;
#if (QT_VERSION >= QT_VERSION_CHECK(5, 9, 0))
	QScopedPointer<QHash<QString, QHstsPolicy> > m_hstsHash;
#endif // Qt >= 5.9.0
};

/*! \internal Implementation details.
 */
namespace detail {

inline const char* FOLLOWED_REDIRECTS_PROPERTY()
{
	static const char* followedRedirectsProperty = "MockNetworkAccess::FollowedRedirects";
	return followedRedirectsProperty;
}

} // namespace detail


//####### Implementation #######

template<class Matcher>
Rule* Rule::isMatching(const Matcher& matcher)
{
	m_predicates.append(Predicates::createGeneric(matcher));
	return this;
}

template<class Matcher>
Rule* Rule::isNotMatching(const Matcher& matcher)
{
	Predicate::Ptr predicate = Predicates::createGeneric(matcher);
	predicate->negate();
	m_predicates.append(predicate);
	return this;
}


template<class Base>
QNetworkReply* Manager<Base>::createRequest(QNetworkAccessManager::Operation operation, const QNetworkRequest& origRequest, QIODevice* ioDevice)
{
	QByteArray data;
	if (m_inspectBody && ioDevice)
		data = ioDevice->readAll();
	const QNetworkRequest preparedRequest = prepareRequest(origRequest);
	const Request request(operation, preparedRequest, data);

	m_receivedRequests.append(request);
	if (m_signalEmitter)
		Q_EMIT m_signalEmitter->receivedRequest(request);

	QNetworkReply* reply = handleRequest(request);
	finishReply(reply, request);
	return reply;
}

template<class Base>
QNetworkReply* Manager<Base>::handleRequest( const Request& request )
{
	m_handledRequests.append( request );
	if ( m_signalEmitter )
		Q_EMIT m_signalEmitter->handledRequest( request );

	QScopedPointer<MockReply> mockedReply;
	QVector<Rule::Ptr>::iterator iter;
	for ( iter = m_rules.begin(); iter != m_rules.end(); ++iter )
	{
		Rule::Ptr rule = *iter;
		if ( rule->matches( request ) )
		{
			if ( rule->m_passThroughBehavior != Rule::PassThroughReturnDelegatedReply )
			{
				mockedReply.reset( rule->createReply( request ) );
				if ( !mockedReply )
					continue;
			}

			if ( rule->m_passThroughBehavior != Rule::DontPassThrough )
			{
				QScopedPointer<QNetworkReply> passThroughReply( passThrough( request, rule->passThroughManager() ) );
				switch ( rule->m_passThroughBehavior )
				{
				case Rule::PassThroughReturnMockReply:
					QObject::connect( passThroughReply.data(), SIGNAL( finished() ), passThroughReply.data(), SLOT( deleteLater() ) );
					passThroughReply.take();
					break;
				case Rule::PassThroughReturnDelegatedReply:
					m_matchedRequests.append( request );
					rule->m_matchedRequests.append( request );
					if ( m_signalEmitter )
						Q_EMIT m_signalEmitter->matchedRequest( request, rule );
					return passThroughReply.take();
				// LCOV_EXCL_START
				default:
					Q_ASSERT_X(false, Q_FUNC_INFO, "MockNetworkAccessManager: Internal error: Unknown Rule::PassThroughBehavior");
					break;
				// LCOV_EXCL_STOP
				}
			}

			prepareReply( mockedReply.data(), request );
			m_matchedRequests.append( request );
			rule->m_matchedRequests.append( request );
			if ( m_signalEmitter )
				Q_EMIT m_signalEmitter->matchedRequest( request, rule );


			if ( mockedReply->requiresAuthentication() )
			{
				QScopedPointer<QNetworkReply> authedReply( authenticateRequest( mockedReply.data(), request ) ); // POTENTIAL RECURSION
				if ( authedReply ) // Did we start a new, authenticated request?
					return authedReply.take();
			}

		#if ( QT_VERSION >= QT_VERSION_CHECK( 5, 6, 0 ) )
			if ( mockedReply->isRedirectToBeFollowed() )
			{
				QScopedPointer<QNetworkReply> redirectedReply( followRedirect( mockedReply.data(), request ) ); // POTENTIAL RECURSION
				if ( redirectedReply ) // Did we actually redirect?
					return redirectedReply.take();
			}
		#endif // Qt >= 5.6.0

			break;
		}
	}

	if ( mockedReply )
	{
		return mockedReply.take();
	}
	else
	{
		m_unmatchedRequests.append( request );
		if ( m_signalEmitter )
			Q_EMIT m_signalEmitter->unmatchedRequest( request );
		switch ( m_unmatchedRequestBehavior )
		{
		case PredefinedReply:
			return m_unmatchedRequestFactory.createReply();
		case PassThrough:
			return passThrough( request );
		// LCOV_EXCL_START
		default:
			Q_ASSERT_X( false, Q_FUNC_INFO, QStringLiteral( "MockNetworkAccessManager: Unknown behavior for unmatched request: %1" ).arg( static_cast<int>( m_unmatchedRequestBehavior ) ).toLatin1().constData() );
			return Q_NULLPTR;
		// LCOV_EXCL_STOP
		}
	}
}

#if (QT_VERSION >= QT_VERSION_CHECK(5, 9, 0))

template<class Base>
void Manager<Base>::prepareHstsHash()
{
	if (!m_hstsHash)
	{
		m_hstsHash.reset(new QHash<QString, QHstsPolicy>());
		QVector<QHstsPolicy> hstsPolicies = this->strictTransportSecurityHosts();

		QVector<QHstsPolicy>::const_iterator policyIter;
		for (policyIter = hstsPolicies.cbegin(); policyIter != hstsPolicies.cend(); ++policyIter)
		{
			if (!policyIter->isExpired())
				m_hstsHash->insert(policyIter->host(), *policyIter);
		}
	}
}

template<class Base>
bool Manager<Base>::elevateHstsUrl(const QUrl& url)
{
	if ( ! url.isValid() || url.scheme().toLower() != HttpUtils::httpScheme() )
		return false;

	QString host = url.host();
	static const QRegularExpression ipAddressRegEx( "^\\[.*\\]$|^((25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)$" );
	if ( ipAddressRegEx.match( host ).hasMatch() )
		return false; // Don't elevate IP address URLs

	prepareHstsHash();

	// Check if there is a policy for the full host name
	QHash<QString, QHstsPolicy>::Iterator hstsHashIter = m_hstsHash->find( host );

	if ( hstsHashIter != m_hstsHash->end() )
	{
		if ( hstsHashIter.value().isExpired() )
			hstsHashIter = m_hstsHash->erase( hstsHashIter );
		else
			return true;
	}

	// Check if there is a policy for a parent domain
	QStringList domainParts = host.split( QChar( '.' ), QString::SkipEmptyParts );
	domainParts.pop_front();

	while (!domainParts.isEmpty())
	{
		hstsHashIter = m_hstsHash->find( domainParts.join( QChar('.') ) );
		if ( hstsHashIter != m_hstsHash->end() )
		{
			if ( hstsHashIter.value().isExpired() )
				hstsHashIter = m_hstsHash->erase( hstsHashIter );
			else if ( hstsHashIter.value().includesSubDomains() )
				return true;
			// else we continue because there could be a policy for a another parent domain that includes sub domains
		}
		domainParts.pop_front();
	}

	return false;
}

#endif // Qt >= 5.9.0


template<class Base>
QNetworkRequest Manager<Base>::prepareRequest(const QNetworkRequest& origRequest)
{
	QNetworkRequest request(origRequest);

	#if (QT_VERSION >= QT_VERSION_CHECK(5, 9, 0))
		if (this->isStrictTransportSecurityEnabled() && elevateHstsUrl(request.url()))
		{
			QUrl url = request.url();
			url.setScheme(HttpUtils::httpsScheme());
			if (url.port() == 80)
				url.setPort(443);
			request.setUrl(url);
		}
	#endif // Qt >= 5.9.0

	if (static_cast<QNetworkRequest::LoadControl>(request.attribute(QNetworkRequest::CookieLoadControlAttribute, QVariant::fromValue(static_cast<int>(QNetworkRequest::Automatic))).toInt()) == QNetworkRequest::Automatic)
	{
		QNetworkCookieJar* cookieJar = this->cookieJar();
		if (cookieJar)
		{
			QUrl requestUrl = request.url();
			if (requestUrl.path().isEmpty())
				requestUrl.setPath("/");
			QList<QNetworkCookie> cookies = cookieJar->cookiesForUrl(requestUrl);
			if (!cookies.isEmpty())
				request.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(cookies));
		}
	}

	return request;
}

template<class Base>
void Manager<Base>::prepareReply(MockReply* reply, const Request& request) const
{
	reply->setBehaviorFlags(m_behaviorFlags);
	reply->prepare(request);
}

template<class Base>
void Manager<Base>::finishReply(QNetworkReply* reply, const Request& initialRequest) const
{
	// Do we want to read out the headers synchronously for mocked replies?
	MockReply* mockedReply = qobject_cast<MockReply*>(reply);
	if (mockedReply)
		m_replyHeaderHandler->handleReplyHeaders(reply);
	else
		QObject::connect(reply, SIGNAL(metaDataChanged()), m_replyHeaderHandler.data(), SLOT(handleReplyHeaders()));

	const RequestList followedRedirects = reply->property(detail::FOLLOWED_REDIRECTS_PROPERTY()).value<RequestList>();
	/* In case of a real QNetworkReply, we simulate the mocked redirects on the real reply.
	 * This would not work with file: or data: URLs since their real signals would have already been emitted.
	 * But automatic redirection works only for http: and https: anyway so this is not a problem.
	 */
	RequestList::const_iterator redirectIter;
	for (redirectIter = followedRedirects.cbegin(); redirectIter != followedRedirects.cend(); ++redirectIter)
	{
		const qint64 bodySize = redirectIter->body.size();
		if (bodySize > 0)
			QMetaObject::invokeMethod(reply, "uploadProgress", Qt::QueuedConnection, Q_ARG(qint64, bodySize), Q_ARG(qint64, bodySize));
		QMetaObject::invokeMethod(reply, "metaDataChanged", Qt::QueuedConnection);
		QMetaObject::invokeMethod(reply, "redirected", Qt::QueuedConnection, Q_ARG(QUrl, redirectIter->qRequest.url()));
	}
	reply->setProperty(detail::FOLLOWED_REDIRECTS_PROPERTY(), QVariant());

	if ( mockedReply )
	{
		if ( ! followedRedirects.isEmpty() )
			mockedReply->finish( followedRedirects.last() );
		else
			mockedReply->finish( initialRequest );
	}
}

template<class Base>
QIODevice* Manager<Base>::createIODevice(const QByteArray& data) const
{
	QBuffer* buffer = Q_NULLPTR;
	if(m_inspectBody)
	{
		if (!data.isNull())
		{
			buffer = new QBuffer();
			buffer->setData(data);
			buffer->open(QIODevice::ReadOnly);
		}
	}
	return buffer;
}

template<class Base>
QNetworkReply* Manager<Base>::passThrough( const Request& request, QNetworkAccessManager* overridePassThroughNam )
{
	QScopedPointer<QIODevice> ioDevice( createIODevice( request.body ) );

	QNetworkAccessManager* passThroughNam = overridePassThroughNam ? overridePassThroughNam : static_cast<QNetworkAccessManager*>( m_passThroughNam );
	QNetworkReply* reply;
	if ( passThroughNam )
	{
		switch ( request.operation )
		{
		case QNetworkAccessManager::GetOperation:    reply = passThroughNam->get( request.qRequest ); break;
		case QNetworkAccessManager::PostOperation:   reply = passThroughNam->post( request.qRequest, ioDevice.data() ); break;
		case QNetworkAccessManager::PutOperation:    reply = passThroughNam->put( request.qRequest, ioDevice.data() ); break;
		case QNetworkAccessManager::HeadOperation:   reply = passThroughNam->head( request.qRequest ); break;
		case QNetworkAccessManager::DeleteOperation: reply = passThroughNam->deleteResource( request.qRequest ); break;
		case QNetworkAccessManager::CustomOperation:
		default:
			reply = passThroughNam->sendCustomRequest( request.qRequest, request.qRequest.attribute( QNetworkRequest::CustomVerbAttribute ).toByteArray(), ioDevice.data() ); break;
		}
	}
	else
		reply = Base::createRequest( request.operation, request.qRequest, ioDevice.data() );
	if ( ioDevice )
	{
		QObject::connect( reply, SIGNAL( finished() ), ioDevice.data(), SLOT( deleteLater() ) );
		ioDevice.take()->setParent( reply );
	}
	QObject::connect( reply, SIGNAL( metaDataChanged() ), m_replyHeaderHandler.data(), SLOT( handleReplyHeaders() ) );
	m_passedThroughRequests.append( request );
	if ( m_signalEmitter )
		Q_EMIT m_signalEmitter->passedThrough( request );
	return reply;
}

template<class Base>
QNetworkReply* Manager<Base>::authenticateRequest(MockReply* unauthedReply, const Request& unauthedReq)
{
	QVector<HttpUtils::Authentication::Challenge::Ptr> authChallenges = HttpUtils::Authentication::getAuthenticationChallenges(unauthedReply);

	if (authChallenges.isEmpty())
	{
		qWarning("MockNetworkAccessManager: Missing authentication challenge in reply %s", detail::pointerToQString(unauthedReply).toLatin1().data());
		return Q_NULLPTR;
	}

	/* Select the strongest challenge.
	 * If there are multiple challenges with the same stength,
	 * the last one is used according to the order they appear in the HTTP headers.
	 */
	std::stable_sort(authChallenges.begin(), authChallenges.end(), HttpUtils::Authentication::Challenge::StrengthCompare());
	HttpUtils::Authentication::Challenge::Ptr authChallenge = authChallenges.last();

	QAuthenticator authenticator = getAuthenticator(unauthedReply, unauthedReq, authChallenge);
	if (authenticator.user().isNull() && authenticator.password().isNull())
		return Q_NULLPTR;

	QNetworkRequest authedQReq(unauthedReq.qRequest);
	authChallenge->addAuthorization(authedQReq, unauthedReq.operation, unauthedReq.body, authenticator);
	const Request authedReq(unauthedReq.operation, authedQReq, unauthedReq.body);
	QNetworkReply* authedReply = this->handleRequest(authedReq); // POTENTIAL RECURSION
	return authedReply;
}

template<class Base>
QAuthenticator Manager<Base>::getAuthenticator(MockReply* unauthedReply, const Request& unauthedReq, const HttpUtils::Authentication::Challenge::Ptr& authChallenge)
{
	const QString realm = authChallenge->realm().toLower(); // realm is case-insensitive
	const QUrl authScope = HttpUtils::Authentication::authenticationScopeForUrl(unauthedReply->url());
	const QString authKey = realm + '\x1C' + authScope.toString(QUrl::FullyEncoded);
	const QNetworkRequest::LoadControl authReuse = static_cast<QNetworkRequest::LoadControl>(unauthedReq.qRequest.attribute(QNetworkRequest::AuthenticationReuseAttribute, static_cast<int>(QNetworkRequest::Automatic)).toInt());

	if (authReuse == QNetworkRequest::Automatic && m_authenticationCache.contains(authKey))
		return m_authenticationCache.value(authKey);
	else
	{
		QAuthenticator authenticator;
		authenticator.setOption(HttpUtils::Authentication::Basic::realmKey(), realm);
		#if QT_VERSION >= QT_VERSION_CHECK(5,4,0)
			authenticator.setRealm(realm);
		#endif // Qt >= 5.4.0
		Q_EMIT this->authenticationRequired(unauthedReply, &authenticator);
		if (!authenticator.user().isNull() || !authenticator.password().isNull())
			m_authenticationCache.insert(authKey, authenticator);
		return authenticator;
	}
}


#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)

/*! \internal Implementation details
 */
namespace detail
{
/*! Checks if a redirect would cause a security degradation.
 * \param from The URL from which the request is redirected.
 * \param to The target URL of the redirect.
 * \return \c true if a redirect from \p from to \p to degrades protocol security (for example, HTTPS to HTTP).
 */
inline bool secureToUnsecureRedirect(const QUrl& from, const QUrl& to)
{
	return from.scheme().toLower() == HttpUtils::httpsScheme() && to.scheme().toLower() == HttpUtils::httpScheme();
}
}

template<class Base>
QNetworkReply* Manager<Base>::followRedirect(MockReply* prevReply, const Request& prevReq)
{
	using namespace detail;

	const QUrl prevTarget = prevReq.qRequest.url();
	const QUrl nextTarget = prevTarget.resolved( prevReply->locationHeader() );
	const QString nextTargetScheme = nextTarget.scheme().toLower();
	const QVariant statusCodeAttr = prevReply->attribute(QNetworkRequest::HttpStatusCodeAttribute);

	if (!nextTarget.isValid() || (nextTargetScheme != HttpUtils::httpScheme() && nextTargetScheme != HttpUtils::httpsScheme()))
	{
		prevReply->setError(QNetworkReply::ProtocolUnknownError);
		prevReply->setAttribute(QNetworkRequest::RedirectionTargetAttribute, QVariant());
		return Q_NULLPTR;
	}

	QVariant followRedirectsAttr = prevReq.qRequest.attribute(QNetworkRequest::FollowRedirectsAttribute);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 9, 0))
	const QVariant redirectPolicyAttr = prevReq.qRequest.attribute(QNetworkRequest::RedirectPolicyAttribute);
	if (redirectPolicyAttr.isValid())
	{
		QNetworkRequest::RedirectPolicy redirectPolicy = static_cast<QNetworkRequest::RedirectPolicy>(redirectPolicyAttr.toInt());
		if (!applyRedirectPolicy(redirectPolicy, prevReply, prevReq.qRequest, nextTarget))
			return Q_NULLPTR;
	}
	else
#endif // Qt >= 5.9.0
	if (followRedirectsAttr.isValid())
	{
		if (!followRedirectsAttr.toBool())
			return Q_NULLPTR;

		if (detail::secureToUnsecureRedirect(prevTarget, nextTarget))
		{
			prevReply->setError(QNetworkReply::InsecureRedirectError);
			return Q_NULLPTR;
		}
	}
	else
	{
		#if (QT_VERSION >= QT_VERSION_CHECK(5, 9, 0))
			if (!applyRedirectPolicy(this->redirectPolicy(), prevReply, prevReq.qRequest, nextTarget))
				return Q_NULLPTR;
		#else // Qt < 5.9.0
			// Following the redirect is not requested
			return Q_NULLPTR;
		#endif // Qt >= 5.9.0
	}


	if (prevReq.qRequest.maximumRedirectsAllowed() <= 0)
	{
		prevReply->setError(QNetworkReply::TooManyRedirectsError);
		return Q_NULLPTR;
	}

	QNetworkAccessManager::Operation nextOperation;
	QByteArray nextReqBody;
	if (   prevReq.operation == QNetworkAccessManager::GetOperation
	    || prevReq.operation == QNetworkAccessManager::HeadOperation)
		nextOperation = prevReq.operation;
	else if (m_behaviorFlags.testFlag(Behavior_RedirectWithGet))
		// Qt up to 5.9.3 always redirects with a GET
		nextOperation = QNetworkAccessManager::GetOperation;
	else
	{
		nextOperation = prevReq.operation;
		nextReqBody = prevReq.body;

		switch (static_cast<HttpStatus::Code>(statusCodeAttr.toInt()))
		{
		case HttpStatus::TemporaryRedirect: // 307
		case HttpStatus::PermanentRedirect: // 308
			break;
		case HttpStatus::MovedPermanently:  // 301
		case HttpStatus::Found:             // 302
		{
			/* This is the behavior of most browsers and clients.
			 * RFC-7231 defines the request methods GET, HEAD, OPTIONS, and TRACE to be safe
			 * for automatic redirection. GET and HEAD are already handled above.
			 * See https://tools.ietf.org/html/rfc7231#section-6.4
			 * and https://tools.ietf.org/html/rfc7231#section-4.2.1
			 */
			const QString customVerb = prevReq.qRequest.attribute(QNetworkRequest::CustomVerbAttribute).toString().toLower();
			if ( !m_behaviorFlags.testFlag(Behavior_IgnoreSafeRedirectMethods)
				&& prevReq.operation == QNetworkAccessManager::CustomOperation
				&& (customVerb == "options" || customVerb == "trace") )
			{
				break;
			}
			// Fall through
		}
		case HttpStatus::SeeOther:          // 303
		default:
			nextOperation = QNetworkAccessManager::GetOperation;
			nextReqBody.clear();
			break;
		}

	}


	QNetworkRequest nextQReq(prevReq.qRequest);
	nextQReq.setUrl(nextTarget);
	nextQReq.setMaximumRedirectsAllowed(prevReq.qRequest.maximumRedirectsAllowed() - 1);
	if (nextOperation != QNetworkAccessManager::CustomOperation)
		nextQReq.setAttribute(QNetworkRequest::CustomVerbAttribute, QVariant());

	Request nextReq(nextOperation, nextQReq, nextReqBody);
	QNetworkReply* redirectReply = this->handleRequest(nextReq); // POTENTIAL RECURSION

	RequestList followedRedirects = redirectReply->property(FOLLOWED_REDIRECTS_PROPERTY()).value<RequestList>();
	followedRedirects.prepend(nextReq);
	redirectReply->setProperty(FOLLOWED_REDIRECTS_PROPERTY(), QVariant::fromValue(followedRedirects));

	MockReply* mockedReply = qobject_cast<MockReply*>(redirectReply);
	if (mockedReply)
		mockedReply->setUrl(nextQReq.url());

	return redirectReply;
}

#endif // Qt >= 5.6.0


#if (QT_VERSION >= QT_VERSION_CHECK(5, 9, 0))

template<class Base>
bool Manager<Base>::applyRedirectPolicy(QNetworkRequest::RedirectPolicy policy, MockReply* prevReply, const QNetworkRequest& prevRequest, const QUrl& redirectTarget)
{
	const QUrl prevTarget = prevRequest.url();
	switch (policy)
	{
	case QNetworkRequest::ManualRedirectPolicy:
		return false;
	case QNetworkRequest::NoLessSafeRedirectPolicy:
	{
		if (detail::secureToUnsecureRedirect(prevTarget, redirectTarget))
		{
			prevReply->setError(QNetworkReply::InsecureRedirectError);
			return false;
		}
		break;
	}
	case QNetworkRequest::SameOriginRedirectPolicy:
		if (   prevTarget.scheme() != redirectTarget.scheme()
			|| prevTarget.host()   != redirectTarget.host()
			|| prevTarget.port()   != redirectTarget.port())
		{
			prevReply->setError(QNetworkReply::InsecureRedirectError);
			return false;
		}
		break;
	case QNetworkRequest::UserVerifiedRedirectPolicy:
		// TODO: QNetworkRequest::UserVerifiedRedirectPolicy
		/* Does that even make sense? We would probably need to make the limitation that the
		 * QNetworkReply::redirectAllowed() signal must be emitted synchronously inside the slot handling
		 * the QNetworkReply::redirected() signal.
		 * Or we would need to return a proxy QNetworkReply from the Manager which is then "filled" and "finished" with
		 * either a MockReply or a real QNetworkReply after the redirection(s).
		 */
		qWarning("MockNetworkAccessManager: User verified redirection policy is not supported at the moment");
		prevReply->setError(QNetworkReply::InsecureRedirectError);
		return false;
		break;
	// LCOV_EXCL_START
	default:
		qWarning("MockNetworkAccessManager: Unknown redirect policy: %i", static_cast<int>(policy));
		prevReply->setError(QNetworkReply::InsecureRedirectError);
		return false;
	// LCOV_EXCL_STOP
	}

	return true;
}

#endif // Qt >= 5.9.0


} // namespace MockNetworkAccess

Q_DECLARE_METATYPE(MockNetworkAccess::HttpStatus::Code)
Q_DECLARE_OPERATORS_FOR_FLAGS(MockNetworkAccess::BehaviorFlags)
Q_DECLARE_METATYPE(MockNetworkAccess::RequestList)


#endif /* MOCKNETWORKACCESSMANAGER_HPP */
