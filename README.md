# MockNetworkAccessManager #

> Mocking network communication for Qt applications

[![Linux build status](https://gitlab.com/julrich/MockNetworkAccessManager/badges/master/build.svg)](https://gitlab.com/julrich/MockNetworkAccessManager/pipelines?scope=branches)
[![Windows build status](https://ci.appveyor.com/api/projects/status/m0lbburoi1yve1oc/branch/master?svg=true)](https://ci.appveyor.com/project/j-ulrich/mocknetworkaccessmanager/branch/master)
[![Test coverage report](https://julrich.gitlab.io/MockNetworkAccessManager/coverage/test-coverage.svg)](https://julrich.gitlab.io/MockNetworkAccessManager/coverage/)
[![Documentation coverage report](https://julrich.gitlab.io/MockNetworkAccessManager/doc-coverage/doc-coverage.svg)](https://julrich.gitlab.io/MockNetworkAccessManager/doc-coverage/)
[![SonarCloud status](https://sonarcloud.io/api/project_badges/measure?project=julrich%3AMockNetworkAccessManager&amp;metric=alert_status)](https://sonarcloud.io/dashboard?id=julrich%3AMockNetworkAccessManager)

[![Doxygen documentation](https://img.shields.io/badge/docs-Doxygen-blue.svg)](https://julrich.gitlab.io/MockNetworkAccessManager/docs/)


## Introduction ##

Dependencies to external services make unit tests fragile and increase the setup effort of the test environments since it must be ensured the service is reachable for the tests and provides the expected (test) data.

Mocking or faking external services using third party software like proxies or local mock/fake servers helps  but doesn't solve those issues completely. It can even increase the setup effort of  the test environment or make the tests slower due to waits and timeouts.

MockNetworkAccessManager solves these issues by mocking the QNetworkAccessManager in the unit tests and thus, mocking out the network communication completely.


## Features ##

* Mock QNetworkAccessManager and QNetworkReply including headers, attributes and body
* Provide response bodies using static strings or files
* Flexibly match requests with the help of [over 20 predicates](https://julrich.gitlab.io/MockNetworkAccessManager/docs/namespace_mock_network_access_1_1_predicates.html)
* Pass requests through to a real QNetworkAccessManager or another MockNetworkAccess::Manager
* Emits signals when a request was received, handled, matched, did not match or was passed through
* Records all received requests
* Easy to include since it's a single-header-file library
* Simulates HTTP Basic authentication
* Supports automatic redirection handling (requires Qt 5.6 or later)
* Simulates HTTP Strict Transport Security (HSTS) (requires Qt 5.9 or later)
* Extendable with custom predicates and customizable reply generation


## Example ##

```cpp
#include "MyNetworkClient.hpp"
#include "MockNetworkAccessManager.hpp"
#include <QtTest>

class MyNetworkClientTest : public QObject
{
	Q_OBJECT

private slots:
	void testFetchHello()
	{
		// Create and configure the mocked network access manager
		MockNetworkAccess::Manager<QNetworkAccessManager> mnam;

		mnam.whenGet(QUrl("http://example.com/hello"))
		    ->has(MockNetworkAccess::Predicates::HeaderMatching(QNetworkRequest::UserAgentHeader,
		                                                        QRegularExpression(".*MyNetworkClient/.*")))
		    ->reply()->withJson(QJsonDocument::fromJson("{\"hello\":\"world\"}"));

		// Call the method under test
		MyNetworkClient client;
		client.setNetworkAccessManager(&mnam);
		client.fetchHello();

		QTest::qWait(1);

		// Verify expectations
		QCOMPARE(mnam.matchedRequests().length(), 1);
		QCOMPARE(client.hello(), QString("world"));
	}
};
```

(The source code of `MyNetworkClient.hpp` can be found in the [tests directory](tests/MyNetworkClient.hpp))


## Limitations ##

The `MockNetworkAccess::Manager` is intended to be used in unit tests.
It is written to be easy to use and **not** to be secure or performant.
Therefore, it should not be used as a replacement for a web server in a production environment.

Additionally, there are some feature limitations:
- It is not possible to mock a reply of a redirected request which has automatic redirect following
  configured and that was passed through to a real QNetworkAccessManager.   
  So if a request with automatic redirect following enabled gets passed through to a real QNetworkAccessManager
  and the reply is a redirect to a URL that would normally return a mocked reply, then the redirect still goes to the
  network because it is handled by the real QNetworkAccessManager internally (see issue [#15]).
- When a request is redirected and then passed through to a separate QNetworkAccessManager
  (see MockNetworkAccess::Manager::setPassThroughNam()), the `QNetworkReply::metaDataChanged()` and
  `QNetworkReply::redirected()` signals of the mocked redirections are emitted out of order (namely after all
  other signals).
- The mocked replies do not emit the implementation specific signals of a real HTTP based QNetworkReply
  (that is the signals of QNetworkReplyHttpImpl).
- Only HTTP Basic authentication support is built-in. However, this should not be a problem in most cases since the
  handling of authentication is normally done internally between the `MockNetworkAccess::Manager` and the
  `MockReply`.   
  This only limits you if you manually create Authorization headers and have to rely on HTTP Digest or NTLM
  authentication.
- The QAuthenticator passed in the `QNetworkAccessManager::authenticationRequired()` signal does not provide the `realm`
  parameter via the `QAuthenticator::realm()` method in Qt before 5.4.0 but only as option with the key `realm`
  (for example, via `authenticator->option("realm")`).
- Proxy authentication is not supported at the moment.
- `QNetworkRequest::UserVerifiedRedirectPolicy` is not supported at the moment.
- The error messages of the replies (`QNetworkReply::errorString()`) may be different from the ones of real
  QNetworkReply objects.
- `QNetworkReply::setReadBufferSize()` is ignored at the moment.

Some of these limitations might be removed in future versions. Feel free to send a feature request if you hit one
these limitations.

[#15]: https://gitlab.com/julrich/MockNetworkAccessManager/issues/15


## Requirements ##

* Qt 5.2 or later
* Possibility to "inject" a QNetworkAccessManager into the application under test


## Documentation ##

- [API Documentation](https://julrich.gitlab.io/MockNetworkAccessManager/docs/)
- [Changelog](CHANGELOG.md)


## License ##

Copyright (c) 2018-2019 Jochen Ulrich

Licensed under [MIT license](LICENSE).

### Third Party Licenses ###
The tests are using the [Hippomocks](https://github.com/dascandy/hippomocks) library under the [LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.html) license.
A copy of the Hippomocks library, the GPLv3 and the LGPLv3 can be found in the [tests directory](tests).
