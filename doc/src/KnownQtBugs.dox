namespace MockNetworkAccess {

/*!

\page page_knownQtBugs Known %Qt Bugs

Unfortunately, not all %Qt bugs can be worked around by the MockNetworkAccesManager library. So even if \ref Behavior_Expected is used,
there are still certain %Qt bugs that can be observed when using the MockNetworkAccess::Manager.

\note These bugs should only appear when a %Qt version is used which also shows the bug when using a real QNetworkAccessManager.
If this is not the case, then please [open a new issue] for the MockNetworkAccessManager library.

While some of these bugs could be worked around for \link MockReply MockReplies \endlink, this is typically not done when
the bugs cannot be worked around for real \link QNetworkReply QNetworkReplies \endlink as well since this would create
inconsistent behavior depending on whether a request was \ref page_passThrough "passed through" to a real QNetworkAccessManager
or not.



\section page_knownQtBugs_list Bug List

Here is a list of known %Qt bugs that are also present in the MockNetworkAccess::Manager:

- [QTBUG-41061](https://bugreports.qt.io/browse/QTBUG-41061): %QNetworkReply Location header retrieval can't handle Network-Path Reference URI



[open a new issue]: https://gitlab.com/julrich/MockNetworkAccessManager/issues/new

*/

} // namespace MockNetworkAccess