#include "MockNetworkAccessManager.hpp"
#include <QNetworkAccessManager>

int main(int argc, char *argv[])
{
	//! [main]

	using namespace MockNetworkAccess;

	Manager<QNetworkAccessManager> fallbackMockNam;
	fallbackMockNam.setUnmatchedRequestBuilder( *( MockReplyBuilder().withStatus( HttpStatus::BadRequest )->withBody( "Unexpected custom request" ) ) );
	fallbackMockNam.setUnmatchedRequestBehavior( PredefinedReply );

	Rule::Ptr unexpectedGet( new Rule() );
	unexpectedGet->has( Verb( QNetworkAccessManager::GetOperation ) )
	             ->reply()->withStatus( HttpStatus::BadRequest )
	                      ->withBody( "Unexpected GET request" );
	Rule::Ptr unexpectedPost( new Rule() );
	unexpectedPost->has( Verb( QNetworkAccessManager::PostOperation ) )
	              ->reply()->withStatus( HttpStatus::BadRequest )
	                       ->withBody( "Unexpected POST request" );
	Rule::Ptr unexpectedPut( new Rule() );
	unexpectedPost->has( Verb( QNetworkAccessManager::PutOperation ) )
	              ->reply()->withStatus( HttpStatus::BadRequest )
	                       ->withBody( "Unexpected PUT request" );
	Rule::Ptr unexpectedDelete( new Rule() );
	unexpectedPost->has( Verb( QNetworkAccessManager::DeleteOperation ) )
	              ->reply()->withStatus( HttpStatus::BadRequest )
	                       ->withBody( "Unexpected DELETE request" );

	fallbackMockNam.addRule( unexpectedGet );
	fallbackMockNam.addRule( unexpectedPost );
	fallbackMockNam.addRule( unexpectedPut );
	fallbackMockNam.addRule( unexpectedDelete );


	Manager<QNetworkAccessManager> mockNam;
	mockNam.setPassThroughNam( &fallbackMockNam );
	mockNam.setUnmatchedRequestBehavior( PassThrough );

	mockNam.whenGet( "http://example.com" )
	       ->reply()->withBody( "hello world!" );

	//! [main]

	return 0;
}
