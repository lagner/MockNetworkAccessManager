#include "MockNetworkAccessManager.hpp"
#include <QNetworkAccessManager>
#include <QPointer>
#include <QScopedPointer>
#include <QCoreApplication>
#include <iostream>

class MyNetworkClient : public QObject
{
	Q_OBJECT

public:
	void setNetworkAccessManager(QNetworkAccessManager* nam)
	{
		m_nam = nam;
	}

	void sendRequest()
	{
		m_reply = m_nam->get(QNetworkRequest(QUrl("http://example.com")));
		QObject::connect(m_reply.data(), SIGNAL(finished()), this, SLOT(printResponse()));
	}

signals:
	void finished() const;

private slots:
	void printResponse()
	{
		if (m_reply->error() == QNetworkReply::NoError)
			std::cout << m_reply->readAll().constData();
		m_reply.take()->deleteLater();
		emit finished();
	}

private:
	QPointer<QNetworkAccessManager> m_nam;
	QScopedPointer<QNetworkReply> m_reply;
};


int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);

	using namespace MockNetworkAccess;

	//! [Create Manager]

	MockNetworkAccess::Manager<QNetworkAccessManager> mockNam;

	//! [Create Manager]

	//! [Configure Manager]

	mockNam.whenGet(Predicates::Url("http://example.com"))
	       ->reply()->withBody("foo");

	//! [Configure Manager]

	//! [Inject Manager]

	MyNetworkClient client;
	client.setNetworkAccessManager(&mockNam);

	//! [Inject Manager]

	QObject::connect(&client, SIGNAL(finished()), &app, SLOT(quit()));

	//! [Execute]

	client.sendRequest();

	//! [Execute]

	const int returnCode = QCoreApplication::exec();

	//! [Evaluate]

	Q_ASSERT(mockNam.matchedRequests().length() == 1);

	//! [Evaluate]

	return returnCode;
}
