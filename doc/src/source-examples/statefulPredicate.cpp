#include "MockNetworkAccessManager.hpp"

//! [Predicate Definition]

using namespace MockNetworkAccess;

/* Matches every n-th request.
 */
class NthRequest : public Rule::Predicate
{
public:
	NthRequest(unsigned int matchInterval)
		: Rule::Predicate()
		, m_requestCounter(0)
		, m_matchInterval(matchInterval)
	{}

private:
	bool match(const Request&)
	{
		m_requestCounter += 1;
		if (m_requestCounter < m_matchInterval)
			return false;
		else
		{
			m_requestCounter = 0;
			return true;
		}
	}

	unsigned int m_requestCounter;
	unsigned int m_matchInterval;
};

//! [Predicate Definition]

int main(int argc, char *argv[])
{
	//! [Predicate Usage]

	Rule::Ptr overloadedServer(new Rule);
	overloadedServer->has(Predicates::UrlMatching(QRegularExpression("https?://example.com/?.*")))
	                ->has(NthRequest(3)) // the order of the predicates is important!
	                ->reply()->withStatus(HttpStatus::TooManyRequests);

	//! [Predicate Usage]

	return 0;
}
