
find_package(Qt5 COMPONENTS Core Network Test)
get_property(QT_IMPORTED_CONFIGURATIONS TARGET Qt5::Core PROPERTY IMPORTED_CONFIGURATIONS)
if ("DEBUG" IN_LIST QT_IMPORTED_CONFIGURATIONS)
	set_target_properties(Qt5::Test PROPERTIES MAP_IMPORTED_CONFIG_COVERAGE "DEBUG")
endif()

include_directories(${PROJECT_SOURCE_DIR})

# Disable inlining because it breaks HippoMocks
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
	set(COMPILE_OPTIONS "${COMPILE_OPTIONS} /Ob0")
else()
	set(COMPILE_OPTIONS "${COMPILE_OPTIONS} -fno-inline")
endif()

add_executable(RuleTest RuleTest.cpp "${PROJECT_SOURCE_DIR}/MockNetworkAccessManager.hpp" "QSignalInspector.hpp")
target_link_libraries(RuleTest Qt5::Core Qt5::Network Qt5::Test)
add_test(NAME RuleTest COMMAND RuleTest)

add_executable(PredicateTest PredicateTest.cpp "${PROJECT_SOURCE_DIR}/MockNetworkAccessManager.hpp" "QSignalInspector.hpp")
target_link_libraries(PredicateTest Qt5::Core Qt5::Network Qt5::Test)
add_test(NAME PredicateTest COMMAND PredicateTest)

add_executable(HttpUtilsTest HttpUtilsTest.cpp "${PROJECT_SOURCE_DIR}/MockNetworkAccessManager.hpp" "QSignalInspector.hpp")
target_link_libraries(HttpUtilsTest Qt5::Core Qt5::Network Qt5::Test)
add_test(NAME HttpUtilsTest COMMAND HttpUtilsTest)

add_executable(HttpAuthenticationTest HttpAuthenticationTest.cpp "${PROJECT_SOURCE_DIR}/MockNetworkAccessManager.hpp" "QSignalInspector.hpp")
target_link_libraries(HttpAuthenticationTest Qt5::Core Qt5::Network Qt5::Test)
add_test(NAME HttpAuthenticationTest COMMAND HttpAuthenticationTest)

add_executable(MockReplyTest MockReplyTest.cpp "${PROJECT_SOURCE_DIR}/MockNetworkAccessManager.hpp" "QSignalInspector.hpp")
target_link_libraries(MockReplyTest Qt5::Core Qt5::Network Qt5::Test)
add_test(NAME MockReplyTest COMMAND MockReplyTest)

add_executable(ManagerTest ManagerTest.cpp "${PROJECT_SOURCE_DIR}/MockNetworkAccessManager.hpp" "QSignalInspector.hpp")
target_link_libraries(ManagerTest Qt5::Core Qt5::Network Qt5::Test)
add_test(NAME ManagerTest COMMAND ManagerTest)

add_executable(VersionNumberTest VersionNumberTest.cpp "${PROJECT_SOURCE_DIR}/MockNetworkAccessManager.hpp" "QSignalInspector.hpp")
target_link_libraries(VersionNumberTest Qt5::Core Qt5::Network Qt5::Test)
add_test(NAME VersionNumberTest COMMAND VersionNumberTest)

add_executable(MyNetworkClientTest MyNetworkClientTest.cpp "${PROJECT_SOURCE_DIR}/MockNetworkAccessManager.hpp" "MyNetworkClient.hpp")
target_link_libraries(MyNetworkClientTest Qt5::Core Qt5::Network Qt5::Test)
add_test(NAME MyNetworkClientTest COMMAND MyNetworkClientTest)

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++98" COMPILER_SUPPORTS_CXX98)
if(COMPILER_SUPPORTS_CXX98)
	target_compile_options(MyNetworkClientTest PRIVATE -std=c++98)
endif()

