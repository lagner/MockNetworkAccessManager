#include "MockNetworkAccessManager.hpp"

#include <QtTest>

namespace Tests {

/*! Implements unit tests for the methods and classes from the HttpUtils::Authentication namespace.
 */
class HttpAuthenticationTest : public QObject
{
	Q_OBJECT

private Q_SLOTS:

	void testBasicAuthParameterParsing_data();
	void testBasicAuthParameterParsing();
	void testBasicAuthAuthenticateHeader_data();
	void testBasicAuthAuthenticateHeader();
	void testBasicAuthVerifyAuth_data();
	void testBasicAuthVerifyAuth();

	void testAuthenticationScopeForUrl_data();
	void testAuthenticationScopeForUrl();
	void testGetAuthenticationChallenges_data();
	void testGetAuthenticationChallenges();

};

//####### Helpers #######

using namespace MockNetworkAccess;
using namespace MockNetworkAccess::HttpUtils::Authentication;

/*! Dummy QNetworkReply.
 * Helper class for the HttpAuthenticationTest.
 */
class DummyReply : public QNetworkReply
{
	Q_OBJECT

	friend class HttpAuthenticationTest;

public:
	typedef QSharedPointer<DummyReply> Ptr;

	/*! Creates a DummyReply object.
	 * \param parent Parent QObject.
	 */
	DummyReply(QObject* parent = Q_NULLPTR) : QNetworkReply(parent) {}

	static DummyReply::Ptr createAuthChallengeReply(const QString& authHeader)
	{
		DummyReply::Ptr reply(new DummyReply);
		reply->setAttribute(QNetworkRequest::HttpStatusCodeAttribute, static_cast<int>(HttpStatus::Unauthorized));
		reply->setRawHeader(HttpUtils::wwwAuthenticateHeader(), authHeader.toLatin1());
		return reply;
	}

	static DummyReply::Ptr createProxyAuthChallengeReply(const QString& authHeader)
	{
		DummyReply::Ptr reply(new DummyReply);
		reply->setAttribute(QNetworkRequest::HttpStatusCodeAttribute, static_cast<int>(HttpStatus::ProxyAuthenticationRequired));
		reply->setRawHeader(HttpUtils::proxyAuthenticateHeader(), authHeader.toLatin1());
		return reply;
	}


public slots:
	virtual void abort() {}

protected:
	virtual qint64 readData(char*, qint64) { return -1; }

};

/*! Helper struct to compare MockNetworkAccess::HttpUtils::Authentication::Challenge objects
 */
struct AuthChallengeSpec
{
	Challenge::AuthenticationScheme scheme;
	QVariantMap parameters;

	AuthChallengeSpec() : scheme(Challenge::UnknownAuthenticationScheme) {}
	AuthChallengeSpec(Challenge::AuthenticationScheme scheme, QVariantMap parameters) : scheme(scheme), parameters(parameters) {}
	AuthChallengeSpec(Challenge::Ptr challenge) : scheme(challenge->scheme()), parameters(challenge->parameters()) {}

	friend bool operator==(const AuthChallengeSpec& left, const AuthChallengeSpec& right)
	{
		return left.scheme == right.scheme
		    && left.parameters == right.parameters;
	}

	friend bool operator!=(const AuthChallengeSpec& left, const AuthChallengeSpec& right)
	{
		return !(left == right);
	}
};

typedef QVector<AuthChallengeSpec> AuthChallengeVector;
typedef QPair<QString, QString> StringPair;


} // namespace Tests


Q_DECLARE_METATYPE(Tests::DummyReply::Ptr)
Q_DECLARE_METATYPE(Tests::AuthChallengeVector)
Q_DECLARE_METATYPE(Tests::StringPair)
Q_DECLARE_METATYPE(MockNetworkAccess::HttpUtils::Authentication::Challenge::Ptr)


namespace Tests
{


const QString defaultRealm("world");
const QString utf8Charset("utf-8");


//####### Tests #######

/*! Provides the data for the testBasicAuthParameterParsing() test.
 */
void HttpAuthenticationTest::testBasicAuthParameterParsing_data()
{
	QTest::addColumn<QStringList>("parameters");
	QTest::addColumn<bool>("valid");
	QTest::addColumn<QVariantMap>("parsedParameters");

	QVariantMap parsedParameters;

	QVariantMap invalidChallengeParameters;
	invalidChallengeParameters.insert(Challenge::realmKey(), QString());

	parsedParameters.clear();
	parsedParameters.insert(Challenge::realmKey(), defaultRealm);
	QTest::newRow("simple challenge") << (QStringList() << "realm=world") << true << parsedParameters;

	parsedParameters.clear();
	parsedParameters.insert(Challenge::realmKey(), defaultRealm);
	QTest::newRow("quoted string") << (QStringList() << "realm=\"world\"") << true << parsedParameters;

	parsedParameters.clear();
	parsedParameters.insert(Challenge::realmKey(), defaultRealm);
	parsedParameters.insert(Basic::charsetKey(), utf8Charset);
	QTest::newRow("charset parameter") << (QStringList() << "realm=\"world\"" << "charset=utf-8") << true << parsedParameters;

	parsedParameters.clear();
	parsedParameters.insert(Challenge::realmKey(), "second");
	QTest::newRow("duplicate parameters") << (QStringList() << "realm=\"world\"" << "realm=\"second\"") << true << parsedParameters;

	QTest::newRow("empty parameter list") << QStringList()                      << false << invalidChallengeParameters;
	QTest::newRow("missing realm")        << (QStringList() << "charset=utf-8") << false << invalidChallengeParameters;
}

/*! \test Tests the parameter parsing of the HttpUtils::Authenticate::Basic class.
 */
void HttpAuthenticationTest::testBasicAuthParameterParsing()
{
	QFETCH(QStringList, parameters);

	QScopedPointer<Basic> challenge(new Basic(parameters));

	QTEST(challenge->isValid(), "valid");
	QTEST(challenge->parameters(), "parsedParameters");
}

/*! Provides the data for the testBasicAuthAuthenticateHeader() test.
 */
void HttpAuthenticationTest::testBasicAuthAuthenticateHeader_data()
{
	QTest::addColumn<QVariantMap>("parameters");
	QTest::addColumn<QString>("authenticateHeader");

	QVariantMap parameters;

	parameters.clear();
	parameters.insert(Challenge::realmKey(), defaultRealm);
	QTest::newRow("simple challenge") << parameters << "Basic realm=\"world\"";

	parameters.clear();
	parameters.insert(Challenge::realmKey(), defaultRealm);
	parameters.insert(Basic::charsetKey(), utf8Charset);
	QTest::newRow("with charset parameter") << parameters << "Basic realm=\"world\", charset=\"utf-8\"";

	parameters.clear();
	parameters.insert(Basic::charsetKey(), utf8Charset);
	QTest::newRow("invalid challenge") << parameters << QString();

}

/*! \test Tests the HttpUtils::Authenticate::Basic::authenticateHeader() method.
 */
void HttpAuthenticationTest::testBasicAuthAuthenticateHeader()
{
	QFETCH(QVariantMap, parameters);

	QScopedPointer<Basic> challenge(new Basic(parameters));

	QTEST(QString::fromLatin1(challenge->authenticateHeader()), "authenticateHeader");
}

/*! Provides the data for the testBasicAuthVerifyAuth() test.
 */
void HttpAuthenticationTest::testBasicAuthVerifyAuth_data()
{
	QTest::addColumn<Challenge::Ptr>("challenge");
	QTest::addColumn<QNetworkRequest>("request");
	QTest::addColumn<StringPair>("credentials");
	QTest::addColumn<bool>("credentialsMatch");

	const QString username("foo");
	const QString password("secret");
	const QString specialCharUsername = QString::fromWCharArray(L"\u00E4\u00F6\u00FC");
	const QString specialCharPassword = QString::fromWCharArray(L"\\u00C4\u00DF\u00C7");

	const QByteArray matchingAuthHeader = QByteArray("Basic ") + (username + ":" + password).toLatin1().toBase64();
	const QByteArray latin1AuthHeader = QByteArray("Basic ") + (specialCharUsername + ":" + specialCharPassword).toLatin1().toBase64();

	Challenge::Ptr challenge(new Basic(defaultRealm));
	Challenge::Ptr invalidChallenge(new Basic(QStringList()));
	Challenge::Ptr latin1Challenge(new Basic(QStringList(defaultRealm)));
	latin1Challenge->setBehaviorFlags(MockNetworkAccess::Behavior_HttpAuthLatin1Encoding);

	QNetworkRequest request;
	request.setRawHeader(HttpUtils::authorizationHeader(), matchingAuthHeader);

	QNetworkRequest latin1AuthRequest;
	latin1AuthRequest.setRawHeader(HttpUtils::authorizationHeader(), latin1AuthHeader);


	//                                    // challenge        // request           // credentials                                         // credentialsMatch
	QTest::newRow("simple challenge")     << challenge        << request           << qMakePair(username, password)                       << true;
	QTest::newRow("invalid challenge")    << invalidChallenge << request           << qMakePair(username, password)                       << true;
	QTest::newRow("non matching")         << challenge        << request           << qMakePair(QString("bar"), QString("test"))          << false;
	QTest::newRow("latin1 encoding")      << latin1Challenge  << latin1AuthRequest << qMakePair(specialCharUsername, specialCharPassword) << true;
	QTest::newRow("mismatching encoding") << challenge        << latin1AuthRequest << qMakePair(specialCharUsername, specialCharPassword) << false;
}

/*! \test Tests the HttpUtils::Authentication::Basic::verifyAuthorization() method.
 */
void HttpAuthenticationTest::testBasicAuthVerifyAuth()
{
	QFETCH(Challenge::Ptr, challenge);
	QFETCH(QNetworkRequest, request);
	QFETCH(StringPair, credentials);

	QAuthenticator authenticator;
	authenticator.setUser(credentials.first);
	authenticator.setPassword(credentials.second);

	QTEST(challenge->verifyAuthorization(request, authenticator), "credentialsMatch");
}


/*! Provides the data for the testAuthenticationScopeForUrl() test.
 */
void HttpAuthenticationTest::testAuthenticationScopeForUrl_data()
{
	QTest::addColumn<QUrl>("url");
	QTest::addColumn<QUrl>("authenticationScope");

	//                                          // url                                        // authenticationScope
	QTest::newRow("domain root")                << QUrl("http://example.com")                 << QUrl("http://example.com/");
	QTest::newRow("domain root trailing slash") << QUrl("http://example.com/")                << QUrl("http://example.com/");
	QTest::newRow("root document")              << QUrl("http://example.com/foo")             << QUrl("http://example.com/");
	QTest::newRow("subdir document")            << QUrl("http://example.com/foo/bar")         << QUrl("http://example.com/foo/");
	QTest::newRow("trailing slash")             << QUrl("http://example.com/foo/bar/")        << QUrl("http://example.com/foo/bar/");
	QTest::newRow("query")                      << QUrl("http://example.com/foo/?some=query") << QUrl("http://example.com/foo/");
	QTest::newRow("https scheme")               << QUrl("https://example.com/foo")            << QUrl("https://example.com/");
}

/*! \test Tests the HttpUtils::Authentication::authenticationScopeForUrl() method.
 */
void HttpAuthenticationTest::testAuthenticationScopeForUrl()
{
	QFETCH(QUrl, url);

	QTEST(authenticationScopeForUrl(url), "authenticationScope");
}

/*! Provides the data for the testGetAuthenticationChallenges() test.
 */
void HttpAuthenticationTest::testGetAuthenticationChallenges_data()
{
	QTest::addColumn<DummyReply::Ptr>("reply");
	QTest::addColumn<AuthChallengeVector>("expectedChallenges");

	QVariantMap authParams;
	AuthChallengeVector authChallenges;

	authParams.clear();
	authParams.insert(Challenge::realmKey(), defaultRealm);
	QTest::newRow("basic auth") << DummyReply::createAuthChallengeReply("Basic realm=\"world\"")
	                            << (AuthChallengeVector()
	                                << AuthChallengeSpec(Challenge::BasicAuthenticationScheme, authParams));

	authParams.clear();
	authParams.insert(Challenge::realmKey(), defaultRealm);
	authParams.insert(Basic::charsetKey(), utf8Charset);
	QTest::newRow("multiple parameters") << DummyReply::createAuthChallengeReply("Basic realm=\"world\", charset=\"utf-8\"")
	                                     << (AuthChallengeVector()
	                                         << AuthChallengeSpec(Challenge::BasicAuthenticationScheme, authParams));

	authParams.clear();
	authParams.insert(Challenge::realmKey(), defaultRealm);
	authParams.insert(Basic::charsetKey(), utf8Charset);
	QTest::newRow("case is ignored") << DummyReply::createAuthChallengeReply("Basic reAlM=\"world\", CHARset=\"UtF-8\"")
	                                 << (AuthChallengeVector()
	                                     << AuthChallengeSpec(Challenge::BasicAuthenticationScheme, authParams));

	QTest::newRow("empty challenge")   << DummyReply::createAuthChallengeReply("   ")               << AuthChallengeVector();
	QTest::newRow("invalid challenge") << DummyReply::createAuthChallengeReply("realm=\"world\"")   << AuthChallengeVector();
	QTest::newRow("corrupt parameter") << DummyReply::createAuthChallengeReply("Basic blah-foo")    << AuthChallengeVector();
	QTest::newRow("unknown scheme")    << DummyReply::createAuthChallengeReply("foo realm=\"bar\"") << AuthChallengeVector();

	authParams.clear();
	authChallenges.clear();
	authParams.insert(Challenge::realmKey(), defaultRealm);
	authChallenges << AuthChallengeSpec(Challenge::BasicAuthenticationScheme, authParams);
	authParams.insert(Challenge::realmKey(), "second");
	authChallenges << AuthChallengeSpec(Challenge::BasicAuthenticationScheme, authParams);
	QTest::newRow("multiple challenges") << DummyReply::createAuthChallengeReply("Basic realm=\"world\", Basic realm=\"second\"")
	                                     << authChallenges;

	authParams.clear();
	authChallenges.clear();
	authParams.insert(Challenge::realmKey(), defaultRealm);
	authParams.insert(Basic::charsetKey(), utf8Charset);
	authChallenges << AuthChallengeSpec(Challenge::BasicAuthenticationScheme, authParams);
	authParams.insert(Challenge::realmKey(), "second");
	authParams.insert(Basic::charsetKey(), utf8Charset);
	authChallenges << AuthChallengeSpec(Challenge::BasicAuthenticationScheme, authParams);
	QTest::newRow("multiple challenges with multiple parameters") << DummyReply::createAuthChallengeReply("Basic realm=\"world\", charset=utf-8, Basic realm=\"second\", charset=\"utf-8\"")
	                                                              << authChallenges;

	authParams.clear();
	authChallenges.clear();
	authParams.insert(Challenge::realmKey(), "second");
	authChallenges << AuthChallengeSpec(Challenge::BasicAuthenticationScheme, authParams);
	QTest::newRow("multiple challenges, one corrupt start") << DummyReply::createAuthChallengeReply("realm=\"world\", some=fail, Basic realm=\"second\"")
	                                                        << authChallenges;

	authParams.clear();
	authChallenges.clear();
	authParams.insert(Challenge::realmKey(), "second");
	authChallenges << AuthChallengeSpec(Challenge::BasicAuthenticationScheme, authParams);
	QTest::newRow("multiple challenges, one corrupt parameter") << DummyReply::createAuthChallengeReply("Basic realm=\"world\", some:fail, Basic realm=\"second\"")
	                                                            << authChallenges;

	QTest::newRow("proxy authentication") << DummyReply::createProxyAuthChallengeReply("Basic realm=\"proxy\"")
	                                      << AuthChallengeVector();

}

/*! \test Tests the HttpUtils::Authentication::getAuthenticationChallenges() method.
 */
void HttpAuthenticationTest::testGetAuthenticationChallenges()
{
	QFETCH(DummyReply::Ptr, reply);

	const QVector<Challenge::Ptr> challenges = getAuthenticationChallenges(reply.data());

	QVector<Challenge::Ptr>::ConstIterator iter;
	AuthChallengeVector actualChallenges;
	for (iter = challenges.constBegin(); iter != challenges.constEnd(); ++iter)
		actualChallenges << AuthChallengeSpec(*iter);

	QTEST(actualChallenges, "expectedChallenges");
}


} // namespace Tests


QTEST_MAIN(Tests::HttpAuthenticationTest)
#include "HttpAuthenticationTest.moc"

