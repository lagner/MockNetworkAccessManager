#include "MockNetworkAccessManager.hpp"

#include <QtTest>

namespace Tests {

/*! Implements unit tests for the methods from the HttpUtils namespace.
 */
class HttpUtilsTest : public QObject
{
	Q_OBJECT

private Q_SLOTS:
	void testTrimmed_data();
	void testTrimmed();
	void testWhiteSpaceCleaned_data();
	void testWhiteSpaceCleaned();
	void testIsValidQuotedString_data();
	void testIsValidQuotedString();
	void testUnquoteString_data();
	void testUnquoteString();
	void testQuoteString_data();
	void testQuoteString();
	void testSplitCommaSeparatedList_data();
	void testSplitCommaSeparatedList();

private:
	void quoteUnquoteStringCommonTestData();
};

using namespace MockNetworkAccess;

/*! Provides the data for the testTrimmed() test.
 */
void HttpUtilsTest::testTrimmed_data()
{
	QTest::addColumn<QString>("string");
	QTest::addColumn<QString>("trimmedString");

	//                                          // string            // trimmedString
	QTest::newRow("empty string")               << ""                << "";
	QTest::newRow("only whitespace")            << " \t  \t\t"       << "";
	QTest::newRow("simple string")              << "asdf"            << "asdf";
	QTest::newRow("leading space")              << " asd"            << "asd";
	QTest::newRow("trailing space")             << "asd "            << "asd";
	QTest::newRow("leading tab")                << "\tasd"           << "asd";
	QTest::newRow("trailing tab")               << "asd\t"           << "asd";
	QTest::newRow("mixed leading and trailing") << " \t  aawd\t \t"  << "aawd";
	QTest::newRow("intermediate whitespace")    << "a \t  fb\td a"   << "a \t  fb\td a";
	QTest::newRow("whitespace everywhere")      << " a\t  fd a\t  "  << "a\t  fd a";
	QTest::newRow("illegal whitespace is data") << "\v asd\n"        << "\v asd\n";
}

/*! \test Tests the HttpUtils::trimmed() method.
 */
void HttpUtilsTest::testTrimmed()
{
	QFETCH(QString, string);

	QTEST(HttpUtils::trimmed(string), "trimmedString");
}

/*! Provides the data for the testWhiteSpaceCleaned() test.
 */
void HttpUtilsTest::testWhiteSpaceCleaned_data()
{
	QTest::addColumn<QString>("string");
	QTest::addColumn<QString>("cleanedString");

	//                                       // string               // cleanedString
	QTest::newRow("empty string")            << ""                   << "";
	QTest::newRow("only whitespace")         << " \t\t  "            << "";
	QTest::newRow("simple string")           << "asdf"               << "asdf";
	QTest::newRow("trimming")                << " \t  aawd\t \t"     << "aawd";
	QTest::newRow("obs-fold")                << "as\r\n df"          << "as df";
	QTest::newRow("only obs-fold")           << "\r\n "              << "";
	QTest::newRow("multiple obs-folds")      << "as\r\n  df\r\n\tp"  << "as  df p";
	QTest::newRow("obs-fold-like sequences") << "as\n\r  df\r\np"    << "as\n\r  df\r\np";
	QTest::newRow("obs-folds and trimming")  << "\tas\r\n\t df\r\n " << "as  df";

}

/*! \test Tests the HttpUtils::whiteSpaceCleaned() method.
 */
void HttpUtilsTest::testWhiteSpaceCleaned()
{
	QFETCH(QString, string);

	QTEST(HttpUtils::whiteSpaceCleaned(string), "cleanedString");
}


/*! Provides the data for the testIsValidQuotedString() test.
 */
void HttpUtilsTest::testIsValidQuotedString_data()
{
	QTest::addColumn<QString>("string");
	QTest::addColumn<bool>("isValid");

	//                                       // string               // isValid
	QTest::newRow("empty quoted string")     << "\"\""               << true;
	QTest::newRow("simple string")           << "\"asdf\""           << true;
	QTest::newRow("quoted pair")             << "\"as\\ndf\""        << true;
	QTest::newRow("escaped quote")           << "\"as\\\"df\""       << true;
	QTest::newRow("escaped backslash")       << "\"as\\\\\""         << true;
	QTest::newRow("quoted obs-text")         << QString::fromWCharArray(L"\"as\\\u00B6\\\u00FCf\"") << true;
	QTest::newRow("white space")             << "\" a\ts  d\t\t f\"" << true;
	QTest::newRow("special chars")           << "\"#!;&{|}?`'~\""    << true;
	QTest::newRow("empty string")            << ""                   << false;
	QTest::newRow("missing opening quote")   << "asd\""              << false;
	QTest::newRow("missing closing quote")   << "\"asd"              << false;
	QTest::newRow("single double quote")     << "\""                 << false;
	QTest::newRow("escaped closing quote")   << "\"\\\""             << false;
	QTest::newRow("unescaped quote")         << "\"a\"b\""           << false;
	QTest::newRow("unescaped obs-text")      << QString::fromWCharArray(L"\"a\\\u00E4\u00F6b\"")          << false;
	QTest::newRow("illegal chars")           << QString::fromWCharArray(L"\"\u03B1\u2194\u03B2\u1F608\"") << false;
}

/*! \test Tests the HttpUtils::isValidQuotedString() method.
 */
void HttpUtilsTest::testIsValidQuotedString()
{
	QFETCH(QString, string);

	QTEST(HttpUtils::isValidQuotedString(string), "isValid");
}


/*! Provides the data shared between the testQuoteString() and testUnquoteString() tests.
 */
void HttpUtilsTest::quoteUnquoteStringCommonTestData()
{
	QTest::addColumn<QString>("unquotedString");
	QTest::addColumn<QString>("quotedString");

	//                                         // unquotedString        // quotedString
	QTest::newRow("empty string")              << ""                    << "\"\"";
	QTest::newRow("simple string")             << "asdf"                << "\"asdf\"";
	QTest::newRow("escaped quote")             << "as\"df"              << "\"as\\\"df\"";
	QTest::newRow("escaped backslash")         << "as\\"                << "\"as\\\\\"";
	QTest::newRow("quoted obs-text")           << QString::fromWCharArray(L"as\u00B6\u00FCdf") << QString::fromWCharArray(L"\"as\\\u00B6\\\u00FCdf\"");
	QTest::newRow("white space")               << " a\ts  d\t\t f"      << "\" a\ts  d\t\t f\"";
	QTest::newRow("special chars")             << "#!;&{|}?`'~"         << "\"#!;&{|}?`'~\"";

}

/*! Provides the data for the testQuoteString() test.
 */
void HttpUtilsTest::testQuoteString_data()
{
	quoteUnquoteStringCommonTestData();

	//                                         // unquotedString // quotedString
	QTest::newRow("null string")               << QString()      << "\"\"";
	QTest::newRow("multiple escape sequences") << "as\\\"df"     << "\"as\\\\\\\"df\"";
	QTest::newRow("illegal characters")        << QString::fromWCharArray(L"a\u001Eb") << QString();
}

/*! \test Tests the HttpUtils::quoteString() method.
 */
void HttpUtilsTest::testQuoteString()
{
	QFETCH(QString, unquotedString);

	QTEST(HttpUtils::quoteString(unquotedString), "quotedString");
}

/*! Provides the data for the testUnquoteString() test.
 */
void HttpUtilsTest::testUnquoteString_data()
{
	quoteUnquoteStringCommonTestData();

	//                                         // unquotedString // quotedString
	QTest::newRow("quoted pair")               << "asndf"        << "\"as\\ndf\"";
	QTest::newRow("multiple escape sequences") << "as\\\"df"     << "\"as\\\\\\\"d\\f\"";
	QTest::newRow("empty string")              << QString()      << "";
	QTest::newRow("missing opening quote")     << QString()      << "asd\"";
	QTest::newRow("missing closing quote")     << QString()      << "\"asd";
	QTest::newRow("single double quote")       << QString()      << "\"";
	QTest::newRow("escaped closing quote")     << QString()      << "\"\\\"";

}

/*! \test Tests the HttpUtils::unquoteString() method.
 */
void HttpUtilsTest::testUnquoteString()
{
	QFETCH(QString, quotedString);

	QTEST(HttpUtils::unquoteString(quotedString), "unquotedString");
}

/*! Provides the data for the testSplitCommaSeparatedList() test.
 */
void HttpUtilsTest::testSplitCommaSeparatedList_data()
{
	QTest::addColumn<QString>("commaSepList");
	QTest::addColumn<QStringList>("list");

	//                                  // commaSepList                  // list
	QTest::newRow("simple list")        << "foo,bar,baz"                 << (QStringList() << "foo" << "bar" << "baz");
	QTest::newRow("whitespace")         << " foo ,bar , baz"             << (QStringList() << "foo" << "bar" << "baz");
	QTest::newRow("string")             << "\"foo \",bar,\" baz\""       << (QStringList() << "\"foo \"" << "bar" << "\" baz\"");
	QTest::newRow("string with comma")  << "\"foo , bar\",\"baz\",test"  << (QStringList() << "\"foo , bar\"" << "\"baz\"" << "test");
	QTest::newRow("string with escape") << "\"foo\\\", bar\\\\\",baz"    << (QStringList() << "\"foo\\\", bar\\\\\"" << "baz");
	QTest::newRow("empty entries")      << "foo,,,,bar,,baz"             << (QStringList() << "foo" << "bar" << "baz");
}

/*! \test Tests the HttpUtils::splitCommaSeparatedList() method.
 */
void HttpUtilsTest::testSplitCommaSeparatedList()
{
	QFETCH(QString, commaSepList);

	QTEST(HttpUtils::splitCommaSeparatedList(commaSepList), "list");
}



} // namespace Tests


QTEST_MAIN(Tests::HttpUtilsTest)
#include "HttpUtilsTest.moc"

