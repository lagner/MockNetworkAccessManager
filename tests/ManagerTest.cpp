#include "QSignalInspector.hpp"
#include "TestUtils.hpp"
#include "hippomocks.h"

#include "MockNetworkAccessManager.hpp"

#include <QtTest>
#include <QSharedPointer>
#include <QScopedPointer>
#include <QNetworkAccessManager>

#include <cstdlib>

namespace Tests {

/*! Implements unit tests for the Manager class.
 */
class ManagerTest : public QObject
{
	Q_OBJECT

public:
	ManagerTest(QObject* parent = Q_NULLPTR) : QObject(parent), m_mutex(QMutex::Recursive) {}

protected slots:
	void authenticate(QNetworkReply*, QAuthenticator* authenticator)
	{
		QMutexLocker locker(&m_mutex);
		authenticator->setUser(m_username);
		authenticator->setPassword(m_password);
#if QT_VERSION >= QT_VERSION_CHECK(5,4,0)
		m_lastAuthRealm = authenticator->realm();
#else
		m_lastAuthRealm = authenticator->option(MockNetworkAccess::HttpUtils::Authentication::Basic::realmKey()).toString();
#endif // Qt < 5.4.0
	}
	void proxyAuthenticate(const QNetworkProxy&, QAuthenticator* authenticator)
	{
		QMutexLocker locker(&m_mutex);
		authenticator->setUser(m_proxyUsername);
		authenticator->setPassword(m_proxyPassword);
#if QT_VERSION >= QT_VERSION_CHECK(5,4,0)
		m_lastAuthRealm = authenticator->realm();
#else
		m_lastAuthRealm = authenticator->option(MockNetworkAccess::HttpUtils::Authentication::Basic::realmKey()).toString();
#endif // Qt < 5.4.0
	}

private slots:
	void initTestCase();

	void testGetDefaultBehavior();
	void testGetDefaultBehavior_data();
	void testSetAddRules();
	void testWhenMethods();
	void testCreateRequest_data();
	void testCreateRequest();
	void testUnmatchedRequests_PredefinedReply_data();
	void testUnmatchedRequests_PredefinedReply();
	void testPassThrough();
	void testPassThrough_data();
	void testPassThroughNam();
	void testPassThroughNam_data();
	void testPassThroughRuleNam();
	void testPassThroughRuleNam_data();
	void testSignals_data();
	void testSignals();
	void testRequestEqualityOperators_data();
	void testRequestEqualityOperators();
	void testRequestRecording_data();
	void testReplySignals_data();
	void testReplySignals();
	void testRequestRecording();
	void testMultipleRequestsRecording();
	void testAuthentication_data();
	void testAuthentication();
	void testAuthenticationCache_data();
	void testAuthenticationCache();
	void testCookies_data();
	void testCookies();

#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
	void testAutoRedirect();
	void testAutoRedirect_data();
#endif // Qt >= 5.6.0

#if QT_VERSION >= QT_VERSION_CHECK(5, 9, 0)
	void testHsts_data();
	void testHsts();
	void testHstsPolicyExpiry_data();
	void testHstsPolicyExpiry();
	void testHstsResponse_data();
	void testHstsResponse();
	void testInvalidHstsHeaders_data();
	void testInvalidHstsHeaders();
#endif // Qt >= 5.9.0

private:
	QMutex m_mutex;
	QString m_username;
	QString m_password;
	QString m_lastAuthRealm;
	QString m_proxyUsername;
	QString m_proxyPassword;
	QString m_lastProxyAuthRealm;
};


//####### Helpers #######

using namespace MockNetworkAccess;
using namespace MockNetworkAccess::Predicates;

typedef QVector<Rule::Ptr> RuleVector;
typedef QVector<Request> RequestVector;
typedef QVector<QUrl> UrlVector;
typedef QPair<QMetaMethod, QList<QVariant> > SignalEmission;
typedef QList<SignalEmission> SignalEmissionList;
typedef QPair<QString, QString> StringPair;
#if QT_VERSION >= QT_VERSION_CHECK(5, 9, 0)
typedef QVector<QHstsPolicy> HstsPolicyVector;
#endif // Qt >= 5.9.0

class DummyNetworkAccessManager;

/*! Dummy QNetworkReply.
 * Helper class for the DummyNetworkAccessManager.
 */
class DummyReply : public QNetworkReply
{
	Q_OBJECT

	friend class DummyNetworkAccessManager;

public:
	/*! Creates a DummyReply object.
	 * \param parent Parent QObject.
	 */
	DummyReply(QObject* parent = Q_NULLPTR) : QNetworkReply(parent) {}

public slots:
	virtual void abort() {}

protected:
	virtual qint64 readData(char*, qint64) { return -1; }

};

const QNetworkReply::NetworkError operationCancelError = QNetworkReply::OperationCanceledError;

/*! Helper class for the MockNetworkAccessManagerTest.
 */
class DummyNetworkAccessManager : public QNetworkAccessManager
{
	Q_OBJECT

public:
	/*! Smart pointer to DummyNetworkAccessManager */
	typedef QSharedPointer<DummyNetworkAccessManager> Ptr;

	/*! Creates a DummyNetworkAccessManager
	 * \param parent Parent QObject
	 */
	DummyNetworkAccessManager(QObject* parent = Q_NULLPTR) : QNetworkAccessManager(parent) {}

protected:
	/*! Records the request and creates a DummyReply.
	 * \param op The HTTP verb.
	 * \param req The QNetworkRequest object.
	 * \param ioDevice The body of the request.
	 * \return A DummyReply. Ownership is with the caller.
	 */
	virtual QNetworkReply* createRequest(QNetworkAccessManager::Operation op, const QNetworkRequest& req, QIODevice* ioDevice)
	{
		Request request;
		request.operation = op;
		request.qRequest = req;
		if (ioDevice)
			request.body = ioDevice->readAll();
		requests.append(request);

		DummyReply* reply = new DummyReply(this);
		reply->setError(operationCancelError, QString("DummyReply"));
		reply->setRequest(request.qRequest);
		reply->setUrl(request.qRequest.url());
		reply->setOperation(request.operation);
		reply->setSslConfiguration(request.qRequest.sslConfiguration());
		reply->open(QIODevice::ReadOnly);
		reply->setFinished(true);
		return reply;
	}

public:
	/*! The requests received by this manager.
	 */
	QVector<Request> requests;
};

bool compareRequests(const Request& left, const Request& right)
{
	if (left.operation != right.operation)
		return false;

	if (left.qRequest != right.qRequest)
		return false;

	if (left.body != right.body)
		return false;

	qint64 timestampDiff = qAbs(left.timestamp.msecsTo(right.timestamp));
	if (timestampDiff > 100)
		return false;

	return true;
}

bool compareRequestVectors(const RequestVector& left, const RequestVector& right)
{
	if (left.size() != right.size())
		return false;

	for (int i = 0; i < left.size(); ++i)
	{
		if (!compareRequests(left.at(i), right.at(i)))
			return false;
	}

	return true;
}

void updateRequestTimestamps(RequestVector& requests)
{
	const QDateTime now = QDateTime::currentDateTime();
	RequestVector::Iterator iter;
	for (iter = requests.begin(); iter != requests.end(); ++iter)
		iter->timestamp = now;
}

RequestVector getRequestsFromSignalEmissions(const QList<QList<QVariant> >& signalEmissions)
{
	RequestVector requests;

	QList<QList<QVariant> >::ConstIterator emissionIter;
	for (emissionIter = signalEmissions.cbegin(); emissionIter != signalEmissions.cend(); ++emissionIter)
		requests.append(emissionIter->first().value<Request>());

	return requests;
}

void removeDuplicateSignals(SignalEmissionList& emissionList, const QByteArray& signalName)
{
	QVector<int> signalIndexes;
	for (int i=0; i < emissionList.size(); ++i)
	{
		if (emissionList.at(i).first.name() == signalName)
			signalIndexes << i;
	}

	if (!signalIndexes.isEmpty())
		signalIndexes.removeLast();

	QVector<int>::ConstIterator signalIndexIter = signalIndexes.constEnd();
	--signalIndexIter;
	for (; signalIndexIter >= signalIndexes.constBegin(); --signalIndexIter)
		emissionList.removeAt(*signalIndexIter);
}

} // namespace Tests


Q_DECLARE_METATYPE(Tests::RuleVector)
Q_DECLARE_METATYPE(Tests::RequestVector)
Q_DECLARE_METATYPE(Tests::UrlVector)
Q_DECLARE_METATYPE(Tests::StringPair)
Q_DECLARE_METATYPE(Tests::DummyNetworkAccessManager::Ptr)
Q_DECLARE_METATYPE(MockNetworkAccess::UnmatchedRequestBehavior)
Q_DECLARE_METATYPE(MockNetworkAccess::BehaviorFlags)
Q_DECLARE_METATYPE(QNetworkReply*)
Q_DECLARE_METATYPE(QAuthenticator*)

#if QT_VERSION >= QT_VERSION_CHECK(5, 9, 0)
Q_DECLARE_METATYPE(QHstsPolicy)
Q_DECLARE_METATYPE(Tests::HstsPolicyVector)
#endif // Qt >= 5.9.0


namespace Tests
{

const QUrl exampleRootUrl("http://example.com");
const QUrl httpsExampleRootUrl("https://example.com");
const QUrl httpsOtherOriginUrl("https://example.com:8080");
const QUrl fooUrl("http://example.com/foo");
const QUrl httpsFooUrl("https://example.com/foo");
const QNetworkReply::NetworkError noError = QNetworkReply::NoError;
const QNetworkReply::NetworkError hostNotFoundError = QNetworkReply::HostNotFoundError;
const QNetworkReply::NetworkError protocolUnknownError = QNetworkReply::ProtocolUnknownError;
const QByteArray statusOkJson("{ \"status\": \"OK\" }");
const QByteArray fooObject("{ \"foo\": \"bar\" }");
const QByteArray cookieCreated("cookie created");
const QByteArray cookieSent("cookie sent");


const Request getExampleRootRequest(QNetworkRequest(exampleRootUrl), QNetworkAccessManager::GetOperation);
const Request getFooRequest(QNetworkRequest(fooUrl), QNetworkAccessManager::GetOperation);

Request postFooRequest()
{
	Request postFoo(QNetworkAccessManager::PostOperation,
	                QNetworkRequest(fooUrl),
	                fooObject);
	postFoo.qRequest.setHeader(QNetworkRequest::ContentTypeHeader, QByteArray("application/json"));
	return postFoo;
}


Rule::Ptr getExampleRootRule()
{
	Rule::Ptr getExampleRoot = Rule::Ptr::create();
	getExampleRoot->has(Verb(QNetworkAccessManager::GetOperation))
	              ->has(Url(exampleRootUrl))
	              ->reply()->withBody(statusOkJson);
	return getExampleRoot;
}

Rule::Ptr postFooRule()
{
	Rule::Ptr postFoo = Rule::Ptr::create();
	postFoo->has(Verb(QNetworkAccessManager::PostOperation))
	       ->has(Url(fooUrl))
	       ->has(Body(fooObject))
	       ->reply()->withBody(statusOkJson);
	return postFoo;
}

Rule::Ptr catchAllRule()
{
	Rule::Ptr catchAll = Rule::Ptr::create();
	catchAll->has(Anything())
	        ->reply()->withError(hostNotFoundError);
	return catchAll;
}

Rule::Ptr getFooRule()
{
	Rule::Ptr getFoo = Rule::Ptr::create();
	getFoo->has(Url(fooUrl))->reply()->withStatus(HttpStatus::OK);
	return getFoo;
}

Rule::Ptr gerRedirectRule()
{
	Rule::Ptr gerRedirect = Rule::Ptr::create();
	gerRedirect->has(Verb(QNetworkAccessManager::GetOperation))
	           ->has(Url(exampleRootUrl))
	           ->reply()->withRedirect(fooUrl);
	return gerRedirect;
}

template<class MNAM>
QNetworkReply* sendRequest(MNAM& mnam, const Request& request)
{
	QBuffer* bodyBuffer = Q_NULLPTR;
	QNetworkReply* reply = Q_NULLPTR;
	switch (request.operation)
	{
	case QNetworkAccessManager::GetOperation:    reply = mnam.get(request.qRequest); break;
	case QNetworkAccessManager::PostOperation:   reply = mnam.post(request.qRequest, request.body); break;
	case QNetworkAccessManager::PutOperation:    reply = mnam.put(request.qRequest, request.body); break;
	case QNetworkAccessManager::DeleteOperation: reply = mnam.deleteResource(request.qRequest); break;
	case QNetworkAccessManager::HeadOperation:   reply = mnam.head(request.qRequest); break;
	case QNetworkAccessManager::CustomOperation:
	{
		if (!request.body.isEmpty())
		{
			bodyBuffer = new QBuffer();
			bodyBuffer->setData(request.body);
			bodyBuffer->open(QIODevice::ReadOnly);
		}
		reply = mnam.sendCustomRequest(request.qRequest, request.qRequest.attribute(QNetworkRequest::CustomVerbAttribute).toByteArray(), bodyBuffer);
		if (bodyBuffer)
		{
			QObject::connect(reply, SIGNAL(finished()), bodyBuffer, SLOT(deleteLater()));
			bodyBuffer->setParent(reply);
		}
		break;
	}
	default:
		return Q_NULLPTR;
	}

	DummyReply* dummyReply = qobject_cast<DummyReply*>(reply);
	if (dummyReply)
	{
		if (dummyReply->error() != QNetworkReply::NoError)
			QMetaObject::invokeMethod(dummyReply, "error", Qt::QueuedConnection, Q_ARG(QNetworkReply::NetworkError, dummyReply->error()));
		if (dummyReply->isFinished())
			QMetaObject::invokeMethod(dummyReply, "finished", Qt::QueuedConnection);
	}
	return reply;
}


//####### Administration #######

void ManagerTest::initTestCase()
{
	qRegisterMetaType<QNetworkReply*>();
	qRegisterMetaType<QAuthenticator*>();
}


//####### Tests #######

/*! \test Tests the Manager::getDefaultBehavior() method and the default behavior flags of the Manager.
 */
void ManagerTest::testGetDefaultBehavior()
{
#ifdef _HIPPOMOCKS__ENABLE_CFUNC_MOCKING_SUPPORT
	QFETCH(QString, qtVersion);

	const QByteArray qtVersionLatin1 = qtVersion.toLatin1();
	HippoMocks::MockRepository mocks;
	mocks.OnCallFunc(qVersion).Return(qtVersionLatin1.constData());

	Manager<QNetworkAccessManager> manager;
	QTEST(MockNetworkAccess::getDefaultBehaviorFlags(), "expectedBehavior");
	QCOMPARE(manager.behaviorFlags(), MockNetworkAccess::getDefaultBehaviorFlags());
#endif
}

/*! Provides the data for the testGetDefaultBehavior() test.
 */
void ManagerTest::testGetDefaultBehavior_data()
{
#ifndef _HIPPOMOCKS__ENABLE_CFUNC_MOCKING_SUPPORT
	QSKIP("This test requires HippoMocks C function mocking support");
#endif

	QTest::addColumn<QString>("qtVersion");
	QTest::addColumn<BehaviorFlags>("expectedBehavior");

	QTest::newRow("Qt 5.2.0") << "5.2.0" << static_cast<BehaviorFlags>(Behavior_Qt_5_2_0);
	QTest::newRow("Qt 5.6.0") << "5.6.0" << static_cast<BehaviorFlags>(Behavior_Qt_5_6_0);
	QTest::newRow("Qt 5.6.1") << "5.6.1" << static_cast<BehaviorFlags>(Behavior_Qt_5_6_0);
}

/*! \test Tests the MockNetworkAccessManager::rules(), MockNetworkAccessManager::setRules() and
 * MockNetworkAccessManager::addRule() methods.
 */
void ManagerTest::testSetAddRules()
{
	Manager<QNetworkAccessManager> mnam;

	QVector<Rule::Ptr> rules;
	Rule::Ptr getRule = Rule::Ptr::create();
	getRule->has(Verb(QNetworkAccessManager::GetOperation));
	rules.append(getRule);
	Rule::Ptr urlMatchingRule = Rule::Ptr::create();
	urlMatchingRule->has(UrlMatching(QRegularExpression("^https?://example.com([/#?].*)")));
	rules.append(urlMatchingRule);

	QVERIFY(mnam.rules().isEmpty());

	Rule::Ptr urlRule = Rule::Ptr::create();
	urlRule->has(Url("http://example.com"));

	mnam.addRule(urlRule);
	QCOMPARE(mnam.rules().size(), 1);

	mnam.setRules(rules);
	QCOMPARE(mnam.rules(), rules);

	mnam.addRule(urlRule);
	QCOMPARE(mnam.rules().size(), rules.size() + 1);

	mnam.setRules(QVector<Rule::Ptr>());
	QVERIFY(mnam.rules().isEmpty());
}

/*! \test Tests the MockNetworkAccessManager::whenGet(), MockNetworkAccessManager::whenPost(),
 * MockNetworkAccessManager::whenPut(), MockNetworkAccessManager::whenDelete(), MockNetworkAccessManager::whenHead()
 * and MockNetworkAccessManager::when() methods.
 */
void ManagerTest::testWhenMethods()
{
	Manager<QNetworkAccessManager> mnam;

	const QRegularExpression urlRegEx("^https://example.com/foo$");
	const QUrl url("http://example.com");
	const QByteArray customVerb("FOO");

	Request fixedUrlRequest = Request(QNetworkRequest(url));
	Request regExUrlRequest = Request(QNetworkRequest(QUrl("https://example.com/foo")));

	QVector<Rule::Ptr> rules;
	QVector<Rule::Predicate::Ptr> predicates;

	fixedUrlRequest.operation = QNetworkAccessManager::GetOperation;
	regExUrlRequest.operation = QNetworkAccessManager::GetOperation;

	rules << mnam.whenGet(urlRegEx);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(regExUrlRequest));
	QVERIFY(predicates.last().dynamicCast<UrlMatching>());
	QVERIFY(predicates.last().dynamicCast<UrlMatching>()->matches(regExUrlRequest));

	rules << mnam.whenGet(url);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(fixedUrlRequest));
	QVERIFY(predicates.last().dynamicCast<Url>());
	QVERIFY(predicates.last().dynamicCast<Url>()->matches(fixedUrlRequest));

	fixedUrlRequest.operation = QNetworkAccessManager::PostOperation;
	regExUrlRequest.operation = QNetworkAccessManager::PostOperation;

	rules << mnam.whenPost(urlRegEx);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(regExUrlRequest));
	QVERIFY(predicates.last().dynamicCast<UrlMatching>());
	QVERIFY(predicates.last().dynamicCast<UrlMatching>()->matches(regExUrlRequest));

	rules << mnam.whenPost(url);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(fixedUrlRequest));
	QVERIFY(predicates.last().dynamicCast<Url>());
	QVERIFY(predicates.last().dynamicCast<Url>()->matches(fixedUrlRequest));

	fixedUrlRequest.operation = QNetworkAccessManager::PutOperation;
	regExUrlRequest.operation = QNetworkAccessManager::PutOperation;

	rules << mnam.whenPut(urlRegEx);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(regExUrlRequest));
	QVERIFY(predicates.last().dynamicCast<UrlMatching>());
	QVERIFY(predicates.last().dynamicCast<UrlMatching>()->matches(regExUrlRequest));

	rules << mnam.whenPut(url);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(fixedUrlRequest));
	QVERIFY(predicates.last().dynamicCast<Url>());
	QVERIFY(predicates.last().dynamicCast<Url>()->matches(fixedUrlRequest));

	fixedUrlRequest.operation = QNetworkAccessManager::DeleteOperation;
	regExUrlRequest.operation = QNetworkAccessManager::DeleteOperation;

	rules << mnam.whenDelete(urlRegEx);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(regExUrlRequest));
	QVERIFY(predicates.last().dynamicCast<UrlMatching>());
	QVERIFY(predicates.last().dynamicCast<UrlMatching>()->matches(regExUrlRequest));

	rules << mnam.whenDelete(url);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(fixedUrlRequest));
	QVERIFY(predicates.last().dynamicCast<Url>());
	QVERIFY(predicates.last().dynamicCast<Url>()->matches(fixedUrlRequest));

	fixedUrlRequest.operation = QNetworkAccessManager::HeadOperation;
	regExUrlRequest.operation = QNetworkAccessManager::HeadOperation;

	rules << mnam.whenHead(urlRegEx);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(regExUrlRequest));
	QVERIFY(predicates.last().dynamicCast<UrlMatching>());
	QVERIFY(predicates.last().dynamicCast<UrlMatching>()->matches(regExUrlRequest));

	rules << mnam.whenHead(url);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(fixedUrlRequest));
	QVERIFY(predicates.last().dynamicCast<Url>());
	QVERIFY(predicates.last().dynamicCast<Url>()->matches(fixedUrlRequest));

	fixedUrlRequest.operation = QNetworkAccessManager::CustomOperation;
	regExUrlRequest.operation = QNetworkAccessManager::CustomOperation;
	fixedUrlRequest.qRequest.setAttribute(QNetworkRequest::CustomVerbAttribute, customVerb);
	regExUrlRequest.qRequest.setAttribute(QNetworkRequest::CustomVerbAttribute, customVerb);

	rules << mnam.when(QNetworkAccessManager::CustomOperation, urlRegEx, customVerb);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(regExUrlRequest));
	QVERIFY(predicates.last().dynamicCast<UrlMatching>());
	QVERIFY(predicates.last().dynamicCast<UrlMatching>()->matches(regExUrlRequest));

	rules << mnam.when(QNetworkAccessManager::CustomOperation, url, customVerb);
	predicates = rules.last()->predicates();
	QVERIFY(predicates.first().dynamicCast<Verb>());
	QVERIFY(predicates.first().dynamicCast<Verb>()->matches(fixedUrlRequest));
	QVERIFY(predicates.last().dynamicCast<Url>());
	QVERIFY(predicates.last().dynamicCast<Url>()->matches(fixedUrlRequest));

	QCOMPARE(mnam.rules(), rules);
}

/*! Provides the data for the testCreateRequest() test.
 */
void ManagerTest::testCreateRequest_data()
{
	QTest::addColumn<Request>("request");
	QTest::addColumn<RuleVector>("rules");
	QTest::addColumn<QNetworkReply::NetworkError>("error");
	QTest::addColumn<int>("statusCode");
	QTest::addColumn<QByteArray>("body");

	Rule::Ptr noReply(new Rule);
	noReply->has(Anything());

	Rule::Ptr passThroughNoReply(new Rule);
	passThroughNoReply->has(Anything())
	                  ->passThrough(Rule::PassThroughReturnMockReply);


	QTest::newRow("simple GET")            << getExampleRootRequest << (RuleVector() << getExampleRootRule())                  << noError              << static_cast<int>(HttpStatus::OK) << statusOkJson;
	QTest::newRow("simple POST")           << postFooRequest()      << (RuleVector() << postFooRule())                         << noError              << static_cast<int>(HttpStatus::OK) << statusOkJson;
	QTest::newRow("multiple rules")        << getExampleRootRequest << (RuleVector() << postFooRule() << getExampleRootRule()) << noError              << static_cast<int>(HttpStatus::OK) << statusOkJson;
	QTest::newRow("catch all")             << getExampleRootRequest << (RuleVector() << postFooRule() << catchAllRule())       << hostNotFoundError    << -1                               << QByteArray();
	QTest::newRow("null rule")             << getExampleRootRequest << (RuleVector() << Rule::Ptr::create() << catchAllRule()) << hostNotFoundError    << -1                               << QByteArray();
	QTest::newRow("no reply rule")         << getExampleRootRequest << (RuleVector() << noReply << catchAllRule())             << hostNotFoundError    << -1                               << QByteArray();
	QTest::newRow("pass through no reply") << getExampleRootRequest << (RuleVector() << passThroughNoReply)                    << operationCancelError << -1                               << QByteArray();

}

/*! \test Tests the MockNetworkAccessManager::createRequest() method.
 */
void ManagerTest::testCreateRequest()
{
	QFETCH(Request, request);
	QFETCH(RuleVector, rules);
	QFETCH(int, statusCode);

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setRules(rules);

	QScopedPointer<QNetworkReply> reply(sendRequest(mnam, request));

	QVERIFY(reply->isFinished());
	QTEST(reply->error(), "error");
	const QVariant statusCodeVariant = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
	if (statusCode != -1)
		QCOMPARE(statusCodeVariant.toInt(), statusCode);
	else
		QVERIFY(!statusCodeVariant.isValid());
	QVERIFY(reply->isReadable());
	QTEST(reply->readAll(), "body");
}

/*! Provides the data for the testUnmatchedRequests_PredefinedReply() test.
 */
void ManagerTest::testUnmatchedRequests_PredefinedReply_data()
{
	QTest::addColumn<Request>("request");
	QTest::addColumn<RuleVector>("rules");
	QTest::addColumn<QNetworkReply::NetworkError>("error");
	QTest::addColumn<int>("statusCode");

	QTest::newRow("no rules")         << getExampleRootRequest << RuleVector()                    << hostNotFoundError << -1;
	QTest::newRow("no matching rule") << getExampleRootRequest << (RuleVector() << postFooRule()) << hostNotFoundError << -1;

}

/*! \test Tests the MockNetworkAccessManager::PredefinedReply behavior.
 */
void ManagerTest::testUnmatchedRequests_PredefinedReply()
{
	QFETCH(Request, request);
	QFETCH(RuleVector, rules);

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setUnmatchedRequestBehavior(PredefinedReply);
	mnam.setRules(rules);

	MockReplyBuilder builder;
	builder.withError(hostNotFoundError);
	builder.withStatus(-1);
	mnam.setUnmatchedRequestBuilder(builder);

	QScopedPointer<QNetworkReply> reply(sendRequest(mnam, request));

	QTEST(reply->error(), "error");
	QTEST(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), "statusCode");
}

/*! \test Tests the pass through functionality of the MockNetworkAccessManager.
 */
void ManagerTest::testPassThrough()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setUnmatchedRequestBehavior( PassThrough );
	mnam.setRules( rules );

	QScopedPointer<QNetworkReply> reply( sendRequest( mnam, request ) );
	request.timestamp = QDateTime::currentDateTime();

	QVERIFY( reply );
	QVERIFY( ! mnam.requests.isEmpty() );
	QVERIFY( compareRequests( mnam.requests.last(), request ) );
	QTEST( static_cast<bool>( dynamic_cast<MockReply*>( reply.data() ) ), "expectMockReply" );
}

/*! Provides the data for the testPassThrough() test.
 */
void ManagerTest::testPassThrough_data()
{
	QTest::addColumn<Request>( "request" );
	QTest::addColumn<RuleVector>( "rules" );
	QTest::addColumn<bool>( "expectMockReply" );

	Rule::Ptr gerPassThroughReturn = Rule::Ptr::create();
	gerPassThroughReturn->has( Verb( QNetworkAccessManager::GetOperation ) )
	                    ->has( Url( exampleRootUrl ) )
	                    ->passThrough( Rule::PassThroughReturnDelegatedReply );

	Rule::Ptr gerPassThroughMock = Rule::Ptr::create();
	gerPassThroughMock->has( Verb( QNetworkAccessManager::GetOperation ) )
	                  ->has( Url( exampleRootUrl ) )
	                  ->passThrough( Rule::PassThroughReturnMockReply )
	                  ->reply()->withStatus( HttpStatus::OK );

	const Request putFooRequest( QNetworkAccessManager::PutOperation, QNetworkRequest( fooUrl ), QByteArray( "foo bar" ) );
	const Request headFooRequest( QNetworkAccessManager::HeadOperation, QNetworkRequest( fooUrl ) );
	const Request deleteFooRequest( QNetworkAccessManager::DeleteOperation, QNetworkRequest( fooUrl ) );
	Request customFooRequest( QNetworkAccessManager::CustomOperation, QNetworkRequest( fooUrl ) );
	customFooRequest.qRequest.setAttribute( QNetworkRequest::CustomVerbAttribute, QByteArray( "FOO" ) );

	QTest::newRow( "no rules" )                  << getExampleRootRequest << RuleVector()                             << false;
	QTest::newRow( "return pass through" )       << getExampleRootRequest << ( RuleVector() << gerPassThroughReturn ) << false;
	QTest::newRow( "pass through, return mock" ) << getExampleRootRequest << ( RuleVector() << gerPassThroughMock )   << true;
	QTest::newRow( "no matching rule" )          << getExampleRootRequest << ( RuleVector() << postFooRule() )        << false;
	QTest::newRow( "pass through POST" )         << postFooRequest()      << RuleVector()                             << false;
	QTest::newRow( "pass through PUT" )          << putFooRequest         << RuleVector()                             << false;
	QTest::newRow( "pass through HEAD" )         << headFooRequest        << RuleVector()                             << false;
	QTest::newRow( "pass through DELETE" )       << deleteFooRequest      << RuleVector()                             << false;
	QTest::newRow( "pass through custom verb" )  << customFooRequest      << RuleVector()                             << false;

}

/*! \test Tests the pass through functionality of the MockNetworkAccessManager using a separate QNetworkAccessManager.
 */
void ManagerTest::testPassThroughNam()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );

	DummyNetworkAccessManager passThroughNam;

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setUnmatchedRequestBehavior( PassThrough );
	mnam.setRules( rules );
	mnam.setPassThroughNam( &passThroughNam );

	QCOMPARE( mnam.passThroughNam(), &passThroughNam );

	QScopedPointer<QNetworkReply> reply( sendRequest( mnam, request ) );
	request.timestamp = QDateTime::currentDateTime();

	QVERIFY( mnam.requests.isEmpty() );
	QVERIFY( !passThroughNam.requests.isEmpty() );
	QVERIFY( compareRequests( passThroughNam.requests.last(), request ) );
}

/*! Provides the data for the testPassThroughNam() test.
 */
void ManagerTest::testPassThroughNam_data()
{
	testPassThrough_data();
}

/*! \test Tests the pass through functionality of the MockNetworkAccessManager using a QNetworkAccessManager provided by the Rule.
 */
void ManagerTest::testPassThroughRuleNam()
{
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );
	QFETCH( DummyNetworkAccessManager::Ptr, passThroughNam );

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setUnmatchedRequestBehavior( PassThrough );
	mnam.setRules( rules );

	QScopedPointer<QNetworkReply> reply( sendRequest( mnam, request ) );
	request.timestamp = QDateTime::currentDateTime();

	QVERIFY( reply );
	if ( passThroughNam )
	{
		QVERIFY( ! passThroughNam->requests.isEmpty() );
		QVERIFY( compareRequests( passThroughNam->requests.last(), request ) );
	}
	else
	{
		QVERIFY( ! mnam.requests.isEmpty() );
		QVERIFY( compareRequests( mnam.requests.last(), request ) );
	}
}

/*! Provides the data for the ManagerTest::testPassThroughRuleNam() test.
 */
void ManagerTest::testPassThroughRuleNam_data()
{
	QTest::addColumn<Request>( "request" );
	QTest::addColumn<RuleVector>( "rules" );
	QTest::addColumn<DummyNetworkAccessManager::Ptr>( "passThroughNam" );

	Rule::Ptr gerPassThroughNam = Rule::Ptr::create();
	DummyNetworkAccessManager::Ptr passThroughMNam( new DummyNetworkAccessManager() );
	gerPassThroughNam->has( Verb( QNetworkAccessManager::GetOperation ) )
	                 ->has( Url( exampleRootUrl ) )
	                 ->passThrough( Rule::PassThroughReturnDelegatedReply, passThroughMNam.data() );

	//                                           // request               // rules                                 // passThroughNam
	QTest::newRow( "rule pass through NAM" )     << getExampleRootRequest << ( RuleVector() << gerPassThroughNam ) << passThroughMNam;
	QTest::newRow( "manager pass through NAM" )  << getFooRequest         << ( RuleVector() << gerPassThroughNam ) << DummyNetworkAccessManager::Ptr();
}

/*! Provides the data for the testSignals() and the testRequestRecording() tests.
 */
void ManagerTest::testSignals_data()
{
	QTest::addColumn<RequestVector>("requests");
	QTest::addColumn<RuleVector>("rules");
	QTest::addColumn<RequestVector>("expectedHandledRequests");
	QTest::addColumn<RequestVector>("expectedMatchedRequests");
	QTest::addColumn<RequestVector>("expectedUnmatchedRequests");
	QTest::addColumn<RequestVector>("expectedPassThroughs");

	QTest::newRow("matched request")   << (RequestVector() << getExampleRootRequest)
	                                   << (RuleVector() << catchAllRule())
	                                   << (RequestVector() << getExampleRootRequest)
	                                   << (RequestVector() << getExampleRootRequest)
	                                   << RequestVector()
	                                   << RequestVector();

	QTest::newRow("unmatched request") << (RequestVector() << getExampleRootRequest)
	                                   << (RuleVector() << postFooRule())
	                                   << (RequestVector() << getExampleRootRequest)
	                                   << RequestVector()
	                                   << (RequestVector() << getExampleRootRequest)
	                                   << (RequestVector() << getExampleRootRequest);

	QTest::newRow("no rules")          << (RequestVector() << getExampleRootRequest)
	                                   << RuleVector()
	                                   << (RequestVector() << getExampleRootRequest)
	                                   << RequestVector()
	                                   << (RequestVector() << getExampleRootRequest)
	                                   << (RequestVector() << getExampleRootRequest);

	QTest::newRow("multiple rules")    << (RequestVector() << getExampleRootRequest)
	                                   << (RuleVector() << postFooRule() << getExampleRootRule() << catchAllRule())
	                                   << (RequestVector() << getExampleRootRequest)
	                                   << (RequestVector() << getExampleRootRequest)
	                                   << RequestVector()
	                                   << RequestVector();


	Rule::Ptr gerNoReply(new Rule);
	gerNoReply->has(Verb(QNetworkAccessManager::GetOperation))
	          ->has(Url(exampleRootUrl));
	QTest::newRow("no reply") << (RequestVector() << getExampleRootRequest)
	                          << (RuleVector() << gerNoReply)
	                          << (RequestVector() << getExampleRootRequest)
	                          << RequestVector()
                              << (RequestVector() << getExampleRootRequest)
                              << (RequestVector() << getExampleRootRequest);

	Rule::Ptr gerPassThroughReturn(new Rule);
	gerPassThroughReturn->has(Verb(QNetworkAccessManager::GetOperation))
	                    ->has(Url(exampleRootUrl))
	                    ->passThrough(Rule::PassThroughReturnDelegatedReply);
	QTest::newRow("matched, return pass through") << (RequestVector() << getExampleRootRequest)
	                                              << (RuleVector() << gerPassThroughReturn)
	                                              << (RequestVector() << getExampleRootRequest)
	                                              << (RequestVector() << getExampleRootRequest)
	                                              << RequestVector()
	                                              << (RequestVector() << getExampleRootRequest);

	Rule::Ptr gerPassThroughMock(new Rule);
	gerPassThroughMock->has(Verb(QNetworkAccessManager::GetOperation))
	                  ->has(Url(exampleRootUrl))
	                  ->passThrough(Rule::PassThroughReturnMockReply)
	                  ->reply()->withStatus(HttpStatus::OK);
	QTest::newRow("matched, pass through, return mock") << (RequestVector() << getExampleRootRequest)
	                                                    << (RuleVector() << gerPassThroughMock)
	                                                    << (RequestVector() << getExampleRootRequest)
	                                                    << (RequestVector() << getExampleRootRequest)
		                                                << RequestVector()
		                                                << (RequestVector() << getExampleRootRequest);

	QTest::newRow("multiple, matched requests") << (RequestVector() << getExampleRootRequest << getFooRequest)
	                                            << (RuleVector() << catchAllRule())
	                                            << (RequestVector() << getExampleRootRequest << getFooRequest)
	                                            << (RequestVector() << getExampleRootRequest << getFooRequest)
	                                            << RequestVector()
	                                            << RequestVector();

	QTest::newRow("multiple, unmatched requests") << (RequestVector() << getExampleRootRequest << getFooRequest)
	                                              << (RuleVector() << postFooRule())
	                                              << (RequestVector() << getExampleRootRequest << getFooRequest)
	                                              << RequestVector()
	                                              << (RequestVector() << getExampleRootRequest << getFooRequest)
	                                              << (RequestVector() << getExampleRootRequest << getFooRequest);

	QTest::newRow("multiple requests") << (RequestVector() << getExampleRootRequest << getFooRequest)
	                                   << (RuleVector() << getExampleRootRule())
	                                   << (RequestVector() << getExampleRootRequest << getFooRequest)
	                                   << (RequestVector() << getExampleRootRequest)
	                                   << (RequestVector() << getFooRequest)
	                                   << (RequestVector() << getFooRequest);


#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
	Request gerFollowRedirectRequest = getExampleRootRequest;
	gerFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);

	QNetworkRequest fooQRequest(fooUrl);
	fooQRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
	fooQRequest.setMaximumRedirectsAllowed(49);
	Request gerRedirectedRequest(fooQRequest);

	QTest::newRow("redirected") << (RequestVector() << gerFollowRedirectRequest)
	                            << (RuleVector() << gerRedirectRule())
	                            << (RequestVector() << gerFollowRedirectRequest << gerRedirectedRequest)
	                            << (RequestVector() << gerFollowRedirectRequest)
	                            << (RequestVector() << gerRedirectedRequest)
	                            << (RequestVector() << gerRedirectedRequest);
#endif

}

/*! \test Tests the signal emission of the MockNetworkAccess::Manager
 * via MockNetworkAcces::SignalEmitter.
 */
void ManagerTest::testSignals()
{
	QFETCH(RequestVector, requests);
	QFETCH(RuleVector, rules);
	QFETCH(RequestVector, expectedHandledRequests);
	QFETCH(RequestVector, expectedMatchedRequests);
	QFETCH(RequestVector, expectedUnmatchedRequests);
	QFETCH(RequestVector, expectedPassThroughs);

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setRules(rules);

	SignalEmitter* emitter = mnam.signalEmitter();

	QSignalSpy receivedRequestSpy (emitter, SIGNAL(receivedRequest(MockNetworkAccess::Request)));
	QSignalSpy handledRequestSpy  (emitter, SIGNAL(handledRequest(MockNetworkAccess::Request)));
	QSignalSpy matchedRequestSpy  (emitter, SIGNAL(matchedRequest(MockNetworkAccess::Request, MockNetworkAccess::Rule::Ptr)));
	QSignalSpy unmatchedRequestSpy(emitter, SIGNAL(unmatchedRequest(MockNetworkAccess::Request)));
	QSignalSpy passedThroughSpy   (emitter, SIGNAL(passedThrough(MockNetworkAccess::Request)));

	updateRequestTimestamps(requests);
	updateRequestTimestamps(expectedHandledRequests);
	updateRequestTimestamps(expectedMatchedRequests);
	updateRequestTimestamps(expectedUnmatchedRequests);
	updateRequestTimestamps(expectedPassThroughs);


	RequestVector::ConstIterator reqIter;
	for (reqIter = requests.cbegin(); reqIter != requests.cend(); ++reqIter)
		QScopedPointer<QNetworkReply> reply(sendRequest(mnam, *reqIter));

	const RequestVector receivedRequests = getRequestsFromSignalEmissions(receivedRequestSpy);
	const RequestVector handledRequests = getRequestsFromSignalEmissions(handledRequestSpy);
	const RequestVector matchedRequests = getRequestsFromSignalEmissions(matchedRequestSpy);
	const RequestVector unmatchedRequests = getRequestsFromSignalEmissions(unmatchedRequestSpy);
	const RequestVector passedThroughRequests = getRequestsFromSignalEmissions(passedThroughSpy);

	QVERIFY(compareRequestVectors(receivedRequests, requests));
	QVERIFY(compareRequestVectors(handledRequests, expectedHandledRequests));
	QVERIFY(compareRequestVectors(matchedRequests, expectedMatchedRequests));
	QVERIFY(compareRequestVectors(unmatchedRequests, expectedUnmatchedRequests));
	QVERIFY(compareRequestVectors(passedThroughRequests, expectedPassThroughs));

	for (int i = 0; i < matchedRequestSpy.size(); ++i)
	{
		const QVariantList& matchedSpyParams = matchedRequestSpy.at(i);
		QVERIFY(rules.contains(matchedSpyParams.at(1).value<Rule::Ptr>()));
	}

}

/*! Provides teh data for the testRequestEqualityOperator() test.
 */
void ManagerTest::testRequestEqualityOperators_data()
{
	QTest::addColumn<Request>("left");
	QTest::addColumn<Request>("right");
	QTest::addColumn<bool>("equal");

	Request gerRequest(QNetworkRequest(exampleRootUrl), QNetworkAccessManager::GetOperation);
	gerRequest.timestamp = getExampleRootRequest.timestamp;

	Request httpsGerRequest(QNetworkRequest(httpsExampleRootUrl), QNetworkAccessManager::GetOperation);
	httpsGerRequest.timestamp = getExampleRootRequest.timestamp;

	Request noSaveCookieRequest(QNetworkRequest(exampleRootUrl), QNetworkAccessManager::GetOperation);
	noSaveCookieRequest.qRequest.setAttribute(QNetworkRequest::CacheSaveControlAttribute, false);
	noSaveCookieRequest.timestamp = getExampleRootRequest.timestamp;

	Request postFooRequest(QNetworkRequest(fooUrl), QNetworkAccessManager::PostOperation);
	postFooRequest.timestamp = getFooRequest.timestamp;

	Request postFooRequestWithBody(QNetworkRequest(fooUrl), QNetworkAccessManager::PostOperation, fooObject);
	postFooRequestWithBody.timestamp = postFooRequest.timestamp;

	Request differentTimestampRequest = getExampleRootRequest;
	differentTimestampRequest.timestamp = getExampleRootRequest.timestamp.addSecs(1);

	//                                            // left                  // right                     // equal
	QTest::newRow("null requests")                << Request()             << Request()                 << true;
	QTest::newRow("equal requests")               << getExampleRootRequest << gerRequest                << true;
	QTest::newRow("different URLs")               << getExampleRootRequest << httpsGerRequest           << false;
	QTest::newRow("different request attributes") << getExampleRootRequest << noSaveCookieRequest       << false;
	QTest::newRow("different operation")          << getFooRequest         << postFooRequest            << false;
	QTest::newRow("different body data")          << postFooRequest        << postFooRequestWithBody    << false;
	QTest::newRow("different timestamps")         << getExampleRootRequest << differentTimestampRequest << false;
}

/*! \test Tests the Request::operator==() and Request::operator!=().
 */
void ManagerTest::testRequestEqualityOperators()
{
	QFETCH(Request, left);
	QFETCH(Request, right);
	QFETCH(bool, equal);

	QVERIFY(left  == left);
	QVERIFY(right == right);
	QCOMPARE(left  == right, equal);
	QCOMPARE(right == left,  equal);
	QCOMPARE(left  != right, !equal);
	QCOMPARE(right != left,  !equal);
}

/*! Provides the data for the testRequestRecording() test.
 */
void ManagerTest::testRequestRecording_data()
{
	// We reuse the data from the testSignals() test.
	testSignals_data();
}

/*! \test Tests the recording of requests.
 */
void ManagerTest::testRequestRecording()
{
	QFETCH(RequestVector, requests);
	QFETCH(RuleVector, rules);
	QFETCH(RequestVector, expectedHandledRequests);
	QFETCH(RequestVector, expectedMatchedRequests);
	QFETCH(RequestVector, expectedUnmatchedRequests);
	QFETCH(RequestVector, expectedPassThroughs);

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setRules(rules);

	updateRequestTimestamps(requests);
	updateRequestTimestamps(expectedHandledRequests);
	updateRequestTimestamps(expectedMatchedRequests);
	updateRequestTimestamps(expectedUnmatchedRequests);
	updateRequestTimestamps(expectedPassThroughs);


	RequestVector::ConstIterator reqIter;
	for (reqIter = requests.cbegin(); reqIter != requests.cend(); ++reqIter)
		QScopedPointer<QNetworkReply> reply(sendRequest(mnam, *reqIter));

	QVERIFY(compareRequestVectors(mnam.receivedRequests(), requests));
	QVERIFY(compareRequestVectors(mnam.handledRequests(), expectedHandledRequests));
	QVERIFY(compareRequestVectors(mnam.matchedRequests(), expectedMatchedRequests));
	QVERIFY(compareRequestVectors(mnam.unmatchedRequests(), expectedUnmatchedRequests));
	QVERIFY(compareRequestVectors(mnam.passedThroughRequests(), expectedPassThroughs));


	for (reqIter = expectedMatchedRequests.cbegin(); reqIter != expectedMatchedRequests.cend(); ++reqIter)
	{
		int ruleRecordedRequestsCount = 0;
		QVector<Rule::Ptr>::ConstIterator iter;
		for (iter = rules.cbegin(); iter != rules.cend(); ++iter)
		{
			Rule::Ptr rule = *iter;
			if (!rule->matchedRequests().isEmpty())
			{
				const RequestVector matchedRequests = rule->matchedRequests();
				RequestVector::ConstIterator matchedReqIter;
				for (matchedReqIter = matchedRequests.cbegin(); matchedReqIter != matchedRequests.cend(); ++matchedReqIter)
				{
					if (compareRequests(*matchedReqIter, *reqIter))
						ruleRecordedRequestsCount += 1;
				}
			}
		}
		QCOMPARE(ruleRecordedRequestsCount, 1);
	}
}

/*! \test Tests the recording of multiple requests.
 */
void ManagerTest::testMultipleRequestsRecording()
{
	Request matchedRequest1 = getFooRequest;
	Request matchedRequest2 = getFooRequest;
	matchedRequest2.qRequest.setRawHeader(QByteArray("X-Custom-Header"), QByteArray("Foobar"));

	QVERIFY(!compareRequests(matchedRequest1, matchedRequest2));

	Request unmatchedRequest = getExampleRootRequest;
	const QUrl passThroughUrl("http://passthrough.com");
	const QNetworkRequest passThroughRequestObj(passThroughUrl);
	Request passThroughRequest(passThroughRequestObj);

	Manager<DummyNetworkAccessManager> mnam;

	Rule::Ptr matchedRule = mnam.whenGet(fooUrl);
	matchedRule->reply()->withStatus(HttpStatus::OK);

	mnam.setUnmatchedRequestBehavior(PredefinedReply);
	MockReplyBuilder unmatchedRequestReply;
	unmatchedRequestReply.withStatus(HttpStatus::NotFound);
	mnam.setUnmatchedRequestBuilder(unmatchedRequestReply);

	Rule::Ptr passThroughRule = mnam.whenGet(passThroughUrl);
	passThroughRule->passThrough(Rule::PassThroughReturnDelegatedReply);

	QScopedPointer<QNetworkReply> matchedReply1(sendRequest(mnam, matchedRequest1));
	matchedRequest1.timestamp = QDateTime::currentDateTime();

	QScopedPointer<QNetworkReply> unmatchedReply(sendRequest(mnam, unmatchedRequest));
	unmatchedRequest.timestamp = QDateTime::currentDateTime();

	QScopedPointer<QNetworkReply> passThroughReply(sendRequest(mnam, passThroughRequest));
	passThroughRequest.timestamp = QDateTime::currentDateTime();

	QScopedPointer<QNetworkReply> matchedReply2(sendRequest(mnam, matchedRequest2));
	matchedRequest2.timestamp = QDateTime::currentDateTime();

	QCOMPARE(mnam.receivedRequests().count(), 4);
	QVERIFY(compareRequests(mnam.receivedRequests().at(0), matchedRequest1));
	QVERIFY(compareRequests(mnam.receivedRequests().at(1), unmatchedRequest));
	QVERIFY(compareRequests(mnam.receivedRequests().at(2), passThroughRequest));
	QVERIFY(compareRequests(mnam.receivedRequests().at(3), matchedRequest2));

	QCOMPARE(mnam.matchedRequests().count(), 3);
	QVERIFY(compareRequests(mnam.matchedRequests().at(0), matchedRequest1));
	QVERIFY(compareRequests(mnam.matchedRequests().at(1), passThroughRequest));
	QVERIFY(compareRequests(mnam.matchedRequests().at(2), matchedRequest2));

	QCOMPARE(matchedRule->matchedRequests().count(), 2);
	QVERIFY(compareRequests(matchedRule->matchedRequests().at(0), matchedRequest1));
	QVERIFY(compareRequests(matchedRule->matchedRequests().at(1), matchedRequest2));

	QCOMPARE(mnam.unmatchedRequests().count(), 1);
	QVERIFY(compareRequests(mnam.unmatchedRequests().at(0), unmatchedRequest));

	QCOMPARE(mnam.passedThroughRequests().count(), 1);
	QVERIFY(compareRequests(mnam.passedThroughRequests().at(0), passThroughRequest));

	QCOMPARE(passThroughRule->matchedRequests().count(), 1);
	QVERIFY(compareRequests(passThroughRule->matchedRequests().at(0), passThroughRequest));
}

void ManagerTest::testReplySignals_data()
{
	QTest::addColumn<bool>("useRealNam");
	QTest::addColumn<BehaviorFlags>("behaviorFlags");
	QTest::addColumn<Request>("request");
	QTest::addColumn<RuleVector>("rules");
	QTest::addColumn<SignalEmissionList>("expectedSignals");

	const QMetaObject& replyMetaObj = QNetworkReply::staticMetaObject;

	QMetaMethod metaDataChangedSignal =     getSignal(replyMetaObj,"metaDataChanged()");
	QMetaMethod uploadProgressSignal =      getSignal(replyMetaObj,"uploadProgress(qint64, qint64)");
	QMetaMethod downloadProgressSignal =    getSignal(replyMetaObj,"downloadProgress(qint64, qint64)");
	QMetaMethod finishedSignal =            getSignal(replyMetaObj,"finished()");
	QMetaMethod errorSignal =               getSignal(replyMetaObj,"error(QNetworkReply::NetworkError)");
	QMetaMethod readyReadSignal =           getSignal(replyMetaObj,"readyRead()");
	QMetaMethod readChannelFinishedSignal = getSignal(replyMetaObj,"readChannelFinished()");
	#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
	QMetaMethod redirectedSignal =          getSignal(replyMetaObj,"redirected(QUrl)");
	#endif // Qt >= 5.6.0

	QTest::newRow("basic signals") << false << static_cast<BehaviorFlags>(Behavior_Expected) << getExampleRootRequest << (RuleVector() << getExampleRootRule())
	<< (SignalEmissionList()
	    << qMakePair(metaDataChangedSignal, QVariantList())
	    << qMakePair(readyReadSignal, QVariantList())
	    << qMakePair(downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size())
	    << qMakePair(downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size())
	    << qMakePair(readChannelFinishedSignal, QVariantList())
	    << qMakePair(finishedSignal, QVariantList()));

	const QByteArray simpleBody("foo bar");
	Rule::Ptr gerError(new Rule);
	gerError->has(Verb(QNetworkAccessManager::GetOperation))
	        ->has(Url(exampleRootUrl))
	        ->reply()->withStatus(HttpStatus::NotFound)
	                 ->withBody(simpleBody);

	QTest::newRow("error signal") << false << static_cast<BehaviorFlags>(Behavior_Expected) << getExampleRootRequest << (RuleVector() << gerError)
	<< (SignalEmissionList()
	    << qMakePair(metaDataChangedSignal, QVariantList())
	    << qMakePair(readyReadSignal, QVariantList())
	    << qMakePair(downloadProgressSignal, QVariantList() << simpleBody.size() << simpleBody.size())
	    << qMakePair(errorSignal, QVariantList() << QVariant::fromValue(QNetworkReply::ContentNotFoundError))
	    << qMakePair(downloadProgressSignal, QVariantList() << simpleBody.size() << simpleBody.size())
	    << qMakePair(readChannelFinishedSignal, QVariantList())
	    << qMakePair(finishedSignal, QVariantList()));

	QTest::newRow("upload") << false << static_cast<BehaviorFlags>(Behavior_Expected) << postFooRequest() << (RuleVector() << postFooRule())
	<< (SignalEmissionList()
	    << qMakePair(uploadProgressSignal, QVariantList() << fooObject.size() << fooObject.size())
	    << qMakePair(metaDataChangedSignal, QVariantList())
	    << qMakePair(readyReadSignal, QVariantList())
	    << qMakePair(downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size())
	    << qMakePair(downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size())
	    << qMakePair(readChannelFinishedSignal, QVariantList())
	    << qMakePair(finishedSignal, QVariantList()));

	QTest::newRow("final upload(0,0) signal") << false << static_cast<BehaviorFlags>(Behavior_FinalUpload00Signal) << postFooRequest() << (RuleVector() << postFooRule())
	<< (SignalEmissionList()
	    << qMakePair(uploadProgressSignal, QVariantList() << fooObject.size() << fooObject.size())
	    << qMakePair(metaDataChangedSignal, QVariantList())
	    << qMakePair(readyReadSignal, QVariantList())
	    << qMakePair(downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size())
	    << qMakePair(downloadProgressSignal, QVariantList() << statusOkJson.size() << statusOkJson.size())
	    << qMakePair(uploadProgressSignal, QVariantList() << 0 << 0)
	    << qMakePair(readChannelFinishedSignal, QVariantList())
	    << qMakePair(finishedSignal, QVariantList()));

	#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)

		Request gerFollowRedirectRequest = getExampleRootRequest;
		gerFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
		gerFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::AlwaysNetwork);
		gerFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::CacheSaveControlAttribute, false);

		const QUrl barUrl("http://example.com/bar");
		Rule::Ptr getFooRedirect(new Rule);
		getFooRedirect->has(Verb(QNetworkAccessManager::GetOperation))
		              ->has(Url(fooUrl))
		              ->reply()->withRedirect(barUrl);

		const QByteArray barPayload("bar");
		Rule::Ptr getBar(new Rule);
		getBar->has(Verb(QNetworkAccessManager::GetOperation))
		      ->has(Url(barUrl))
		      ->reply()->withBody(barPayload);

		const QUrl getHttpBinJsonUrl("http://eu.httpbin.org/json");
		const int expectedPayload = 429;
		Rule::Ptr gerRedirectHttpBin(new Rule);
		gerRedirectHttpBin->has(Verb(QNetworkAccessManager::GetOperation))
		                  ->has(Url(exampleRootUrl))
		                  ->reply()->withRedirect(getHttpBinJsonUrl);

		QTest::newRow("redirected pass through to real NAM") << true << static_cast<BehaviorFlags>(Behavior_Expected) << gerFollowRedirectRequest << (RuleVector() << gerRedirectHttpBin)
		<< (SignalEmissionList()
		    << qMakePair(metaDataChangedSignal, QVariantList())
		    << qMakePair(redirectedSignal, QVariantList() << getHttpBinJsonUrl)
		    << qMakePair(metaDataChangedSignal, QVariantList())
		    << qMakePair(readyReadSignal, QVariantList())
		    << qMakePair(downloadProgressSignal, QVariantList() << expectedPayload << expectedPayload)
		    << qMakePair(readChannelFinishedSignal, QVariantList())
		    << qMakePair(finishedSignal, QVariantList()));

		QTest::newRow("redirected") << false << static_cast<BehaviorFlags>(Behavior_Expected) << gerFollowRedirectRequest << (RuleVector() << gerRedirectRule() << getFooRedirect << getBar)
		<< (SignalEmissionList()
		    << qMakePair(metaDataChangedSignal, QVariantList())
		    << qMakePair(redirectedSignal, QVariantList() << fooUrl)
		    << qMakePair(metaDataChangedSignal, QVariantList())
		    << qMakePair(redirectedSignal, QVariantList() << barUrl)
		    << qMakePair(metaDataChangedSignal, QVariantList())
		    << qMakePair(readyReadSignal, QVariantList())
		    << qMakePair(downloadProgressSignal, QVariantList() << barPayload.size() << barPayload.size())
		    << qMakePair(downloadProgressSignal, QVariantList() << barPayload.size() << barPayload.size())
		    << qMakePair(readChannelFinishedSignal, QVariantList())
		    << qMakePair(finishedSignal, QVariantList()));

		QTest::newRow("redirected pass through") << false << static_cast<BehaviorFlags>(Behavior_Expected) << gerFollowRedirectRequest << (RuleVector() << gerRedirectRule() << getFooRedirect)
		<< (SignalEmissionList()
		    << qMakePair(metaDataChangedSignal, QVariantList())
		    << qMakePair(redirectedSignal, QVariantList() << fooUrl)
		    << qMakePair(metaDataChangedSignal, QVariantList())
		    << qMakePair(redirectedSignal, QVariantList() << barUrl)
		    << qMakePair(errorSignal, QVariantList() << operationCancelError)
		    << qMakePair(finishedSignal, QVariantList()));

	#endif // Qt >= 5.6.0
}

void ManagerTest::testReplySignals()
{
	QFETCH(bool, useRealNam);
	QFETCH(BehaviorFlags, behaviorFlags);
	QFETCH(Request, request);
	QFETCH(RuleVector, rules);

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setBehaviorFlags(behaviorFlags);
	mnam.setUnmatchedRequestBehavior(PassThrough);
	mnam.setRules(rules);

	Manager<QNetworkAccessManager> mockedRealNam;
	mockedRealNam.setBehaviorFlags(behaviorFlags);
	mockedRealNam.setUnmatchedRequestBehavior(PassThrough);
	mockedRealNam.setRules(rules);


	QScopedPointer<QNetworkReply> reply;
	if (useRealNam)
		reply.reset(sendRequest(mockedRealNam, request));
	else
		reply.reset(sendRequest(mnam, request));

	QSignalInspector signalInspector(reply.data());

	QTest::qWait(100);
	QTRY_VERIFY(reply->isFinished());

	SignalEmissionList emissions = getSignalEmissionListFromInspector(signalInspector);
	if (useRealNam)
	{
		/* The downloadProgress signals are unreliable for real network requests.
		 * Therefore, we remove all of them except for the last one.
		 */
		removeDuplicateSignals(emissions, "downloadProgress");
	}
	QTEST(emissions, "expectedSignals");

}

/*! Provides the data for the testAuthentication() test.
 */
void ManagerTest::testAuthentication_data()
{
	QTest::addColumn<Request>("request");
	QTest::addColumn<RuleVector>("rules");
	QTest::addColumn<StringPair>("credentials");
	QTest::addColumn<int>("statusCode");
	QTest::addColumn<QNetworkReply::NetworkError>("error");
	QTest::addColumn<QString>("expectedRealm");

	const QString defaultRealm("world");

	const QByteArray basicAuthChallenge("Basic realm=\"world\", charset=\"UTF-8\"");
	const QByteArray unsupportedSchemeChallenge("FooAuth realm=\"bar\"");
	const QByteArray multipleBasicAuthChallenges("Basic realm=\"world\", Basic realm=\"second\"");

	const QString username("foo");
	const QString password("bar");

	Rule::Ptr gerUnauthed(new Rule);
	gerUnauthed->has(Url(exampleRootUrl))
	           ->has(Verb(QNetworkAccessManager::GetOperation))
	           ->has(RawHeader(HttpUtils::authorizationHeader(), QByteArray()))
	           ->reply()->withStatus(HttpStatus::Unauthorized)
	                    ->withRawHeader(HttpUtils::wwwAuthenticateHeader(), basicAuthChallenge);

	Rule::Ptr gerAuthed(new Rule);
	gerAuthed->has(Url(exampleRootUrl))
	         ->has(Verb(QNetworkAccessManager::GetOperation))
	         ->has(Authorization(username, password))
	         ->reply()->withStatus(HttpStatus::OK);

	Rule::Ptr gerUnauthedWithoutChallenge(new Rule);
	gerUnauthedWithoutChallenge->has(Url(exampleRootUrl))
	                           ->has(Verb(QNetworkAccessManager::GetOperation))
	                           ->has(RawHeader(HttpUtils::authorizationHeader(), QByteArray()))
	                           ->reply()->withStatus(HttpStatus::Unauthorized);

	Rule::Ptr gerUnsupportedSchemeChallenge(new Rule);
	gerUnsupportedSchemeChallenge->has(Url(exampleRootUrl))
	                             ->has(Verb(QNetworkAccessManager::GetOperation))
	                             ->has(RawHeader(HttpUtils::authorizationHeader(), QByteArray()))
	                             ->reply()->withStatus(HttpStatus::Unauthorized)
	                                      ->withRawHeader(HttpUtils::wwwAuthenticateHeader(), unsupportedSchemeChallenge);

	Rule::Ptr gerMultipleBasicChallenges(new Rule);
	gerMultipleBasicChallenges->has(Url(exampleRootUrl))
	                          ->has(Verb(QNetworkAccessManager::GetOperation))
	                          ->has(RawHeader(HttpUtils::authorizationHeader(), QByteArray()))
	                          ->reply()->withStatus(HttpStatus::Unauthorized)
	                                   ->withRawHeader(HttpUtils::wwwAuthenticateHeader(), multipleBasicAuthChallenges);

	Rule::Ptr gerProxyAuthRequired(new Rule);
	gerProxyAuthRequired->has(Url(exampleRootUrl))
	                    ->has(Verb(QNetworkAccessManager::GetOperation))
	                    ->has(RawHeader(HttpUtils::proxyAuthorizationHeader(), QByteArray()))
	                    ->reply()->withStatus(HttpStatus::ProxyAuthenticationRequired)
	                             ->withRawHeader(HttpUtils::proxyAuthenticateHeader(), basicAuthChallenge);


	QTest::newRow("basic auth") << getExampleRootRequest
	                            << (RuleVector() << gerUnauthed << gerAuthed)
	                            << qMakePair(username, password)
	                            << static_cast<int>(HttpStatus::OK)
	                            << noError
	                            << defaultRealm;

	QTest::newRow("auth and pass through") << getExampleRootRequest
	                                       << (RuleVector() << gerUnauthed)
	                                       << qMakePair(username, password)
	                                       << 0
	                                       << operationCancelError
	                                       << defaultRealm;

	QTest::newRow("401 without challenge") << getExampleRootRequest
	                                       << (RuleVector() << gerUnauthedWithoutChallenge << gerAuthed)
	                                       << qMakePair(username, password)
	                                       << static_cast<int>(HttpStatus::Unauthorized)
	                                       << QNetworkReply::AuthenticationRequiredError
	                                       << QString();

	QTest::newRow("no credentials") << getExampleRootRequest
	                                << (RuleVector() << gerUnauthed << gerAuthed)
	                                << qMakePair(QString(), QString())
	                                << static_cast<int>(HttpStatus::Unauthorized)
	                                << QNetworkReply::AuthenticationRequiredError
	                                << defaultRealm;

	QTest::newRow("unsupported auth scheme") << getExampleRootRequest
	                                         << (RuleVector() << gerUnsupportedSchemeChallenge << gerAuthed)
	                                         << qMakePair(username, password)
	                                         << static_cast<int>(HttpStatus::Unauthorized)
	                                         << QNetworkReply::AuthenticationRequiredError
	                                         << QString();

	QTest::newRow("multiple basic auths") << getExampleRootRequest
	                                      << (RuleVector() << gerMultipleBasicChallenges << gerAuthed)
	                                      << qMakePair(username, password)
	                                      << static_cast<int>(HttpStatus::OK)
	                                      << noError
	                                      << "second";

	QTest::newRow("proxy auth") << getExampleRootRequest
	                            << (RuleVector() << gerProxyAuthRequired)
	                            << qMakePair(username, password)
	                            << static_cast<int>(HttpStatus::ProxyAuthenticationRequired)
	                            << QNetworkReply::ProxyAuthenticationRequiredError
	                            << QString();

}

/*! \test Tests the authentication functionality of the MockNetworkAccess::Manager.
 */
void ManagerTest::testAuthentication()
{
	QFETCH(Request, request);
	QFETCH(RuleVector, rules);
	QFETCH(StringPair, credentials);

	QMutexLocker locker(&m_mutex);
	m_username = credentials.first;
	m_password = credentials.second;
	m_lastAuthRealm.clear();
	m_lastProxyAuthRealm.clear();

	Manager<DummyNetworkAccessManager> mnam;
	QObject::connect(&mnam, SIGNAL(authenticationRequired(QNetworkReply*, QAuthenticator*)), this, SLOT(authenticate(QNetworkReply*, QAuthenticator*)));
	QObject::connect(&mnam, SIGNAL(proxyAuthenticationRequired(const QNetworkProxy&, QAuthenticator*)), this, SLOT(proxyAuthenticate(const QNetworkProxy&, QAuthenticator*)));
	mnam.setRules(rules);

	QScopedPointer<QNetworkReply> reply(sendRequest(mnam, request));

	QTEST(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), "statusCode");
	QTEST(reply->error(), "error");
	QTEST(m_lastAuthRealm, "expectedRealm");
}

/*! Provides the data for the testAuthenticationCache() test.
 */
void ManagerTest::testAuthenticationCache_data()
{
	QTest::addColumn<Request>("request");
	QTest::addColumn<int>("expectedAuthRequiredSignals");

	Request noAuthCacheRequest = getExampleRootRequest;
	noAuthCacheRequest.qRequest.setAttribute(QNetworkRequest::AuthenticationReuseAttribute, QVariant::fromValue(static_cast<int>(QNetworkRequest::Manual)));

	QTest::newRow("no cache") << noAuthCacheRequest << 2;
	QTest::newRow("with cache") << getExampleRootRequest << 1;
}

/*! \test Tests the authentication cache functionality of the MockNetworkAccess::Manager.
 */
void ManagerTest::testAuthenticationCache()
{
	QFETCH(Request, request);

	QMutexLocker locker(&m_mutex);
	m_username = "foo";
	m_password = "bar";

	Manager<DummyNetworkAccessManager> mnam;
	mnam.whenGet(exampleRootUrl)->has(RawHeader(HttpUtils::authorizationHeader(), QByteArray()))->reply()->withStatus(HttpStatus::Unauthorized)->withAuthenticate("world");
	mnam.whenGet(exampleRootUrl)->has(Authorization(m_username, m_password))->reply()->withStatus(HttpStatus::OK);
	QObject::connect(&mnam, SIGNAL(authenticationRequired(QNetworkReply*, QAuthenticator*)), this, SLOT(authenticate(QNetworkReply*, QAuthenticator*)));

	QSignalSpy authenticationRequiredSpy(&mnam, SIGNAL(authenticationRequired(QNetworkReply*, QAuthenticator*)));

	QScopedPointer<QNetworkReply> reply;
	reply.reset(sendRequest(mnam, request));
	QTest::qWait(100);

	reply.reset(sendRequest(mnam, request));
	QTest::qWait(100);

	QTEST(authenticationRequiredSpy.count(), "expectedAuthRequiredSignals");
	QCOMPARE(reply->error(), noError);
	QCOMPARE(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), static_cast<int>(HttpStatus::OK));
}

void ManagerTest::testCookies_data()
{
	QTest::addColumn<Request>("request");
	QTest::addColumn<int>("cookieExpiry");
	QTest::addColumn<QByteArray>("firstReply");
	QTest::addColumn<QByteArray>("secondReply");

	Request noCookieSaveRequest = getExampleRootRequest;
	noCookieSaveRequest.qRequest.setAttribute(QNetworkRequest::CookieSaveControlAttribute, static_cast<int>(QNetworkRequest::Manual));
	Request noCookieLoadRequest = getExampleRootRequest;
	noCookieLoadRequest.qRequest.setAttribute(QNetworkRequest::CookieLoadControlAttribute, static_cast<int>(QNetworkRequest::Manual));

	//                                 // request               // cookieExpiry // firstReply    // secondReply
	QTest::newRow("session cookie")    << getExampleRootRequest << 0            << cookieCreated << cookieSent;
	QTest::newRow("expiring cookie")   << getExampleRootRequest << 10000        << cookieCreated << cookieSent;
	QTest::newRow("expired cookie")    << getExampleRootRequest << -1000        << cookieCreated << cookieCreated;
	QTest::newRow("no automatic save") << noCookieSaveRequest   << 0            << cookieCreated << cookieCreated;
	QTest::newRow("no automatic load") << noCookieLoadRequest   << 0            << cookieCreated << cookieCreated;
}

void ManagerTest::testCookies()
{
	QFETCH(Request, request);
	QFETCH(int, cookieExpiry);

	QNetworkCookie cookie("dummyCookie", "foo");
	cookie.setDomain(".example.com");
	cookie.setPath("/");

	if (cookieExpiry == 0)
		cookie.setExpirationDate(QDateTime()); // Make it a session cookie
	else
		cookie.setExpirationDate(QDateTime::currentDateTime().addMSecs(cookieExpiry));

	QList<QNetworkCookie> setCookies;
	setCookies << cookie;

	const QVariant setCookiesVariant = QVariant::fromValue(setCookies);

	const QByteArray cookieHeader("Cookie");

	Rule::Ptr gerWithCookie(getExampleRootRule());
	gerWithCookie->has(RawHeader(cookieHeader, cookie.toRawForm(QNetworkCookie::NameAndValueOnly)))
	             ->reply()->withBody(cookieSent);

	Rule::Ptr gerSetCookie(new Rule);
	gerSetCookie->has(Verb(QNetworkAccessManager::GetOperation))
	            ->has(Url(exampleRootUrl))
	            ->hasNot(RawHeader(cookieHeader, cookie.toRawForm(QNetworkCookie::NameAndValueOnly)))
	            ->reply()->withHeader(QNetworkRequest::SetCookieHeader, setCookiesVariant)
	                     ->withStatus(HttpStatus::Created)
	                     ->withBody(cookieCreated);


	Manager<DummyNetworkAccessManager> mnam;
	mnam.addRule(gerSetCookie);
	mnam.addRule(gerWithCookie);

	QScopedPointer<QNetworkReply> reply;
	reply.reset(sendRequest(mnam, request));

	QTEST(reply->readAll(), "firstReply");

	reply.reset(sendRequest(mnam, request));

	QTEST(reply->readAll(), "secondReply");
}


#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)

/*! \test Tests the automatic redirect (follow redirect) functionality of the MockNetworkAccessManager.
 */
void ManagerTest::testAutoRedirect()
{
	QFETCH( BehaviorFlags, behaviorFlags );
	QFETCH( int, namRedirectPolicy );
	QFETCH( Request, request );
	QFETCH( RuleVector, rules );
	QFETCH( int, statusCode );

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setBehaviorFlags( behaviorFlags );
	mnam.setUnmatchedRequestBehavior( PassThrough );
	mnam.setRules( rules );

	#if QT_VERSION >= QT_VERSION_CHECK(5, 9, 0)
		if ( namRedirectPolicy >= 0 )
			mnam.setRedirectPolicy( static_cast<QNetworkRequest::RedirectPolicy>( namRedirectPolicy ) );
	#endif // Qt >= 5.9.0


	QScopedPointer<QNetworkReply> reply( sendRequest( mnam, request ) );

	if ( statusCode < 0 )
		QVERIFY( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).isNull() );
	else
		QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), statusCode );
	QTEST( reply->url(), "replyUrl" );
	QTEST( reply->error(), "error" );
	QTEST( static_cast<int>( reply->operation() ), "operation" );
	const QByteArray locationHeader = reply->rawHeader( HttpUtils::locationHeader() );
	const QUrl locationHeaderUrl = reply->header( QNetworkRequest::LocationHeader ).toUrl();
	if ( HttpStatus::isRedirection( statusCode ) )
	{
		if ( ! locationHeader.isNull() )
		{
			QTEST( locationHeaderUrl, "locationHeader" );
			QTEST( reply->attribute( QNetworkRequest::RedirectionTargetAttribute ).toUrl(), "redirectTarget" );
		}
	}
	else
	{
		QVERIFY( locationHeader.isNull() );
		QVERIFY( reply->attribute( QNetworkRequest::RedirectionTargetAttribute ).isNull() );
	}

}

/*! Provides the data for the testAutoRedirect() test.
 */
void ManagerTest::testAutoRedirect_data()
{
	QTest::addColumn<BehaviorFlags>("behaviorFlags");
	QTest::addColumn<int>("namRedirectPolicy");
	QTest::addColumn<Request>("request");
	QTest::addColumn<RuleVector>("rules");
	QTest::addColumn<QNetworkReply::NetworkError>("error");
	QTest::addColumn<int>("statusCode");
	QTest::addColumn<QUrl>("replyUrl");
	QTest::addColumn<QUrl>("locationHeader");
	QTest::addColumn<QUrl>("redirectTarget");
	QTest::addColumn<int>("operation");


#if QT_VERSION >= QT_VERSION_CHECK(5, 9, 0)
	const int manualRedirectPolicy = static_cast<int>(QNetworkRequest::ManualRedirectPolicy);
#else
	const int manualRedirectPolicy = 0;
#endif // Qt >= 5.9.0

	const QNetworkReply::NetworkError insecureRedirectError = QNetworkReply::InsecureRedirectError;

	Rule::Ptr unsafeRedirect(new Rule);
	unsafeRedirect->has(Url(httpsExampleRootUrl))
	              ->reply()->withRedirect(fooUrl);

	Rule::Ptr gerRedirect308(new Rule);
	gerRedirect308->has(Verb(QNetworkAccessManager::GetOperation))
	              ->has(Url(exampleRootUrl))
	              ->reply()->withRedirect(fooUrl, HttpStatus::PermanentRedirect);

	int unknownRedirectCode = 388;
	Rule::Ptr gerRedirectUnknown(new Rule);
	gerRedirectUnknown->has(Verb(QNetworkAccessManager::GetOperation))
	                  ->has(Url(exampleRootUrl))
	                  ->reply()->withRedirect(fooUrl)
	                           ->withAttribute(QNetworkRequest::HttpStatusCodeAttribute, unknownRedirectCode);

	Rule::Ptr gerNotModified(new Rule);
	gerNotModified->has(Verb(QNetworkAccessManager::GetOperation))
	              ->has(Url(exampleRootUrl))
	              ->reply()->withStatus(HttpStatus::NotModified)
	              ->withRawHeader("Date", "Sun, 06 Nov 1994 08:49:37 GMT");

	Rule::Ptr perRedirect302(new Rule);
	perRedirect302->has(Verb(QNetworkAccessManager::PostOperation))
	              ->has(Url(exampleRootUrl))
	              ->reply()->withRedirect(fooUrl, HttpStatus::Found);

	Rule::Ptr perRedirect307(new Rule);
	perRedirect307->has(Verb(QNetworkAccessManager::PostOperation))
	              ->has(Url(exampleRootUrl))
	              ->reply()->withRedirect(fooUrl, HttpStatus::TemporaryRedirect);

	Rule::Ptr perRedirect303(new Rule);
	perRedirect303->has(Verb(QNetworkAccessManager::PostOperation))
	              ->has(Url(exampleRootUrl))
	              ->reply()->withRedirect(fooUrl, HttpStatus::SeeOther);

	Rule::Ptr exampleRootRedirect(new Rule);
	exampleRootRedirect->has(Url(exampleRootUrl))
	                   ->reply()->withRedirect(fooUrl);

	Rule::Ptr fooOptionsRule(new Rule);
	fooOptionsRule->has(Verb(QNetworkAccessManager::CustomOperation, "OPTIONS"))
	              ->has(Url(fooUrl))
	              ->reply()->withStatus(HttpStatus::OK);

	Rule::Ptr gerRelativeRedirectRule(new Rule);
	gerRelativeRedirectRule->has(Verb(QNetworkAccessManager::GetOperation))
	                       ->has(Url(exampleRootUrl))
	                       ->reply()->withRedirect(QUrl("foo"));

	Rule::Ptr gerProtRelRedirectRule(new Rule);
	gerProtRelRedirectRule->has(Verb(QNetworkAccessManager::GetOperation))
	                      ->has(Url(exampleRootUrl))
	                      ->reply()->withRedirect(QUrl("//example.com/foo"));

	const QUrl fooProtocolUrl("foo://bar");
	Rule::Ptr unsupportedProtocolRedirect(new Rule);
	unsupportedProtocolRedirect->has(Url(exampleRootUrl))
	                           ->reply()->withRedirect(fooProtocolUrl);

	const QUrl barUrl("http://example.com/bar");
	Rule::Ptr getFooRedirect(new Rule);
	getFooRedirect->has(Verb(QNetworkAccessManager::GetOperation))
	           ->has(Url(fooUrl))
	           ->reply()->withRedirect(barUrl);

	Request gerFollowRedirectRequest = getExampleRootRequest;
	gerFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);

	Request gerNoFollowRedirectRequest = getExampleRootRequest;
	gerNoFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, false);

	const QNetworkRequest httpsExampleRootRequest(httpsExampleRootUrl);
	Request httpsGerFollowRedirectRequest(httpsExampleRootRequest);
	httpsGerFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);

	Request perFollowRedirectRequest(QNetworkAccessManager::PostOperation, QNetworkRequest(exampleRootUrl), fooObject);
	perFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);

	Request optionsFollowRedirectRequest(QNetworkAccessManager::CustomOperation, QNetworkRequest(exampleRootUrl));
	optionsFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::CustomVerbAttribute, QString("OPTIONS"));
	optionsFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);

	Request customVerbFollowRedirectRequest(QNetworkAccessManager::CustomOperation, QNetworkRequest(exampleRootUrl));
	customVerbFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::CustomVerbAttribute, QString("FOO"));
	customVerbFollowRedirectRequest.qRequest.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);

	const BehaviorFlags qt_5_6_Behavior = Behavior_Qt_5_6_0;
	const BehaviorFlags expectedBehavior = Behavior_Expected;

	//                                                              // behaviorFlags    // namRedirectPolicy    // request                         // rules                                                     // error                 // statusCode                                      // replyUrl            // locationHeader // redirectTarget // operation
	QTest::newRow("follow redirect not enabled")                    << qt_5_6_Behavior  << manualRedirectPolicy << getExampleRootRequest           << (RuleVector() << gerRedirectRule() << getFooRule())       << noError               << static_cast<int>(HttpStatus::Found)             << exampleRootUrl      << fooUrl         << fooUrl         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("follow redirect disabled")                       << qt_5_6_Behavior  << manualRedirectPolicy << gerNoFollowRedirectRequest      << (RuleVector() << gerRedirectRule() << getFooRule())       << noError               << static_cast<int>(HttpStatus::Found)             << exampleRootUrl      << fooUrl         << fooUrl         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("unsafe follow redirect")                         << qt_5_6_Behavior  << manualRedirectPolicy << httpsGerFollowRedirectRequest   << (RuleVector() << unsafeRedirect)                          << insecureRedirectError << static_cast<int>(HttpStatus::Found)             << httpsExampleRootUrl << fooUrl         << fooUrl         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("redirect not to be followed")                    << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << gerNotModified)                          << noError               << static_cast<int>(HttpStatus::NotModified)       << exampleRootUrl      << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("redirect match rule")                            << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << gerRedirectRule() << getFooRule())       << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("redirect pass through")                          << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << gerRedirectRule())                       << operationCancelError  << -1                                              << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("multi redirect pass through")                    << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << gerRedirectRule() << getFooRedirect)     << operationCancelError  << -1                                              << barUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("POST redirect 301 (Qt behavior)")                << qt_5_6_Behavior  << manualRedirectPolicy << perFollowRedirectRequest        << (RuleVector() << perRedirect302 << getFooRule())          << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("POST redirect 301 (expected behavior)")          << expectedBehavior << manualRedirectPolicy << perFollowRedirectRequest        << (RuleVector() << perRedirect302 << getFooRule())          << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("POST redirect 307 (Qt behavior)")                << qt_5_6_Behavior  << manualRedirectPolicy << perFollowRedirectRequest        << (RuleVector() << perRedirect307 << postFooRule())         << operationCancelError  << -1                                              << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("POST redirect 307 (expected behavior)")          << expectedBehavior << manualRedirectPolicy << perFollowRedirectRequest        << (RuleVector() << perRedirect307 << postFooRule())         << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::PostOperation);
	QTest::newRow("GET redirect 308 (Qt behavior)")                 << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << gerRedirect308 << getFooRule())          << noError               << static_cast<int>(HttpStatus::PermanentRedirect) << exampleRootUrl      << fooUrl         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("GET redirect 308 (expected behavior)")           << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << gerRedirect308 << getFooRule())          << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("POST redirect 303 (expected behavior)")          << expectedBehavior << manualRedirectPolicy << perFollowRedirectRequest        << (RuleVector() << perRedirect303 << getFooRule())          << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("unknown redirect code")                          << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << gerRedirectUnknown << getFooRule())      << noError               << unknownRedirectCode                             << exampleRootUrl      << fooUrl         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("redirect unsupported protocol")                  << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << unsupportedProtocolRedirect)             << protocolUnknownError  << static_cast<int>(HttpStatus::Found)             << exampleRootUrl      << fooProtocolUrl << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("relative redirect (Qt behavior)")                << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << gerRelativeRedirectRule << getFooRule()) << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("relative redirect (expected behavior)")          << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << gerRelativeRedirectRule << getFooRule()) << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("protocol relative redirect (Qt behavior)")       << qt_5_6_Behavior  << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << gerProtRelRedirectRule << getFooRule())  << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("protocol relative redirect (expected behavior)") << expectedBehavior << manualRedirectPolicy << gerFollowRedirectRequest        << (RuleVector() << gerProtRelRedirectRule << getFooRule())  << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);
	QTest::newRow("OPTIONS redirect (expected behavior)")           << expectedBehavior << manualRedirectPolicy << optionsFollowRedirectRequest    << (RuleVector() << exampleRootRedirect << fooOptionsRule)   << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::CustomOperation);
	QTest::newRow("custom verb redirect (expected behavior)")       << expectedBehavior << manualRedirectPolicy << customVerbFollowRedirectRequest << (RuleVector() << exampleRootRedirect << getFooRule())     << noError               << static_cast<int>(HttpStatus::OK)                << fooUrl              << QUrl()         << QUrl()         << static_cast<int>(QNetworkAccessManager::GetOperation);


	const QUrl redirect1Url("http://example.com/redir1");

	Rule::Ptr redirect1(new Rule);
	redirect1->has(Verb(QNetworkAccessManager::GetOperation))
	         ->has(Url(exampleRootUrl))
	         ->reply()->withRedirect(redirect1Url);
	Rule::Ptr redirect2(new Rule);
	redirect2->has(Verb(QNetworkAccessManager::GetOperation))
	         ->has(Url(redirect1Url))
	         ->reply()->withRedirect(fooUrl);

	Request gerLimitedFollowRedirectRequest = gerFollowRedirectRequest;
	gerLimitedFollowRedirectRequest.qRequest.setMaximumRedirectsAllowed(1);

	QTest::newRow("redirect limit") << qt_5_6_Behavior << manualRedirectPolicy << gerLimitedFollowRedirectRequest << (RuleVector() << getFooRule() << redirect1 << redirect2) << QNetworkReply::TooManyRedirectsError << static_cast<int>(HttpStatus::Found) << redirect1Url << fooUrl << fooUrl << static_cast<int>(QNetworkAccessManager::GetOperation);

	#if QT_VERSION >= QT_VERSION_CHECK(5, 9, 0)

		Request safeRedirectRequest(httpsExampleRootRequest);
		safeRedirectRequest.qRequest.setAttribute(QNetworkRequest::RedirectPolicyAttribute, static_cast<int>(QNetworkRequest::NoLessSafeRedirectPolicy));

		Rule::Ptr otherOriginRedirect(new Rule);
		otherOriginRedirect->has(Url(httpsExampleRootUrl))
		                   ->reply()->withRedirect(httpsOtherOriginUrl);

		Rule::Ptr otherOrigin(new Rule);
		otherOrigin->has(Url(httpsOtherOriginUrl))
		           ->reply()->withStatus(HttpStatus::OK);

		QTest::newRow("safe redirect")   << qt_5_6_Behavior << manualRedirectPolicy << safeRedirectRequest << (RuleVector() << otherOriginRedirect << otherOrigin) << noError               << static_cast<int>(HttpStatus::OK)    << httpsOtherOriginUrl << QUrl() << QUrl() << static_cast<int>(QNetworkAccessManager::GetOperation);
		QTest::newRow("unsafe redirect") << qt_5_6_Behavior << manualRedirectPolicy << safeRedirectRequest << (RuleVector() << unsafeRedirect)                     << insecureRedirectError << static_cast<int>(HttpStatus::Found) << httpsExampleRootUrl << fooUrl << fooUrl << static_cast<int>(QNetworkAccessManager::GetOperation);

		Request sameOriginRedirectRequest(httpsExampleRootRequest);
		sameOriginRedirectRequest.qRequest.setAttribute(QNetworkRequest::RedirectPolicyAttribute, static_cast<int>(QNetworkRequest::SameOriginRedirectPolicy));

		Rule::Ptr sameOriginRedirect(new Rule);
		sameOriginRedirect->has(Url(httpsExampleRootUrl))
		                  ->reply()->withRedirect(httpsFooUrl);

		Rule::Ptr getHttpsFoo(new Rule);
		getHttpsFoo->has(Url(httpsFooUrl))
		           ->reply()->withStatus(HttpStatus::OK);

		QTest::newRow("same origin redirect")     << qt_5_6_Behavior << manualRedirectPolicy << sameOriginRedirectRequest << (RuleVector() << sameOriginRedirect  << getHttpsFoo)    << noError               << static_cast<int>(HttpStatus::OK)    << httpsFooUrl         << QUrl() << QUrl()    << static_cast<int>(QNetworkAccessManager::GetOperation);
		QTest::newRow("other origin redirect")    << qt_5_6_Behavior << manualRedirectPolicy << sameOriginRedirectRequest << (RuleVector() << otherOriginRedirect << otherOrigin)    << insecureRedirectError << static_cast<int>(HttpStatus::Found) << httpsExampleRootUrl << httpsOtherOriginUrl << httpsOtherOriginUrl << static_cast<int>(QNetworkAccessManager::GetOperation);
		QTest::newRow("other protocol redirect")  << qt_5_6_Behavior << manualRedirectPolicy << sameOriginRedirectRequest << (RuleVector() << unsafeRedirect      << getFooRule())   << insecureRedirectError << static_cast<int>(HttpStatus::Found) << httpsExampleRootUrl << fooUrl << fooUrl    << static_cast<int>(QNetworkAccessManager::GetOperation);

		QTest::newRow("nam policy redirect")      << qt_5_6_Behavior << static_cast<int>(QNetworkRequest::NoLessSafeRedirectPolicy) << getExampleRootRequest << (RuleVector() << getFooRule() << gerRedirectRule()) << noError << static_cast<int>(HttpStatus::OK) << fooUrl << QUrl() << QUrl() << static_cast<int>(QNetworkAccessManager::GetOperation);

		Request httpsGerRequest(httpsExampleRootRequest);

		QTest::newRow("nam same origin redirect") << qt_5_6_Behavior << static_cast<int>(QNetworkRequest::SameOriginRedirectPolicy) << httpsGerRequest << (RuleVector() << sameOriginRedirect << getHttpsFoo) << noError << static_cast<int>(HttpStatus::OK) << httpsFooUrl << QUrl() << QUrl() << static_cast<int>(QNetworkAccessManager::GetOperation);

	#endif // Qt >= 5.9.0
}


#endif // Qt >= 5.6.0


#if QT_VERSION >= QT_VERSION_CHECK(5, 9, 0)

/*! Provides the data for the testHsts() test.
 */
void ManagerTest::testHsts_data()
{
	QTest::addColumn<Request>("request");
	QTest::addColumn<RuleVector>("rules");
	QTest::addColumn<HstsPolicyVector>("hstsPolicies");
	QTest::addColumn<QUrl>("replyUrl");
	QTest::addColumn<int>("statusCode");


	const QHstsPolicy exampleHstsPolicy(QDateTime::currentDateTime().addDays(1), QHstsPolicy::PolicyFlags(), "example.com");
	const QHstsPolicy subdomainHstsPolicy(QDateTime::currentDateTime().addDays(1), QHstsPolicy::IncludeSubDomains, "sub.example.com");
	const QHstsPolicy expiredHstsPolicy(QDateTime::currentDateTime().addSecs(-100), QHstsPolicy::PolicyFlags(), "example.com");

	Rule::Ptr httpsGerRule(new Rule);
	httpsGerRule->has(Url(httpsExampleRootUrl))
	              ->reply()->withStatus(HttpStatus::ImATeapot);

	QTest::newRow("no hsts policy") << getExampleRootRequest << (RuleVector() << getExampleRootRule() << httpsGerRule) << HstsPolicyVector() << exampleRootUrl << static_cast<int>(HttpStatus::OK);

	Rule::Ptr httpsFooRule(new Rule);
	httpsFooRule->has(Url(httpsFooUrl))
	              ->reply()->withStatus(HttpStatus::ImATeapot);

	QTest::newRow("example hsts policy") << getFooRequest << (RuleVector() << getFooRule() << httpsFooRule) << (HstsPolicyVector() << exampleHstsPolicy) << httpsFooUrl << static_cast<int>(HttpStatus::ImATeapot);

	const int httpDefaultPort(80);
	const int httpsDefaultPort(443);
	QUrl explicitPortFooUrl(fooUrl);
	explicitPortFooUrl.setPort(httpDefaultPort);
	Request explicitPortFooRequest = getFooRequest;
	explicitPortFooRequest.qRequest.setUrl(explicitPortFooUrl);
	QUrl explicitPortHttpsFooUrl(httpsFooUrl);
	explicitPortHttpsFooUrl.setPort(httpsDefaultPort);
	QTest::newRow("explicit port") << explicitPortFooRequest << (RuleVector() << getFooRule() << httpsFooRule) << (HstsPolicyVector() << exampleHstsPolicy) << explicitPortHttpsFooUrl << static_cast<int>(HttpStatus::ImATeapot);

	const QHstsPolicy ipHstsPolicy(QDateTime::currentDateTime().addDays(1), QHstsPolicy::PolicyFlags(), "127.0.0.0");
	const QUrl ipUrl("http://127.0.0.0");
	const Request ipRequest(QNetworkAccessManager::GetOperation, QNetworkRequest(ipUrl));
	Rule::Ptr httpIpRule(new Rule);
	httpIpRule->has(Url(ipUrl))
	            ->reply()->withStatus(HttpStatus::OK);
	Rule::Ptr httpsIpRule(new Rule);
	httpsIpRule->has(Url("https://127.0.0.0"))
	             ->reply()->withStatus(HttpStatus::ImATeapot);
	QTest::newRow("ip request") << ipRequest << (RuleVector() << httpIpRule << httpsIpRule) << (HstsPolicyVector() << ipHstsPolicy) << ipUrl << static_cast<int>(HttpStatus::OK);

	const QUrl subdomainUrl("http://sub.sub.example.com");
	const QUrl httpsSubdomainUrl("https://sub.sub.example.com");
	const QNetworkRequest subdomainRequestObj(subdomainUrl);
	const Request subdomainRequest(subdomainRequestObj);
	Rule::Ptr subdomainRule(new Rule);
	subdomainRule->has(Url(httpsSubdomainUrl))
	               ->reply()->withStatus(HttpStatus::OK);

	QTest::newRow("subdomain hsts policy") << subdomainRequest << (RuleVector() << subdomainRule) << (HstsPolicyVector() << subdomainHstsPolicy) << httpsSubdomainUrl << static_cast<int>(HttpStatus::OK);

	const QUrl otherSubdomainUrl("http://other.example.com");
	const QNetworkRequest otherSubdomainRequestObj(otherSubdomainUrl);
	const Request otherSubdomainRequest(otherSubdomainRequestObj);

	QTest::newRow("no matching hsts policy") << otherSubdomainRequest << (RuleVector() << subdomainRule) << (HstsPolicyVector() << subdomainHstsPolicy << exampleHstsPolicy) << otherSubdomainUrl << 0;

	QTest::newRow("expired hsts policy") << getExampleRootRequest << (RuleVector() << getExampleRootRule() << httpsGerRule) << (HstsPolicyVector() << expiredHstsPolicy) << exampleRootUrl << static_cast<int>(HttpStatus::OK);
}

/*! \test Tests the request rewriting according to HTTP strict transport security.
 */
void ManagerTest::testHsts()
{
	QFETCH(Request, request);
	QFETCH(RuleVector, rules);
	QFETCH(HstsPolicyVector, hstsPolicies);

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setRules(rules);
	mnam.setStrictTransportSecurityEnabled(true);
	mnam.addStrictTransportSecurityHosts(hstsPolicies);

	QScopedPointer<QNetworkReply> reply(sendRequest(mnam, request));

	QTEST(reply->url(), "replyUrl");
	QTEST(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), "statusCode");
}

/*! Provides the data the for testHstsPolicyExpiry() test.
 */
void ManagerTest::testHstsPolicyExpiry_data()
{
	QTest::addColumn<Request>("request");

	QTest::newRow("full host name match") << getExampleRootRequest;

	const QUrl subdomainUrl("http://sub.sub.example.com");
	const QNetworkRequest subdomainRequestObj(subdomainUrl);
	const Request subdomainRequest(subdomainRequestObj);
	QTest::newRow("sub-domain match") << subdomainRequest;
}

/*! \test Tests the expiration of HSTS policies.
 */
void ManagerTest::testHstsPolicyExpiry()
{
	QFETCH(Request, request);

	Manager<DummyNetworkAccessManager> mnam;
	mnam.addRule(getExampleRootRule());
	mnam.whenGet(httpsExampleRootUrl)->reply()->withStatus(HttpStatus::ImATeapot);
	mnam.whenGet(QRegularExpression("http://(.+\\.)?sub\\.example\\.com"))->reply()->withStatus(HttpStatus::OK);
	mnam.whenGet(QRegularExpression("https://(.+\\.)?sub\\.example\\.com"))->reply()->withStatus(HttpStatus::ImATeapot);
	mnam.setStrictTransportSecurityEnabled(true);

	const int policyExpiry = 200;

	HstsPolicyVector hstsPolicies;
	hstsPolicies << QHstsPolicy(QDateTime::currentDateTime().addMSecs(policyExpiry), QHstsPolicy::PolicyFlags(), "example.com");
	hstsPolicies << QHstsPolicy(QDateTime::currentDateTime().addMSecs(policyExpiry), QHstsPolicy::IncludeSubDomains, "sub.example.com");

	mnam.addStrictTransportSecurityHosts(hstsPolicies);

	QScopedPointer<QNetworkReply> reply;

	reply.reset(sendRequest(mnam, request)); // Send the request to fill the HSTS policy hash

	QTest::qWait(policyExpiry + 200); // Ensure policies are expired

	reply.reset(sendRequest(mnam, request));

	QCOMPARE(reply->error(), QNetworkReply::NoError);
	QCOMPARE(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), static_cast<int>(HttpStatus::OK));
}

/*! Provides the data for the testHstsResponse() test.
 */
void ManagerTest::testHstsResponse_data()
{
	QTest::addColumn<QByteArray>("stsHeader");
	QTest::addColumn<int>("maxAge");
	QTest::addColumn<QHstsPolicy>("expectedPolicy");

	QHstsPolicy policy;
	policy.setHost("example.com");
	QTest::newRow("simple policy") << QByteArray("max-age=17") << 17 << policy;

	QTest::newRow("multiple headers") << QByteArray("max-age=17,max-age=42") << 17 << policy;
	QTest::newRow("invalid headers") << QByteArray("foo=bar,max-age=17,includeSubdomains") << 17 << policy;

	policy.setIncludesSubDomains(true);
	QTest::newRow("include subdomain") << QByteArray("max-age=17; includeSubdomains") << 17 << policy;

}

/*! \test Tests the handling of HSTS response headers.
 */
void ManagerTest::testHstsResponse()
{
	QFETCH(QByteArray, stsHeader);
	QFETCH(int, maxAge);
	QFETCH(QHstsPolicy, expectedPolicy);

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setStrictTransportSecurityEnabled(true);
	mnam.whenGet(httpsFooUrl)
	    ->reply()->withRawHeader("Strict-Transport-Security", stsHeader);

	QScopedPointer<QNetworkReply> reply(mnam.get(QNetworkRequest(httpsFooUrl)));
	expectedPolicy.setExpiry(QDateTime::currentDateTimeUtc().addSecs(maxAge));

	QSignalSpy finishedSpy(reply.data(), SIGNAL(finished()));

	QTRY_VERIFY2(!finishedSpy.isEmpty(), "Reply did not finish");
	QVector<QHstsPolicy> stsHosts = mnam.strictTransportSecurityHosts();
	QVector<QHstsPolicy>::ConstIterator iter;
	for (iter = stsHosts.cbegin(); iter != stsHosts.cend(); ++iter)
	{
		if (   iter->host() == expectedPolicy.host()
		    && iter->includesSubDomains() == expectedPolicy.includesSubDomains()
		    && iter->expiry().secsTo(expectedPolicy.expiry()) < 2)
			return;
	}
	QFAIL("HSTS policy missing in network access manager");
}
/*! Provides the data for the testInvalidHstsHeaders() test.
 */
void ManagerTest::testInvalidHstsHeaders_data()
{
	QTest::addColumn<QByteArray>("stsHeader");

	QTest::newRow("empty header")            << QByteArray("");
	QTest::newRow("duplicate directive")     << QByteArray("max-age=10; max-age=20");
	QTest::newRow("invalid max-age")         << QByteArray("max-age=foo");
	QTest::newRow("invalid directive name")  << QByteArray("foo bar=test");
	QTest::newRow("invalid directive value") << QByteArray("foo=invalid value");
	QTest::newRow("missing max-age")         << QByteArray("includeSubdomains");
}

/*! \test Tests the handling of invalid HSTS response headers.
 */
void ManagerTest::testInvalidHstsHeaders()
{
	QFETCH(QByteArray, stsHeader);

	Manager<DummyNetworkAccessManager> mnam;
	mnam.setStrictTransportSecurityEnabled(true);
	mnam.whenGet(httpsFooUrl)
	        ->reply()->withRawHeader("Strict-Transport-Security", stsHeader);

	QScopedPointer<QNetworkReply> reply(mnam.get(QNetworkRequest(httpsFooUrl)));

	QSignalSpy finishedSpy(reply.data(), SIGNAL(finished()));

	QTRY_VERIFY2(!finishedSpy.isEmpty(), "Reply did not finish");
	QVector<QHstsPolicy> stsHosts = mnam.strictTransportSecurityHosts();
	QVERIFY(stsHosts.isEmpty());
}

#endif // Qt >= 5.9.0

} // namespace Tests


QTEST_MAIN(Tests::ManagerTest)
#include "ManagerTest.moc"
