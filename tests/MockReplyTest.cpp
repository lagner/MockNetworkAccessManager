#include "QSignalInspector.hpp"
#include "TestUtils.hpp"

#include "MockNetworkAccessManager.hpp"

#include <QtTest>
#include <QtDebug>
#include <QSharedPointer>
#include <QScopedPointer>

namespace Tests {

/*! Implements unit tests for the MockReply and MockReplyBuilder classes.
 */
class MockReplyTest : public QObject
{
	Q_OBJECT

private slots:
	void testCopy();
	void testCopy_data();
	void testCloneFinishedReply();
	void testIsRedirectToBeFollowed();
	void testIsRedirectToBeFollowed_data();
	void testPrepare();
	void testPrepare_data();
	void testFinish_data();
	void testFinish();
	void testAbort();

	void testNullBuilder();
	void testWithBodyByteArray_data();
	void testWithBodyByteArray();
	void testWithBodyJson_data();
	void testWithBodyJson();
	void testWithFile_data();
	void testWithFile();
	void testWithStatus_data();
	void testWithStatus();
	void testWithError_data();
	void testWithError();
	void testWithRedirect();
	void testWithRedirect_data();
	void testWithHeader();
	void testWithHeader_data();
	void testWithRawHeader_data();
	void testWithRawHeader();
	void testWithAttribute_data();
	void testWithAttribute();
	void testWithAuthenticate_data();
	void testWithAuthenticate();
	void testWithCookie_data();
	void testWithCookie();
	void testEqualityOperators_data();
	void testEqualityOperators();

};


using namespace MockNetworkAccess;


//####### Helpers #######

typedef QHash<QNetworkRequest::Attribute, QVariant> AttributeHash;
typedef QSharedPointer<MockReplyBuilder> MockReplyBuilderPtr;
typedef QScopedPointer<MockReply> MockReplyScopedPtr;



class MockReplyTestHelper : public MockReply
{
	Q_OBJECT

public:
	/* Hack to access protected methods of base class: https://stackoverflow.com/a/23904680/490560
	 * For this to work, MockReplyTestHelper may not implement methods with the same name
	 * as the protected methods of MockReply we want to call.
	 * Therefore, we use a "call" prefix.
	 */

	static void callPrepare(MockReply* reply, const Request& req)
	{
		(reply->*&MockReplyTestHelper::prepare)(req);
	}

	static void callFinish(MockReply* reply, const Request& req)
	{
		(reply->*&MockReplyTestHelper::finish)(req);
	}

	static void callSetBehaviorFlags(MockReply* reply, BehaviorFlags flags)
	{
		(reply->*&MockReplyTestHelper::setBehaviorFlags)(flags);
	}

};


const QByteArray contentTypeTextPlain("text/plain");
const QByteArray contentTypeOctetstream("application/octet-stream");
const QByteArray contentTypeJson("application/json");
const QByteArray customHeader("X-Custom-Header");


namespace HelperMethods
{
	MockReplyBuilderPtr createFullBuilder();
	MockReplyBuilderPtr createErrorBuilder();
	HeaderHash getKnownHeaders(QNetworkReply* reply);
};

} // namespace Tests


Q_DECLARE_METATYPE(MockNetworkAccess::BehaviorFlags)
Q_DECLARE_METATYPE(MockNetworkAccess::MockReplyBuilder)
Q_DECLARE_METATYPE(MockNetworkAccess::HeaderHash)
Q_DECLARE_METATYPE(Tests::MockReplyBuilderPtr)
Q_DECLARE_METATYPE(QNetworkRequest::KnownHeaders)
Q_DECLARE_METATYPE(QNetworkRequest::Attribute)
Q_DECLARE_METATYPE(MockNetworkAccess::HttpUtils::Authentication::Challenge::Ptr)


namespace Tests
{

//####### Tests #######


/*! \test Tests the methods to copy MockReplyBuilder objects.
 * Implicitly also tests the MockReply::clone() method.
 */
void MockReplyTest::testCopy()
{
	QFETCH(MockReplyBuilderPtr, original);
	QFETCH(MockReplyBuilderPtr, copy);

	MockReplyScopedPtr originalReply(original->createReply());
	MockReplyScopedPtr copyReply(copy->createReply());

	if (!originalReply)
		QVERIFY(!copyReply);
	else
	{
		QCOMPARE(copyReply->isFinished(), originalReply->isFinished());
		QCOMPARE(copyReply->isOpen(), originalReply->isOpen());
		QCOMPARE(copyReply->body(), originalReply->body());
		QCOMPARE(copyReply->bytesAvailable(), originalReply->bytesAvailable());
		QCOMPARE(copyReply->size(), originalReply->size());
		QCOMPARE(copyReply->attributes(), originalReply->attributes());
		QCOMPARE(copyReply->rawHeaderPairs(), originalReply->rawHeaderPairs());
		QCOMPARE(HelperMethods::getKnownHeaders(copyReply.data()), HelperMethods::getKnownHeaders(originalReply.data()));
		QCOMPARE(copyReply->error(), originalReply->error());
	}
}

/*! Provides the data for the testCopy() test.
 */
void MockReplyTest::testCopy_data()
{
	QTest::addColumn<MockReplyBuilderPtr>("original");
	QTest::addColumn<MockReplyBuilderPtr>("copy");

	MockReplyBuilderPtr original;
	MockReplyBuilderPtr copy;

	// Unconfigured builder
	original.reset(new MockReplyBuilder());
	copy.reset(new MockReplyBuilder(*original));
	QTest::newRow("unconfigured; copy constructor") << original << copy;

	copy.reset(new MockReplyBuilder(MockReplyBuilder(*original)));
	QTest::newRow("unconfigured; move constructor") << original << copy;

	copy.reset(new MockReplyBuilder());
	*copy = *original;
	QTest::newRow("unconfigured; copy operator") << original << copy;

	copy.reset(new MockReplyBuilder());
	*copy = MockReplyBuilder(*original);
	QTest::newRow("unconfigured; move operator") << original << copy;

	copy.reset(new MockReplyBuilder());
	copy->with(original.data());
	QTest::newRow("unconfigured; with method") << original << copy;

	copy.reset(new MockReplyBuilder());
	copy->with(MockReplyBuilder(*original));
	QTest::newRow("unconfigured; with move method") << original << copy;

	// Successful replies
	original.reset(new MockReplyBuilder());
	original->withStatus(HttpStatus::OK);
	original->withBody("foo bar");
	original->withHeader(QNetworkRequest::ContentTypeHeader, QString("text/plain"));
	original->withAttribute(QNetworkRequest::HttpStatusCodeAttribute, 200);
	copy.reset(new MockReplyBuilder(*original));
	QTest::newRow("successful replies; copy constructor") << original << copy;

	copy.reset(new MockReplyBuilder(MockReplyBuilder(*original)));
	QTest::newRow("successful replies; move constructor") << original << copy;

	copy.reset(new MockReplyBuilder());
	*copy = *original;
	QTest::newRow("successful replies; copy operator") << original << copy;

	copy.reset(new MockReplyBuilder());
	*copy = MockReplyBuilder(*original);
	QTest::newRow("successful replies; move operator") << original << copy;

	copy.reset(new MockReplyBuilder());
	copy->with(original.data());
	QTest::newRow("successful replies; with method") << original << copy;

	copy.reset(new MockReplyBuilder());
	copy->with(MockReplyBuilder(*original));
	QTest::newRow("successful replies; with move method") << original << copy;
}

/*! \test Tests MockReply::clone() for finished MockReply objects.
 */
void MockReplyTest::testCloneFinishedReply()
{
	MockReplyBuilderPtr builder(new MockReplyBuilder());
	builder->withStatus(HttpStatus::OK);
	builder->withBody("foo bar");

	MockReplyScopedPtr reply(builder->createReply());

	Request request(QNetworkRequest(QUrl("http://example.com")));
	MockReplyTestHelper::callFinish(reply.data(), request);

	MockReplyScopedPtr clone(reply->clone());

	QCOMPARE(clone->isFinished(), reply->isFinished());
	QCOMPARE(clone->isOpen(), reply->isOpen());
	QCOMPARE(clone->body(), reply->body());
	QCOMPARE(clone->bytesAvailable(), reply->bytesAvailable());
	QCOMPARE(clone->size(), reply->size());
	QCOMPARE(clone->attributes(), reply->attributes());
	QCOMPARE(clone->rawHeaderPairs(), reply->rawHeaderPairs());
	QCOMPARE(HelperMethods::getKnownHeaders(clone.data()), HelperMethods::getKnownHeaders(reply.data()));
	QCOMPARE(clone->error(), reply->error());

}

/*! \test Tests MockReply::isRedirectToBeFollowed().
 */
void MockReplyTest::testIsRedirectToBeFollowed()
{
	QFETCH( MockReplyBuilder, builder );
	QFETCH( BehaviorFlags, behaviorFlags );

	MockReplyScopedPtr reply( builder.createReply() );

	MockReplyTestHelper::callSetBehaviorFlags( reply.data(), behaviorFlags );

	QTEST( reply->isRedirectToBeFollowed(), "isRedirectToBeFollowed" );

}

/*! Provides the data for the MockReplyTest::testIsRedirectToBeFollowed() test.
 */
void MockReplyTest::testIsRedirectToBeFollowed_data()
{
	QTest::addColumn<MockReplyBuilder>( "builder" );
	QTest::addColumn<BehaviorFlags>( "behaviorFlags" );
	QTest::addColumn<bool>( "isRedirectToBeFollowed" );

	const QUrl exampleRootUrl("http://example.com");

	MockReplyBuilder noStatusCodeBuilder;
	noStatusCodeBuilder.withBody("foo");
	MockReplyBuilder invalidLocationBuilder;
	invalidLocationBuilder.withRedirect(QUrl());
	MockReplyBuilder relativeRedirectBuilder;
	relativeRedirectBuilder.withRedirect(QUrl("foo/bar"));
	MockReplyBuilder protRelRedirectBuilder;
	protRelRedirectBuilder.withRedirect(QUrl("//example.com/foo"));
	MockReplyBuilder redirect302Builder;
	redirect302Builder.withRedirect(exampleRootUrl, HttpStatus::Found);
	MockReplyBuilder redirect307Builder;
	redirect307Builder.withRedirect(exampleRootUrl, HttpStatus::TemporaryRedirect);
	MockReplyBuilder redirect308Builder;
	redirect308Builder.withRedirect(exampleRootUrl, HttpStatus::PermanentRedirect);

	const BehaviorFlags qt_5_6_Behavior = Behavior_Qt_5_6_0;
	const BehaviorFlags expectedBehavior = Behavior_Expected;


	//                                                       // builder                 //behaviorFlags     // isRedirectToBeFollowed
	QTest::newRow( "no status code" )                        << noStatusCodeBuilder     << qt_5_6_Behavior  << false;
	QTest::newRow( "invalid location header" )               << invalidLocationBuilder  << qt_5_6_Behavior  << false;
	QTest::newRow( "relative redirect (Qt 5.6)" )            << relativeRedirectBuilder << qt_5_6_Behavior  << true;
	QTest::newRow( "relative redirect (expected)" )          << relativeRedirectBuilder << expectedBehavior << true;
	QTest::newRow( "protocol relative redirect (Qt 5.6)" )   << protRelRedirectBuilder  << qt_5_6_Behavior  << true;
	QTest::newRow( "protocol relative redirect (expected)" ) << protRelRedirectBuilder  << expectedBehavior << true;
	QTest::newRow( "redirect 302" )                          << redirect302Builder      << qt_5_6_Behavior  << true;
	QTest::newRow( "redirect 307" )                          << redirect307Builder      << qt_5_6_Behavior  << true;
	QTest::newRow( "redirect 308 (Qt 5.6)" )                 << redirect308Builder      << qt_5_6_Behavior  << false;
	QTest::newRow( "redirect 308 (expected)" )               << redirect308Builder      << expectedBehavior << true;
}


/*! \test Tests the MockReply::prepare() method.
 */
void MockReplyTest::testPrepare()
{
	QFETCH(BehaviorFlags, behaviorFlags);
	QFETCH(MockReplyBuilder, builder);
	QFETCH(Request, request);
	QFETCH(AttributeHash, attributes);
	QFETCH(RawHeaderHash, rawHeaders);
	QFETCH(HeaderHash, headers);

	MockReplyScopedPtr reply(builder.createReply());

	MockReplyTestHelper::callSetBehaviorFlags(reply.data(), behaviorFlags);
	MockReplyTestHelper::callPrepare(reply.data(), request);

	QVERIFY(!reply->isFinished());
	QVERIFY(!reply->isReadable());
	QVERIFY(!reply->isOpen());
	QCOMPARE(reply->request(), request.qRequest);
	QTEST(reply->error(), "error");
	QCOMPARE(reply->url(), request.qRequest.url());
	for (AttributeHash::const_iterator iter = attributes.cbegin(); iter != attributes.cend(); ++iter)
		QCOMPARE(reply->attribute(iter.key()), iter.value());

	for (RawHeaderHash::const_iterator iter = rawHeaders.cbegin(); iter != rawHeaders.cend(); ++iter)
		QCOMPARE(reply->rawHeader(iter.key()), iter.value());

	for (HeaderHash::const_iterator iter = headers.cbegin(); iter != headers.cend(); ++iter)
		QCOMPARE(reply->header(iter.key()), iter.value());
}

/*! Provides the data for the testPrepare() test.
 */
void MockReplyTest::testPrepare_data()
{
	QTest::addColumn<MockReplyBuilder>( "builder" );
	QTest::addColumn<BehaviorFlags>( "behaviorFlags" );
	QTest::addColumn<Request>( "request" );
	QTest::addColumn<AttributeHash>( "attributes" );
	QTest::addColumn<RawHeaderHash>( "rawHeaders" );
	QTest::addColumn<HeaderHash>( "headers" );
	QTest::addColumn<QNetworkReply::NetworkError>( "error" );

	const QNetworkReply::NetworkError noError = QNetworkReply::NoError;
	const QNetworkReply::NetworkError notFoundError = QNetworkReply::ContentNotFoundError;
	const QNetworkReply::NetworkError accessDeniedError = QNetworkReply::ContentAccessDenied;
	const QNetworkReply::NetworkError temporaryNetworkError = QNetworkReply::TemporaryNetworkFailureError;
#if QT_VERSION >= QT_VERSION_CHECK( 5,3,0 )
	const QNetworkReply::NetworkError internalServerError = QNetworkReply::InternalServerError;
	const HttpStatus::Code internalServErrorCode = HttpStatus::InternalServerError;
#endif // Qt >= 5.3.0
	const HttpStatus::Code successStatusCode = HttpStatus::OK;
	const HttpStatus::Code forbiddenStatusCode = HttpStatus::Forbidden;
	const QString customReasonPhrase( "foo" );
	const QByteArray body( "{\"status\":\"ok\"}" );
	const QByteArray contentLengthHeader( "Content-Length" );

	const Request getReq( QNetworkRequest( QUrl( "http://example.com" ) ), QNetworkAccessManager::GetOperation );
	const Request getHttpsReq( QNetworkRequest( QUrl( "https://example.com" ) ), QNetworkAccessManager::GetOperation );

	const BehaviorFlags expectedBehavior = Behavior_Expected;
	const BehaviorFlags qt56Behavior = Behavior_Qt_5_6_0;

	MockReplyBuilder simpleReplyBuilder;
	simpleReplyBuilder.withBody( body );
	AttributeHash defaultAttributes;
	defaultAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, successStatusCode );
	defaultAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, HttpStatus::reasonPhrase( successStatusCode ) );
	defaultAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	RawHeaderHash defaultRawHeaders;
	defaultRawHeaders.insert( contentLengthHeader, QByteArray::number( body.size() ) );
	HeaderHash defaultHeaders;
	defaultHeaders.insert( QNetworkRequest::ContentLengthHeader, body.size() );
	QTest::newRow( "set default properties" ) << simpleReplyBuilder << expectedBehavior  << getReq << defaultAttributes << defaultRawHeaders << defaultHeaders << noError;

	AttributeHash additionalAttributes( defaultAttributes );
	additionalAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, true );
	QTest::newRow( "additional attributes" ) << simpleReplyBuilder << expectedBehavior << getHttpsReq << additionalAttributes << defaultRawHeaders << defaultHeaders << noError;

	MockReplyBuilder dontOverrideBuilder;
	const unsigned int fakeContentLength = 1337;
	dontOverrideBuilder.withError( notFoundError )->withStatus( HttpStatus::Created );
	dontOverrideBuilder.withBody( body )->withHeader( QNetworkRequest::ContentLengthHeader, fakeContentLength );
	dontOverrideBuilder.withAttribute( QNetworkRequest::HttpReasonPhraseAttribute, customReasonPhrase);
	AttributeHash dontOverrideAttributes;
	dontOverrideAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, HttpStatus::Created );
	dontOverrideAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, customReasonPhrase );
	dontOverrideAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	RawHeaderHash dontOverrideRawHeaders;
	dontOverrideRawHeaders.insert( contentLengthHeader, "1337" );
	HeaderHash dontOverrideHeaders;
	dontOverrideHeaders.insert( QNetworkRequest::ContentLengthHeader, fakeContentLength );
	QTest::newRow( "don't override explicitly set properties" ) << dontOverrideBuilder << expectedBehavior << getReq << dontOverrideAttributes << dontOverrideRawHeaders << dontOverrideHeaders << notFoundError;

	MockReplyBuilder invalidStatusCodeBuilder;
	invalidStatusCodeBuilder.withAttribute(QNetworkRequest::HttpStatusCodeAttribute, QByteArray("Foo"));
	QTest::newRow( "invalid status code attribute" ) << invalidStatusCodeBuilder << expectedBehavior << getReq << defaultAttributes << RawHeaderHash() << HeaderHash() << noError;

	MockReplyBuilder accessDeniedBuilder;
	accessDeniedBuilder.withError( accessDeniedError );
	AttributeHash forbiddenStatusAttributes;
	forbiddenStatusAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, forbiddenStatusCode );
	forbiddenStatusAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, HttpStatus::reasonPhrase( forbiddenStatusCode ) );
	forbiddenStatusAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	QTest::newRow( "set status code from client error" ) << accessDeniedBuilder << expectedBehavior << getReq << forbiddenStatusAttributes << RawHeaderHash() << HeaderHash() << accessDeniedError;

#if QT_VERSION >= QT_VERSION_CHECK( 5,3,0 )
	MockReplyBuilder internalServerErrorBuilder;
	internalServerErrorBuilder.withError( internalServerError );
	AttributeHash internalServerErrorStatusAttributes;
	internalServerErrorStatusAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, internalServErrorCode );
	internalServerErrorStatusAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, HttpStatus::reasonPhrase( internalServErrorCode ) );
	internalServerErrorStatusAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	QTest::newRow( "set status code from server error" ) << internalServerErrorBuilder << expectedBehavior << getReq << internalServerErrorStatusAttributes << RawHeaderHash() << HeaderHash() << internalServerError;
#endif // Qt >= 5.3.0

	MockReplyBuilder temporaryNetworkErrorBuilder;
	temporaryNetworkErrorBuilder.withError( temporaryNetworkError );
	AttributeHash temporaryNetworkErrorAttributes;
	temporaryNetworkErrorAttributes.insert( QNetworkRequest::HttpStatusCodeAttribute, QVariant() );
	temporaryNetworkErrorAttributes.insert( QNetworkRequest::HttpReasonPhraseAttribute, QVariant() );
	temporaryNetworkErrorAttributes.insert( QNetworkRequest::ConnectionEncryptedAttribute, false );
	QTest::newRow( "set status code from network error" ) << temporaryNetworkErrorBuilder << expectedBehavior << getReq << temporaryNetworkErrorAttributes << RawHeaderHash() << HeaderHash() << temporaryNetworkError;

}


/*! Provides the data for the testFinish() test.
 */
void MockReplyTest::testFinish_data()
{
	QTest::addColumn<MockReplyBuilder>("builder");
	QTest::addColumn<Request>("request");
	QTest::addColumn<QByteArray>("replyPayload");
	QTest::addColumn<SignalEmissionList>("expectedSignals");

	// Possible signals
	const QMetaObject& replyMetaObj = QNetworkReply::staticMetaObject;

	QMetaMethod metaDataChangedSignal =     getSignal(replyMetaObj,"metaDataChanged()");
	QMetaMethod uploadProgressSignal =      getSignal(replyMetaObj,"uploadProgress(qint64, qint64)");
	QMetaMethod downloadProgressSignal =    getSignal(replyMetaObj,"downloadProgress(qint64, qint64)");
	QMetaMethod finishedSignal =            getSignal(replyMetaObj,"finished()");
	QMetaMethod errorSignal =               getSignal(replyMetaObj,"error(QNetworkReply::NetworkError)");
	QMetaMethod readyReadSignal =           getSignal(replyMetaObj,"readyRead()");
	QMetaMethod readChannelFinishedSignal = getSignal(replyMetaObj,"readChannelFinished()");


	const Request getReq(QNetworkRequest(QUrl("http://example.com")));
	#if QT_VERSION >= QT_VERSION_CHECK(5,2,2)
		const QByteArray successPayload("{\"status\":\"ok\"}");
	#else
		// QTBUG-36682
		const QByteArray successPayload("{\"status\": \"ok\"}");
	#endif // Qt < 5.2.2

	MockReplyBuilder successBuilder;
	successBuilder.withStatus(HttpStatus::OK)->withBody(QJsonDocument::fromJson(successPayload));
	QTest::newRow("succeeding, finished GET") << successBuilder << getReq << successPayload <<
	(SignalEmissionList()
	 << qMakePair(metaDataChangedSignal, QVariantList())
	 << qMakePair(readyReadSignal, QVariantList())
	 << qMakePair(downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size())
	 << qMakePair(downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size())
	 << qMakePair(readChannelFinishedSignal, QVariantList())
	 << qMakePair(finishedSignal, QVariantList()));

	#if QT_VERSION >= QT_VERSION_CHECK(5,2,2)
		const QByteArray requestPayload("{\"request\":\"foo\"}");
	#else
		// QTBUG-36682
		const QByteArray requestPayload("{\"request\": \"foo\"}");
	#endif // Qt < 5.2.2

	const Request postReq(QNetworkRequest(QUrl("http://example.com")), QNetworkAccessManager::PostOperation, requestPayload);
	QTest::newRow("succeeding, finished POST") << successBuilder << postReq << successPayload <<
	(SignalEmissionList()
	 << qMakePair(uploadProgressSignal, QVariantList() << requestPayload.size() << requestPayload.size())
	 << qMakePair(metaDataChangedSignal, QVariantList())
	 << qMakePair(readyReadSignal, QVariantList())
	 << qMakePair(downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size())
	 << qMakePair(downloadProgressSignal, QVariantList() << successPayload.size() << successPayload.size())
	 << qMakePair(readChannelFinishedSignal, QVariantList())
	 << qMakePair(finishedSignal, QVariantList()));

	MockReplyBuilder failureBuilder;
	failureBuilder.withStatus(HttpStatus::NotFound);
	QTest::newRow("failing, finished GET") << failureBuilder << getReq << QByteArray() <<
	(SignalEmissionList()
	 << qMakePair(metaDataChangedSignal, QVariantList())
	 << qMakePair(errorSignal, QVariantList() << QVariant::fromValue(QNetworkReply::ContentNotFoundError))
	 << qMakePair(downloadProgressSignal, QVariantList() << 0 << 0)
	 << qMakePair(readChannelFinishedSignal, QVariantList())
	 << qMakePair(finishedSignal, QVariantList()));

	QTest::newRow("failing, finished POST") << failureBuilder << postReq << QByteArray() <<
	(SignalEmissionList()
	 << qMakePair(uploadProgressSignal, QVariantList() << requestPayload.size() << requestPayload.size())
	 << qMakePair(metaDataChangedSignal, QVariantList())
	 << qMakePair(errorSignal, QVariantList() << QVariant::fromValue(QNetworkReply::ContentNotFoundError))
	 << qMakePair(downloadProgressSignal, QVariantList() << 0 << 0)
	 << qMakePair(readChannelFinishedSignal, QVariantList())
	 << qMakePair(finishedSignal, QVariantList()));

}

/*! \test Tests the MockReply::finish() method.
 */
void MockReplyTest::testFinish()
{
	QFETCH(MockReplyBuilder, builder);
	QFETCH(Request, request);
	QFETCH(QByteArray, replyPayload);

	MockReplyScopedPtr reply(builder.createReply());

	QSignalInspector inspector(reply.data());

	MockReplyTestHelper::callFinish(reply.data(), request);
	QTest::qWait(100);

	QVERIFY(reply->isFinished());
	QCOMPARE(reply->bytesAvailable(), static_cast<qint64>(replyPayload.size()));
	QVERIFY(reply->isReadable());
	QVERIFY(reply->isOpen());
	QCOMPARE(reply->readAll(), replyPayload);

	SignalEmissionList emissions = getSignalEmissionListFromInspector(inspector);

	QTEST(emissions, "expectedSignals");
}

/*! \test Tests the MockReply::abort() method.
 */
void MockReplyTest::testAbort()
{
	MockReplyBuilder builder;
	builder.withStatus(HttpStatus::OK);

	MockReplyScopedPtr reply(builder.createReply());

	QVERIFY(reply->isRunning());
	QVERIFY(!reply->isFinished());
	QCOMPARE(reply->error(), QNetworkReply::NoError);

	reply->abort();

	QVERIFY(!reply->isRunning());
	QVERIFY(reply->isFinished());
	QVERIFY(!reply->isOpen());
	QCOMPARE(reply->error(), QNetworkReply::OperationCanceledError);
}

/*! \test Tests the behavior of an unconfigured MockReplyBuilder.
 */
void MockReplyTest::testNullBuilder()
{
	MockReplyBuilder builder;
	QCOMPARE(builder.createReply(), static_cast<MockReply*>(Q_NULLPTR));
}

/*! Provides the data for the testWithBodyByteArray() test.
 */
void MockReplyTest::testWithBodyByteArray_data()
{
	QTest::addColumn<QByteArray>("body");
	QTest::addColumn<QVariant>("finishedContentLength");
	QTest::addColumn<QVariant>("finishedContentType");

	QTest::newRow("null body")  << QByteArray()   << QVariant()  << QVariant();
	QTest::newRow("empty body") << QByteArray("") << QVariant(0) << QVariant();

	QByteArray plainText("foo bar");
	QTest::newRow("plain text") << plainText      << QVariant::fromValue(plainText.size()) << QVariant::fromValue(contentTypeTextPlain);
}

/*! \test Tests the MockReplyBuilder::withBody(const QByteArray&) method.
 */
void MockReplyTest::testWithBodyByteArray()
{
	QFETCH(QByteArray, body);

	MockReplyBuilder builder;
	builder.withBody(body);

	MockReplyScopedPtr reply(builder.createReply());

	MockReplyTestHelper::callPrepare(reply.data(), Request(QNetworkRequest()));
	MockReplyTestHelper::callFinish(reply.data(), Request(QNetworkRequest()));

	QCOMPARE(reply->bytesAvailable(), body.size());
	QCOMPARE(reply->body(), body);
	QCOMPARE(reply->readAll(), body);

	MockReplyTestHelper::callFinish(reply.data(), Request(QNetworkRequest()));
	QTEST(reply->header(QNetworkRequest::ContentLengthHeader), "finishedContentLength");
	QTEST(reply->header(QNetworkRequest::ContentTypeHeader), "finishedContentType");
}

/*! Provides the data for the testWithBodyJson() test.
 */
void MockReplyTest::testWithBodyJson_data()
{
	QTest::addColumn<QJsonDocument>("body");
	QTest::addColumn<QVariant>("finishedContentLength");

	QTest::newRow("null body")  << QJsonDocument()                     << QVariant();
	QTest::newRow("empty body") << QJsonDocument::fromJson("")         << QVariant();
#if QT_VERSION >= QT_VERSION_CHECK(5,2,2)
	const QByteArray fooBarJson("{\"foo\":\"bar\"}");
#else
	// QTBUG-36682
	const QByteArray fooBarJson("{\"foo\": \"bar\"}");
#endif
	QTest::newRow("object")     << QJsonDocument::fromJson(fooBarJson) << QVariant(fooBarJson.size());
}

/*! \test Tests the MockReplyBuilder::withBody(const QJsonDocument&) method.
 */
void MockReplyTest::testWithBodyJson()
{
	QFETCH(QJsonDocument, body);

	MockReplyBuilder builder;
	builder.withBody(body);

	MockReplyScopedPtr reply(builder.createReply());
	MockReplyTestHelper::callPrepare(reply.data(), Request(QNetworkRequest()));
	MockReplyTestHelper::callFinish(reply.data(), Request(QNetworkRequest()));

	QByteArray rawBody = body.toJson(QJsonDocument::Compact);
	QCOMPARE(reply->bytesAvailable(), rawBody.size());
	QCOMPARE(reply->readAll(), rawBody);
	QCOMPARE(reply->header(QNetworkRequest::ContentTypeHeader).toByteArray(), contentTypeJson);
	QTEST(reply->header(QNetworkRequest::ContentLengthHeader), "finishedContentLength");
}

/*! Provides the data for the testWithFile() test.
 */
void MockReplyTest::testWithFile_data()
{
	QTest::addColumn<QString>("file");
	QTest::addColumn<QByteArray>("contentType");

	QTest::newRow("plain text")        << "data/plainText.txt"                << contentTypeTextPlain;
	QTest::newRow("xml")               << "data/simple.xml"                   << QByteArray("application/xml");
	QTest::newRow("empty text file")   << "data/empty.txt"                    << contentTypeTextPlain;
	QTest::newRow("non existing file") << "data/this_file_does_not_exist.foo" << QByteArray();
}

/*! \test Tests the MockReplyBuilder::withFile() method.
 */
void MockReplyTest::testWithFile()
{
	QFETCH(QString, file);

	QString filePath = QFINDTESTDATA(file);

	MockReplyBuilder builder;
	builder.withFile(filePath);

	MockReplyScopedPtr reply(builder.createReply());

	QFile fileObj(filePath);
	fileObj.open(QIODevice::ReadOnly);

	QByteArray fileContent = fileObj.readAll();

	MockReplyTestHelper::callPrepare(reply.data(), Request(QNetworkRequest()));
	MockReplyTestHelper::callFinish(reply.data(), Request(QNetworkRequest()));

	QCOMPARE(reply->bytesAvailable(), fileContent.size());
	QCOMPARE(reply->readAll(), fileContent);
	QTEST(reply->header(QNetworkRequest::ContentTypeHeader).toByteArray(), "contentType");
}

/*! Provides the data for the testWithStatus() test.
 */
void MockReplyTest::testWithStatus_data()
{
	QTest::addColumn<int>("statusCode");
	QTest::addColumn<QString>("reasonPhrase");
	QTest::addColumn<QNetworkReply::NetworkError>("error");

	QTest::newRow("success")                         << static_cast<int>(HttpStatus::OK)                << "OK"         << QNetworkReply::NoError;
	QTest::newRow("Qt unknown content error")        << static_cast<int>(HttpStatus::ExpectationFailed) << QString()    << QNetworkReply::UnknownContentError;
#if QT_VERSION >= QT_VERSION_CHECK(5,3,0)
	QTest::newRow("Qt unknown server error")         << static_cast<int>(HttpStatus::NotExtended)       << QString()    << QNetworkReply::UnknownServerError;
#endif // Qt >= 5.3.0
	QTest::newRow("custom error code")               << 666                                             << QString()    << QNetworkReply::ProtocolFailure;
	QTest::newRow("default reason phrase")           << static_cast<int>(HttpStatus::Created)           << QString()    << QNetworkReply::NoError;
	QTest::newRow("custom reason phrase")            << static_cast<int>(HttpStatus::MovedPermanently)  << "Go there"   << QNetworkReply::NoError;
	QTest::newRow("error")                           << static_cast<int>(HttpStatus::NotFound)          << QString()    << QNetworkReply::ContentNotFoundError;
	QTest::newRow("error with custom reason phrase") << static_cast<int>(HttpStatus::BadRequest)        << "Oops Error" << QNetworkReply::ProtocolInvalidOperationError;
}

/*! \test Tests the MockReplyBuilder::withStatus() method.
 */
void MockReplyTest::testWithStatus()
{
	QFETCH(int, statusCode);
	QFETCH(QString, reasonPhrase);

	MockReplyBuilder builder;
	builder.withStatus(statusCode, reasonPhrase);

	MockReplyScopedPtr reply(builder.createReply());

	MockReplyTestHelper::callPrepare(reply.data(), Request(QNetworkRequest()));
	QCOMPARE(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), statusCode);
	if (reasonPhrase.isNull())
		QCOMPARE(reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString(), HttpStatus::reasonPhrase(statusCode));
	else
		QCOMPARE(reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString(), reasonPhrase);
	QTEST(reply->error(), "error");

	// Finishing the reply should not change anything
	MockReplyTestHelper::callFinish(reply.data(), Request(QNetworkRequest()));
	QCOMPARE(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), statusCode);
	if (reasonPhrase.isNull())
		QCOMPARE(reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString(), HttpStatus::reasonPhrase(statusCode));
	else
		QCOMPARE(reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString(), reasonPhrase);
	QTEST(reply->error(), "error");
}

/*! Provides the data for the testWithError() test.
 */
void MockReplyTest::testWithError_data()
{
	QTest::addColumn<QNetworkReply::NetworkError>("error");
	QTest::addColumn<QString>("errorMsg");
	QTest::addColumn<int>("finishedStatusCode");

	QTest::newRow("no error")             << QNetworkReply::NoError              << QString("Unknown Error")     << static_cast<int>(HttpStatus::OK);
	QTest::newRow("content not found")    << QNetworkReply::ContentNotFoundError << QString("Content not found") << static_cast<int>(HttpStatus::NotFound);
	QTest::newRow("unknown server error") << QNetworkReply::UnknownContentError  << QString("Unknown Error")     << static_cast<int>(HttpStatus::BadRequest);
#if QT_VERSION >= QT_VERSION_CHECK(5,3,0)
	QTest::newRow("unknown server error") << QNetworkReply::UnknownServerError   << QString("Unknown Error")     << static_cast<int>(HttpStatus::InternalServerError);
#endif // Qt >= 5.3.0
	QTest::newRow("host not found")       << QNetworkReply::HostNotFoundError    << QString("Host not found")    << -1;
}

/*! \test Tests the MockReplyBuilder::withError() method.
 */
void MockReplyTest::testWithError()
{
	QFETCH(QNetworkReply::NetworkError, error);
	QFETCH(QString, errorMsg);
	QFETCH(int, finishedStatusCode);

	MockReplyBuilder builder;
	builder.withError(error, errorMsg);

	MockReplyScopedPtr reply(builder.createReply());

	QCOMPARE(reply->error(), error);
	QCOMPARE(reply->errorString(), errorMsg);
	QVERIFY(!reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).isValid());

	MockReplyTestHelper::callPrepare(reply.data(), Request(QNetworkRequest()));
	MockReplyTestHelper::callFinish(reply.data(), Request(QNetworkRequest()));
	if (finishedStatusCode < 0)
		QVERIFY(!reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).isValid());
	else
	{
		QVERIFY(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).isValid());
		QCOMPARE(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), finishedStatusCode);
		QCOMPARE(reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString(), HttpStatus::reasonPhrase(finishedStatusCode));
	}
}

/*! \test Tests the MockReplyBuilder::withRedirect() method.
 */
void MockReplyTest::testWithRedirect()
{
	QFETCH( QUrl, redirectTarget );
	QFETCH( int, statusCode );

	MockReplyBuilder builder;
	builder.withRedirect( redirectTarget, static_cast<HttpStatus::Code>( statusCode ) );

	MockReplyScopedPtr reply( builder.createReply() );

	const QUrl locationHeader = QUrl::fromEncoded( reply->rawHeader( HttpUtils::locationHeader() ), QUrl::StrictMode );
	QCOMPARE( locationHeader, redirectTarget );
	QCOMPARE( reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt(), statusCode );

	const QUrl reqTarget( "http://foo.bar/test" );
	MockReplyTestHelper::callPrepare( reply.data(), Request( QNetworkRequest( reqTarget ) ) );
	QCOMPARE( locationHeader, redirectTarget );
	QCOMPARE( reply->attribute( QNetworkRequest::RedirectionTargetAttribute ).toUrl(), redirectTarget );

}

/*! Provides the data for the testWithRedirect() test.
 */
void MockReplyTest::testWithRedirect_data()
{
	QTest::addColumn<QUrl>( "redirectTarget" );
	QTest::addColumn<int>( "statusCode" );

	//                                    // redirectTarget               // statusCode
	QTest::newRow( "relative url" )       << QUrl( "see/other" )          << static_cast<int>( HttpStatus::MovedPermanently );
	QTest::newRow( "absolute url" )       << QUrl( "http://example.com" ) << static_cast<int>( HttpStatus::MovedPermanently );
	QTest::newRow( "temporary redirect" ) << QUrl( "http://example.com" ) << static_cast<int>( HttpStatus::TemporaryRedirect );
}

/*! \test Tests the MockReplyBuilder::withHeader() method.
 */
void MockReplyTest::testWithHeader()
{
	QFETCH( QNetworkRequest::KnownHeaders, header );
	QFETCH( QVariant, value );

	MockReplyBuilder builder;
	builder.withHeader( header, value );

	MockReplyScopedPtr reply( builder.createReply() );

	QCOMPARE( reply->header( header ), value );
}

/*! Provides the data for the testWithHeader() test.
 */
void MockReplyTest::testWithHeader_data()
{
	QTest::addColumn<QNetworkRequest::KnownHeaders>( "header" );
	QTest::addColumn<QVariant>( "value" );

	QTest::newRow( "content type" )      << QNetworkRequest::ContentTypeHeader   << QVariant( "text/plain" );
	QTest::newRow( "content length" )    << QNetworkRequest::ContentLengthHeader << QVariant::fromValue( 42 );
	QTest::newRow( "null value" )        << QNetworkRequest::LocationHeader      << QVariant();
	QTest::newRow( "relative location" ) << QNetworkRequest::LocationHeader      << QVariant( QUrl( "//example.com" ) );
}

/*! Provides the data for the testWithRawHeader() test.
 */
void MockReplyTest::testWithRawHeader_data()
{
	QTest::addColumn<QByteArray>("header");
	QTest::addColumn<QByteArray>("value");

	QTest::newRow("content type")    << QByteArray("Content-Type") << QByteArray("text/plain");
	QTest::newRow("case is ignored") << QByteArray("conTent-Type") << QByteArray("text/plain");
	QTest::newRow("custom header")   << QByteArray("X-Foo") << QByteArray("17");
	QTest::newRow("null value")      << QByteArray("Content-Length") << QByteArray();
	QTest::newRow("empty value")     << QByteArray("Location") << QByteArray("");
}

/*! \test Tests the MockReplyBuilder::withRawHeader() method.
 */
void MockReplyTest::testWithRawHeader()
{
	QFETCH(QByteArray, header);
	QFETCH(QByteArray, value);

	MockReplyBuilder builder;
	builder.withRawHeader(header, value);

	MockReplyScopedPtr reply(builder.createReply());

	QCOMPARE(reply->rawHeader(header), value);
}

/*! Provides the data for the testWithAttribute() test.
 */
void MockReplyTest::testWithAttribute_data()
{
	QTest::addColumn<QNetworkRequest::Attribute>("attribute");
	QTest::addColumn<QVariant>("value");

	QTest::newRow("status code") << QNetworkRequest::HttpStatusCodeAttribute   << QVariant::fromValue(200);
	QTest::newRow("null value")  << QNetworkRequest::HttpReasonPhraseAttribute << QVariant();
	QTest::newRow("empty value") << QNetworkRequest::HttpReasonPhraseAttribute << QVariant("");
}

/*! \test Tests the MockReplyBuilder::withAttribute() method.
 */
void MockReplyTest::testWithAttribute()
{
	QFETCH(QNetworkRequest::Attribute, attribute);
	QFETCH(QVariant, value);

	MockReplyBuilder builder;
	builder.withAttribute(attribute, value);

	MockReplyScopedPtr reply(builder.createReply());

	QCOMPARE(reply->attribute(attribute), value);
}


/*! Provides the data for the testWithAuthenticate() test.
 */
void MockReplyTest::testWithAuthenticate_data()
{
	QTest::addColumn<QString>("realm");
	QTest::addColumn<int>("statusCode");
	QTest::addColumn<QByteArray>("authenticateHeader");

	QTest::newRow("basic auth")        << "world"   << static_cast<int>(HttpStatus::Unauthorized) << QByteArray("Basic realm=\"world\"");
	QTest::newRow("invalid challenge") << QString() << 0                                          << QByteArray();

}


/*! \test Tests the MockReplyBuilder::withAuthenticate() method.
 */
void MockReplyTest::testWithAuthenticate()
{
	QFETCH(QString, realm);

	MockReplyBuilder builder;
	builder.withAuthenticate(realm);

	MockReplyScopedPtr reply(builder.createReply());

	QTEST(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(), "statusCode");
	QTEST(reply->rawHeader("WWW-Authenticate"), "authenticateHeader");
}

/*! Provides the data for the testWithCookie() test.
 */
void MockReplyTest::testWithCookie_data()
{
	QTest::addColumn<QList<QNetworkCookie> >("cookies");

	const QByteArray cookieName("foo");

	QNetworkCookie sessionCookie(cookieName, "bar");
	sessionCookie.setPath("/foo");
	sessionCookie.setDomain(".example.com");

	QNetworkCookie expiringCookie("expiring", "tick tack");
	expiringCookie.setExpirationDate(QDateTime::currentDateTime().addDays(2));

	QNetworkCookie expiredCookie("expired", "boom");
	expiredCookie.setExpirationDate(QDateTime::currentDateTime().addSecs(-10));

	QNetworkCookie overridingCookie(cookieName, "overridden");
	overridingCookie.setPath("/foo");
	overridingCookie.setDomain(".example.com");


	//                                  // cookies
	QTest::newRow("session cookie")     << (QList<QNetworkCookie>() << sessionCookie);
	QTest::newRow("expiring cookie")    << (QList<QNetworkCookie>() << expiringCookie);
	QTest::newRow("expired cookie")     << (QList<QNetworkCookie>() << expiredCookie);
	QTest::newRow("multiple cookies")   << (QList<QNetworkCookie>() << sessionCookie << expiringCookie);
	QTest::newRow("overriding cookies") << (QList<QNetworkCookie>() << sessionCookie << overridingCookie);
}

/*! \test Tests the MockReplyBuilder::withCookie() method.
 */
void MockReplyTest::testWithCookie()
{
	QFETCH(QList<QNetworkCookie>, cookies);

	MockReplyBuilder builder;
	QList<QNetworkCookie>::Iterator iter;
	for (iter = cookies.begin(); iter != cookies.end(); ++iter)
		builder.withCookie(*iter);

	MockReplyScopedPtr reply(builder.createReply());
	const QList<QNetworkCookie> actualCookies = reply->header(QNetworkRequest::SetCookieHeader).value<QList<QNetworkCookie> >();

	QCOMPARE(actualCookies, cookies);
}

/*! Provides the data for the testOperatorEquals() test.
 */
void MockReplyTest::testEqualityOperators_data()
{
	QTest::addColumn<MockReplyBuilderPtr>("left");
	QTest::addColumn<MockReplyBuilderPtr>("right");
	QTest::addColumn<bool>("equal");

	MockReplyBuilderPtr nullBuilder(new MockReplyBuilder);
	MockReplyBuilderPtr nullBuilder2(new MockReplyBuilder);
	MockReplyBuilderPtr simpleBuilder(new MockReplyBuilder);
	simpleBuilder->withStatus(HttpStatus::OK);
	MockReplyBuilderPtr simpleBuilder2(new MockReplyBuilder);
	simpleBuilder2->withStatus(HttpStatus::OK);
	MockReplyBuilderPtr differentSimpleBuilder(new MockReplyBuilder);
	differentSimpleBuilder->withStatus(HttpStatus::Created);

	MockReplyBuilderPtr fullBuilder = HelperMethods::createFullBuilder();
	MockReplyBuilderPtr fullBuilder2 = HelperMethods::createFullBuilder();
	MockReplyBuilderPtr differentBodyBuilder = HelperMethods::createFullBuilder();
	differentBodyBuilder->withBody("This is different");
	MockReplyBuilderPtr differentHeaderBuilder = HelperMethods::createFullBuilder();
	differentHeaderBuilder->withHeader(QNetworkRequest::ContentTypeHeader, QByteArray("something/different"));
	MockReplyBuilderPtr additionalHeaderBuilder = HelperMethods::createFullBuilder();
	additionalHeaderBuilder->withHeader(QNetworkRequest::ServerHeader, QByteArray("TestServer"));
	MockReplyBuilderPtr differentRawHeaderBuilder = HelperMethods::createFullBuilder();
	differentRawHeaderBuilder->withRawHeader(customHeader, "different");
	MockReplyBuilderPtr additionalRawHeaderBuilder = HelperMethods::createFullBuilder();
	additionalRawHeaderBuilder->withRawHeader("X-X", "o-o");
	MockReplyBuilderPtr differentAttributeBuilder = HelperMethods::createFullBuilder();
	differentAttributeBuilder->withAttribute(QNetworkRequest::HttpReasonPhraseAttribute, QByteArray("Created"));
	MockReplyBuilderPtr additionalAttributeBuilder = HelperMethods::createFullBuilder();
	additionalAttributeBuilder->withAttribute(QNetworkRequest::RedirectionTargetAttribute, QUrl("http://example.com"));

	MockReplyBuilderPtr errorBuilder = HelperMethods::createErrorBuilder();
	MockReplyBuilderPtr errorBuilder2 = HelperMethods::createErrorBuilder();
	MockReplyBuilderPtr differentErrorCodeBuilder = HelperMethods::createErrorBuilder();
	differentErrorCodeBuilder->withError(QNetworkReply::ContentAccessDenied, "Content resend");
	MockReplyBuilderPtr differentErrorMessageBuilder = HelperMethods::createErrorBuilder();
	differentErrorMessageBuilder->withError(QNetworkReply::ContentReSendError, "Some other error message");


	//                                         // left          // right                        // equal
	QTest::newRow("equal simple builders")     << simpleBuilder << simpleBuilder2               << true;
	QTest::newRow("equal full builders")       << fullBuilder   << fullBuilder2                 << true;
	QTest::newRow("equal error builders")      << errorBuilder  << errorBuilder2                << true;
	QTest::newRow("two null")                  << nullBuilder   << nullBuilder2                 << true;
	QTest::newRow("different simple builders") << simpleBuilder << differentSimpleBuilder       << false;
	QTest::newRow("different body")            << fullBuilder   << differentBodyBuilder         << false;
	QTest::newRow("different header")          << fullBuilder   << differentHeaderBuilder       << false;
	QTest::newRow("additional header")         << fullBuilder   << additionalHeaderBuilder      << false;
	QTest::newRow("different raw header")      << fullBuilder   << differentRawHeaderBuilder    << false;
	QTest::newRow("additional raw header")     << fullBuilder   << additionalRawHeaderBuilder   << false;
	QTest::newRow("different attribute")       << fullBuilder   << differentAttributeBuilder    << false;
	QTest::newRow("additional attribute")      << fullBuilder   << additionalAttributeBuilder   << false;
	QTest::newRow("different error code")      << errorBuilder  << differentErrorCodeBuilder    << false;
	QTest::newRow("different error message")   << errorBuilder  << differentErrorMessageBuilder << false;
	QTest::newRow("left null")                 << nullBuilder   << simpleBuilder                << false;
	QTest::newRow("right null")                << simpleBuilder << nullBuilder                  << false;

}

/*! \test Tests the MockReplyBuilder::operator==() and MockReplyBuilder::operator!=().
 */
void MockReplyTest::testEqualityOperators()
{
	QFETCH(MockReplyBuilderPtr, left);
	QFETCH(MockReplyBuilderPtr, right);
	QFETCH(bool, equal);

	QVERIFY(*left  == *left);
	QVERIFY(*right == *right);
	QCOMPARE(*left  == *right, equal);
	QCOMPARE(*right == *left,  equal);
	QCOMPARE(*left  != *right, !equal);
	QCOMPARE(*right != *left,  !equal);
}

MockReplyBuilderPtr HelperMethods::createFullBuilder()
{
	MockReplyBuilderPtr fullBuilder(new MockReplyBuilder);
	fullBuilder->withStatus(HttpStatus::Created);
	fullBuilder->withBody("foo bar");
	fullBuilder->withHeader(QNetworkRequest::ContentTypeHeader, QByteArray("foo/bar"));
	fullBuilder->withRawHeader(customHeader, "woot");
	fullBuilder->withAttribute(QNetworkRequest::HttpReasonPhraseAttribute, QByteArray("Spawned"));
	return fullBuilder;
}

MockReplyBuilderPtr HelperMethods::createErrorBuilder()
{
	MockReplyBuilderPtr errorBuilder(new MockReplyBuilder);
	errorBuilder->withError(QNetworkReply::ContentReSendError, "Content resend");
	errorBuilder->withBody("Error");
	return errorBuilder;
}

HeaderHash HelperMethods::getKnownHeaders(QNetworkReply* reply)
{
	HeaderHash result;
	KnownHeadersSet headers;
	headers << QNetworkRequest::ContentTypeHeader
	        << QNetworkRequest::ContentLengthHeader
	        << QNetworkRequest::LocationHeader
	        << QNetworkRequest::LastModifiedHeader
	        << QNetworkRequest::CookieHeader
	        << QNetworkRequest::SetCookieHeader
	        << QNetworkRequest::ContentDispositionHeader
	        << QNetworkRequest::UserAgentHeader
	        << QNetworkRequest::ServerHeader;
#if QT_VERSION >= QT_VERSION_CHECK(5,12,0)
	headers << QNetworkRequest::IfModifiedSinceHeader
	        << QNetworkRequest::ETagHeader
	        << QNetworkRequest::IfMatchHeader
	        << QNetworkRequest::IfNoneMatchHeader;
#endif // Qt >= 5.12.0

	for (KnownHeadersSet::const_iterator iter = headers.cbegin(); iter != headers.cend(); ++iter)
		result.insert(*iter, reply->header(*iter));

	return result;
}


} // namespace Tests

QTEST_MAIN(Tests::MockReplyTest)
#include "MockReplyTest.moc"
