﻿/*! \file
 */

#ifndef MOCKNETWORKACCESSMANAGER_TESTS_MYNETWORKCLIENT_HPP
#define MOCKNETWORKACCESSMANAGER_TESTS_MYNETWORKCLIENT_HPP

#include <QtNetwork>

namespace Tests
{

/*! Dummy network client to demonstrate the usage
 * of the MockNetworkAccess::Manager.
 *
 * \sa \ref MyNetworkClientTest.cpp
 */
class MyNetworkClient : public QObject
{
	Q_OBJECT

public:
	MyNetworkClient(QObject* parent = Q_NULLPTR)
		: QObject(parent)
	{}

	void fetchHello()
	{
		if (m_networkAccessManager)
		{
			clearPreviousHelloRequest();
			const QNetworkRequest helloRequest = createHelloRequest();
			sendHelloRequest(helloRequest);
		}
	}

	void setNetworkAccessManager(QNetworkAccessManager* networkAccessManager)
	{
		m_networkAccessManager = networkAccessManager;
	}

	QString hello() const
	{
		return m_hello;
	}

private Q_SLOTS:
	QNetworkRequest createHelloRequest()
	{
		const QUrl helloUrl("http://example.com/hello");
		QNetworkRequest request(helloUrl);
		request.setHeader(QNetworkRequest::UserAgentHeader, "MyNetworkClient/1.0.0");
		return request;
	}

	void clearPreviousHelloRequest()
	{
		if (m_lastReply)
			delete m_lastReply;
	}

	void sendHelloRequest(const QNetworkRequest& request)
	{
		m_lastReply = m_networkAccessManager->get(request);
		m_lastReply->setParent(this);
		connect(m_lastReply, SIGNAL(finished()), this, SLOT(extractHelloFromReply()));
	}

	void extractHelloFromReply()
	{
		if (requestWasSuccessful())
		{
			const QJsonObject jsonObj = extractJsonFromReply();
			if (!jsonObj.isEmpty())
				extractHelloFromJson(jsonObj);
		}
	}

	bool requestWasSuccessful() const
	{
		return m_lastReply
		       && m_lastReply->error() == QNetworkReply::NoError
		       && m_lastReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 200;
	}

	QJsonObject extractJsonFromReply() const
	{
		const QByteArray body = m_lastReply->readAll();
		const QJsonObject jsonObj = QJsonDocument::fromJson(body).object();
		return jsonObj;
	}

	void extractHelloFromJson(const QJsonObject& jsonObj)
	{
		m_hello = jsonObj.value("hello").toString();
	}

private:
	QPointer<QNetworkAccessManager> m_networkAccessManager;
	QPointer<QNetworkReply> m_lastReply;
	QString m_hello;
};

} // namespace Tests

#endif /* MOCKNETWORKACCESSMANAGER_TESTS_MYNETWORKCLIENT_HPP */
