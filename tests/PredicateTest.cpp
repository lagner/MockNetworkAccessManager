
#include "MockNetworkAccessManager.hpp"

#include <QtTest>
#include <QtDebug>

#if __cplusplus >= 201103L || ( defined(_MSC_VER) && _MSC_VER >= 1900 )
#include <functional>
#endif /* C++11 */

namespace Tests {

/*! Implements unit tests for the matching predicates in the Predicates namespace.
 */
class PredicateTest : public QObject
{
	Q_OBJECT

private Q_SLOTS:
	void testNegate();
	void testAnything();
	void testGeneric_data();
	void testGeneric();
	void testVerb_data();
	void testVerb();
	void testUrlMatching_data();
	void testUrlMatching();
	void testUrl_data();
	void testUrl();
	void testQueryParameter();
	void testQueryParameter_data();
	void testMultiValueQueryParameter();
	void testMultiValueQueryParameter_data();
	void testQueryParameterMatching();
	void testQueryParameterMatching_data();
	void testQueryParameters();
	void testQueryParameters_data();
	void testMultiValueQueryParameters();
	void testMultiValueQueryParameters_data();
	void testQueryParameterTemplates();
	void testQueryParameterTemplates_data();
	void testBodyMatching_data();
	void testBodyMatching();
	void testBodyContaining_data();
	void testBodyContaining();
	void testBody_data();
	void testBody();
	void testHeaders_data();
	void testHeaders();
	void testHeader_data();
	void testHeader();
	void testHeaderMatching_data();
	void testHeaderMatching();
	void testRawHeaders_data();
	void testRawHeaders();
	void testRawHeader_data();
	void testRawHeader();
	void testRawHeaderMatching_data();
	void testRawHeaderMatching();
	void testRawHeaderTemplates_data();
	void testRawHeaderTemplates();
	void testAttribute_data();
	void testAttribute();
	void testAttributeMatching_data();
	void testAttributeMatching();
	void testAuthorization_data();
	void testAuthorization();
	void testCookie_data();
	void testCookie();
	void testCookieMatching_data();
	void testCookieMatching();

};

//####### Helpers #######

using namespace MockNetworkAccess;
using namespace MockNetworkAccess::Predicates;

typedef QHash<QString, QString> StringHash;
typedef QVector<QNetworkCookie> CookieVector;

static const QByteArray contentTypeHeader("Content-Type");
static const QByteArray contentTypeTextPlain("text/plain");
static const QByteArray locationHeader("Location");

} // namespace Tests

Q_DECLARE_METATYPE(MockNetworkAccess::QueryParameterHash)
Q_DECLARE_METATYPE(MockNetworkAccess::HeaderHash)
Q_DECLARE_METATYPE(MockNetworkAccess::Rule::Predicate::Ptr)

Q_DECLARE_METATYPE(QNetworkAccessManager::Operation)
Q_DECLARE_METATYPE(QNetworkRequest::KnownHeaders)
Q_DECLARE_METATYPE(QNetworkRequest::Attribute)
Q_DECLARE_METATYPE(QUrl::FormattingOptions)
Q_DECLARE_METATYPE(QUrl::ComponentFormattingOptions)
Q_DECLARE_METATYPE(QRegularExpression)
Q_DECLARE_METATYPE(QTextCodec*)
Q_DECLARE_METATYPE(Tests::CookieVector)

struct PredicateFunctor
{
	bool operator()(const MockNetworkAccess::Request& req) const
	{
		return req.operation == QNetworkAccessManager::GetOperation;
	}

	static bool emptyBodyMatcher(const MockNetworkAccess::Request& req)
	{
		return req.body.isEmpty();
	}
};


namespace Tests
{

const QUrl exampleRootUrl("http://example.com");

const Request getExampleRootRequest( QNetworkRequest( exampleRootUrl ), QNetworkAccessManager::GetOperation );
const Request singleParamRequest( QNetworkRequest( QUrl( "http://example.com?foo=bar" ) ) );
const Request doubleParamRequest( QNetworkRequest( QUrl( "http://example.com?a=1&foo=bar" ) ) );


//####### Tests #######

/*! \test Tests Rule::Predicate::negate().
 */
void PredicateTest::testNegate()
{
	Anything pred;
	pred.negate( true );
	QVERIFY( ! pred.matches( getExampleRootRequest ) );
}

/*! \test Tests MockNetworkAccess::Predicates::Anything.
 */
void PredicateTest::testAnything()
{
	QVERIFY(Anything().matches(Request(QNetworkRequest())));
}

/*! Provides the data for the testGeneric() test.
 */
void PredicateTest::testGeneric_data()
{
	QTest::addColumn<Rule::Predicate::Ptr>("predicate");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	const Request postHttpsFooRequest(QNetworkAccessManager::PostOperation, QNetworkRequest(QUrl("https://example.com/foo")), QByteArray("foo"));
#if __cplusplus >= 201103L || ( defined(_MSC_VER) && _MSC_VER >= 1900 )
	Rule::Predicate::Ptr lambdaPredicate = Predicates::createGeneric([](const Request& req) -> bool { return req.qRequest.url().scheme() == "https"; });
	QTest::newRow("lambda predicate non-matching") << lambdaPredicate << getExampleRootRequest << false;
	QTest::newRow("lambda predicate matching")     << lambdaPredicate << postHttpsFooRequest   << true;
#endif /* C++11 */


	Rule::Predicate::Ptr functorPredicate(new Predicates::Generic<PredicateFunctor>(PredicateFunctor()));
	Rule::Predicate::Ptr functionPointerPredicate = Predicates::createGeneric(&PredicateFunctor::emptyBodyMatcher);
	QTest::newRow("functor predicate matching")              << functorPredicate         << getExampleRootRequest << true;
	QTest::newRow("functor predicate non-matchuing")         << functorPredicate         << postHttpsFooRequest   << false;
	QTest::newRow("function pointer predicate matching")     << functionPointerPredicate << getExampleRootRequest << true;
	QTest::newRow("function pointer predicate non-matching") << functionPointerPredicate << postHttpsFooRequest   << false;

	// Silence incorrect warning about unused member function on PredicateFunctor::operator()()
	Q_UNUSED(PredicateFunctor()(getExampleRootRequest));

}

/*! \test Tests MockNetworkAccess::Predicates::Generic and MockNetworkAccess::Predicates::createGeneric.
 */
void PredicateTest::testGeneric()
{
	QFETCH(Rule::Predicate::Ptr, predicate);
	QFETCH(Request, request);

	QTEST(predicate->matches(request), "expectMatch");
}

/*! Provides the data for the testVerb() test.
 */
void PredicateTest::testVerb_data()
{
	QTest::addColumn<QNetworkAccessManager::Operation>("verb");
	QTest::addColumn<QByteArray>("customVerb");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	//                                   // verb                                   // customVerb        // request                                                          // expectMatch
	QTest::newRow("matching GET")        << QNetworkAccessManager::GetOperation    << QByteArray()      << Request(QNetworkRequest(), QNetworkAccessManager::GetOperation)  << true;
	QTest::newRow("matching POST")       << QNetworkAccessManager::PostOperation   << QByteArray()      << Request(QNetworkRequest(), QNetworkAccessManager::PostOperation) << true;
	QTest::newRow("non-matching POST")   << QNetworkAccessManager::GetOperation    << QByteArray()      << Request(QNetworkRequest(), QNetworkAccessManager::PostOperation) << false;

	QNetworkRequest customVerbReq;
	QByteArray customVerb("OPTION");
	customVerbReq.setAttribute(QNetworkRequest::CustomVerbAttribute, customVerb);
	//                                   // verb                                   // customVerb        // request                                                          // expectMatch
	QTest::newRow("matching custom")     << QNetworkAccessManager::CustomOperation << customVerb        << Request(customVerbReq, QNetworkAccessManager::CustomOperation)   << true;
	QTest::newRow("non-matching custom") << QNetworkAccessManager::CustomOperation << QByteArray("FOO") << Request(customVerbReq, QNetworkAccessManager::CustomOperation)   << false;
}

/*! \test Tests MockNetworkAccess::Predicates::Verb.
 */
void PredicateTest::testVerb()
{
	QFETCH(QNetworkAccessManager::Operation, verb);
	QFETCH(QByteArray, customVerb);
	QFETCH(Request, request);

	Verb pred(verb, customVerb);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testUrlMatching() test.
 */
void PredicateTest::testUrlMatching_data()
{
	QTest::addColumn<QRegularExpression>("urlRegEx");
	QTest::addColumn<QUrl::FormattingOptions>("urlFormat");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	//                             // urlRegEx                                         // urlFormat                                    // request                                                         // expectMatch
	QTest::newRow("simple regex")  << QRegularExpression("https?://example.com.*")     << QUrl::FormattingOptions(QUrl::FullyEncoded)  << Request(QNetworkRequest(QUrl("http://example.com/index.html"))) << true;
	QTest::newRow("non-matching")  << QRegularExpression("https?://example.com/foo.*") << QUrl::FormattingOptions(QUrl::FullyEncoded)  << Request(QNetworkRequest(QUrl("http://example.com/index.html"))) << false;
	QTest::newRow("match all")     << QRegularExpression(".*")                         << QUrl::FormattingOptions(QUrl::FullyEncoded)  << Request(QNetworkRequest(QUrl("custom.proto://command")))        << true;
	QTest::newRow("match none")    << QRegularExpression("a^")                         << QUrl::FormattingOptions(QUrl::FullyEncoded)  << getExampleRootRequest                                           << false;
	QTest::newRow("match encoded") << QRegularExpression(".*%C3%B6")                   << QUrl::FormattingOptions(QUrl::FullyEncoded)  << Request(QNetworkRequest(QUrl("http://example.com/%C3%B6")))     << true;
	QTest::newRow("match decoded") << QRegularExpression("foo bar")                    << QUrl::FormattingOptions(QUrl::PrettyDecoded) << Request(QNetworkRequest(QUrl("http://example.com/foo%20bar")))  << true;
}

/*! \test Tests MockNetworkAccess::Predicates::UrlMatching.
 */
void PredicateTest::testUrlMatching()
{
	QFETCH(QRegularExpression, urlRegEx);
	QFETCH(QUrl::FormattingOptions, urlFormat);
	QFETCH(Request, request);

	UrlMatching pred(urlRegEx, urlFormat);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testUrl() test.
 */
void PredicateTest::testUrl_data()
{
	QTest::addColumn<QUrl>("url");
	QTest::addColumn<int>("defaultPort");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	const int noDefaultPort = -1;
	const int customDefaultPort = 8080;

	const QUrl exampleRootHttpsUrl("https://example.com");
	const QUrl exampleRootHttpDefaultPortUrl("http://example.com:80");
	const QUrl exampleRootHttpsDefaultPortUrl("https://example.com:443");
	const QUrl exampleRootCustomDefaultPortUrl("http://example.com:8080");
	const QUrl exampleRootOtherPortUrl("http://example.com:8081");
	const QUrl invalidUrl("http://..");
	const QUrl anotherInvalidUrl("http://!!");
	const Request emptyUrlRequest(QNetworkAccessManager::GetOperation, QNetworkRequest(QUrl("")));
	const Request invalidUrlRequest(QNetworkAccessManager::GetOperation, QNetworkRequest(invalidUrl));
	const Request anotherInvalidUrlRequest(QNetworkAccessManager::GetOperation, QNetworkRequest(anotherInvalidUrl));

	//                                                     // url                                        // defaultPort       // request                                                              // expectMatch
	QTest::newRow("simple URL")                            << exampleRootUrl                             << noDefaultPort     << getExampleRootRequest                                                << true;
	QTest::newRow("http default port in request")          << exampleRootUrl                             << noDefaultPort     << Request(QNetworkRequest(exampleRootHttpDefaultPortUrl))              << true;
	QTest::newRow("http default port in predicate")        << exampleRootHttpDefaultPortUrl              << noDefaultPort     << getExampleRootRequest                                                << true;
	QTest::newRow("https default port in request")         << exampleRootHttpsUrl                        << noDefaultPort     << Request(QNetworkRequest(exampleRootHttpsDefaultPortUrl))             << true;
	QTest::newRow("https default port in predicate")       << exampleRootHttpsDefaultPortUrl             << noDefaultPort     << Request(QNetworkRequest(exampleRootHttpsUrl))                        << true;
	QTest::newRow("superfluous default port")              << exampleRootUrl                             << customDefaultPort << getExampleRootRequest                                                << true;
	QTest::newRow("custom default port in request")        << exampleRootUrl                             << customDefaultPort << Request(QNetworkRequest(exampleRootCustomDefaultPortUrl))            << true;
	QTest::newRow("custom default port in predicate")      << exampleRootCustomDefaultPortUrl            << customDefaultPort << getExampleRootRequest                                                << true;
	QTest::newRow("complex URI")                           << QUrl("custom.proto://user:pass@example.com/resource?a=b#anchor-a") << -1 << Request(QNetworkRequest(QUrl("custom.proto://user:pass@example.com/resource?a=b#anchor-a"))) << true;
	QTest::newRow("matching null URL")                     << QUrl()                                     << noDefaultPort     << emptyUrlRequest                                                      << true;
	QTest::newRow("matching empty URL")                    << QUrl("")                                   << noDefaultPort     << emptyUrlRequest                                                      << true;
	QTest::newRow("matching invalid URL")                  << invalidUrl                                 << noDefaultPort     << invalidUrlRequest                                                    << true;
	QTest::newRow("mismatching invalid URLs match")        << invalidUrl                                 << noDefaultPort     << anotherInvalidUrlRequest                                             << true;
	QTest::newRow("empty and invalid URLs match")          << invalidUrl                                 << noDefaultPort     << emptyUrlRequest                                                      << true;
	QTest::newRow("null URL")                              << QUrl()                                     << noDefaultPort     << getExampleRootRequest                                                << false;
	QTest::newRow("empty URL")                             << QUrl("")                                   << noDefaultPort     << getExampleRootRequest                                                << false;
	QTest::newRow("invalid URL")                           << invalidUrl                                 << noDefaultPort     << getExampleRootRequest                                                << false;
	QTest::newRow("different host")                        << exampleRootUrl                             << noDefaultPort     << Request(QNetworkRequest(QUrl("http://foobar.com")))                  << false;
	QTest::newRow("different path")                        << QUrl("http://example.com/bar")             << noDefaultPort     << Request(QNetworkRequest(QUrl("http://example.com/foo")))             << false;
	QTest::newRow("query order mismatch")                  << QUrl("http://example.com?foo=bar&bar=foo") << noDefaultPort     << Request(QNetworkRequest(QUrl("http://example.com?bar=foo&foo=bar"))) << false;
	QTest::newRow("trailing slash mismatch")               << exampleRootUrl                             << noDefaultPort     << Request(QNetworkRequest(QUrl("http://example.com/")))                << false;
	QTest::newRow("mismatching default port in request")   << exampleRootUrl                             << customDefaultPort << Request(QNetworkRequest(exampleRootOtherPortUrl))                    << false;
	QTest::newRow("mismatching default port in predicate") << exampleRootOtherPortUrl                    << customDefaultPort << getExampleRootRequest                                                << false;
	QTest::newRow("mismatching ports with default port")   << exampleRootOtherPortUrl                    << customDefaultPort << Request(QNetworkRequest(QUrl("http://example.com:77")))              << false;
}

/*! \test Tests MockNetworkAccess::Predicates::Url.
 */
void PredicateTest::testUrl()
{
	QFETCH(QUrl, url);
	QFETCH(int, defaultPort);
	QFETCH(Request, request);

	Url pred(url, defaultPort);
	QTEST(pred.matches(request), "expectMatch");
}

/*! \test Tests MockNetworkAccess::Predicates::QueryParameter  using the constructor
 * MockNetworkAccess::Predicates::QueryParameter(const QString&, const QString&, QUrl::ComponentFormattingOptions).
 */
void PredicateTest::testQueryParameter()
{
	QFETCH( QString, queryParam );
	QFETCH( QStringList, values );
	QFETCH( Request, request );

	if ( values.size() != 1 )
		return;

	QueryParameter pred( queryParam, values.at(0) );
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testQueryParameter() test.
 */
void PredicateTest::testQueryParameter_data()
{
	QTest::addColumn< QString >( "queryParam" );
	QTest::addColumn< QStringList >( "values" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< bool >( "expectMatch" );

	const QString paramKey( "foo" );
	const QString paramValue( "bar" );
	const QStringList valueList = ( QStringList() << "one" << "two" );
	const QStringList otherValueList = ( QStringList() << "one" << "three" );

	const QUrl urlWithEmptyQueryKey( "http://example.com?=bar" );
	const QUrl urlWithEmptyQueryValue( "http://example.com?foo=" );
	const QUrl urlWithEmptyQueryPair( "http://example.com?=" );
	const QUrl urlWithFlag( "http://example.com?foo" );
	const QUrl urlWithMultiValueQuery( "http://example.com?foo=one&foo=two" );

	//                                                 // queryParam // values                                  // request                                              // expectMatch
	QTest::newRow( "single parameter" )                << paramKey   << ( QStringList() << paramValue )         << singleParamRequest                                   << true;
	QTest::newRow( "multiple parameters" )             << paramKey   << ( QStringList() << paramValue )         << doubleParamRequest                                   << true;
	QTest::newRow( "no match parameter" )              << "test"     << ( QStringList() << paramValue )         << singleParamRequest                                   << false;
	QTest::newRow( "mismatching value" )               << paramKey   << ( QStringList() << QString( "dummy" ) ) << singleParamRequest                                   << false;
	QTest::newRow( "empty key mismatch" )              << ""         << ( QStringList() << paramValue )         << singleParamRequest                                   << false;
	QTest::newRow( "empty key match" )                 << ""         << ( QStringList() << paramValue )         << Request( QNetworkRequest( urlWithEmptyQueryKey ) )   << true;
	QTest::newRow( "empty value mismatch" )            << paramKey   << ( QStringList() << QString( "" ) )      << singleParamRequest                                   << false;
	QTest::newRow( "empty value match" )               << paramKey   << ( QStringList() << QString( "" ) )      << Request( QNetworkRequest( urlWithEmptyQueryValue ) ) << true;
	QTest::newRow( "empty query pair" )                << ""         << ( QStringList() << QString( "" ) )      << Request( QNetworkRequest( urlWithEmptyQueryPair ) )  << true;
	QTest::newRow( "flag parameter ")                  << paramKey   << ( QStringList() << QString( "" ) )      << Request( QNetworkRequest( urlWithFlag ) )            << true;
	QTest::newRow( "multi valued parameter" )          << paramKey   << valueList                               << Request( QNetworkRequest( urlWithMultiValueQuery ) ) << true;
	QTest::newRow( "multi valued parameter mismatch" ) << paramKey   << otherValueList                          << Request( QNetworkRequest( urlWithMultiValueQuery ) ) << false;
}

/*! \test Tests MockNetworkAccess::Predicates::QueryParameter using the constructor
 * MockNetworkAccess::Predicates::QueryParameter(const QString&, const QStringList&, QUrl::ComponentFormattingOptions).
 */
void PredicateTest::testMultiValueQueryParameter()
{
	QFETCH( QString, queryParam );
	QFETCH( QStringList, values );
	QFETCH( Request, request );

	QueryParameter pred( queryParam, values );
	QTEST( pred.matches( request ), "expectMatch" );
}

/*! Provides the data for the testMultiValueQueryParameter() test.
 */
void PredicateTest::testMultiValueQueryParameter_data()
{
	testQueryParameter_data();
}

/*! \test Tests MockNetworkAccess::Predicates::QueryParameterMatching.
*/
void PredicateTest::testQueryParameterMatching()
{
	QFETCH( QString, queryParam );
	QFETCH( QRegularExpression, regEx );
	QFETCH( QUrl::ComponentFormattingOptions, format );
	QFETCH( Request, request );

	QueryParameterMatching pred( queryParam, regEx, format );
	QTEST( pred.matches( request ), "expectMatch" );
}

/*! Provides the data for the PredicateTest::testQueryParameterMatching() test.
*/
void PredicateTest::testQueryParameterMatching_data()
{
	QTest::addColumn< QString >( "queryParam" );
	QTest::addColumn< QRegularExpression >( "regEx" );
	QTest::addColumn< QUrl::ComponentFormattingOptions >( "format" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< bool >( "expectMatch" );

	const QString paramKey( "foo" );
	const QRegularExpression valueContainingRegEx( ".*bar.*" );
	const QUrl urlWithMultiValueQuery( "http://example.com?foo=test&foo=testbar&bar=test" );

	const QUrl::ComponentFormattingOptions prettyDecodedFormat( QUrl::PrettyDecoded );

	//                                        // queryParam // regEx                          // format              // request                                                     << expectMatch
	QTest::newRow( "single parameter" )       << paramKey   << valueContainingRegEx           << prettyDecodedFormat << singleParamRequest                                          << true;
	QTest::newRow( "multiple parameters" )    << paramKey   << valueContainingRegEx           << prettyDecodedFormat << doubleParamRequest                                          << true;
	QTest::newRow( "mismatching value" )      << paramKey   << QRegularExpression( "other" )  << prettyDecodedFormat << singleParamRequest                                          << false;
	QTest::newRow( "no matching parameter" )  << "test"     << QRegularExpression( ".*" )     << prettyDecodedFormat << singleParamRequest                                          << false;
	QTest::newRow( "empty value match" )      << paramKey   << QRegularExpression( ".*" )     << prettyDecodedFormat << Request( QNetworkRequest( QUrl("http://example.com?foo="))) << true;
	QTest::newRow( "multi value match" )      << paramKey   << QRegularExpression( "test.*" ) << prettyDecodedFormat << Request( QNetworkRequest( urlWithMultiValueQuery ) )        << true;
	QTest::newRow( "multi value mismatch" )   << paramKey   << QRegularExpression( ".*bar" )  << prettyDecodedFormat << Request( QNetworkRequest( urlWithMultiValueQuery ) )        << false;
}

/*! \test Tests MockNetworkAccess::Predicates::QueryParameters using the constructor
 * MockNetworkAccess::Predicates::QueryParameters(const QueryParameterHash&, QUrl::ComponentFormattingOptions).
 */
void PredicateTest::testQueryParameters()
{
	QFETCH( QueryParameterHash, queryParams );
	QFETCH( Request, request );

	QueryParameters pred( queryParams );
	QTEST( pred.matches( request ), "expectMatch" );
}

/*! Provides the data for the testQueryParameters() test.
 */
void PredicateTest::testQueryParameters_data()
{
	QTest::addColumn< QueryParameterHash >( "queryParams" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< bool >( "expectMatch" );

	QueryParameterHash queryParams;

	//                                                     // queryParams // request               // expectMatch
	QTest::newRow( "no parameters required" )              << queryParams << singleParamRequest    << true;
	QTest::newRow( "no parameters required and no query" ) << queryParams << getExampleRootRequest << true;

	queryParams.insert( "foo", "bar" );
	//                                       // queryParams // request            // expectMatch
	QTest::newRow( "single parameter" )      << queryParams << singleParamRequest << true;
	QTest::newRow( "additional parameters" ) << queryParams << doubleParamRequest << true;

	queryParams.insert( "a", "17" );
	//                                       // queryParams // request                                                                 // expectMatch
	QTest::newRow( "multiple parameters" )   << queryParams << Request( QNetworkRequest( QUrl( "http://example.com?a=17&foo=bar" ) ) ) << true;
	QTest::newRow( "missing parameter" )     << queryParams << Request( QNetworkRequest( QUrl( "http://example.com?a=17&bar=foo" ) ) ) << false;
	QTest::newRow( "missing query" )         << queryParams << getExampleRootRequest                                                   << false;
	QTest::newRow( "different value" )       << queryParams << doubleParamRequest                                                      << false;

	queryParams.clear();
	queryParams.insert( "foo", "bar" );
	queryParams.insert( "a", "true" );
	//                                       // queryParams // request            // expectMatch
	QTest::newRow( "different value type" )  << queryParams << doubleParamRequest << false;
}

/*! \test Tests MockNetworkAccess::Predicates::QueryParameters using the constructor
 * MockNetworkAccess::Predicates::QueryParameters(const MultiValueQueryParameterHash&, QUrl::ComponentFormattingOptions).
 */
void PredicateTest::testMultiValueQueryParameters()
{
	QFETCH( MultiValueQueryParameterHash, queryParams );
	QFETCH( Request, request );

	QueryParameters pred( queryParams );
	QTEST( pred.matches( request ), "expectMatch" );
}

/*! Provides the data for the testMultiValueQueryParameters() test.
 */
void PredicateTest::testMultiValueQueryParameters_data()
{
	QTest::addColumn< MultiValueQueryParameterHash >( "queryParams" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< bool >( "expectMatch" );

	MultiValueQueryParameterHash queryParams;

	const QString foo( "foo" );
	const QString fooValue( "bar" );

	//                                                     // queryParams // request               // expectMatch
	QTest::newRow( "no parameters required" )              << queryParams << singleParamRequest    << true;
	QTest::newRow( "no parameters required and no query" ) << queryParams << getExampleRootRequest << true;

	queryParams.insert( foo, QStringList() << fooValue );
	//                                       // queryParams // request            // expectMatch
	QTest::newRow( "single parameter" )      << queryParams << singleParamRequest << true;
	QTest::newRow( "additional parameters" ) << queryParams << doubleParamRequest << true;

	queryParams[ foo ] << "two";
	//                                        // queryParams // request                                                                             // expectMatch
	QTest::newRow( "multi: match" )           << queryParams << Request( QNetworkRequest( QUrl( "http://example.com?foo=bar&foo=two" ) ) )          << true;
	QTest::newRow( "multi: value mismatch" )  << queryParams << Request( QNetworkRequest( QUrl( "http://example.com?foo=bar&foo=bla" ) ) )          << false;
	QTest::newRow( "multi: too many values" ) << queryParams << Request( QNetworkRequest( QUrl( "http://example.com?foo=bar&foo=bla&foo=test" ) ) ) << false;

	queryParams[ foo ] << "test";
	QTest::newRow( "multi: missing value" )   << queryParams << Request( QNetworkRequest( QUrl( "http://example.com?foo=bar&foo=two" ) ) )          << false;

	queryParams.clear();
	queryParams.insert( foo, QStringList() << "test" );
	//                                           // queryParams // request            // expectMatch
	QTest::newRow( "single parameter mismatch" ) << queryParams << singleParamRequest << false;

	queryParams.clear();
	queryParams.insert( foo, QStringList() << fooValue );
	queryParams.insert( "a", QStringList() << "1" );
	//                                           // queryParams // request            // expectMatch
	QTest::newRow( "multiple parameters match" ) << queryParams << doubleParamRequest << true;
	QTest::newRow( "missing parameter" )         << queryParams << singleParamRequest << false;
}

/*! \test Tests Predicates::QueryParameterTemplates.
 */
void PredicateTest::testQueryParameterTemplates()
{
	QFETCH( RegExPairVector, queryParamTemplates );
	QFETCH( Request, request );
	QFETCH( QUrl::ComponentFormattingOptions, format );

	QueryParameterTemplates pred( queryParamTemplates, format );
	QTEST( pred.matches( request ), "expectMatch" );
}

/*! Provides the data for the PredicateTest::testQueryParameterTemplates() test.
 */
void PredicateTest::testQueryParameterTemplates_data()
{
	QTest::addColumn< RegExPairVector >( "queryParamTemplates" );
	QTest::addColumn< Request >( "request" );
	QTest::addColumn< QUrl::ComponentFormattingOptions >( "format" );
	QTest::addColumn< bool >( "expectMatch" );

	const QUrl urlWithSingleQueryParam( "http://example.com?foo=testbar" );
	const QUrl urlWithOtherQueryParam( "http://example.com?some=testbar" );
	const QUrl urlWithMultipleQueryParams( "http://example.com?foo=testbar&some=other" );
	const QUrl urlWithMultiValueQueryParam( "http://example.com?foo=testbar&foo=bar" );
	const QUrl urlWithOtherMultiValueQueryParam( "http://example.com?foo=testbar&foo=test" );
	const QUrl urlWithSpecialCharQueryParam( "http://example.com?foo=%26%2F%2F");

	RegExPairVector singleTemplate;
	singleTemplate << qMakePair( QRegularExpression( ".*foo.*" ), QRegularExpression( ".*bar.*" ) );

	RegExPairVector otherTemplate;
	otherTemplate << qMakePair( QRegularExpression( "some" ), QRegularExpression( ".*other.*" ) );

	RegExPairVector multipleTemplates;
	multipleTemplates << qMakePair( QRegularExpression( "some" ), QRegularExpression( ".*" ) )
	                  << qMakePair( QRegularExpression( ".*foo.*" ), QRegularExpression( ".*bar.*" ) );

	RegExPairVector specialCharTemplate;
	specialCharTemplate << qMakePair( QRegularExpression( ".*" ), QRegularExpression( "&//" ) );

	//                                          // queryParamTemplates // request                                                        // format                                                // expectMatch
	QTest::newRow( "single template match" )    << singleTemplate      << Request( QNetworkRequest( urlWithSingleQueryParam ) )          << QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded) << true;
	QTest::newRow( "multiple templates match" ) << multipleTemplates   << Request( QNetworkRequest( urlWithSingleQueryParam ) )          << QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded) << true;
	QTest::newRow( "param key mismatch" )       << singleTemplate      << Request( QNetworkRequest( urlWithOtherQueryParam ) )           << QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded) << false;
	QTest::newRow( "param value mismatch" )     << otherTemplate       << Request( QNetworkRequest( urlWithOtherQueryParam ) )           << QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded) << false;
	QTest::newRow( "multiple param match" )     << multipleTemplates   << Request( QNetworkRequest( urlWithMultipleQueryParams ) )       << QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded) << true;
	QTest::newRow( "multiple param mismatch" )  << singleTemplate      << Request( QNetworkRequest( urlWithMultipleQueryParams ) )       << QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded) << false;
	QTest::newRow( "multi value match" )        << singleTemplate      << Request( QNetworkRequest( urlWithMultiValueQueryParam ) )      << QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded) << true;
	QTest::newRow( "multi value mismatch" )     << singleTemplate      << Request( QNetworkRequest( urlWithOtherMultiValueQueryParam ) ) << QUrl::ComponentFormattingOptions(QUrl::PrettyDecoded) << false;
	QTest::newRow( "fully decoded match" )      << specialCharTemplate << Request( QNetworkRequest( urlWithSpecialCharQueryParam ) )     << QUrl::ComponentFormattingOptions(QUrl::FullyDecoded)  << true;

}

/*! Provides the data for the testBodyMatching() test.
 */
void PredicateTest::testBodyMatching_data()
{
	QTest::addColumn<QRegularExpression>("regEx");
	QTest::addColumn<QTextCodec*>("textCodec");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	QTextCodec* nullTextCodecPtr = Q_NULLPTR;

	QNetworkRequest plainTextReq;
	plainTextReq.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	QByteArray plainTextBody("test foo text");
	//                                // regEx                                 // textCodec        // request                                                                    // expectMatch
	QTest::newRow("plain text")       << QRegularExpression("foo")             << nullTextCodecPtr << Request(plainTextReq, QNetworkAccessManager::PostOperation, plainTextBody) << true;
	QTest::newRow("exact match")      << QRegularExpression("^test foo text$") << nullTextCodecPtr << Request(plainTextReq, QNetworkAccessManager::PostOperation, plainTextBody) << true;
	QTest::newRow("GET request body") << QRegularExpression("foo")             << nullTextCodecPtr << Request(plainTextReq, QNetworkAccessManager::GetOperation,  plainTextBody) << true;
	QTest::newRow("non-matching")     << QRegularExpression("bar")             << nullTextCodecPtr << Request(plainTextReq, QNetworkAccessManager::PostOperation, plainTextBody) << false;

	QNetworkRequest jsonReq;
	/* application/json doesn't exist in the built-in MIME database of some Qt 5.x releases (including LTS 5.6).
	 * So we use it's parent type, application/javascript.
	 */
	jsonReq.setHeader(QNetworkRequest::ContentTypeHeader, "application/javascript");
	const QByteArray jsonBody = QString::fromWCharArray(L"{ \"text\": \"f\u00F6\u00F6\" }").toUtf8();
	QTest::newRow("json") << QRegularExpression(".*\"f\\x{00F6}\\x{00F6}\".*") << nullTextCodecPtr << Request(jsonReq, QNetworkAccessManager::PostOperation, jsonBody) << true;

	QNetworkRequest nonUtf8Req;
	const QString codecName("ISO 8859-7");
	nonUtf8Req.setHeader(QNetworkRequest::ContentTypeHeader, "text/plain;charset:"+codecName);
	QTextCodec* iso8859_7 = QTextCodec::codecForName(codecName.toLatin1());
	const QByteArray nonUtf8Body = iso8859_7->fromUnicode(QString::fromWCharArray(L"\u03A3\u03B2\u03C0"));
	QTest::newRow("non-UTF8") << QRegularExpression("\\x{03A3}\\x{03B2}\\x{03C0}") << nullTextCodecPtr << Request(nonUtf8Req, QNetworkAccessManager::PostOperation, nonUtf8Body) << true;

	const QRegularExpression aeRegEx("\\x{00E6}");
	const QRegularExpression betaRegEx("\\x{03B2}");
	const QByteArray latin1Body = QString::fromWCharArray(L"\u00E4\u00E6\u03B2").toLatin1();
	const QByteArray utf16Body = QTextCodec::codecForName("utf-16")->fromUnicode(QString::fromWCharArray(L"\u03A3\u03B2\u03C0"));

	QNetworkRequest binaryReq;
	binaryReq.setHeader(QNetworkRequest::ContentTypeHeader, "application/octet-stream");
	QTest::newRow("binary") << betaRegEx << nullTextCodecPtr << Request(binaryReq, QNetworkAccessManager::PostOperation, utf16Body) << true;

	QNetworkRequest unknownContentReq;
	unknownContentReq.setHeader(QNetworkRequest::ContentTypeHeader, "x/foo");
	QTest::newRow("unknown content type") << aeRegEx << nullTextCodecPtr << Request(unknownContentReq, QNetworkAccessManager::PostOperation, latin1Body) << true;

	QNetworkRequest unknownCharsetReq;
	unknownCharsetReq.setHeader(QNetworkRequest::ContentTypeHeader, "text/plain;charset:foo123321");
	QTest::newRow("unknown charset") << aeRegEx << nullTextCodecPtr << Request(unknownCharsetReq, QNetworkAccessManager::PostOperation, latin1Body) << true;

	QTest::newRow("explicit codec")  << betaRegEx << iso8859_7 << Request(unknownContentReq, QNetworkAccessManager::PostOperation, nonUtf8Body) << true;

}

/*! \test Tests MockNetworkAccess::Predicates::BodyMatching.
 */
void PredicateTest::testBodyMatching()
{
	QFETCH(QRegularExpression, regEx);
	QFETCH(QTextCodec*, textCodec);
	QFETCH(Request, request);

	BodyMatching pred(regEx, textCodec);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testBodyContaining() test.
 */
void PredicateTest::testBodyContaining_data()
{
	QTest::addColumn<QByteArray>("snippet");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	QNetworkRequest binaryReq;
	binaryReq.setHeader(QNetworkRequest::ContentTypeHeader, "application/octet-stream");

	//                            // snippet                           // request                                                                                     // expectMatch
	QTest::newRow("simple match") << QByteArray("foo")                 << Request(binaryReq, QNetworkAccessManager::PostOperation, QByteArray("\xE4\xE6\xDF foobar")) << true;
	QTest::newRow("exact match")  << QByteArray("\xE4\xE6\xDF foobar") << Request(binaryReq, QNetworkAccessManager::PostOperation, QByteArray("\xE4\xE6\xDF foobar")) << true;
	QTest::newRow("no match")     << QByteArray("test")                << Request(binaryReq, QNetworkAccessManager::PostOperation, QByteArray("\xE4\xE6\xDF foobar")) << false;

	QNetworkRequest textReq;
	textReq.setHeader(QNetworkRequest::ContentTypeHeader, "text/plain;charset:big5");
	QTest::newRow("charset is irrelevant") << QByteArray("\xE6") << Request(textReq, QNetworkAccessManager::PostOperation, QByteArray("\xE4\xE6\xDF")) << true;
}

/*! \test Tests MockNetworkAccess::Predicates::BodyContaining.
 */
void PredicateTest::testBodyContaining()
{
	QFETCH(QByteArray, snippet);
	QFETCH(Request, request);

	BodyContaining pred(snippet);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testBody() test.
 */
void PredicateTest::testBody_data()
{
	QTest::addColumn<QByteArray>("body");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	QNetworkRequest binaryReq;
	binaryReq.setHeader(QNetworkRequest::ContentTypeHeader, "application/octet-stream");
	QTest::newRow("simple match")     << QByteArray("foobar") << Request(binaryReq, QNetworkAccessManager::PostOperation, QByteArray("foobar")) << true;
	QTest::newRow("no partial match") << QByteArray("foo")    << Request(binaryReq, QNetworkAccessManager::PostOperation, QByteArray("foobar")) << false;

	QNetworkRequest textReq;
	textReq.setHeader(QNetworkRequest::ContentTypeHeader, "text/plain;charset:big5");
	QTest::newRow("content type is irrelevant") << QByteArray("\xE4\xE6\xDF") << Request(textReq, QNetworkAccessManager::PostOperation, QByteArray("\xE4\xE6\xDF")) << true;
}

/*! \test Tests MockNetworkAccess::Predicates::Body.
 */
void PredicateTest::testBody()
{
	QFETCH(QByteArray, body);
	QFETCH(Request, request);

	Body pred(body);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testHeaders() test.
 */
void PredicateTest::testHeaders_data()
{
	QTest::addColumn<HeaderHash>("headers");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	const QString userAgentValue("MyClient/1.0.0");

	HeaderHash headers;

	headers.insert(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	QNetworkRequest textReq;
	textReq.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	QTest::newRow("single header") << headers << Request(textReq) << true;
	headers.clear();

	headers.insert(QNetworkRequest::UserAgentHeader, userAgentValue);
	headers.insert(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	QNetworkRequest multipleHeaderReq;
	multipleHeaderReq.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	multipleHeaderReq.setHeader(QNetworkRequest::UserAgentHeader, userAgentValue);
	QTest::newRow("multiple headers") << headers << Request(multipleHeaderReq) << true;
	headers.clear();

	headers.insert(QNetworkRequest::UserAgentHeader, userAgentValue);
	QTest::newRow("matching subset of headers") << headers << Request(multipleHeaderReq) << true;
	headers.clear();

	headers.insert(QNetworkRequest::LocationHeader, "http://missing-header.com");
	headers.insert(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	QTest::newRow("missing header") << headers << Request(textReq) << false;
	headers.clear();

	headers.insert(QNetworkRequest::ContentTypeHeader, "application/json");
	QTest::newRow("mismatching value") << headers << Request(textReq) << false;
	headers.clear();
}

/*! \test Tests MockNetworkAccess::Predicates::Headers.
 */
void PredicateTest::testHeaders()
{
	QFETCH(HeaderHash, headers);
	QFETCH(Request, request);

	Headers pred(headers);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testHeader() test.
 */
void PredicateTest::testHeader_data()
{
	QTest::addColumn<QNetworkRequest::KnownHeaders>("header");
	QTest::addColumn<QVariant>("value");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	QNetworkRequest textReq;
	textReq.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	QTest::newRow("single header") << QNetworkRequest::ContentTypeHeader << QVariant::fromValue(contentTypeTextPlain) << Request(textReq) << true;

	QNetworkRequest multipleHeaderReq;
	multipleHeaderReq.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	multipleHeaderReq.setHeader(QNetworkRequest::UserAgentHeader, "MyClient/1.0.0");
	QTest::newRow("matching subset of headers") << QNetworkRequest::ContentTypeHeader << QVariant::fromValue(contentTypeTextPlain)                 << Request(multipleHeaderReq) << true;
	QTest::newRow("missing header")             << QNetworkRequest::LocationHeader    << QVariant::fromValue(QString("http://missing-header.com")) << Request(textReq)           << false;
	QTest::newRow("mismatching value")          << QNetworkRequest::ContentTypeHeader << QVariant::fromValue(QString("application/json"))          << Request(textReq)           << false;
}

/*! \test Tests MockNetworkAccess::Predicates::Header.
 */
void PredicateTest::testHeader()
{
	QFETCH(QNetworkRequest::KnownHeaders, header);
	QFETCH(QVariant, value);
	QFETCH(Request, request);

	Header pred(header, value);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testHeaderMatching() test.
 */
void PredicateTest::testHeaderMatching_data()
{
	QTest::addColumn<QNetworkRequest::KnownHeaders>("header");
	QTest::addColumn<QRegularExpression>("regEx");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	QNetworkRequest textReq;
	textReq.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	//                                    // header                             // regEx                                // request          // expectMatch
	QTest::newRow("match")                << QNetworkRequest::ContentTypeHeader << QRegularExpression("text/.*")        << Request(textReq) << true;
	QTest::newRow("match missing header") << QNetworkRequest::LocationHeader    << QRegularExpression(".*")             << Request(textReq) << true;
	QTest::newRow("no match")             << QNetworkRequest::ContentTypeHeader << QRegularExpression("application/.*") << Request(textReq) << false;
	QTest::newRow("missing header")       << QNetworkRequest::LocationHeader    << QRegularExpression(".+")             << Request(textReq) << false;
}

/*! \test Tests MockNetworkAccess::Predicates::HeaderMatching.
 */
void PredicateTest::testHeaderMatching()
{
	QFETCH(QNetworkRequest::KnownHeaders, header);
	QFETCH(QRegularExpression, regEx);
	QFETCH(Request, request);

	HeaderMatching pred(header, regEx);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testRawHeaders() test.
 */
void PredicateTest::testRawHeaders_data()
{
	QTest::addColumn<RawHeaderHash>("headers");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	const QByteArray userAgentHeader("User-Agent");
	const QByteArray userAgentMyClient("MyClient/1.0.0");

	RawHeaderHash headers;

	headers.insert(contentTypeHeader, contentTypeTextPlain);
	QNetworkRequest textReq;
	textReq.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	QTest::newRow("single header") << headers << Request(textReq) << true;
	headers.clear();

	headers.insert(userAgentHeader, userAgentMyClient);
	headers.insert(contentTypeHeader, contentTypeTextPlain);
	QNetworkRequest multipleHeaderReq;
	multipleHeaderReq.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	multipleHeaderReq.setHeader(QNetworkRequest::UserAgentHeader, userAgentMyClient);
	QTest::newRow("multiple headers") << headers << Request(multipleHeaderReq) << true;
	headers.clear();

	headers.insert(userAgentHeader.toLower(), userAgentMyClient);
	headers.insert(contentTypeHeader.toUpper(), contentTypeTextPlain);
	QTest::newRow("case is ignored") << headers << Request(multipleHeaderReq) << true;
	headers.clear();

	headers.insert(userAgentHeader, userAgentMyClient);
	QTest::newRow("matching subset of headers") << headers << Request(multipleHeaderReq) << true;
	headers.clear();

	headers.insert(locationHeader, "http://missing-header.com");
	headers.insert(contentTypeHeader, contentTypeTextPlain);
	QTest::newRow("missing header") << headers << Request(textReq) << false;
	headers.clear();

	headers.insert(contentTypeHeader, "application/json");
	QTest::newRow("mismatching value") << headers << Request(textReq) << false;
	headers.clear();
}

/*! \test Tests MockNetworkAccess::Predicates::RawHeaders.
 */
void PredicateTest::testRawHeaders()
{
	QFETCH(RawHeaderHash, headers);
	QFETCH(Request, request);

	RawHeaders pred(headers);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testRawHeader() test.
 */
void PredicateTest::testRawHeader_data()
{
	QTest::addColumn<QByteArray>("header");
	QTest::addColumn<QByteArray>("value");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");



	QNetworkRequest textReq;
	textReq.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	QTest::newRow("single header") << contentTypeHeader << contentTypeTextPlain << Request(textReq) << true;

	QNetworkRequest multipleHeaderReq;
	multipleHeaderReq.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	multipleHeaderReq.setHeader(QNetworkRequest::UserAgentHeader, "MyClient/1.0.0");
	QTest::newRow("matching subset of headers") << contentTypeHeader << contentTypeTextPlain << Request(multipleHeaderReq) << true;

	const QByteArray customHeader("X-Custom-Header");
	QNetworkRequest customHeaderReq;
	customHeaderReq.setRawHeader(customHeader, contentTypeTextPlain);
	QTest::newRow("custom header")     << customHeader           << contentTypeTextPlain                    << Request(customHeaderReq) << true;
	QTest::newRow("case is ignored")   << customHeader.toLower() << contentTypeTextPlain                    << Request(customHeaderReq) << true;
	QTest::newRow("missing header")    << locationHeader         << QByteArray("http://missing-header.com") << Request(textReq)         << false;
	QTest::newRow("mismatching value") << contentTypeHeader      << QByteArray("application/json")          << Request(textReq)         << false;
}

/*! \test Tests MockNetworkAccess::Predicates::RawHeader.
 */
void PredicateTest::testRawHeader()
{
	QFETCH(QByteArray, header);
	QFETCH(QByteArray, value);
	QFETCH(Request, request);

	RawHeader pred(header, value);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testRawHeaderMatching() test.
 */
void PredicateTest::testRawHeaderMatching_data()
{
	QTest::addColumn<QByteArray>("header");
	QTest::addColumn<QRegularExpression>("regEx");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	QNetworkRequest textReq;
	textReq.setHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);
	//                                    // header                      // regEx                                // request          // expectMatch
	QTest::newRow("match")                << contentTypeHeader           << QRegularExpression("text/.*")        << Request(textReq) << true;
	QTest::newRow("case is ignored")      << contentTypeHeader.toLower() << QRegularExpression("text/.*")        << Request(textReq) << true;
	QTest::newRow("match missing header") << locationHeader              << QRegularExpression(".*")             << Request(textReq) << true;
	QTest::newRow("no match")             << contentTypeHeader           << QRegularExpression("application/.*") << Request(textReq) << false;
	QTest::newRow("missing header")       << locationHeader              << QRegularExpression(".+")             << Request(textReq) << false;
}

/*! \test Tests MockNetworkAccess::Predicates::RawHeaderMatching.
 */
void PredicateTest::testRawHeaderMatching()
{
	QFETCH(QByteArray, header);
	QFETCH(QRegularExpression, regEx);
	QFETCH(Request, request);

	RawHeaderMatching pred(header, regEx);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the PredicateTest::testRawHeaderTemplates() test.
 */
void PredicateTest::testRawHeaderTemplates_data()
{
	QTest::addColumn<RegExPairVector>( "headerTemplates" );
	QTest::addColumn<Request>( "request" );
	QTest::addColumn<bool>( "expectMatch" );

	const QUrl url( "http://example.com" );

	const QByteArray headerName1( "x-foo" );
	const QByteArray headerName2( "x-bar" );

	const QByteArray headerValue1( "test" );
	const QByteArray headerValue2( "blubb" );

	Request singleHeaderRequest( QNetworkAccessManager::GetOperation, QNetworkRequest( url ) );
	singleHeaderRequest.qRequest.setRawHeader( headerName1, headerValue1 );

	Request multipleHeaderRequest( QNetworkAccessManager::GetOperation, QNetworkRequest( url ) );
	multipleHeaderRequest.qRequest.setRawHeader( headerName1, headerValue1 );
	multipleHeaderRequest.qRequest.setRawHeader( headerName2, headerValue2 );

	Request otherHeaderRequest( QNetworkAccessManager::GetOperation, QNetworkRequest( url ) );
	otherHeaderRequest.qRequest.setRawHeader( headerName2, headerValue1 );

	RegExPairVector singleTemplate;
	singleTemplate << qMakePair( QRegularExpression( "^x-foo" ), QRegularExpression( ".*test.*" ) );

	RegExPairVector multipleTemplates = singleTemplate;
	multipleTemplates << qMakePair( QRegularExpression( "^x-bar" ), QRegularExpression( ".*blubb.*" ) );

	RegExPairVector otherTemplate;
	otherTemplate << qMakePair( QRegularExpression( "^x-bar" ), QRegularExpression( ".*other.*" ) );

	//                                          // headerTemplates   // request               // expectMatch
	QTest::newRow( "single template match" )    << singleTemplate    << singleHeaderRequest   << true;
	QTest::newRow( "multiple template match" )  << multipleTemplates << singleHeaderRequest   << true;
	QTest::newRow( "header name mismatch" )     << singleTemplate    << otherHeaderRequest    << false;
	QTest::newRow( "header value mismatch" )    << otherTemplate     << otherHeaderRequest    << false;
	QTest::newRow( "multiple header match" )    << multipleTemplates << multipleHeaderRequest << true;
	QTest::newRow( "multiple header mismatch" ) << singleTemplate    << multipleHeaderRequest << false;
}

/*! \test Tests Predicates::RawHeaderTemplates.
 */
void PredicateTest::testRawHeaderTemplates()
{
	QFETCH( RegExPairVector, headerTemplates );
	QFETCH( Request, request );

	RawHeaderTemplates pred( headerTemplates );
	QTEST( pred.matches( request ), "expectMatch" );
}


/*! Provides the data for the testAttribute() test.
 */
void PredicateTest::testAttribute_data()
{
	QTest::addColumn<QNetworkRequest::Attribute>("attribute");
	QTest::addColumn<QVariant>("value");
	QTest::addColumn<bool>("matchInvalid");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	QNetworkRequest saveCookieReq;
	saveCookieReq.setAttribute(QNetworkRequest::CacheSaveControlAttribute, true);
	QTest::newRow("boolean match") << QNetworkRequest::CacheSaveControlAttribute << QVariant(true) << false << Request(saveCookieReq) << true;

	const QByteArray customVerb("OPTION");
	QNetworkRequest customVerbReq;
	customVerbReq.setAttribute(QNetworkRequest::CustomVerbAttribute, customVerb);
	QTest::newRow("byte array match") << QNetworkRequest::CustomVerbAttribute << QVariant(customVerb) << false << Request(customVerbReq) << true;

	QTest::newRow("match invalid") << QNetworkRequest::CustomVerbAttribute << QVariant(QByteArray("Foo")) << true << Request(QNetworkRequest()) << true;
	QTest::newRow("value mismatch") << QNetworkRequest::CustomVerbAttribute << QVariant(QByteArray("Bar")) << false << Request(customVerbReq) << false;
	QTest::newRow("value mismatch with match invalid") << QNetworkRequest::CustomVerbAttribute << QVariant(QByteArray("FILTER")) << true << Request(customVerbReq) << false;

	const QNetworkRequest::Attribute userAttributeId = static_cast<QNetworkRequest::Attribute>(static_cast<int>(QNetworkRequest::User) + 1);
	const int userAttrVal = 42;
	QNetworkRequest userAttrReq;
	userAttrReq.setAttribute(userAttributeId, userAttrVal);
	QTest::newRow("user attribute") << userAttributeId << QVariant(userAttrVal) << false << Request(userAttrReq) << true;
}

/*! \test Tests MockNetworkAccess::Predicates::Attribute.
 */
void PredicateTest::testAttribute()
{
	QFETCH(QNetworkRequest::Attribute, attribute);
	QFETCH(QVariant, value);
	QFETCH(bool, matchInvalid);
	QFETCH(Request, request);

	Attribute pred(attribute, value, matchInvalid);
	QTEST(pred.matches(request), "expectMatch");

}

/*! Provides the data for the testAttributeMatching() test.
 */
void PredicateTest::testAttributeMatching_data()
{
	QTest::addColumn<QNetworkRequest::Attribute>("attribute");
	QTest::addColumn<QRegularExpression>("regEx");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	QNetworkRequest saveCookieReq;
	saveCookieReq.setAttribute(QNetworkRequest::CacheSaveControlAttribute, true);
	QTest::newRow("boolean match") << QNetworkRequest::CacheSaveControlAttribute << QRegularExpression("^true$") << Request(saveCookieReq) << true;

	const QByteArray customVerb("OPTION");
	const QRegularExpression anyWordRegEx("\\w+");
	QNetworkRequest customVerbReq;
	customVerbReq.setAttribute(QNetworkRequest::CustomVerbAttribute, customVerb);
	QTest::newRow("byte array match") << QNetworkRequest::CustomVerbAttribute << anyWordRegEx << Request(customVerbReq) << true;

	QTest::newRow("no match") << QNetworkRequest::CustomVerbAttribute << QRegularExpression("^Bar") << Request(customVerbReq) << false;
}

/*! \test Tests MockNetworkAccess::Predicates::AttributeMatching.
 */
void PredicateTest::testAttributeMatching()
{
	QFETCH(QNetworkRequest::Attribute, attribute);
	QFETCH(QRegularExpression, regEx);
	QFETCH(Request, request);

	AttributeMatching pred(attribute, regEx);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the testAuthorization() test.
 */
void PredicateTest::testAuthorization_data()
{
	QTest::addColumn<StringHash>("credentials");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	QHash<QString, QString> creds;
	creds.insert("foo", "bar");

	const QByteArray fooBarAuthorization = "Basic " + QByteArray("foo:bar").toBase64();
	QNetworkRequest authdReq(exampleRootUrl);
	authdReq.setRawHeader(HttpUtils::authorizationHeader(), fooBarAuthorization);
	QTest::newRow("simple match") << creds << Request(authdReq) << true;

	creds.insert("bar", "foo");
	QTest::newRow("multiple credentials") << creds << Request(authdReq) << true;

	const QByteArray herpDerpAuthorization = "Basic " + QByteArray("herp:derp").toBase64();
	QNetworkRequest invalidCredReq(exampleRootUrl);
	invalidCredReq.setRawHeader(HttpUtils::authorizationHeader(), herpDerpAuthorization);
	QTest::newRow("invalid credentials") << creds << Request(invalidCredReq) << false;

	QNetworkRequest unauthdReq(exampleRootUrl);
	QTest::newRow("unauth'ed request") << creds << Request(unauthdReq) << false;
}

/*! \test Tests MockNetworkAccess::Predicates::Authorization.
 */
void PredicateTest::testAuthorization()
{
	QFETCH(StringHash, credentials);
	QFETCH(Request, request);

	Authorization pred(credentials);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the PredicateTest::testCookie() test.
 */
void PredicateTest::testCookie_data()
{
	QTest::addColumn<QNetworkCookie>("cookie");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	const QByteArray cookieName("foo");
	const QByteArray cookieValue("bar test");
	const QByteArray domain(".example.com");
	const QByteArray path("/foo/bar");

	QNetworkCookie sessionCookie(cookieName, cookieValue);
	sessionCookie.setDomain(domain);
	sessionCookie.setPath(path);
	QNetworkCookie otherSessionCookie("x", "y");
	otherSessionCookie.setDomain(domain);
	otherSessionCookie.setPath(path);
	QNetworkCookie overrideCookie(cookieName, "overridden");
	overrideCookie.setDomain(domain);
	overrideCookie.setPath(path);
	QNetworkCookie otherDomainCookie(cookieName, cookieValue);
	otherDomainCookie.setDomain(".example2.com");
	otherDomainCookie.setPath(path);
	QNetworkCookie expiredCookie(cookieName, cookieValue);
	expiredCookie.setExpirationDate(QDateTime::currentDateTime().addSecs(-10));
	QNetworkCookie expiringCookie(cookieName, cookieValue);
	expiringCookie.setExpirationDate(QDateTime::currentDateTime().addDays(2));

	QNetworkRequest singleCookieRequest(exampleRootUrl);
	singleCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << sessionCookie));

	QNetworkRequest multipleCookieRequest(exampleRootUrl);
	multipleCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << otherSessionCookie << sessionCookie));

	QNetworkRequest expiredCookieRequest(exampleRootUrl);
	expiredCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << expiredCookie));

	QNetworkRequest expiringCookieRequest(exampleRootUrl);
	expiringCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << expiringCookie));

	QNetworkRequest otherDomainCookieRequest(exampleRootUrl);
	otherDomainCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << otherDomainCookie));

	QNetworkRequest otherCookieRequest(exampleRootUrl);
	otherCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << otherSessionCookie));

	QNetworkRequest overridingCookieRequest(exampleRootUrl);
	overridingCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << sessionCookie << overrideCookie));

	//                                         // cookie         // request                                  // expectMatch
	QTest::newRow("single cookie match")       << sessionCookie  << Request(singleCookieRequest)             << true;
	QTest::newRow("multiple cookie match")     << sessionCookie  << Request(multipleCookieRequest)           << true;
	QTest::newRow("expired cookie match")      << expiredCookie  << Request(expiredCookieRequest)            << true;
	QTest::newRow("expiring cookie match")     << expiringCookie << Request(expiringCookieRequest)           << true;
	QTest::newRow("other domain cookie")       << sessionCookie  << Request(otherDomainCookieRequest)        << true;
	QTest::newRow("use first matching cookie") << sessionCookie  << Request(overridingCookieRequest)         << true;
	QTest::newRow("no cookies")                << sessionCookie  << Request(QNetworkRequest(exampleRootUrl)) << false;
	QTest::newRow("no matching cookie")        << sessionCookie  << Request(otherCookieRequest)              << false;
}

/*! \test Tests MockNetworkAccess::Predicates::Cookie.
 */
void PredicateTest::testCookie()
{
	QFETCH(QNetworkCookie, cookie);
	QFETCH(Request, request);

	Cookie pred(cookie);
	QTEST(pred.matches(request), "expectMatch");
}

/*! Provides the data for the PredicateTest::testCookieMatching() test.
 */
void PredicateTest::testCookieMatching_data()
{
	QTest::addColumn<QByteArray>("cookieName");
	QTest::addColumn<QRegularExpression>("regEx");
	QTest::addColumn<Request>("request");
	QTest::addColumn<bool>("expectMatch");

	const QByteArray cookieName("foo");
	const QByteArray domain(".example.com");
	const QByteArray path("/foo/bar");

	QNetworkCookie sessionCookie(cookieName, "bar test");
	sessionCookie.setDomain(domain);
	sessionCookie.setPath(path);

	QNetworkCookie otherCookie("bar", "bar test");
	otherCookie.setDomain(domain);
	otherCookie.setPath(path);

	QNetworkCookie overridingCookie(cookieName, "foo");
	overridingCookie.setDomain(domain);
	overridingCookie.setPath(path);

	const QByteArray emptyCookieName("empty");
	QNetworkCookie emptyCookie(emptyCookieName, "");
	emptyCookie.setDomain(domain);

	QNetworkRequest singleCookieRequest(exampleRootUrl);
	singleCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << sessionCookie));

	QNetworkRequest otherCookieRequest(exampleRootUrl);
	otherCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << otherCookie));

	QNetworkRequest emptyCookieRequest(exampleRootUrl);
	emptyCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << emptyCookie));

	QNetworkRequest multipleCookieRequest(exampleRootUrl);
	multipleCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << sessionCookie << otherCookie));

	QNetworkRequest overridingCookieRequest(exampleRootUrl);
	overridingCookieRequest.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(QList<QNetworkCookie>() << sessionCookie << overridingCookie));


	//                                         // cookieName      // regEx                          // request                                  // expectMatch
	QTest::newRow("simple match")              << cookieName      << QRegularExpression("bar.*")    << Request(singleCookieRequest)             << true;
	QTest::newRow("empty value match")         << emptyCookieName << QRegularExpression("^$")       << Request(emptyCookieRequest)              << true;
	QTest::newRow("use first matching cookie") << cookieName      << QRegularExpression("bar test") << Request(overridingCookieRequest)         << true;
	QTest::newRow("value mismatch")            << cookieName      << QRegularExpression("foo.*")    << Request(singleCookieRequest)             << false;
	QTest::newRow("no cookies")                << cookieName      << QRegularExpression(".*")       << Request(QNetworkRequest(exampleRootUrl)) << false;
	QTest::newRow("no matching cookie")        << cookieName      << QRegularExpression(".*")       << Request(otherCookieRequest)              << false;
}

/*! \test
 */
void PredicateTest::testCookieMatching()
{
	QFETCH(QByteArray, cookieName);
	QFETCH(QRegularExpression, regEx);
	QFETCH(Request, request);

	CookieMatching pred(cookieName, regEx);
	QTEST(pred.matches(request), "expectMatch");
}

} // namespace Tests

QTEST_MAIN(Tests::PredicateTest)
#include "PredicateTest.moc"
