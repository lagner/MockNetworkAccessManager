#include "MockNetworkAccessManager.hpp"

#include <QtTest>

namespace Tests {

/*! Implements unit tests for the Rule class.
 */
class RuleTest : public QObject
{
	Q_OBJECT

private Q_SLOTS:
	void testGetSetPredicates();
	void testHasHasNot();
	void testIsMatchingIsNotMatching();
	void testMatches();
	void testNegate();
	void testCreateReply();
	void testClone();
	void testClone_data();
};

using namespace MockNetworkAccess;
using namespace MockNetworkAccess::Predicates;

/*! \test Tests the Rule::predicates() and Rule::setPredicates() methods.
 */
void RuleTest::testGetSetPredicates()
{
	Rule conf;

	QVERIFY(conf.predicates().isEmpty());

	QVector<Rule::Predicate::Ptr> preds;
	preds.append(Rule::Predicate::Ptr(new Anything()));
	preds.append(Rule::Predicate::Ptr(new Url(QUrl("http://example.com"))));
	conf.setPredicates(preds);

	QCOMPARE(conf.predicates(), preds);
}

/*! \test Tests the Rule::has() and Rule::hasNot() methods.
 */
void RuleTest::testHasHasNot()
{
	Rule conf;

	Anything pred;

	QCOMPARE(conf.has(pred), &conf);
	QCOMPARE(conf.hasNot(pred), &conf);

	Request simpleReq(QNetworkRequest(QUrl("http://example.com")));

	QVERIFY(conf.predicates().at(0)->matches(simpleReq));
	QVERIFY(!conf.predicates().at(1)->matches(simpleReq));
}

} // namespace Tests

namespace {

struct PredicateFunctor
{
	bool operator()(const MockNetworkAccess::Request& req)
	{
		return req.operation == QNetworkAccessManager::GetOperation;
	}
};

}

namespace Tests {


/*! \test Tests the Rule::isMatching() and Rule::isNotMatching() methods.
 */
void RuleTest::testIsMatchingIsNotMatching()
{
	Rule conf;

	QCOMPARE(conf.isMatching(PredicateFunctor()), &conf);
	QCOMPARE(conf.isNotMatching(PredicateFunctor()), &conf);
	QVERIFY(conf.predicates().at(0).dynamicCast<Generic<PredicateFunctor> >());
	QVERIFY(conf.predicates().at(1).dynamicCast<Generic<PredicateFunctor> >());

	Request simpleReq(QNetworkAccessManager::GetOperation, QNetworkRequest(QUrl("http://example.com")));

	QVERIFY(conf.predicates().at(0)->matches(simpleReq));
	QVERIFY(!conf.predicates().at(1)->matches(simpleReq));

	// Silence incorrect warning about unused member function on PredicateFunctor::operator()()
	Q_UNUSED(PredicateFunctor()(simpleReq));
}

/*! \test Tests the Rule::matches() method.
 */
void RuleTest::testMatches()
{
	Rule conf;

	Request req(QNetworkRequest(QUrl("http://example.com")));
	QVERIFY(conf.matches(req));

	conf.has(UrlMatching(QRegularExpression(".*example\\.com")));

	QVERIFY(!conf.matches(Request(QNetworkRequest())));
	QVERIFY(conf.matches(req));
}

/*! \test Tests the Rule::negate() method.
 */
void RuleTest::testNegate()
{
	Rule conf;
	conf.has(Anything());

	QVERIFY(conf.matches(Request(QNetworkRequest())));

	conf.negate();

	QVERIFY(!conf.matches(Request(QNetworkRequest())));
}

/*! \test Tests the Rule::createReply() method.
 */
void RuleTest::testCreateReply()
{
	Rule conf;

	QCOMPARE(conf.createReply(Request(QNetworkRequest())), static_cast<MockReply*>(Q_NULLPTR));

	QByteArray dummyBody("foo bar");
	QString contentTypeTextPlain("text/plain");
	conf.reply()->withBody(dummyBody)->withHeader(QNetworkRequest::ContentTypeHeader, contentTypeTextPlain);

	MockReply* reply = conf.createReply(Request(QNetworkRequest()));

	QVERIFY(reply);

	QByteArray body = reply->body();
	QCOMPARE(body, dummyBody);
	QCOMPARE(reply->header(QNetworkRequest::ContentTypeHeader).toString(), contentTypeTextPlain);
}

/*! \test Tests the Rule::clone() method.
 */
void RuleTest::testClone()
{
	QFETCH( Rule::Ptr, rule );

	QScopedPointer<Rule> clone( rule->clone() );

	QCOMPARE( clone->isNegated(), rule->isNegated() );
	QCOMPARE( clone->passThroughBehavior(), rule->passThroughBehavior() );
	QCOMPARE( clone->predicates(), rule->predicates() );
	QCOMPARE( *clone->reply(), *rule->reply() );
	if ( ! rule->matchedRequests().isEmpty() )
		QVERIFY( clone->matchedRequests() != rule->matchedRequests() );
}

/*! Provides the data for the testClone() test.
 */
void RuleTest::testClone_data()
{
	QTest::addColumn<Rule::Ptr>( "rule" );

	Rule::Ptr emptyRule( new Rule() );
	QTest::newRow( "empty rule" ) << emptyRule;

	Rule::Ptr singlePredicate( new Rule() );
	singlePredicate->has( Anything() );
	QTest::newRow( "single predicate" ) << singlePredicate;

	Rule::Ptr multiplePredicates( new Rule() );
	multiplePredicates->has( Url( "http://example.com" ) )
	                  ->has( Body( "foo bar" ) );
	QTest::newRow( "multiple predicates" ) << multiplePredicates;

	Rule::Ptr negated( new Rule() );
	negated->negate();
	QTest::newRow( "negated" ) << negated;

	Rule::Ptr passThrough( new Rule() );
	passThrough->passThrough();
	QTest::newRow( "pass through" ) << passThrough;

	Rule::Ptr returnPassThrough( new Rule() );
	returnPassThrough->passThrough( Rule::PassThroughReturnDelegatedReply );
	QTest::newRow( "return pass through" ) << returnPassThrough;

	QNetworkAccessManager* passThroughNam = new QNetworkAccessManager(this);
	Rule::Ptr passThroughManager( new Rule() );
	passThroughManager->passThrough( Rule::PassThroughReturnMockReply, passThroughNam );
	QTest::newRow( "pass through NAM" ) << passThroughManager;

	Rule::Ptr simpleReplyBuilder( new Rule() );
	simpleReplyBuilder->reply()->withBody( "foo bar" );
	QTest::newRow( "simple reply builder" ) << simpleReplyBuilder;

	Rule::Ptr matchedRequest( new Rule() );
	matchedRequest->has( Anything() )
	              ->reply()->withBody( "foo" );
	Manager<QNetworkAccessManager> mnam;
	mnam.addRule( matchedRequest );
	QScopedPointer<QNetworkReply> reply( mnam.get( QNetworkRequest( QUrl( "http://example.com" ) ) ) );
	QVERIFY( !matchedRequest->matchedRequests().isEmpty() );
	QTest::newRow( "matched request" ) << matchedRequest;

	Rule::Ptr fullRule( new Rule() );
	fullRule->has( Url( "http://example.com" ) )
	          ->negate()
	          ->reply()->withBody( QJsonDocument::fromJson( "{ \"foo\": \"bar\" }" ) );
	QTest::newRow( "full rule" ) << fullRule;
}


} // namespace Tests

QTEST_MAIN( Tests::RuleTest )
#include "RuleTest.moc"
