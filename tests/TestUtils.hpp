/*! \file
 */

#ifndef MOCKNETWORKACCESSMANAGER_TESTS_TESTUTILS_HPP
#define MOCKNETWORKACCESSMANAGER_TESTS_TESTUTILS_HPP

#include "QSignalInspector.hpp"

#include <QPair>
#include <QVariant>
#include <QMetaMethod>
#include <QList>

namespace Tests {

typedef QPair<QMetaMethod, QList<QVariant> > SignalEmission;
typedef QList<SignalEmission> SignalEmissionList;

QMetaMethod getSignal(const QMetaObject& metaObj, const char* signal)
{
	const int signalIndex = metaObj.indexOfSignal(QMetaObject::normalizedSignature(signal));
	return metaObj.method(signalIndex);
}

SignalEmissionList getSignalEmissionListFromInspector(const QSignalInspector& inspector)
{
	const QStringList signalBlacklist = (QStringList() <<
	                                     "startHttpRequest");
	SignalEmissionList emissions;
	QSignalInspector::ConstIterator iter;
	for (iter = inspector.cbegin(); iter != inspector.cend(); ++iter)
	{
		if (!signalBlacklist.contains(iter->signal.name()))
			emissions << qMakePair(iter->signal, iter->parameters);
	}
	return emissions;
}

} // namespace Tests

Q_DECLARE_METATYPE(Tests::SignalEmissionList)


#endif // MOCKNETWORKACCESSMANAGER_TESTS_TESTUTILS_HPP
