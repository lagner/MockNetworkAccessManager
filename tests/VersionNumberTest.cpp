﻿#include "MockNetworkAccessManager.hpp"
#include <QtTest>

namespace Tests {

/*! Implements unit tests for the VersionNumber struct.
 */
class VersionNumberTest : public QObject
{
	Q_OBJECT

private slots:
	void testConstructor();
	void testFromToString();
	void testFromToString_data();
	void testComparison();
	void testComparison_data();

};

using namespace MockNetworkAccess;

/*! \test Tests the VersionNumber() constructor.
 */
void VersionNumberTest::testConstructor()
{
	const unsigned int major = 1;
	const unsigned int minor = 3;
	const unsigned int patch = 37;

	VersionNumber::SegmentVector segments;
	segments.push_back(major);
	segments.push_back(minor);
	segments.push_back(patch);

	const QString suffix("-pre");

	VersionNumber versionNumber(major, minor, patch, suffix);

	QCOMPARE(versionNumber.segments, segments);
	QCOMPARE(versionNumber.suffix, suffix);
}

/*! \test Tests the VersionNumber::fromString() and VersionNumber::toString() methods.
 */
void VersionNumberTest::testFromToString()
{
	QFETCH(QString, versionString);
	QFETCH(VersionNumber, versionNumber);

	QCOMPARE(VersionNumber::fromString(versionString), versionNumber);
	QCOMPARE(versionNumber.toString(), versionString);
}

/*! Provides the data for the testFromToString() test.
 */
void VersionNumberTest::testFromToString_data()
{
	QTest::addColumn<QString>("versionString");
	QTest::addColumn<VersionNumber>("versionNumber");

	VersionNumber versionNumber;
	QTest::newRow("simple version") << "1.2.3" << VersionNumber(1, 2, 3);
	versionNumber.segments.clear();
	versionNumber.segments.push_back(4);
	QTest::newRow("single digit") << "4" << versionNumber;
	versionNumber.segments.clear();
	versionNumber.segments.push_back(4);
	versionNumber.segments.push_back(12);
	versionNumber.segments.push_back(3);
	versionNumber.segments.push_back(0);
	versionNumber.segments.push_back(0);
	versionNumber.segments.push_back(32428);
	QTest::newRow("long version number") << "4.12.3.0.0.32428" << versionNumber;
	QTest::newRow("with suffix") << "2.1.0-rc1" << VersionNumber(2, 1, 0, "-rc1");
	versionNumber.segments.clear();
	versionNumber.segments.push_back(9);
	versionNumber.segments.push_back(9);
	versionNumber.suffix = "-2";
	QTest::newRow("negative number is suffix") << "9.9-2" << versionNumber;
}

/*! \test Tests the comparison operators of the VersionNumber struct.
 */
void VersionNumberTest::testComparison()
{
	QFETCH(VersionNumber, left);
	QFETCH(VersionNumber, right);

	QTEST( left == right, "equal" );
	QTEST( left != right, "notEqual" );
	QTEST( left < right,  "lesser" );
	QTEST( left <= right, "lesserEqual" );
	QTEST( left > right,  "greater" );
	QTEST( left >= right, "greaterEqual" );
}

/*! Provides the data for the testComparison() test.
 */
void VersionNumberTest::testComparison_data()
{
	QTest::addColumn<VersionNumber>("left");
	QTest::addColumn<VersionNumber>("right");
	QTest::addColumn<bool>("equal");
	QTest::addColumn<bool>("notEqual");
	QTest::addColumn<bool>("lesser");
	QTest::addColumn<bool>("lesserEqual");
	QTest::addColumn<bool>("greater");
	QTest::addColumn<bool>("greaterEqual");

	//                                                 // left                              // right                            // equal // notEqual // lesser // lesserEqual // greater // greaterEqual
	QTest::newRow("simple equal")                      << VersionNumber(1, 2, 3)            << VersionNumber(1, 2, 3)           << true  << false    << false  << true        << false   << true;
	QTest::newRow("equal with suffix")                 << VersionNumber(1, 2, 3, "-alpha")  << VersionNumber(1, 2, 3, "-alpha") << true  << false    << false  << true        << false   << true;
	QTest::newRow("lesser")                            << VersionNumber(1, 2, 1)            << VersionNumber(1, 2, 3)           << false << true     << true   << true        << false   << false;
	QTest::newRow("lesser with suffix")                << VersionNumber(1, 2, 1, "#abc")    << VersionNumber(1, 2, 3, "#abc")   << false << true     << true   << true        << false   << false;
	QTest::newRow("lesser suffix")                     << VersionNumber(1, 2, 3, "-alpha")  << VersionNumber(1, 2, 3, "-beta")  << false << true     << true   << true        << false   << false;
	QTest::newRow("greater")                           << VersionNumber(1, 0, 0)            << VersionNumber(0, 2, 3)           << false << true     << false  << false       << true    << true;
	QTest::newRow("greater with suffix")               << VersionNumber(1, 0, 0, "-alpha")  << VersionNumber(0, 2, 3, "-beta")  << false << true     << false  << false       << true    << true;
	QTest::newRow("greater suffix")                    << VersionNumber(1, 0, 0, "+zz")     << VersionNumber(1, 0, 0, "+z")     << false << true     << false  << false       << true    << true;

	VersionNumber shortVersion;
	shortVersion.segments.push_back(12);
	QTest::newRow("missing segments equal 0")          << shortVersion                      << VersionNumber(12, 0, 0)          << true  << false    << false  << true        << false   << true;
	QTest::newRow("lesser short version")              << shortVersion                      << VersionNumber(12, 0, 1)          << false << true     << true   << true        << false   << false;
	QTest::newRow("greater short version")             << shortVersion                      << VersionNumber(2, 0, 1)           << false << true     << false  << false       << true    << true;
	QTest::newRow("suffix makes left version lesser")  << VersionNumber(1, 2, 3, "-alpha")  << VersionNumber(1, 2, 3)           << false << true     << true   << true        << false   << false;
	QTest::newRow("suffix makes right version lesser") << VersionNumber(1, 2, 3)            << VersionNumber(1, 2, 3, "-alpha") << false << true     << false  << false       << true    << true;
	QTest::newRow("segments have precedence")          << VersionNumber(12, 0, 1, "-alpha") << VersionNumber(12, 0, 0)          << false << true     << false  << false       << true    << true;
}


} // namespace Tests

QTEST_MAIN(Tests::VersionNumberTest)
#include "VersionNumberTest.moc"
